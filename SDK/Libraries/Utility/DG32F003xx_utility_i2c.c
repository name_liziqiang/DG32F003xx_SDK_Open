#include "DG32F003xx.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_utility_i2c.h"
#include "DG32F003xx_i2c.h"

#include "misc.h"
#include "string.h"
#include "stdio.h"

#define I2C_DEBUG             0
#if I2C_DEBUG
  #include "stdio.h"
  #define i2c_debug(x)        printf("i2c error: %s\r\n", x);
#else
  #define i2c_debug(x)
#endif


#define TIMEOUT               0x400000
#define NUM_SPEEDS            3
#define I2C_RX_FIFO_SIZE        8
#define I2C_TX_FIFO_SIZE        8



/** 
  * @brief  write bytes in pio mode
  * @param  dev_addr: device 7bit address on i2c bus
  *         internal_addr: i2c device internal address to write into
  *         addr_size: internal address byte length
  *         buf: written data address
  *         write_size: write size in bytes
  * @retval 0 = success
  *         -1= failed
  */
int I2C_Write(uint8_t dev_addr, uint32_t internal_addr, int addr_size, uint8_t *buf,  int write_size)
{
  int i;
  uint32_t timeout;
  uint32_t temreg;
  uint32_t len;
  
  /*disable I2C module*/
  timeout = TIMEOUT;
  I2C0->ENABLE = 0;
  while ((I2C0->STAT & I2C_IC_STATUS_ACTIVITY) || (I2C0->ENABLE_STATUS & I2C_IC_ENABLE_STATUS_IC_EN)){
    I2C0->ENABLE = 0;
    timeout--;
    if(timeout == 0){
      i2c_debug("init failed");
      return -1;
    }
  }
  
  uint32_t SpeedMode;
  SpeedMode = I2C0->CON;
  SpeedMode = (SpeedMode>>1) & 0X00000003;
  //if work in SpeedHigh  the restart signal mast enbale
  if(SpeedMode == 3)I2C0->CON |= I2C_CON_IC_RESTART_EN;  //restart enable
  else I2C0->CON &= ~I2C_CON_IC_RESTART_EN;  //restart disable
  
  temreg = I2C0->TAR;
  temreg &= ~0x000003ff;
  temreg |= 1 << 10; //generate START BYTE
  temreg |= dev_addr;
  I2C0->TAR = temreg;
  I2C0->TX_TL = 0;
  I2C0->RX_TL = 0;
  I2C0->ENABLE = 1;
  
  /*send device internal addr*/
  if(addr_size != 0){
    for (i = addr_size - 1; i >= 0; i--){
      timeout = TIMEOUT;
      I2C_SendData(I2C0, (internal_addr >> i*8) & 0x00ff);
      while (((I2C0->STAT & (1 << 2)) == 0) && timeout)timeout--;
      if (timeout == 0){
        i2c_debug("send internal address failed");
        return -1;
      }
    }
  }
  //write data
  len = write_size;
  timeout = TIMEOUT;
  
  while(1){
    while((len != 0) && ((I2C0->STAT & (1 << 1)) != 0)){
      //TX FIFO is not full
      I2C_SendData(I2C0, (*buf++) & 0x00ff);
      len--;
    }
    if(len == 0)break;
    // while((I2C0->STAT & (1 << 1)) == 0);//wait TX FIFO is not full
  }
  timeout = TIMEOUT;
  while (((I2C0->STAT & (1 << 2)) == 0) && timeout)timeout--;//wait TX FIFO is empty
  if (timeout == 0){
    i2c_debug("wait data write end timeout");
    return -1;
  }
  return 0;
}
/** 
  * @brief  write bytes in pio mode
  * @param  dev_addr: device 7bit address on i2c bus
  *         internal_addr: i2c device internal address to write into   0 = no internal address
  *         addr_size: internal address byte length
  *         buf: written data address
  *         write_size: write size in bytes
  * @retval 0 = success
  *         -1= failed
  */
int I2C_WriteIRQ( uint8_t dev_addr, uint32_t internal_addr, int addr_size)
{
  int i;
  uint32_t timeout;
  uint32_t temreg;
  
  /*disable I2C module*/
  timeout = TIMEOUT;
  I2C0->ENABLE = 0;
  while ((I2C0->STAT & I2C_IC_STATUS_ACTIVITY) || (I2C0->ENABLE_STATUS & I2C_IC_ENABLE_STATUS_IC_EN)){
    I2C0->ENABLE = 0;
    timeout--;
    if(timeout == 0){
      i2c_debug("init failed");
      return -1;
    }
  }
  
  uint32_t SpeedMode;
  SpeedMode = I2C0->CON;
  SpeedMode = (SpeedMode>>1) & 0X00000003;
  //if work in SpeedHigh  the restart signal mast enbale
  if(SpeedMode == 3)I2C0->CON |= I2C_CON_IC_RESTART_EN;  //restart enable
  else I2C0->CON &= ~I2C_CON_IC_RESTART_EN;  //restart disable
  
  temreg = I2C0->TAR;
  temreg &= ~0x000003ff;
  temreg |= 1 << 10; //generate START BYTE
  temreg |= dev_addr;
  I2C0->TAR = temreg;
  I2C0->TX_TL = I2C_TX_FIFO_SIZE;
  I2C0->RX_TL = 0;
  I2C0->ENABLE = 1;
  
  /*send device internal addr*/
  if(addr_size != 0){
    for (i = addr_size - 1; i >= 0; i--){
      timeout = TIMEOUT;
      I2C_SendData(I2C0, (internal_addr >> i*8) & 0x00ff);
      while (((I2C0->STAT & (1 << 2)) == 0) && timeout)timeout--;
      if (timeout == 0){
        i2c_debug("send internal address failed");
        return -1;
      }
    }
  }
  //write data
  I2C_ITConfig(I2C0, I2C_IT_TX_EMPTY, ENABLE);
  return 0;
}

/** 
  * @brief  read bytes in pio mode
  * @param  dev_addr: device address on i2c bus
  *         internal_addr: i2c device internal address to write into
  *         addr_size: internal address byte length
  *         buf: mem to store read data
  *         read_size: read size in bytes
  * @retval 0 = success
  *         -1= failed
  */
int I2C_Read(uint8_t dev_addr, uint32_t internal_addr, int addr_size, uint8_t *buf,  int read_size)
{
  int i;
  uint32_t timeout;
  uint32_t temreg;
  uint32_t len, readCount;
  uint8_t *recBuf;
  
  /*disable I2C module*/
  timeout = TIMEOUT;
  I2C0->ENABLE = 0;
  while ((I2C0->STAT & I2C_IC_STATUS_ACTIVITY) || (I2C0->ENABLE_STATUS & I2C_IC_ENABLE_STATUS_IC_EN)){
    I2C0->ENABLE = 0;
    timeout--;
    if(timeout == 0){
      i2c_debug("init failed");
      return -1;
    }
  }
  
  I2C0->CON |= I2C_CON_IC_RESTART_EN;  //restart enable
  
  temreg = I2C0->TAR;
  temreg &= ~0x000003ff;
  temreg |= 1 << 10; //generate START BYTE
  temreg |= dev_addr;
  I2C0->TAR = temreg;
  
  I2C0->TX_TL = 0;
  I2C0->RX_TL = 0;
  I2C0->ENABLE = 1;
  
  /*send device internal addr*/
  if(addr_size != 0){
    for (i = addr_size - 1; i >= 0; i--){
      timeout = TIMEOUT;
      I2C_SendData(I2C0, (internal_addr >> i*8) & 0x00ff);
      while (((I2C0->STAT & (1 << 2)) == 0) && timeout)timeout--;
      if (timeout == 0){
        i2c_debug("send internal address timeout");
        I2C0->ENABLE = 0;
        return -1;
      }
    }
  }
  //read data
  len = read_size;
  readCount = 0;
  recBuf = buf;
  timeout = TIMEOUT;
  while(timeout != 0){
    while((len != 0) && ((I2C0->STAT & (1 << 1)) != 0)){
      //TX FIFO is not full
      I2C_TriggerRecData(I2C0);
      len--;
    }
    if ((I2C0->STAT & (1 << 3)) != 0){
      //rx fifo is not empty
      *recBuf = I2C_ReceiveData(I2C0);
      recBuf++;
      readCount++;
    }
    if(readCount == read_size)break;
    timeout --;
  }
  if (timeout == 0){
    i2c_debug("read data timeout");
    I2C0->ENABLE = 0;
    return -1;
  }
  return 0;
}


/** 
  * @brief  read bytes in irq mode
  * @param  dev_addr: device address on i2c bus
  *         internal_addr: i2c device internal address to write into
  *         addr_size: internal address byte length  0 = no internal address
  *         buf: mem to store read data
  *         read_size: read size in bytes
  * @retval 0 = success
  *         -1= failed
  */
int I2C_ReadIRQ(uint8_t dev_addr, uint32_t internal_addr, int addr_size)
{
  int i;
  uint32_t timeout;
  uint32_t temreg;
  
  /*disable I2C module*/
  timeout = TIMEOUT;
  I2C0->ENABLE = 0;
  while ((I2C0->STAT & I2C_IC_STATUS_ACTIVITY) || (I2C0->ENABLE_STATUS & I2C_IC_ENABLE_STATUS_IC_EN)){
    I2C0->ENABLE = 0;
    timeout--;
    if(timeout == 0){
      i2c_debug("init failed");
      return -1;
    }
  }
  
  I2C0->CON |= I2C_CON_IC_RESTART_EN;  //restart enable
  
  temreg = I2C0->TAR;
  temreg &= ~0x000003ff;
  temreg |= 1 << 10; //generate START BYTE
  temreg |= dev_addr;
  I2C0->TAR = temreg;
  
  I2C0->TX_TL = I2C_TX_FIFO_SIZE;
  I2C0->RX_TL = 0;
  I2C0->ENABLE = 1;
  
  /*send device internal addr*/
  if(addr_size != 0){
    for (i = addr_size - 1; i >= 0; i--){
      timeout = TIMEOUT;
      I2C_SendData(I2C0, (internal_addr >> i*8) & 0x00ff);
      while (((I2C0->STAT & (1 << 2)) == 0) && timeout)timeout--;
      if (timeout == 0){
        i2c_debug("send internal address timeout");
        I2C0->ENABLE = 0;
        return -1;
      }
    }
  }
  I2C_ITConfig(I2C0, I2C_IT_RX_FULL | I2C_IT_TX_EMPTY, ENABLE);

  return 0;
}
