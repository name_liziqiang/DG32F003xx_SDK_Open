================================================================================
                                样例使用说明
================================================================================
功能描述：
本样例主要展示flash的读写功能。

================================================================================
测试环境：
测试用板：DG32F003_STK
MDK版本： 5.20
================================================================================
使用步骤：
1. 编译下载程序到MCU，并运行；


================================================================================
注意事项：
1. flash的写入需要四字节对齐；

================================================================================