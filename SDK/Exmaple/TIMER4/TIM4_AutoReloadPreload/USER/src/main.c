/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"

/* Private define ------------------------------------------------------------*/
#define LED_PIN GPIO2_0
/* Private variables ---------------------------------------------------------*/
TIM_TypeDef* TIMx = TIM4;
uint32_t temp;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void TIM4_Init(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	GPIO_SetPinMux(LED_PIN, IO_GPIO);//tim mark signal
	GPIO_SetPinDir(LED_PIN, GPIO_Mode_OUT); 
	GPIO_ClearPin(LED_PIN);	
	TIM4_Init();
	while(1){

  }
}

static void TIM4_Init(void)
{
	//open triger source
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_TIM4);

	uint32_t TIM_Prescaler = 1000-1;
	uint32_t TIM_Period = 2400-1;
	TIM_TimeBaseInitTypeDef  TIM4_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM4_TimeBaseStructure);
	TIM4_TimeBaseStructure.TIM_Prescaler=TIM_Prescaler; //Frequency division coefficient
	TIM4_TimeBaseStructure.TIM_Period=TIM_Period;  //Automatic reload values
	TIM4_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//clock separation
	TIM4_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//count mode
	TIM4_TimeBaseStructure.TIM_RepetitionCounter = 0;//Repeated count value
	TIM_TimeBaseInit(TIM4, &TIM4_TimeBaseStructure);

	TIM_ARRPreloadConfig(TIM4, ENABLE); //Enable the TIM4 preloaded register on the ARR

	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);     		
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE); 	//Enable TIM interrupt
	//Interrupt configuration
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM4_IRQn;	  
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;  
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_Init(&NVIC_InitStruct);

	TIM_Cmd(TIM4,ENABLE);
}
/**
 * @brief 
 * 
 */
void TIM4_IRQHandler(void)
{
  // Gets the timer update interrupt flag
	if ((TIM_GetITStatus(TIM4, TIM_IT_Update)) != RESET)
	{ 
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update  );
		temp++;
		GPIO_TogglePin(LED_PIN);		

		if (temp == 3)
		{
			/* 修改TIM重装载值 */
			TIM_SetAutoreload(TIM4, 9600 - 1);
		}
	}
}
