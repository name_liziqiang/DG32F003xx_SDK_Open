/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_adc.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_rcc.h"

#include "usart.h"	  
#include "delay.h"

/* Private define ------------------------------------------------------------*/
#define adc_testchannel ADC_CHANNEL4

/* Private variables ---------------------------------------------------------*/
static uint32_t adc_value;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void ADC_init(void);
static void TIM1_init(void);
/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	Debug_uart_init(115200);
	printf("ADC_TEST\r\n");
	delay_init(24);
	ADC_init();
	TIM1_init();
	while(1){
		
  }
}
/**
  * @brief  tim1初始化.
  * @param  无
  * @retval int
  */
static void TIM1_init(void)
{
  
	TIM_TimeBaseInitTypeDef  TIMx_TimeBaseStructure;  

    RCC_ResetAHBCLK(1 << AHBCLK_BIT_TIM1);
 
	// 输出周期1000ms
	TIM_TimeBaseStructInit(&TIMx_TimeBaseStructure);
	TIMx_TimeBaseStructure.TIM_Prescaler=2399; //Frequency division coefficient
	TIMx_TimeBaseStructure.TIM_Period=9999;  //Automatic reload values
	TIMx_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//clock separation
	TIMx_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//count mode
	TIMx_TimeBaseStructure.TIM_RepetitionCounter = 0;//Repeated count value
	TIM_TimeBaseInit(TIM1, &TIMx_TimeBaseStructure);
    
	TIM_ARRPreloadConfig(TIM1, ENABLE); //Enable the TIMX preloaded register on the ARR
	TIM_SelectOutputTrigger(TIM1,TIM_TRGOSource_Update); //Select the output trigger signal of the master timer  
	TIM_Cmd(TIM1,ENABLE);	
}
/**
  * @brief  ADC初始化.
  * @param  无
  * @retval int
  */
static void ADC_init(void)
{
  ADC_InitTypeDef ADC_InitStruct;


	/*ADC_Channel 配置 ADC 转换通道 */
  ADC_InitStruct.ADC_Channel = adc_testchannel;
  /*ADC_ConvMode ADC 转换模式， 单次模式 or 连续模式 */
  ADC_InitStruct.ADC_ConvMode = ADC_ConversionMode_Single;
  /*ADC_Count_SCycle ADC 工作在连续模式时 2轮转换之间的时间间隔，计数频率为AHB频率 */
  ADC_InitStruct.ADC_Count_SCycle = 0;
  /*ADC_ReferenceSrcSel ADC 参考电压，AVDD or 内部2.4V参考源*/
  ADC_InitStruct.ADC_ReferenceSrcSel = ADC_ReferenceSource_AVDD;
  /*ADC_OffsetErrorComp 是否使用offset error对转换进过进行补偿*/
  ADC_InitStruct.ADC_OffsetErrorComp = ADC_OffsetErrorComp_ENABLE;
  /*ADC_TrigConvSrcSel ADC触发模式*/
  ADC_InitStruct.ADC_TrigConvSrcSel = ADC_Internal_TrigSrc_TIM1_TRGO;
  /*ADC_TrigConvEdge ADC触发边沿*/
  ADC_InitStruct.ADC_TrigConvEdge = ADC_TrigEdge_Rising;
  /*ADC_First_DisSampleNum ADC首次转换 需要丢弃的值*/
  ADC_InitStruct.ADC_First_DisSampleNum = ADC_First_DisSampleNum_0;
  /*ADC_Exch_DisSampleNum ADC切换通道之后，需要丢弃的值*/
  ADC_InitStruct.ADC_Exch_DisSampleNum = ADC_Exch_DisSampleNum_0;
  /*ADC_ClockDiv ADC转换时钟分频系数，(15 - 1) 表示将AHB时钟 15 分频之后作为ADC工作时钟。
    5.5个clk用于采样，10个时钟用于转换，0.5个clk用于准备数据输出*/
  ADC_InitStruct.ADC_ClockDiv = 15 - 1;

  ADC_Init(ADC0, &ADC_InitStruct);
	

  ADC_ClearITFlag(ADC0,ADC_CHANNEL_ALL);
  ADC_Run(ADC0, ENABLE);
  
  ADC_ITConfig(ADC0, ENABLE);
  NVIC_InitTypeDef NVIC_InitStruct;
  NVIC_InitStruct.NVIC_IRQChannel = ADC0_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
  NVIC_Init(&NVIC_InitStruct);
}
/**
  * @brief  ADC中断.
  * @param  无
  * @retval int
  */
void ADC0_IRQHandler(void)
{

 if(ADC_GetITStatus(ADC0,adc_testchannel)!=RESET){
	 ADC_ClearITFlag(ADC0,adc_testchannel);
	 adc_value = ADC_GetConvValue(ADC0,adc_testchannel); 
	 printf("ADC_data:%d\r\n", adc_value);
 }

}