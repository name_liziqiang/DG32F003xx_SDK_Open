/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "delay.h"

/* Private define ------------------------------------------------------------*/
#define LED_PIN GPIO2_0
/* Private variables ---------------------------------------------------------*/

uint32_t temp;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void TIM1_Init(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	GPIO_SetPinMux( GPIO_Pin_2, IO_TIM1_CH1);
	delay_init(24);
    TIM1_Init();

	while(1){
	delay_ms(5000);
	TIM_Cmd(TIM1,DISABLE);
	TIM_SelectOnePulseMode(TIM1, TIM_OPMode_Single);//Set to monopulse mode
	TIM_Cmd(TIM1,ENABLE);
  }
}

static void TIM1_Init(void)
{
	//open triger source
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_TIM1);

	uint32_t TIM_Prescaler = 0;
	uint32_t TIM_Period = 2000;
	TIM_TimeBaseInitTypeDef  TIM1_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM1_TimeBaseStructure);
	TIM1_TimeBaseStructure.TIM_Prescaler=TIM_Prescaler; //Frequency division coefficient
	TIM1_TimeBaseStructure.TIM_Period=TIM_Period;  //Automatic reload values
	TIM1_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//clock separation
	TIM1_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//count mode
	TIM1_TimeBaseStructure.TIM_RepetitionCounter = 8;//Repeated count value
	TIM_TimeBaseInit(TIM1, &TIM1_TimeBaseStructure);

	
	TIM_OCInitTypeDef  TIM1_OCInitStructure;
	TIM_OCStructInit(&TIM1_OCInitStructure);

	TIM1_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; 
	TIM1_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;        
	TIM1_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM1_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset; 

	TIM1_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
	TIM1_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; 
	TIM1_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;                  
	TIM1_OCInitStructure.TIM_Pulse = TIM_Period/2; 
	TIM_OC1Init(TIM1, &TIM1_OCInitStructure);  //Output comparison channel 1 initialization
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable); //CH1 pre-loaded to enable	
	
    TIM_SelectOnePulseMode(TIM1, TIM_OPMode_Single);//Set to monopulse mode
	TIM_CtrlPWMOutputs(TIM1,ENABLE);	//MOE Pulse output
	TIM_ARRPreloadConfig(TIM1, ENABLE); //Enable the TIMX preloaded register on the ARR


	TIM_Cmd(TIM1,ENABLE);
}
