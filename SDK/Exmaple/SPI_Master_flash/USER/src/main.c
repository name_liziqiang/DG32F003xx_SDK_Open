/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_utility_spi.h"
#include "DG32F003xx_spi.h"
#include "usart.h"	  
#include "delay.h"

/* Private define ------------------------------------------------------------*/
#define TEST_BYTES 64

/* Private variables ---------------------------------------------------------*/
__align(4) static uint8_t FLASH_wbuf[TEST_BYTES]={0};
__align(4) static uint8_t FLASH_rbuf[TEST_BYTES]={0};

/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


void showstatus(SPI_TypeDef *SPIptr)
{
  char buff[32];
  spi_flash_read_status1(SPIptr, buff+1);
  printf("status1: %2x\r\n", buff[1]);
  spi_flash_read_status2(SPIptr, buff+2);
  printf("status2: %2x\r\n", buff[2]);
}


void spi_flash_demo(SPI_TypeDef *SPIptr)
{
	//****SPI Function test start//
	int i;
	volatile uint8_t status;

	spi_flash_init(SPIptr, 2, SPI_MASTER_MODE, 2, 0);
	spi_initPins(GPIO1_0, GPIO1_1, GPIO1_6, GPIO1_2);	

	showstatus(SPIptr);

	for (i = 0; i < 10; i++){
	  *(FLASH_rbuf + i) = 0xa5;
	}
	if (spi_flash_read_id(SPIptr, FLASH_rbuf)){
	  printf("error!\r\n");
	}
	printf("quad JEDEC ID: %2x %2x %2x %2x\r\n", *FLASH_rbuf, *(FLASH_rbuf + 1), *(FLASH_rbuf + 2), *(FLASH_rbuf + 3));

	 for (int i = 0; i < TEST_BYTES; i++){
	  FLASH_wbuf [i]= ((i + 0x3) % 256);
	}
	for (int i = 0; i < TEST_BYTES; i++){
	  FLASH_rbuf[i] = 0;
	}
	spi_flash_global_unprotect(SPI0);
	spi_flash_erase_block_4k(SPI0, 0x0); //Must be done before write
	spi_flash_write(SPI0, FLASH_wbuf, 0x0, TEST_BYTES);
	delay_ms(100);
	//read flash
	spi_flash_read(SPI0, FLASH_rbuf, 0x0, TEST_BYTES);

	uint32_t cnt = 0;
	for (cnt = 0; cnt < TEST_BYTES; cnt++){
		if (FLASH_wbuf[cnt] != FLASH_rbuf[cnt]) break;
		else{
		   printf("EXTER FLASH_rbuf[%d] = 0x%02x\r\n", cnt, FLASH_rbuf[cnt]);
		}
		}
		if(cnt < TEST_BYTES){
		printf("EXTER FLASH read and write not match!, at loc: %d\r\r\n", cnt);
		printf("EXTER FLASH_rbuf[%d] = 0x%02x\r\n", cnt, FLASH_rbuf[cnt]);
		}
		else{
		  printf("EXTER FLASH read and write match!\r\r\n");
		}	

	while(1);  
  //****SPI Function test end//

}


/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{
	Debug_uart_init(115200);
	delay_init(24);
	spi_flash_demo(SPI0);

	while(1)
	{

	}
}

