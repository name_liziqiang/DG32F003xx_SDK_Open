/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"

/* Private define ------------------------------------------------------------*/
#define LED_PIN GPIO2_0
/* Private variables ---------------------------------------------------------*/

uint32_t temp;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void TIM4_Init(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	GPIO_SetPinMux( GPIO_Pin_2, IO_TIM4_CH1);
  TIM4_Init();
	while(1){

  }
}

static void TIM4_Init(void)
{
	//open triger source
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_TIM4);

	uint32_t TIM_Prescaler = 1000-1;
	uint32_t TIM_Period = 24-1;
	TIM_TimeBaseInitTypeDef  TIM4_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM4_TimeBaseStructure);
	TIM4_TimeBaseStructure.TIM_Prescaler=TIM_Prescaler; //Frequency division coefficient
	TIM4_TimeBaseStructure.TIM_Period=TIM_Period;  //Automatic reload values
	TIM4_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//clock separation
	TIM4_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//count mode
	TIM4_TimeBaseStructure.TIM_RepetitionCounter = 0;//Repeated count value
	TIM_TimeBaseInit(TIM4, &TIM4_TimeBaseStructure);

	TIM_OCInitTypeDef  TIM4_OCInitStructure;
	TIM_OCStructInit(&TIM4_OCInitStructure);

	TIM4_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; 
	TIM4_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;        
	TIM4_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM4_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset; 

	TIM4_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
	TIM4_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; 
	//TIM4_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;                  
	TIM4_OCInitStructure.TIM_Pulse = TIM_Period/2; 
	TIM_OC1Init(TIM4, &TIM4_OCInitStructure);  //Output comparison channel 1 initialization
	TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable); //CH1 pre-loaded to enable	
	
	TIM_CtrlPWMOutputs(TIM4,ENABLE);	//MOE Pulse output
	TIM_ARRPreloadConfig(TIM4, ENABLE); //Enable the TIMX preloaded register on the ARR


	TIM_Cmd(TIM4,ENABLE);
}
