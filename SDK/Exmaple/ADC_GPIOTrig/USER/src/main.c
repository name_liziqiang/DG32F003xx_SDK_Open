/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_adc.h"
#include "DG32F003xx_gpio.h"
#include "usart.h"	  
#include "delay.h"

/* Private define ------------------------------------------------------------*/
#define adc_testchannel ADC_CHANNEL4
#define adet_io GPIO0_5
#define adc_single GPIO0_7
/* Private variables ---------------------------------------------------------*/
static uint32_t adc_value;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void GPIO_init(void);
static void ADC_init(void);
/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	Debug_uart_init(115200);
	printf("ADC_TEST\r\n");
	delay_init(24);
	GPIO_init();
	ADC_init();
	while(1){
		
		delay_ms(500);
		GPIO_TogglePin(adc_single);
		
  }
}
/**
  * @brief  ADC中断.
  * @param  无
  * @retval int
  */
void ADC0_IRQHandler(void)
{

 if(ADC_GetITStatus(ADC0,adc_testchannel)!=RESET){
	 ADC_ClearITFlag(ADC0,adc_testchannel);
	 adc_value = ADC_GetConvValue(ADC0,adc_testchannel); 
	 printf("ADC_data:%d\r\n", adc_value);
 }

}
/**
  * @brief  GPIO初始化.
  * @param  无
  * @retval int
  */
static void GPIO_init(void)
{
	GPIO_SetPinMux(adet_io, IO_ADET);
	GPIO_SetPinMux(adc_single, IO_GPIO);
	GPIO_SetPinDir(adc_single, GPIO_Mode_OUT);
	GPIO_ClearPin(adc_single);
}

/**
  * @brief  ADC初始化.
  * @param  无
  * @retval int
  */
static void ADC_init(void)
{
  ADC_InitTypeDef ADC_InitStruct;


	/*ADC_Channel 配置 ADC 转换通道 */
  ADC_InitStruct.ADC_Channel = adc_testchannel;
  /*ADC_ConvMode ADC 转换模式， 单次模式 or 连续模式 */
  ADC_InitStruct.ADC_ConvMode = ADC_ConversionMode_Single;
  /*ADC_Count_SCycle ADC 工作在连续模式时 2轮转换之间的时间间隔，计数频率为AHB频率 */
  ADC_InitStruct.ADC_Count_SCycle = 0;
  /*ADC_ReferenceSrcSel ADC 参考电压，AVDD or 内部2.4V参考源*/
  ADC_InitStruct.ADC_ReferenceSrcSel = ADC_ReferenceSource_AVDD;
  /*ADC_OffsetErrorComp 是否使用offset error对转换进过进行补偿*/
  ADC_InitStruct.ADC_OffsetErrorComp = ADC_OffsetErrorComp_ENABLE;
  /*ADC_TrigConvSrcSel ADC触发模式*/
  ADC_InitStruct.ADC_TrigConvSrcSel = ADC_Internal_TrigSrc_GPIO_ADET;
  /*ADC_TrigConvEdge ADC触发边沿*/
  ADC_InitStruct.ADC_TrigConvEdge = ADC_TrigEdge_Rising;
  /*ADC_First_DisSampleNum ADC首次转换 需要丢弃的值*/
  ADC_InitStruct.ADC_First_DisSampleNum = ADC_First_DisSampleNum_0;
  /*ADC_Exch_DisSampleNum ADC切换通道之后，需要丢弃的值*/
  ADC_InitStruct.ADC_Exch_DisSampleNum = ADC_Exch_DisSampleNum_0;
  /*ADC_ClockDiv ADC转换时钟分频系数，(15 - 1) 表示将AHB时钟 15 分频之后作为ADC工作时钟。
    5.5个clk用于采样，10个时钟用于转换，0.5个clk用于准备数据输出*/
  ADC_InitStruct.ADC_ClockDiv = 15 - 1;

  ADC_Init(ADC0, &ADC_InitStruct);
	

  ADC_ClearITFlag(ADC0,ADC_CHANNEL_ALL);
  ADC_Run(ADC0, ENABLE);

  ADC_ITConfig(ADC0, ENABLE);
  NVIC_InitTypeDef NVIC_InitStruct;
  NVIC_InitStruct.NVIC_IRQChannel = ADC0_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
  NVIC_Init(&NVIC_InitStruct);


}
