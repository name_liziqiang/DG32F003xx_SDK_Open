/**
  ******************************************************************************
  * @file    DG32F003xx_wdg.h
  * @author  Alpscale Software Team
  * @version V1.0.0
  * @date    12/20/2013
  * @brief   This file contains all the functions prototypes for the WDG 
  *          firmware library.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 Alpscale</center></h2>
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DG32F003xx_WDG_H
#define __DG32F003xx_WDG_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"


/** @addtogroup DG32F003xx_StdPeriph_Driver
  * @{
  */

/** @addtogroup WDG
  * @{
  */

/** @defgroup WDG_Exported_Types
  * @{
  */

/**
  * @}
  */

/** @defgroup WDG_Exported_Constants
  * @{
  */

/** @defgroup WDG_SetMode
  * @{
  */

#define WDG_SETMODE_DEBUG     ((uint32_t)0x00000000)
#define WDG_SETMODE_IT        ((uint32_t)0x00000001)
#define WDG_SETMODE_RESET     ((uint32_t)0x00000003)
#define IS_WDG_MODE(MODE) (((MODE) == WDG_SETMODE_DEBUG) || \
                 ((MODE) == WDG_SETMODE_IT) || \
                               ((MODE) == WDG_SETMODE_RESET))                 

/**
  * @}
  */

/** @defgroup FaultReset enable/disable
  * @{
  */

#define WDG_FAULT_DISABLE     ((uint32_t)0x00000000)
#define WDG_FAULT_ENABLE      ((uint32_t)0x00000010)
#define IS_WDG_FAULT(FAULT)   (((FAULT) == WDG_FAULT_DISABLE) || \
                               ((FAULT) == WDG_FAULT_ENABLE))                 

/**
  * @}
  */


/** @defgroup WDG_Flag 
  * @{
  */

#define WDG_FLAG_WDTOF               ((uint32_t)0x00000004)
#define WDG_FLAG_WDINT               ((uint32_t)0x00000008)
#define WDG_FLAG_FAULT               ((uint32_t)0x00000020)
#define IS_WDG_FLAG(FLAG)            (((FLAG) == WDG_FLAG_WDTOF) || \
                                      ((FLAG) == WDG_FLAG_WDINT) || \
                                      ((FLAG) == WDG_FLAG_FAULT))

#define IS_WDG_RELOAD(RELOAD) ((RELOAD) >= 0x000000FF)

/**
  * @}
  */

/**
  * @}
  */

/** @defgroup WDG_Exported_Macros
  * @{
  */

/**
  * @}
  */

/** @defgroup WDG_Exported_Functions
  * @{
  */
  
void WDT_CLKSET(FunctionalState NewState);
void WDT_Reset(void);
void WDG_SetMode(uint32_t Mode, uint32_t FaultMode);
void WDG_SetReload(uint32_t Reload);
void WDG_ReloadCounter(void);
FlagStatus WDG_GetFlagStatus(uint32_t WDG_FLAG);
void WDG_ClearTimeOutFlag(void);
void WDG_ClearITFlag(void);
void WDG_ClearFaultFlag(void);

#ifdef __cplusplus
}
#endif

#endif /* __DG32F003xx_WDG_H */
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/

