/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx_utility_iic_MasterSHT30.h"
#include "DG32F003xx_gpio.h"

//-- Enumerations -------------------------------------------------------------
// Sensor Commands
typedef enum{
    CMD_READ_SERIALNBR	= 0x3780, // read serial number 
    CMD_READ_STATUS	= 0xF32D, // read status register 
    CMD_CLEAR_STATUS	= 0x3041, // clear status register 
    CMD_HEATER_ENABLE	= 0x306D, // enabled heater 
    CMD_HEATER_DISABLE	= 0x3066, // disable heater 
    CMD_SOFT_RESET	= 0x30A2, // soft reset
    CMD_MEAS_CLOCKSTR_H = 0x2C06, // measurement: clock stretching, high repeatability 
    CMD_MEAS_CLOCKSTR_M = 0x2C0D, // measurement: clock stretching, medium repeatability
    CMD_MEAS_CLOCKSTR_L = 0x2C10, // measurement: clock stretching, low repeatability 
    CMD_MEAS_POLLING_H	= 0x2400, // measurement: polling, high repeatability 
    CMD_MEAS_POLLING_M	= 0x240B, // measurement: polling, medium repeatability 
    CMD_MEAS_POLLING_L	= 0x2416, // measurement: polling, low repeatability 
    CMD_MEAS_PERI_05_H	= 0x2032, // measurement: periodic 0.5 mps, high repeatability 
    CMD_MEAS_PERI_05_M	= 0x2024, // measurement: periodic 0.5 mps, medium repeatability
    CMD_MEAS_PERI_05_L	= 0x202F, // measurement: periodic 0.5 mps, low repeatability 
    CMD_MEAS_PERI_1_H	= 0x2130, // measurement: periodic 1 mps, high repeatability 
    CMD_MEAS_PERI_1_M	= 0x2126, // measurement: periodic 1 mps, medium repeatability 
    CMD_MEAS_PERI_1_L	= 0x212D, // measurement: periodic 1 mps, low repeatability 
    CMD_MEAS_PERI_2_H	= 0x2236, // measurement: periodic 2 mps, high repeatability 
    CMD_MEAS_PERI_2_M	= 0x2220, // measurement: periodic 2 mps, medium repeatability 
    CMD_MEAS_PERI_2_L	= 0x222B, // measurement: periodic 2 mps, low repeatability 
    CMD_MEAS_PERI_4_H	= 0x2334, // measurement: periodic 4 mps, high repeatability 
    CMD_MEAS_PERI_4_M	= 0x2322, // measurement: periodic 4 mps, medium repeatability 
    CMD_MEAS_PERI_4_L	= 0x2329, // measurement: periodic 4 mps, low repeatability 
    CMD_MEAS_PERI_10_H	= 0x2737, // measurement: periodic 10 mps, high repeatability 
    CMD_MEAS_PERI_10_M	= 0x2721, // measurement: periodic 10 mps, medium repeatability
    CMD_MEAS_PERI_10_L	= 0x272A, // measurement: periodic 10 mps, low repeatability 
    CMD_FETCH_DATA	= 0xE000, // readout measurements for periodic mode 
    CMD_R_AL_LIM_LS	= 0xE102, // read alert limits, low set
    CMD_R_AL_LIM_LC	= 0xE109, // read alert limits, low clear 
    CMD_R_AL_LIM_HS	= 0xE11F, // read alert limits, high set 
    CMD_R_AL_LIM_HC	= 0xE114, // read alert limits, high clear 
    CMD_W_AL_LIM_HS	= 0x611D, // write alert limits, high set 
    CMD_W_AL_LIM_HC	= 0x6116, // write alert limits, high clear
    

    CMD_W_AL_LIM_LC	= 0x610B, // write alert limits, low clear 
    CMD_W_AL_LIM_LS	= 0x6100, // write alert limits, low set 
    CMD_NO_SLEEP	= 0x303E,
}etCommands;

#define DEVADDR  0x44
//LCNT min = 8  actual quantity = LCNT+1
//HCNT min = 6  actual quantity = HCNT+8
//HOLD set 0  actual quantity = 1   set other  actual quantity = HOLD + 5��mast smaller then LCNT
//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
#define IIC_LCNT    8
#define IIC_HCNT    6
#define IIC_HOLD    0


static void delay_us(uint16_t us)
{
  int i;
  for (; us > 0; us--)
    for (i = 0; i < 100; i++);
}



int SHT30_SendCMD(uint16_t cmd)
{
	int result=-1;
	uint8_t buff[2];
	buff[0]=cmd>>8;
	buff[1]=cmd&0xff;
	result = I2C_Write(DEVADDR, buff[0],1,&buff[1],1);
	delay_us(100);
	return result;
}
/**
 * @brief	Read the data once from SHT30
 * @param	dat--Store the address of the received data (6-byte array)
 * @retval success--  return:0
 * 			   failure--return:-1
*/
int SHT30_Read_Dat(uint8_t* dat)
{
	int result=-1;
	SHT30_SendCMD(CMD_FETCH_DATA);//Periodic measure mode read data command
	result = I2C_Read(DEVADDR, 0, 0, dat, 6);
	delay_us(100);
	return result;	
}
#define CRC8_POLYNOMIAL 0x31

uint8_t CheckCrc8(uint8_t* const message, uint8_t initial_value)
{
    uint8_t  remainder;	    
    uint8_t  i = 0, j = 0;  //cyclic variable

    /* init */
    remainder = initial_value;

    for(j = 0; j < 2;j++)
    {
        remainder ^= message[j];

        /* Start with the highest bit  */
        for (i = 0; i < 8; i++)
        {
            if (remainder & 0x80)
            {
                remainder = (remainder << 1)^CRC8_POLYNOMIAL;
            }
            else
            {
                remainder = (remainder << 1);
            }
        }
    }

    /* Returns the calculated CRC code */
    return remainder;
}


/**
 * @brief	The 6 bytes of data received by SHT30 were CRC checked 
 *				and converted into temperature and humidity values
 * @param	dat--Store the address of the received data (6-byte array)
 * @retval	Check success--  return:0
 * 			Check failure--return:1,Set the temperature and humidity values to 0
*/
uint8_t SHT30_Dat_To_Float(uint8_t* const dat, float* temperature, float* humidity)
{
	uint16_t recv_temperature = 0;
	uint16_t recv_humidity = 0;
	
	/* Verify that temperature data and humidity data are received correctly */
	if(CheckCrc8(dat, 0xFF) != dat[2] || CheckCrc8(&dat[3], 0xFF) != dat[5])
		return 1;
	
	/* Conversion temperature data */
	recv_temperature = ((uint16_t)dat[0]<<8)|dat[1];
	*temperature = -45 + 175*((float)recv_temperature/65535);
	
	/* Conversion humidity data */
	recv_humidity = ((uint16_t)dat[3]<<8)|dat[4];
	*humidity = 100 * ((float)recv_humidity / 65535);
	
	return 0;
}

static void IIC_Config_Master(void)
{
#ifdef I2C_IRQ_ENABLE
  NVIC_InitTypeDef NVIC_InitStructure;
#endif
	I2C_InitTypeDef I2C_InitStruct;

	I2C_InitStruct.I2C_Mode = I2C_MASTER_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = I2C_Speedmode_High;
	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
	I2C_InitStruct.I2C_OwnAddress = 0x73;       //casually specify 
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
	I2C_InitStruct.I2C_SCL_HCNT = IIC_HCNT;
	I2C_InitStruct.I2C_SCL_LCNT = IIC_LCNT;
	I2C_InitStruct.I2C_SDA_HOLD = IIC_HOLD;

	I2C_InitStruct.I2C_HS_MADDR = 12;
	I2C_InitStruct.I2C_RX_TL = 0;
	I2C_InitStruct.I2C_TX_TL = 0;

	I2C_Reset(I2C0);

	/* i2c interrupt request */
#ifdef I2C_IRQ_ENABLE
/*Enable I2C interrupt here*/
	NVIC_InitStructure.NVIC_IRQChannel = I2C0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	set_IRQ_state(STATUS_IDLE);
		
#endif

	I2C_Init(I2C0, &I2C_InitStruct);
}



void I2C_TestReadSHT30(void)
{
	
	uint8_t recv_dat[6] = {0};
	float temperature = 0.0;
	float humidity = 0.0;	
    GPIO_SetPinMux(GPIO_Pin_0, IO_I2C0_SCL); //I2C_SCL
    GPIO_SetPinMux(GPIO_Pin_1, IO_I2C0_SDA); //I2C_SDA	
	IIC_Config_Master();	
	SHT30_SendCMD( CMD_SOFT_RESET);
	SHT30_SendCMD(CMD_MEAS_PERI_2_M);
	
  while (1)
  {
		delay_us(60000);
		if(SHT30_Read_Dat(recv_dat) != (-1))
		{
			if(SHT30_Dat_To_Float(recv_dat, &temperature, &humidity)==0)
			{
				printf("temperature = %f, humidity = %f\n", temperature, humidity);
			}
			else
			{
				printf("crc check fail.\n");
			}
		}
		else
		{
			printf("read data from sht30 fail.\n");
		}
	}
}
