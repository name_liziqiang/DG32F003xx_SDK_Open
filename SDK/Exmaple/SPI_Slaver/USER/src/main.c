/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_utility_spi.h"
#include "DG32F003xx_spi.h"
#include "usart.h"	  

/* Private define ------------------------------------------------------------*/
#define SCS        GPIO1_0
#define SCLK       GPIO1_1
#define MOSI       GPIO1_2
#define MISO       GPIO1_3
#define SYNC       GPIO0_4

// READ DATA = BYTE
// WRITE DATA = WORD/BYTE

#define TEST_BYTES 64
/* Private variables ---------------------------------------------------------*/
uint32_t buff32[TEST_BYTES];
uint16_t buff16[TEST_BYTES];
uint8_t  buff8[TEST_BYTES];
volatile int p_tx    = 0;
int          tx_size = 0;
uint8_t*     p_data;

volatile int p_rx    = 0;
int          rx_size = 0;
uint8_t*     r_data;

uint8_t ovrflw_irq_flag = 0;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/



void SPI_IRQHandler(void)
{
  if (SPI0->CTRL1 & SPI_CTRL1_RECV_OVRFLW_IRQ_EN) {
    if (SPI0->CTRL1 & SPI_CTRL1_RECV_OVRFLW_IRQ) {
      SPI0->CTRL1_CLR = SPI_CTRL1_RECV_OVRFLW_IRQ_EN;
      SPI0->CTRL1_CLR = SPI_CTRL1_RECV_OVRFLW_IRQ;
      ovrflw_irq_flag = 1;
    }
  } else {
    if (SPI0->CTRL1 & SPI_CTRL1_RECV_IRQ) {
      SPI0->CTRL1_CLR = SPI_CTRL1_RECV_IRQ;
      while (SPI0->STATUS & SPI_STATUS_RECV_NOT_EMPTY) {
        r_data[p_rx] = *(uint8_t*)&SPI0->DATA;
        p_rx++;
      }
    }

    if (SPI0->CTRL1 & SPI_CTRL1_RECV_TIMEOUT_IRQ) {
      SPI0->CTRL1_CLR = SPI_CTRL1_RECV_TIMEOUT_IRQ;
      while (SPI0->STATUS & SPI_STATUS_RECV_NOT_EMPTY) {
        r_data[p_rx] = *(uint8_t*)&SPI0->DATA;
        p_rx++;
      }
    }
  }

  if (SPI0->CTRL1 & SPI_CTRL1_XMIT_IRQ_EN) {
    if (SPI0->CTRL1 & SPI_CTRL1_XMIT_IRQ) {
      SPI0->CTRL1_CLR = SPI_CTRL1_XMIT_IRQ;
      while (SPI0->STATUS & SPI_STATUS_XMIT_NOT_FULL) {
        *(uint8_t*)&SPI0->DATA = p_data[p_tx++];
        if (p_tx == tx_size) {
          SPI0->CTRL1_CLR = SPI_CTRL1_XMIT_IRQ_EN;
          break;
        }
      }
    }
  }
}

void SPI_Tx_IRQ(SPI_TypeDef* SPIx, uint8_t* buff, int num)
{
  SPI_DataInitTypeDef QSPI_DataInitStruct;

  QSPI_DataInitStruct.SPI_DataLength  = num;
  QSPI_DataInitStruct.SPI_DUPLEX      = SPI_HalfDuplex;
  QSPI_DataInitStruct.SPI_TransferDir = SPI_Transfer_Write;

  p_data  = buff;
  tx_size = num;
  p_tx    = 0;

  SPI_DataConfig(SPIx, &QSPI_DataInitStruct);

  NVIC->ISER[SPI_IRQn >> 5] = 1 << (SPI_IRQn & 0x1F);
  SPIx->CTRL1_SET           = SPI_CTRL1_XMIT_IRQ_EN;

  GPIO_SetPinMux(MOSI, IO_GPIO);
  GPIO_SetPinDir(MOSI, GPIO_Mode_OUT);
  GPIO_ClearPin(MOSI);
  while (p_tx < tx_size)
    GPIO_SetPin(MOSI);

  GPIO_SetPinMux(MOSI, IO_SPI0_MOSI);
  while (SPI0->STATUS & SPI_STATUS_SPI_BUSY)
    ;
}

void SPI_Rx_IRQ(SPI_TypeDef* SPIx, uint8_t* buff, int num, uint8_t ovrflw)
{
  SPI_DataInitTypeDef QSPI_DataInitStruct;

  QSPI_DataInitStruct.SPI_DataLength  = num;
  QSPI_DataInitStruct.SPI_DUPLEX      = SPI_HalfDuplex;
  QSPI_DataInitStruct.SPI_TransferDir = SPI_Transfer_Read;

  r_data          = buff;
  rx_size         = num;
  p_rx            = 0;
  ovrflw_irq_flag = 0;

  SPI_DataConfig(SPIx, &QSPI_DataInitStruct);

  NVIC->ISER[SPI_IRQn >> 5] = 1 << (SPI_IRQn & 0x1F);
  SPIx->CTRL1_SET           = SPI_CTRL1_RECV_IRQ_EN | SPI_CTRL1_RECV_TIMEOUT_IRQ_EN;
  if (ovrflw) SPIx->CTRL1_SET = SPI_CTRL1_RECV_OVRFLW_IRQ_EN;
  else SPIx->CTRL1_CLR = SPI_CTRL1_RECV_OVRFLW_IRQ_EN;
  GPIO_SetPinMux(MISO, IO_GPIO);
  GPIO_SetPinDir(MISO, GPIO_Mode_OUT);
  GPIO_ClearPin(MISO);
  if (ovrflw) {
    GPIO_SetPin(MISO);
  } else {
    while (p_rx < rx_size)
      GPIO_SetPin(MISO);
  }
  GPIO_SetPinMux(MISO, IO_SPI0_MISO);
  while (SPI0->STATUS & SPI_STATUS_SPI_BUSY)
    ;
}

// slaver发送数据时需cs拉低前准备好发送数据
int main()
{
  int i;
  Debug_uart_init(115200); // TX=GP2_0 , RX=GP2_1

  spi_flash_init(SPI0, 1, SPI_SLAVE_MODE, 2, 0);
  spi_initPins(SCS, SCLK, MOSI, MISO); // CS=GP0_0 , CLK=GP0_1 , MOSI=GP0_2 , MISO=GP0_3

  char cmd;
  while (1) {
    printf("spi slaver start\r\n");
    cmd = '\0';
    SPI_Rx_IRQ(SPI0, (uint8_t*)&cmd, 1, 0);
    switch (cmd) {
      case 'w':
        SPI_Rx_IRQ(SPI0, buff8, 33, 0);
        while (p_rx < rx_size)
          ;
        for (i = 0; i < 33; i++) {
          if (buff8[i] != i + 0x8) {
             while (1);

          }
        }
        memset(buff8, 0, 33);
        break;
      case 'r':
        for (i = 0; i < 34; i++) {
          buff8[i] = i + 1;
        }
        SPI_Tx_IRQ(SPI0, buff8, 33);
        break;
      case 'o':
        SPI_Rx_IRQ(SPI0, buff8, 33, 1);
        break;
      default:
        break;
    }
  }
}
