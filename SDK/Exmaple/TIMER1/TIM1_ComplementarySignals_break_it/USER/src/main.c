/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"

/* Private define ------------------------------------------------------------*/
#define LED_PIN GPIO1_0
/* Private variables ---------------------------------------------------------*/

uint32_t temp;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void TIM1_Init(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	
	
	GPIO_SetPinMux(LED_PIN, IO_GPIO);//tim mark signal
    GPIO_SetPinDir(LED_PIN, GPIO_Mode_OUT); 
    GPIO_ClearPin(LED_PIN);
	GPIO_SetPinMux( GPIO_Pin_2, IO_TIM1_CH1);
	GPIO_SetPinMux( GPIO_Pin_3, IO_TIM1_CH1N);
	GPIO_SetPinMux( GPIO_Pin_4, IO_TIM1_BKIN);
    TIM1_Init();
	while(1){

  }
}

static void TIM1_Init(void)
{
	//open triger source
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_TIM1);

	uint32_t TIM_Prescaler = 1000-1;
	uint32_t TIM_Period = 24-1;
	TIM_TimeBaseInitTypeDef  TIM1_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM1_TimeBaseStructure);
	TIM1_TimeBaseStructure.TIM_Prescaler=TIM_Prescaler; //Frequency division coefficient
	TIM1_TimeBaseStructure.TIM_Period=TIM_Period;  //Automatic reload values
	TIM1_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//clock separation
	TIM1_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//count mode
	TIM1_TimeBaseStructure.TIM_RepetitionCounter = 0;//Repeated count value
	TIM_TimeBaseInit(TIM1, &TIM1_TimeBaseStructure);

	
	TIM_OCInitTypeDef  TIM1_OCInitStructure;
	TIM_OCStructInit(&TIM1_OCInitStructure);

	TIM1_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; 
	TIM1_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;        
	TIM1_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM1_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset; 

	TIM1_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
	TIM1_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; 
	TIM1_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;                  
	TIM1_OCInitStructure.TIM_Pulse = TIM_Period/2; 
	TIM_OC1Init(TIM1, &TIM1_OCInitStructure);  //Output comparison channel 1 initialization
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable); //CH1 pre-loaded to enable	
	
	
	/* Automatic Output enable, Break, dead time and lock configuration*/
	TIM_BDTRInitTypeDef TIM1_BDTRInitStructure;
	TIM1_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
	TIM1_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
	TIM1_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_OFF; 
	TIM1_BDTRInitStructure.TIM_DeadTime = 200;
	TIM1_BDTRInitStructure.TIM_Break = TIM_Break_Enable;//Effective enabling of brake signal
	TIM1_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;//Brake signal active high level
	TIM1_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Enable;//Automatic brake recovery
	TIM_BDTRConfig(TIM1, &TIM1_BDTRInitStructure);//config TIM1 BDTR

	TIM_CtrlPWMOutputs(TIM1,ENABLE);	//MOE Pulse output
	TIM_ARRPreloadConfig(TIM1, ENABLE); //Enable the TIMX preloaded register on the ARR
	TIM_ClearITPendingBit(TIM1, TIM_IT_Break);
	TIM_ITConfig(TIM1,TIM_IT_Break, ENABLE);  
	//Interrupt configuration
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM1_IRQn;	  
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;  
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_Init(&NVIC_InitStruct);

	TIM_Cmd(TIM1,ENABLE);
}


/**
 * @brief 
 * 
 */
void TIM1_IRQHandler(void)
{

  if ((TIM_GetITStatus(TIM1, TIM_IT_Break)) != RESET)
  {
	GPIO_TogglePin(LED_PIN);		
    TIM_ClearITPendingBit(TIM1, TIM_IT_Break);
  }
}
