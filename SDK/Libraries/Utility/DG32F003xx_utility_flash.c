#include "DG32F003xx_utility_flash.h"
#include "DG32F003xx_flash.h"
#include "DG32F003xx_flash_nvr.h"
#include "DG32F003xx_gpio.h"
#include <stdio.h>
#include "DG32F003xx_rcc.h"   




/**
  * @brief  写flash函数. 按page写入
  * @param   addr 要读取的数据的地址，不要和程序代码区重复，为4的整数倍
						*buff 要写入的数据buff
             byteCount  要写入的数据字节数，为4的整数倍
  * @retval void
  */
void flah_writeData8(uint32_t addr, uint8_t* buf, uint32_t byteCount)
{
	uint32_t sectorIndex,sectorcount;
	sectorIndex = addr/512;
	sectorcount = byteCount/512;
	if(byteCount%512) sectorcount++;
	Flash_Unlock();
	for(int i = 0;i <= sectorcount; i++)
	Flash_EraseSector(sectorIndex+i);
	Flash_Write32(addr,(uint32_t*)buf,byteCount);
	Flash_Lock();
}

/**
  * @brief  写flash函数. 按page写入
  * @param   addr 不要和程序代码区重复，为4的整数倍
						*buff 要写入的数据buff
             byteCount  要写入的数据字节数
  * @retval void
  */
void flah_writeData32(uint32_t addr, uint32_t* buf, uint32_t byteCount)
{
	uint32_t sectorIndex,sectorcount;
	sectorIndex = addr/512;
	sectorcount = byteCount/512;
	if(byteCount%512) sectorcount++;
	Flash_Unlock();
	for(int i = 0;i <= sectorcount; i++)
	Flash_EraseSector(sectorIndex+i);
	Flash_Write32(addr,(uint32_t*)buf,byteCount);
	Flash_Lock();	
}
/**
  * @brief  读flash函数.
  * @param  addr  要读取的数据的地址
						*buff 要读出的数据存放buff
            size  要读出的的数据字节数
  * @retval void
  */
void flah_readData8(uint32_t addr, uint8_t* buf, uint32_t byteCount)
{
	uint32_t i;
	uint8_t* pRead = (uint8_t*)addr; 
	uint32_t Count = byteCount;
	for(i=0;i<Count;i++){
		buf[i] = pRead[i];
	}
}

/**
  * @brief  读flash函数.
  * @param  *buff 要读出的数据存放buff
            size  要读出的的数据字节数
  * @retval void
  */
void flah_readData32(uint32_t addr, uint32_t* buf, uint32_t byteCount)
{
	uint32_t i;
	uint32_t* pRead = (uint32_t*)addr;
	uint32_t Count = byteCount/4;
	for(i=0;i<Count;i++){
		buf[i] = pRead[i];
	}
}
