#ifndef __DG32F003xx_utility_iic_Master_h
#define __DG32F003xx_utility_iic_Master_h
#include "stdio.h"
#include "DG32F003xx.h"
#include "DG32F003xx_i2c.h"
#include "DG32F003xx_utility_i2c.h"

 
#ifdef __cplusplus
 extern "C" {
#endif

void I2C_TestMasterWR(void);

#ifdef __cplusplus
}
#endif

#endif
