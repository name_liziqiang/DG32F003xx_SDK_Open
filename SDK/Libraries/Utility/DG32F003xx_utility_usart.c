#include "DG32F003xx.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_uart.h"
#include "DG32F003xx_utility_usart.h"
#include "misc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* Private functions ---------------------------------------------------------*/

UART_TypeDef *Debug_Uart = NULL;


/**
 * @brief  Retargets the C library printf function to the UART.
 * @param  None
 * @retval None
 */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the UART */
  if(Debug_Uart != 0)UART_SendData(Debug_Uart, (uint8_t)ch);

  /* Loop until the end of transmission */
  if(Debug_Uart != 0)while (UART_GetFlagStatus(Debug_Uart, UART_FLAG_TXFE) == RESET) {}

  return ch;
}

int UartPuts(UART_TypeDef *UARTx, char *buf)
{
  uint32_t waittime = 0;
  while ((*buf) != 0) {
    UART_SendData(UARTx, (uint8_t)*buf);
    while (UART_GetFlagStatus(UARTx, UART_FLAG_TXFE) == RESET) {
      waittime++;
      if (waittime > 0x100000)
        break;
    }
    buf++;
  }
  return 0;
}

int UartPuts1(UART_TypeDef *UARTx, char *buf, uint32_t len)
{
  int txfifolen = 0;
  while (len) {
    if ((UARTx->STAT & UART_FLAG_TXFF) == 0) { // fifo is not full
      UARTx->DATA = (uint8_t)*buf;
      buf++;
      len--;
      txfifolen++;
    } else {
      txfifolen = txfifolen;
    }
  }
  // UART_ClearITPendingBit(UARTx, UART_IT_TFEIS);                 // The interrupt flag must be cleared before enabling interrupt
  // UART_ITConfig(UARTx, UART_IT_TFEIEN | UART_IT_TXIEN, ENABLE); // Enable to send FIFO empty interrupt
  while (UART_GetFlagStatus(UARTx, UART_FLAG_TXFE) == RESET)
    ; // Waiting for the end of sending
	
	return 0;
}

#ifdef USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  /* Infinite loop */
  while (1) {
  }
}
#endif

/**
 * @brief  configure uart
 * @param  UARTx: Select the UART peripheral.
 *   This parameter can be one of the following values:
 *   UARTx, x:[1:2].
 * @param  BaudRate: Set uart baudrate
 * @retval:none
 */
void UART_Configuration(UART_TypeDef *UARTx, uint32_t TxPin, uint32_t RxPin, uint32_t BaudRate)
{
  UART_InitTypeDef UART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStruct;

  Debug_Uart = UARTx;

  if (UARTx == UART0) {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
    RCC->UART0CLKDIV                = 2;
    NVIC_InitStruct.NVIC_IRQChannel = UART0_IRQn;
    GPIO_SetPinMux(TxPin, IO_UART0_TX);
    GPIO_SetPinMux(RxPin, IO_UART0_RX);
  } else if (UARTx == UART1) {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART1);
    RCC->UART1CLKDIV                = 2;
    NVIC_InitStruct.NVIC_IRQChannel = UART1_IRQn;
    GPIO_SetPinMux(TxPin, IO_UART1_TX);
    GPIO_SetPinMux(RxPin, IO_UART1_RX);
  }

  NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
  NVIC_Init(&NVIC_InitStruct);

  UART_Reset(UARTx);
  UART_StructInit(&UART_InitStructure);
  UART_InitStructure.UART_BaudRate   = BaudRate;           // Baud rate
  UART_InitStructure.UART_TXIFLSEL   = UART_TXIFLSEL_14;   // Send FIFO depth   TXFIFO LEVEL
  UART_InitStructure.UART_RXIFLSEL   = UART_RXIFLSEL_14;   // Receive FIFO depth  RXFIFO LEVEL
  UART_InitStructure.UART_IdleNum    = UART_IdleNum_4Byte; // Idle detects word length
  UART_InitStructure.UART_IdleEN     = UART_IdleEN_Enable; // Idle detection enabled
  UART_Init(UARTx, &UART_InitStructure);
  /* set timeout to 8 uart clk cycle len */
  UARTx->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UARTx->CTRL0_SET = 8 << 16;

  UART_ITConfig(UARTx, UART_IT_RXIEN | UART_IT_RTIEN, ENABLE);
  if (BaudRate == 0) {
    UART_AUTOBAUDConfig(UARTx, UART_AUTOBAUD_MODE_RE_0);
    UART_ITConfig(UARTx, UART_IT_ABEOEN, ENABLE);
    UART_AUTOBAUDCmd(UARTx, ENABLE);
  }
  UART_Cmd(UARTx, ENABLE);
}
/**
 * @brief config uart ir mode
 * 
 * @param UARTx 
 * @param TxPin 
 * @param RxPin 
 * @param BaudRate 
 */
void UARTIR_Configuration(UART_TypeDef *UARTx, uint32_t TxPin, uint32_t RxPin, uint32_t BaudRate)
{
  UART_InitTypeDef UART_InitStructure;
  

  if (UARTx == UART0) {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
    RCC->UART0CLKDIV                = 2;
    GPIO_SetPinMux(TxPin, IO_UART0_TX);
    GPIO_SetPinMux(RxPin, IO_UART0_RX);
  } else if (UARTx == UART1) {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART1);
    RCC->UART1CLKDIV                = 2;
    GPIO_SetPinMux(TxPin, IO_UART1_TX);
    GPIO_SetPinMux(RxPin, IO_UART1_RX);
  }

  UART_Reset(UARTx);
  UART_StructInit(&UART_InitStructure);
  UART_InitStructure.UART_BaudRate   = BaudRate;           // Baud rate
  UART_InitStructure.UART_TXIFLSEL   = UART_TXIFLSEL_14;   // Send FIFO depth   TXFIFO LEVEL
  UART_InitStructure.UART_RXIFLSEL   = UART_RXIFLSEL_14;   // Receive FIFO depth  RXFIFO LEVEL
  UART_InitStructure.UART_IdleNum    = UART_IdleNum_4Byte; // Idle detects word length
  UART_InitStructure.UART_IdleEN     = UART_IdleEN_Enable; // Idle detection enabled
  UART_Init(UARTx, &UART_InitStructure);
  /* set timeout to 8 uart clk cycle len */
  UARTx->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UARTx->CTRL0_SET = 16 << 16;

  UART_IrDAConfig(UARTx, UART_IrDAMode_LowPower);
  UART_SetILPDVSR(UARTx, 12);
	
	UART_ClearITPendingBit(UART0, UART_INTR_RTIS);
	UART_ClearITPendingBit(UART0, UART_IT_RXIS);
	UART_ClearITPendingBit(UART0, UART_IT_IDLEIEIS);
  // UART_ITConfig(UARTx, UART_IT_RXIEN | UART_IT_RTIEN, ENABLE);
	UART_ITConfig(UARTx, UART_IT_IDLEEN, ENABLE);
	
	UART_IrDACmd(UARTx,ENABLE);
  UART_Cmd(UARTx, ENABLE);
}

void UartITconfig(UART_TypeDef *UARTx, uint32_t IT_EnbaleBit, FunctionalState NewState)
{
  NVIC_InitTypeDef NVIC_InitStruct;
  if (UARTx == UART0)
    NVIC_InitStruct.NVIC_IRQChannel = UART0_IRQn;
  else
    NVIC_InitStruct.NVIC_IRQChannel = UART1_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelCmd      = NewState;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
  NVIC_Init(&NVIC_InitStruct);
  UART_ITConfig(UARTx, IT_EnbaleBit, NewState);
}

void UartConfig(UART_TypeDef *UARTx, uint32_t TxPin, uint32_t RxPin, UART_InitTypeDef *UART_InitStruct)
{
  if (UARTx == UART0) {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
    RCC->UART0CLKDIV = 2;
    GPIO_SetPinMux(TxPin, IO_UART0_TX);
    GPIO_SetPinMux(RxPin, IO_UART0_RX);
  } else if (UARTx == UART1) {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART1);
    RCC->UART1CLKDIV = 2;
    GPIO_SetPinMux(TxPin, IO_UART1_TX);
    GPIO_SetPinMux(RxPin, IO_UART1_RX);
  }

  UART_Reset(UARTx);
  UART_Init(UARTx, UART_InitStruct);
  /* set timeout to 8 uart clk cycle len */
  UARTx->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UARTx->CTRL0_SET = 8 << 16;

  if (UART_InitStruct->UART_BaudRate == 0) {
    UART_AUTOBAUDConfig(UARTx, UART_AUTOBAUD_MODE_RE_0);
    UART_AUTOBAUDCmd(UARTx, ENABLE);
  }
  UART_Cmd(UARTx, ENABLE);
}
