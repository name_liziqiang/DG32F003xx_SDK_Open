/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx_utility_iic_MasterEEPROM.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_uart.h"
#include "DG32F003xx_utility_usart.h"
#include "DG32F003xx_utility_i2c.h"
#include "DG32F003xx_i2c.h"
#include "stdio.h"
#include "stdlib.h"

//#define IIC_SLAVE 
//#define I2C_IRQ_ENABLE      				
#define IIC_SPEED_MODE  		 		I2C_Speedmode_FAST
#define IIC_Use_10bits_Addr
#define IIC_SLAVER_ADDRESS  0X55

#define EEPROM_DEVADDR  				(0xa0 >> 1)
#define DEV_ADDR_SIZE           2

//LCNT min = 8  actual quantity = LCNT+1
//HCNT min = 6  actual quantity = HCNT+8
//HOLD set 0  actual quantity = 1   set other  actual quantity = HOLD + 5 mast smaller then LCNT
//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
#define IIC_LCNT    0   //I2C_Speedmode_Stand 100k  115  116   400k   25 26
#define IIC_HCNT    0
#define IIC_HOLD    0


#define EEPROM_2kb           //32 pages of 8 bytes    2kb
// #define EEPROM_4kb        //32 pages of 16 bytes   4kb
// #define EEPROM_8kb        //64 pages of 16 bytes   8kb
// #define EEPROM_16kb       //128 pages of 16 bytes  16kb
// #define EEPROM_32kb
// #define EEPROM_256kb      //512 pages of 64 bytes  256kb 

#if defined (EEPROM_2kb)
  #define EEPROM_INTERNEL_ADDRES_BITS      8  //eeprom内部寻址位数
  #define EEPROM_PAGE_SIZE                 8  //1页多少字节

#elif defined (EEPROM_4kb)
  #define EEPROM_INTERNEL_ADDRES_BITS      9
  #define EEPROM_PAGE_SIZE                16

#elif defined (EEPROM_8kb)
  #define EEPROM_INTERNEL_ADDRES_BITS      10
  #define EEPROM_PAGE_SIZE                16

#elif defined (EEPROM_16kb)
  #define EEPROM_INTERNEL_ADDRES_BITS      11
  #define EEPROM_PAGE_SIZE                16

#elif defined (EEPROM_32kb)
  #define EEPROM_INTERNEL_ADDRES_BITS      12
  #define EEPROM_PAGE_SIZE                32

#elif defined (EEPROM_256kb)
  #define EEPROM_INTERNEL_ADDRES_BITS      15
  #define EEPROM_PAGE_SIZE                64
#endif


#define BYTENUM  16
//#define SIM_FLAG 0x4000fffc


#define  IIC_TEST_BUFFER_LEN     32
#ifdef IIC_SLAVE
static char Slaver_RxBuffer[IIC_TEST_BUFFER_LEN];
static volatile int8_t Slaver_RxLen = 0;
static volatile uint8_t stopCount;
static volatile uint8_t startCount;//Used to count Start_DET 
static volatile uint8_t reqCount;
static volatile uint8_t Slaver_RxFlag;
#else
static __align(32) uint8_t wbuf[256];
static __align(32) uint8_t rbuf[256];
//static char Master_RxBuffer[IIC_TEST_BUFFER_LEN];
#ifdef I2C_IRQ_ENABLE
static volatile I2CRX_STATE MasterReadStat;
static volatile I2CTX_STATE MasterWriteStat;
#endif
#endif




/**
 * @brief delay function
 * 
 * @param ms 
 */
static void IIC_TestDelay(uint32_t ms)
{
  uint32_t i, j;
  for(i=0; i < ms; i++){
    for(j=0; j<100; j++){
      __nop();
    }
  }
}


#ifdef I2C_IRQ_ENABLE
 void I2C0_IRQHandler(void)
{
	__IO unsigned int stat = 0;
	stat ++ ;
	stat = I2C_GetITFlag(I2C0);
	if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_RX_FULL)){
    //RX BUF full
    while((I2C0->STAT & (1<<3)) != 0){
      //RX_FIFO is not empty
      MasterReadStat.rxBuf[ MasterReadStat.currentRxLen ] = I2C0->DATA_CMD&0xff;
      MasterReadStat.currentRxLen ++;
      if(MasterReadStat.currentRxLen >= MasterReadStat.needRxLen){
        MasterReadStat.overFlag = 1;
        I2C_ITConfig(I2C0, I2C_IT_RX_FULL, DISABLE);
        break;
      }
    }
  }
	
	if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_RX_OVER)){
	   I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_RX_OVER);
  }
	  
  
  
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_TX_EMPTY)){
    //TX BUF is not full
    while((MasterWriteStat.signalCount != 0) && ((I2C0->STAT & (1 << 1)) != 0)){
		  I2C_SendData(I2C0, MasterWriteStat.txBuf[MasterWriteStat.currentTxLen ++]);
		  MasterWriteStat.signalCount--;
		if(MasterWriteStat.signalCount == 0){
		  I2C_ITConfig(I2C0, I2C_IT_TX_EMPTY, DISABLE);
		  I2C_ITConfig(I2C0, I2C_IT_STOP_DET, ENABLE);
		}
	 }
	while((MasterReadStat.signalCount != 0) && ((I2C0->STAT & (1 << 1)) != 0)){
		  I2C_TriggerRecData(I2C0);
		  MasterReadStat.signalCount--;
		
		if(MasterReadStat.signalCount == 0){
		  I2C_ITConfig(I2C0, I2C_IT_TX_EMPTY, DISABLE);
		}
	}
  }
  else if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_STOP_DET)){
    //iic stop
    MasterWriteStat.overFlag = 1;
    I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_STOP_DET);
    I2C_ITConfig(I2C0, I2C_IT_STOP_DET, DISABLE);
	GPIO_TogglePin(GPIO_Pin_2);
  }
  
  
  
	if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_TX_OVER)){
	   I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_TX_OVER);
  }
	
  
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_START_DET)){
    //iic start
    I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_START_DET);
    GPIO_TogglePin(GPIO_Pin_2);

  }
}

#endif
/**
 * @brief Test Function  Init I2C0 Master
 * 
 */
static void IIC_Config_Master(void)
{
	I2C_InitTypeDef I2C_InitStruct;
	
	I2C_InitStruct.I2C_Mode = I2C_MASTER_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = IIC_SPEED_MODE;
	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
	I2C_InitStruct.I2C_OwnAddress = 0x73;       //casually specify 
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
	I2C_InitStruct.I2C_SCL_HCNT = IIC_HCNT;
	I2C_InitStruct.I2C_SCL_LCNT = IIC_LCNT;
	I2C_InitStruct.I2C_SDA_HOLD = IIC_HOLD;

	I2C_InitStruct.I2C_HS_MADDR = 12;
	I2C_InitStruct.I2C_RX_TL = 0;
	I2C_InitStruct.I2C_TX_TL = 0;
    
	I2C_Reset(I2C0);

	/* i2c interrupt request */
#ifdef I2C_IRQ_ENABLE
	/*Enable I2C interrupt here*/
    //Interrupt configuration
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = I2C0_IRQn;	  
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;  
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_Init(&NVIC_InitStruct);
#endif

	I2C_Init(I2C0, &I2C_InitStruct);
}


/**
 * @brief 
 * 
 */
void I2C_TestReadEEPROM(void)
{
  int i;


  GPIO_SetPinMux(GPIO_Pin_2, IO_GPIO);//I2C mark signal
  GPIO_SetPinDir(GPIO_Pin_2, GPIO_Mode_OUT); 
  GPIO_ClearPin(GPIO_Pin_2);
	
  GPIO_SetPinMux(GPIO_Pin_0, IO_I2C0_SCL); //I2C_SCL
  GPIO_SetPinMux(GPIO_Pin_1, IO_I2C0_SDA); //I2C_SDA	
  IIC_Config_Master();
	
  while(1){
    for (i = 0; i < BYTENUM; i++)wbuf[i] = (char)( (i + 1) & 0xff);
    for (i = 0; i < BYTENUM; i++)rbuf[i] = 0;
#ifdef I2C_IRQ_ENABLE
	uint32_t timeout;
	MasterWriteStat.txBuf = (char *)wbuf;
    MasterWriteStat.needTxLen = BYTENUM;
    MasterWriteStat.currentTxLen = 0;
    MasterWriteStat.signalCount = BYTENUM;
    MasterWriteStat.overFlag = 0;
	I2C_ITConfig(I2C0, I2C_IT_START_DET|I2C_IT_STOP_DET|I2C_IT_TX_OVER, ENABLE);
    I2C_WriteIRQ(EEPROM_DEVADDR, 0, DEV_ADDR_SIZE);
	timeout = 0x400000;
    while((MasterWriteStat.overFlag == 0) & timeout)timeout--;
    if(timeout == 0){
      printf("Master Write timeout\r\n");
    }
     IIC_TestDelay(300);
#else
    I2C_Write(EEPROM_DEVADDR, 0, DEV_ADDR_SIZE, wbuf, BYTENUM);
#endif
    IIC_TestDelay(10000);
#ifdef I2C_IRQ_ENABLE	
	
	MasterReadStat.rxBuf = (char *)rbuf;
	MasterReadStat.needRxLen = BYTENUM;
	MasterReadStat.currentRxLen = 0;
	MasterReadStat.signalCount = BYTENUM;
	MasterReadStat.overFlag = 0;		
	I2C_ReadIRQ(EEPROM_DEVADDR, 0, DEV_ADDR_SIZE);
	I2C_ITConfig(I2C0, I2C_IT_START_DET|I2C_IT_STOP_DET|I2C_IT_RX_OVER, ENABLE);

//	timeout = 0x400000;
//	while((MasterReadStat.overFlag == 0) & timeout)timeout--;
//    if(timeout == 0){
//      printf("Master Write timeout\r\n");
//    }
#else	
    I2C_Read(EEPROM_DEVADDR, 0, DEV_ADDR_SIZE, rbuf, BYTENUM);
#endif

    IIC_TestDelay(1000);
    uint32_t cnt = 0;
    for (cnt = 0; cnt < BYTENUM; cnt++){
			if (wbuf[cnt] != rbuf[cnt]) break;
      else{
        printf("rbuf[%d] = 0x%02x\r\n", cnt, rbuf[cnt]);
      }
		}
    if(cnt < BYTENUM){
      printf("read and write not match!, at loc: %d\r\r\n", cnt);
    }
    else{
      printf("read and write match!\r\r\n");
    }
    IIC_TestDelay(60000);
  }
}


