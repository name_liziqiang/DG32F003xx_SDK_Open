/**
  ******************************************************************************
  * @file    DG32F003xx_adc.h
  * @author  Alpscale Application Team
  * @version V1.0.1
  * @date    20-April-2014
  * @brief   This file contains all the functions prototypes for the ADC firmware 
  *          library
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 Alpscale</center></h2>
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DG32F003xx_ADC_H
#define __DG32F003xx_ADC_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"


/** 
  * @brief  ADC Init structure definition
  */
  
typedef struct
{
  uint32_t ADC_Channel;                   /*!< ADC input channel number */
  uint32_t ADC_ConvMode;                  /*!< Specifies whether the conversion that trigger channels 
                                             is performed in Continuous or Single mode. */

  uint32_t ADC_Count_SCycle;              /*!< Time interval in cyclic mode 
                                               The unit is AHB clock*/

  uint32_t ADC_ReferenceSrcSel;           /*!< Specifies ADC reference voltage.
                                             This parameter can be a value of @ref ADC_ReferenceSourceSelect */

  uint32_t ADC_OffsetErrorComp;           /*!< Specifies ADC Whether to use calibration values to compensate for offset errors.
                                             This parameter can be a value of @ref ADC_OffsetErrorCompensateSelect */

  uint32_t ADC_TrigConvSrcSel;            /*!< Selet the external/internal event used to trigger the start of conversion. 
                                             This parameter can be a value of @ref ADC_TriggerSourceSelect*/

  uint32_t ADC_TrigConvEdge;              /*!< Specifies ADC trigger edge.
                                             This parameter can be a value of @ref ADC_TriggerMode */

  uint8_t ADC_First_DisSampleNum;         /*!< discard sample num when conversion at first time */
  uint8_t ADC_Exch_DisSampleNum;          /*!< discard sample num when exchange ADC channel */
 
  uint32_t ADC_ClockDiv;                  /*!< Specifies ADC clock Frequency division value 0 - 31*/
} ADC_InitTypeDef;

/** @addtogroup ADC_SingleTrigConvMode
  * @{
  */
#define ADC_ConversionMode_Single        ((uint32_t)0x00000000)
#define ADC_ConversionMode_Continuous    ((uint32_t)0x00001000)
#define IS_ADC_ConversionMode(mode)      (((mode) == ADC_ConversionMode_Single) || \
                                          ((mode) == ADC_ConversionMode_Continuous))

/** @addtogroup ADC_ReferenceSourceSelect
  * @{ 
  */
#define ADC_ReferenceSource_AVDD         ((uint32_t)(0 << 24))
// #define ADC_ReferenceSource_VREF         ((uint32_t)(0 << 24))
#define ADC_ReferenceSource_Internal     ((uint32_t)(1 << 24))

#define IS_ADC_REFSOURCE(refsrc)         (((refsrc) == ADC_ReferenceSource_AVDD) || \
                                          ((refsrc) == ADC_ReferenceSource_Internal)) 

/** @addtogroup ADC_OffsetErrorCompensateSelect
  * @{ 
  */
#define ADC_OffsetErrorComp_ENABLE       ((uint32_t)((uint32_t)1 << 31))
#define ADC_OffsetErrorComp_DISABLE      ((uint32_t)((uint32_t)0 << 31))

#define IS_ADC_OffsetErrorComp(sel)      (((sel) == ADC_OffsetErrorComp_ENABLE) || \
                                          ((sel) == ADC_OffsetErrorComp_DISABLE)) 

/** @addtogroup ADC_TriggerSourceSelect
  * @{
  */
#define ADC_Internal_TrigSrc_Software    ((uint32_t)(0 << 20))
#define ADC_Internal_TrigSrc_TIM1_TRGO   ((uint32_t)(0 << 20))
#define ADC_Internal_TrigSrc_TIM4_TRGO   ((uint32_t)(1 << 20))
#define ADC_Internal_TrigSrc_TIM15_TRGO  ((uint32_t)(2 << 20))
#define ADC_Internal_TrigSrc_GPIO_ADET   ((uint32_t)(3 << 20))

#define IS_ADC_TRIGSRC(trigsrc)  (((trigsrc) == ADC_Internal_TrigSrc_Software) || \
                                  ((trigsrc) == ADC_Internal_TrigSrc_TIM1_TRGO) || \
                                  ((trigsrc) == ADC_Internal_TrigSrc_TIM4_TRGO) || \
                                  ((trigsrc) == ADC_Internal_TrigSrc_TIM15_TRGO) || \
                                  ((trigsrc) == ADC_Internal_TrigSrc_GPIO_ADET)) 

/** @addtogroup ADC_TriggerMode
  * @{
  */
#define ADC_TrigEdge_Software       ((uint32_t)0x00000000)
#define ADC_TrigEdge_Rising         ((uint32_t)0x00010000)
#define ADC_TrigEdge_Falling        ((uint32_t)0x00020000)
#define ADC_TrigEdge_RisingFalling  ((uint32_t)0x00030000)
#define IS_ADC_TRIG_EDGE(trigEdge)  (((trigEdge) == ADC_TrigEdge_Software) || \
                                     ((trigEdge) == ADC_TrigEdge_Rising) || \
                                     ((trigEdge) == ADC_TrigEdge_Falling) || \
                                     ((trigEdge) == ADC_TrigEdge_RisingFalling))


/**
  * @}Exch 
  */
#define ADC_Exch_DisSampleNum_0         ((uint32_t)0x00000000)
#define ADC_Exch_DisSampleNum_1         ((uint32_t)0x00000010)
#define ADC_Exch_DisSampleNum_2         ((uint32_t)0x00000020)
#define ADC_Exch_DisSampleNum_3         ((uint32_t)0x00000030)
#define IS_ADC_EXCH_DISSAMPLENUM(num)   (((num) == ADC_Exch_DisSampleNum_0) ||\
                                         ((num) == ADC_Exch_DisSampleNum_1) ||\
                                         ((num) == ADC_Exch_DisSampleNum_2) ||\
                                         ((num) == ADC_Exch_DisSampleNum_3))

/**
  * @}first
  */
#define ADC_First_DisSampleNum_0         ((uint32_t)0x00000000)
#define ADC_First_DisSampleNum_1         ((uint32_t)0x00000001)
#define ADC_First_DisSampleNum_2         ((uint32_t)0x00000002)
#define ADC_First_DisSampleNum_3         ((uint32_t)0x00000003)
#define IS_ADC_FIRST_DISSAMPLENUM(num)   (((num) == ADC_First_DisSampleNum_0) ||\
                                         ((num) == ADC_First_DisSampleNum_1) ||\
                                         ((num) == ADC_First_DisSampleNum_2) ||\
                                         ((num) == ADC_First_DisSampleNum_3))

/** @addtogroup ADC clock division
  * @{
  */

#define IS_ADC_CLOCK_DIV(clkDiv)         ((clkDiv) <= (31))


/**
  * @}
  */

/** @defgroup ADC_channels 
  * @{
  */ 
#define ADC_CHANNEL0                    ((uint32_t)(1 << 0))
#define ADC_CHANNEL1                    ((uint32_t)(1 << 1))
#define ADC_CHANNEL2                    ((uint32_t)(1 << 2))
#define ADC_CHANNEL3                    ((uint32_t)(1 << 3))
#define ADC_CHANNEL4                    ((uint32_t)(1 << 4))
#define ADC_CHANNEL5                    ((uint32_t)(1 << 5))
#define ADC_CHANNEL6                    ((uint32_t)(1 << 6))
#define ADC_CHANNEL7                    ((uint32_t)(1 << 7))
#define ADC_CHANNEL8                    ((uint32_t)(1 << 8))
#define ADC_CHANNEL9                    ((uint32_t)(1 << 9))
#define ADC_CHANNEL10                   ((uint32_t)(1 << 10))
#define ADC_CHANNEL11                   ((uint32_t)(1 << 11))
#define ADC_CHANNEL12                   ((uint32_t)(1 << 12))
#define ADC_CHANNEL13                   ((uint32_t)(1 << 13))
#define ADC_CHANNEL14                   ((uint32_t)(1 << 14))
#define ADC_CHANNEL15                   ((uint32_t)(1 << 15))
#define ADC_CHANNEL16                   ((uint32_t)(1 << 16))
#define ADC_CHANNEL17                   ((uint32_t)(1 << 17))
#define ADC_CHANNEL18                   ((uint32_t)(1 << 18))
#define ADC_CHANNEL19                   ((uint32_t)(1 << 19))
#define ADC_CHANNEL20                   ((uint32_t)(1 << 20))
#define ADC_CHANNEL21                   ((uint32_t)(1 << 21))
#define ADC_CHANNEL22                   ((uint32_t)(1 << 22))

#define ADC_CHANNEL_ALL                 ((uint32_t)(0x7FFFFF))
#define ADC_CHANNEL_MAX                 (23)

/** @defgroup ADC_PERIPH 
  * @{
  */
#define IS_ADC_CHANNEL(PERIPH) ((PERIPH) < ((ADC_CHANNEL_ALL) + 1))

/* ADC channel irq enable */
#define ADC_Conv_IRQ                    ((uint32_t)((uint32_t)1<<31))

/* ADC channel dispatch */
#define ADC_Dispatch_Channel0           ((uint32_t)(1 << 0))
#define ADC_Dispatch_Channel1           ((uint32_t)(1 << 1))
#define ADC_Dispatch_Channel2           ((uint32_t)(1 << 2))
#define ADC_Dispatch_Channel3           ((uint32_t)(1 << 3))
#define ADC_Dispatch_Channel4           ((uint32_t)(1 << 4))
#define ADC_Dispatch_Channel5           ((uint32_t)(1 << 5))
#define ADC_Dispatch_Channel6           ((uint32_t)(1 << 6))
#define ADC_Dispatch_Channel7           ((uint32_t)(1 << 7))
#define ADC_Dispatch_Channel8           ((uint32_t)(1 << 8))
#define ADC_Dispatch_Channel9           ((uint32_t)(1 << 9))
#define ADC_Dispatch_Channel10          ((uint32_t)(1 << 10))
#define ADC_Dispatch_Channel11          ((uint32_t)(1 << 11))
#define ADC_Dispatch_Channel12          ((uint32_t)(1 << 12))
#define ADC_Dispatch_Channel13          ((uint32_t)(1 << 13))
#define ADC_Dispatch_Channel14          ((uint32_t)(1 << 14))
#define ADC_Dispatch_Channel15          ((uint32_t)(1 << 15))
#define ADC_Dispatch_Channel16          ((uint32_t)(1 << 16))
#define ADC_Dispatch_Channel17          ((uint32_t)(1 << 17))
#define ADC_Dispatch_Channel18          ((uint32_t)(1 << 18))
#define ADC_Dispatch_Channel19          ((uint32_t)(1 << 19))
#define ADC_Dispatch_Channel20          ((uint32_t)(1 << 20))
#define ADC_Dispatch_Channel21          ((uint32_t)(1 << 21))
#define ADC_Dispatch_Channel22          ((uint32_t)(1 << 22))


/**
 * @brief Bit mask to operate on
 * 
 */
#define ADC_REF_MASK                    ((uint32_t)(1 << 24))
#define ADC_CLKDIV_MASK                 ((uint32_t)(0X1F << 0))


  /*  Function used to set the ADC configuration to the default reset state *****/
void ADC_RCC_Init(ADC_TypeDef *ADCx);
void ADC_Init(ADC_TypeDef *ADCx, ADC_InitTypeDef *ADC_InitStruct);
void ADC_DeInit(ADC_TypeDef *ADCx);
void ADC_AnaEn(uint32_t channel);

/* Channels Configuration functions *******************************************/
void ADC_SetConvChannel(ADC_TypeDef *ADCx, uint32_t channel, FunctionalState NewState);
uint32_t ADC_GetConvValue(ADC_TypeDef *ADCx, uint32_t channel);
uint32_t ADC_GetCalibrationValue(ADC_TypeDef *ADCx);

/* Interrupts and flags management functions **********************************/
void ADC_ITConfig(ADC_TypeDef *ADCx, FunctionalState NewState);
void ADC_ClearITFlag(ADC_TypeDef *ADCx, uint32_t channel);
ITStatus ADC_GetITStatus(ADC_TypeDef *ADCx, uint32_t channel);

/* Software trigger conversion function **********************************/
void ADC_StartSoftwareTrigConv(ADC_TypeDef *ADCx);
void ADC_StartCalibration(ADC_TypeDef *ADCx);
void ADC_Run(ADC_TypeDef *ADCx, FunctionalState NewState);

#ifdef __cplusplus
}
#endif

#endif /*__A1900_ADC_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT Alpscale *****END OF FILE****/
