#ifndef  __DG32F003xx_UTILITY_USART_H
#define  __DG32F003xx_UTILITY_USART_H
#include "DG32F003xx_uart.h"

void UART_Configuration(UART_TypeDef *UARTx, uint32_t TxPin, uint32_t RxPin, uint32_t BaudRate);
void UARTIR_Configuration(UART_TypeDef *UARTx, uint32_t TxPin, uint32_t RxPin, uint32_t BaudRate);

int UartPuts(UART_TypeDef *UARTx, char *buf);
int UartPuts1(UART_TypeDef *UARTx, char *buf, uint32_t len);
void UartConfig(UART_TypeDef *UARTx, uint32_t TxPin, uint32_t RxPin, UART_InitTypeDef *UART_InitStruct);
void UartITconfig(UART_TypeDef *UARTx, uint32_t IT_EnbaleBit, FunctionalState NewState);

#endif //__DG32F003xx_UTILITY_USART_H
