/**
  ******************************************************************************
  * @file    DG32F003xx_utility.h
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   This file contains all the functions prototypes for the I2C firmware 
  *              library.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  ******************************************************************************  
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DG32F003xx_UTILITY_I2C_H
#define __DG32F003xx_UTILITY_I2C_H

#ifdef __cplusplus
 extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "stdint.h"
#include "DG32F003xx.h"
#include "DG32F003xx_i2c.h"

#define I2C_RX_FIFO_SIZE        8
#define I2C_TX_FIFO_SIZE        8

typedef enum { 
  I2C_GP0_0,
  I2C_GP0_1,
  I2C_GP0_5,
  I2C_GP0_8,
  I2C_GP0_3,
  I2C_GP0_9,
  I2C_GP0_13,
  I2C_GP0_17
}I2C_GPIO;

typedef struct _I2CRX_STATE{
  char     *rxBuf;
  uint32_t needRxLen;
  uint32_t currentRxLen;
  uint32_t signalCount;
  uint8_t  overFlag;
}I2CRX_STATE;
typedef I2CRX_STATE* pI2CRX_STATE;

typedef struct _I2CTX_STATE{
  char     *txBuf;
  uint32_t needTxLen;
  uint32_t currentTxLen;
  uint32_t signalCount;
  uint8_t  overFlag;
}I2CTX_STATE;
typedef I2CTX_STATE* pI2CTX_STATE;

typedef void (*I2C_IRQFunction)(void);

void I2C_PinsInit(I2C_TypeDef *I2Cx, I2C_GPIO i2c_io);
int I2C_UtilityInit(I2C_TypeDef* I2Cx, I2C_InitTypeDef* I2C_InitStruct, I2C_GPIO i2c_io);
void I2C_RegistIRQHandler(I2C_TypeDef* I2Cx, I2C_IRQFunction function);
int I2C_Read(I2C_TypeDef* I2Cx, uint8_t dev_addr, uint32_t internal_addr, int addr_size, uint8_t *buf,  int read_size);
int I2C_ReadIRQ(I2C_TypeDef* I2Cx, uint8_t dev_addr, uint32_t internal_addr, int addr_size);
int I2C_Write(I2C_TypeDef* I2Cx, uint8_t dev_addr, uint32_t internal_addr, int addr_size, uint8_t *buf,  int write_size);
int I2C_WriteIRQ(I2C_TypeDef* I2Cx, uint8_t dev_addr, uint32_t internal_addr, int addr_size);

#ifdef __cplusplus
}
#endif

#endif /*__DG32F003xx_I2C_H */

/******************* (C) COPYRIGHT 2013 Alpscale*****END OF FILE****/
