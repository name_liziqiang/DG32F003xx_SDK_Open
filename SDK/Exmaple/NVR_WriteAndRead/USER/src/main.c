/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_adc.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_flash.h"
#include "DG32F003xx_flash_nvr.h"
#include "usart.h"	  
#include "delay.h"
//#include "DG32F003xx_utility_flash.h"

/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t WriteBuf[128];	//512 bytes
uint8_t  ReadBuf[512];		//512	bytes
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{   
	delay_init(24);

	for(unsigned int i=0;i<128;i++){
		WriteBuf[i]=i<<1;
	}

	eFlashErase_NVR();                
	eFlashWrite_NVR(0,WriteBuf,128);
	eFlashRead_NVR(0,ReadBuf,512);
	
	
	while(1){

  }
}

