/**
 ******************************************************************************
 * @file    system_DG32F003xx.c
 * @author  MCD Application Team
 * @version V1.0.0
 * @date    12/12/2013
 * @brief   CMSIS Cortex-M0+ Device Peripheral Access Layer System Source File.
 ******************************************************************************
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
 ******************************************************************************
 */

/** @addtogroup CMSIS
 * @{
 */

/** @addtogroup DG32F003xx_system
 * @{
 */

/** @addtogroup DG32F003xx_System_Private_Includes
 * @{
 */

#include "DG32F003xx_rcc.h"
#include "DG32F003xx_wdg.h"
#include "DG32F003xx_gpio.h"
/*******************************************************************************
 *  Clock Definitions
 *******************************************************************************/

/**
 * @brief  Setup the microcontroller system
 *         Initialize the Embedded Flash Interface, the PLL and update the
 *         SystemCoreClock variable.
 * @note   This function should be used only after reset.
 * @param  None
 * @retval None
 */
void SystemInit(void)
{
  uint32_t timeout = 0;
  //RCC->PDRUNCFG = RCC->PDRUNCFG & 0xFFFFFEFF; // bit8=48MIRC
  RCC->SYSAHBCLKDIV = 2;
  RCC_MAINCLKSel(RCC_MAINCLK_SOURCE_48MIRC);
  RCC_ResetAHBCLK(1 << AHBCLK_BIT_GPIO);
  RCC_ResetAHBCLK(1 << AHBCLK_BIT_IOCON);
  while ((RCC->EFLASHSTAT & RCC_EFLASHSTAT_DONE) == 0) {
    if ((timeout++) >= 0x8000) {
      break;
    }
  }
  FLASH->ACR = 0;
	GPIO_ConfigDriver(GPIO_Pin_All,GPIO_DRIVER_1);
}

/******************* (C) COPYRIGHT 2013 Alphascale *****END OF FILE****/
