/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_wdg.h"

/* Private define ------------------------------------------------------------*/
#define WAKEUP_PIN  GPIO0_7
#define WAKEUP_EDGE GPIO_IRQ_EDGE_DOUBLE

#define MASK_PIN  GPIO2_1
/* Private variables ---------------------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void DeepSleep_WakeupFromGPIO(uint32_t wakeupGpio, EdgeAction gpioIntEdge);
void DeepSleep_WakeupFromWatchdog(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{   
	
    GPIO_ConfigPull(GPIO_Pin_All,GPIO_PULL_UP);
	DeepSleep_WakeupFromGPIO(WAKEUP_PIN,WAKEUP_EDGE);
//	DeepSleep_WakeupFromWatchdog();
		//bit       9      8        6   5    2       1
	   //      eflash   IRC48M   BOR  BOD  ADC0   IRC10K     //0:上电 1：掉电
	RCC->PDRUNCFG =   ~((1 << 9) | (1 << 8) | (1 << 1));
	goSleep(SM_SLEEP, LM_LOW); //SM_DEEPSLEEP, LM_LOW   不配置唤醒，直接进入休眠，
	while(1){
  
  }
}


/**
 * @brief NMI中断， watchdog的中断，system 从中断唤醒Sleep, 唤醒之后先进一次中断
 * 
 */
void NMI_Handler(void)
{
  WDG_ClearFaultFlag();
  WDG_ClearTimeOutFlag();
  WDG_ClearITFlag();
  WDG_ReloadCounter();
	__asm("NOP");
  __asm("NOP");
}
/**
 * @brief IO中断
 * 
 */
void GPIO_IRQHandler(void)
{
	volatile uint32_t data;
	volatile uint32_t ris;

	data		 = GPIO0_IT->MIS;
	ris 		 = GPIO0_IT->RIS;
  GPIO0_IT->IC = data;
  __asm("NOP");
  __asm("NOP");
	GPIO_MaskIT(GPIO_Pin_All);	//屏蔽所有GPIO中断，防止外部测试开关抖动
}

/**
 * @brief 从GPIO中断唤醒 DeepSleep
 * 
 * @param wakeupGpio    从指定的IO唤醒
 * @param gpioIntEdge 	指定唤醒的边沿
 */
void DeepSleep_WakeupFromGPIO(uint32_t wakeupGpio, EdgeAction gpioIntEdge)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	GPIO_SetPinMux(wakeupGpio, IO_GPIO);
	GPIO_SetPinDir(wakeupGpio, GPIO_Mode_IN);
	if(gpioIntEdge == GPIO_IRQ_EDGE_RISING){
		GPIO_ConfigPull(wakeupGpio, GPIO_PULL_DOWN);
	}
	else if(gpioIntEdge == GPIO_IRQ_EDGE_FALLING){
		GPIO_ConfigPull(wakeupGpio, GPIO_PULL_UP);
	}
	else {
		GPIO_ConfigPull(wakeupGpio, GPIO_PULL_UP);
	}
	GPIO_MaskIT(GPIO_Pin_All);	
	GPIO_ClearITFlag(wakeupGpio);
	GPIO_UnmaskIT(wakeupGpio);
	GPIO_EdgeITEnable(wakeupGpio, gpioIntEdge);

	NVIC_InitStruct.NVIC_IRQChannel 		= GPIO_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd		= ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);


}
/**
 * @brief 从watchdog中断唤醒DeepSleep
 * 
 */
void DeepSleep_WakeupFromWatchdog(void)
{
  WDT_CLKSET(ENABLE);
  WDT_Reset();
  commonDelay(10);

  RCC->WDTCLKDIV = 1;
  WDG_SetMode(WDG_SETMODE_IT, WDG_FAULT_ENABLE);      //IRQ
  
  WDG_SetReload(50000);	 /* reset time : 0.1ms*50K=5S */
  WDG_ReloadCounter();


}

