/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_rcc.h"
#include "delay.h"
/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t random_value=0;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
  delay_init(24);
	RCC_InitPRNG();
	
	while(1){
		delay_ms(1000);
		random_value=RCC->PRNG_DATA;
  }
}
