/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"

/* Private define ------------------------------------------------------------*/
#define LED_PIN GPIO2_0
/* Private variables ---------------------------------------------------------*/

uint32_t temp;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void TIM15_Init(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	GPIO_SetPinMux(LED_PIN, IO_GPIO);//tim mark signal
	GPIO_SetPinDir(LED_PIN, GPIO_Mode_OUT); 
  GPIO_ClearPin(LED_PIN);
	
	GPIO_SetPinMux( GPIO0_2, IO_TIM15_CH1);
	TIM15_Init();
	while(1){
		
  }
}

static void TIM15_Init(void)
{
	//open triger source
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_TIM15);

	uint32_t TIM_Prescaler = 4000-1;
	uint32_t TIM_Period = 12800 - 1;
	TIM_TimeBaseInitTypeDef  TIM15_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM15_TimeBaseStructure);
	TIM15_TimeBaseStructure.TIM_Prescaler=TIM_Prescaler; //Frequency division coefficient
	TIM15_TimeBaseStructure.TIM_Period=TIM_Period;  //Automatic reload values
	TIM15_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//clock separation
	TIM15_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//count mode
	TIM15_TimeBaseStructure.TIM_RepetitionCounter = 0;//Repeated count value
	TIM_TimeBaseInit(TIM15, &TIM15_TimeBaseStructure);
	TIM_ARRPreloadConfig(TIM15, ENABLE); //Enable the TIMX preloaded register on the ARR

	
  TIM_ICInitTypeDef       TIM15_ICInitStructure;
	TIM15_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM15_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM15_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_BothEdge;//The edge detector is triggered by rising and falling edges
															  // TIM_ICPolarity_Rising
															  // TIM_ICPolarity_Falling
															  // TIM_ICPolarity_BothEdge
	TIM15_ICInitStructure.TIM_ICFilter = 0;   
	TIM15_ICInitStructure.TIM_Channel = TIM_Channel_1;
	TIM_ICInit(TIM15,&TIM15_ICInitStructure);	
  	
	TIM_ClearITPendingBit(TIM15, TIM_IT_CC1);     		
	TIM_ITConfig(TIM15, TIM_IT_CC1, ENABLE); 	//Enable TIM interrupt
	//Interrupt configuration
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM15_IRQn;	  
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;  
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_Init(&NVIC_InitStruct);	

	TIM_Cmd(TIM15,ENABLE);
}

/**
 * @brief 
 * 
 */
void TIM15_IRQHandler(void)
{
  
  if ((TIM_GetITStatus(TIM15, TIM_IT_CC1)) != RESET)
  {
		GPIO_TogglePin(LED_PIN);
    TIM_ClearITPendingBit(TIM15, TIM_IT_CC1);
  }

}
