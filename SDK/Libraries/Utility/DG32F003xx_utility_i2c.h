#ifndef  __DG32F003xx_UTILITY_I2C_H
#define  __DG32F003xx_UTILITY_I2C_H
#include <stdint.h>

typedef enum
{
  I2C_SPEED_STD,
  I2C_SPEED_FAST,
} I2CSpeedSel;

typedef struct _I2CRX_STATE{
  char     *rxBuf;
  uint32_t needRxLen;
  uint32_t currentRxLen;
  uint32_t signalCount;
  uint8_t  overFlag;
}I2CRX_STATE;
typedef I2CRX_STATE* pI2CRX_STATE;

typedef struct _I2CTX_STATE{
  char     *txBuf;
  uint32_t needTxLen;
  uint32_t currentTxLen;
  uint32_t signalCount;
  uint8_t  overFlag;
}I2CTX_STATE;
typedef I2CTX_STATE* pI2CTX_STATE;
int I2C_Configuration(I2CSpeedSel speed);
int I2C_WriteIRQ( uint8_t dev_addr, uint32_t internal_addr, int addr_size);
int I2C_Write(uint8_t dev_addr, uint32_t internal_addr, int addr_size, uint8_t *buf,  int write_size);
int I2C_Read(uint8_t dev_addr, uint32_t internal_addr, int addr_size, uint8_t *buf,  int read_size);
int I2C_ReadIRQ(uint8_t dev_addr, uint32_t internal_addr, int addr_size);

#endif //__DG32F003xx_UTILITY_I2C_H
