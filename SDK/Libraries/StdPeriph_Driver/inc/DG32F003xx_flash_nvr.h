#ifndef __DG32F003XX_FLASH_NVR_H
#define __DG32F003XX_FLASH_NVR_H

/**
 * @brief Indicates the result of the eFlash operation
 * 
 */
typedef enum { EFLASH_ERROR   = 0,
               EFLASH_SUCCESS = !EFLASH_ERROR } eFlashErrorStatus;
/**
 * @brief Indicates the result of the eFlash operation
 * 
 */
typedef enum { USERCODE_UNLOCKED   = 0,
               USERCODE_LOCKED = !USERCODE_UNLOCKED } userCodeStatus;
/**
* @brief NRST GPIO config message
* 
*/
#define NRST_CONFIG_GP00                                      (0)
#define NRST_CONFIG_GP01                                      (1)
#define NRST_CONFIG_GP02                                      (2)
#define NRST_CONFIG_GP03                                      (3)
#define NRST_CONFIG_GP04                                      (4)
#define NRST_CONFIG_GP05                                      (5)
#define NRST_CONFIG_GP06                                      (6)
#define NRST_CONFIG_GP07                                      (7)
#define NRST_CONFIG_GP10                                      (8)
#define NRST_CONFIG_GP11                                      (9)
#define NRST_CONFIG_GP12                                      (10)
#define NRST_CONFIG_GP13                                      (11)
#define NRST_CONFIG_GP14                                      (12)
#define NRST_CONFIG_GP15                                      (13)
#define NRST_CONFIG_GP16                                      (14)
#define NRST_CONFIG_GP17                                      (15)
#define NRST_CONFIG_GP20                                      (16)
#define NRST_CONFIG_GP21                                      (17)
#define NRST_CONFIG_GP22                                      (18)
#define NRST_CONFIG_GP23                                      (19)
#define NRST_CONFIG_GP24                                      (20)
#define NRST_CONFIG_GP25                                      (21)
#define NRST_CONFIG_NULL                                      (22)
#define NRST_CONFIG_MAX                                       NRST_CONFIG_NULL
#define NRST_CONFIG_MASK                                      (0x0000001F)
#define NRST_CONFIG_PULLUP_ENABLE                             (1 << 8)
#define NRST_CONFIG_PULLUP_MASK                               (1 << 8)
/**
 * @brief 
 * 
 */
#define BOOT_CONFIG_GP00                                      (0)
#define BOOT_CONFIG_GP01                                      (1)
#define BOOT_CONFIG_GP02                                      (2)
#define BOOT_CONFIG_GP03                                      (3)
#define BOOT_CONFIG_GP04                                      (4)
#define BOOT_CONFIG_GP05                                      (5)
#define BOOT_CONFIG_GP06                                      (6)
#define BOOT_CONFIG_GP07                                      (7)
#define BOOT_CONFIG_GP10                                      (8)
#define BOOT_CONFIG_GP11                                      (9)
#define BOOT_CONFIG_GP12                                      (10)
#define BOOT_CONFIG_GP13                                      (11)
#define BOOT_CONFIG_GP14                                      (12)
#define BOOT_CONFIG_GP15                                      (13)
#define BOOT_CONFIG_GP16                                      (14)
#define BOOT_CONFIG_GP17                                      (15)
#define BOOT_CONFIG_GP20                                      (16)
#define BOOT_CONFIG_GP21                                      (17)
#define BOOT_CONFIG_GP22                                      (18)
#define BOOT_CONFIG_GP23                                      (19)
#define BOOT_CONFIG_GP24                                      (20)
#define BOOT_CONFIG_GP25                                      (21)
#define BOOT_CONFIG_NULL                                      (22)
#define BOOT_CONFIG_MAX                                       BOOT_CONFIG_NULL
#define BOOT_CONFIG_MASK                                      (0x0000001F)
/**
 * @brief Read the relevant calibration values of the LDO
 * 
 * @param ldo_adj 
 * @param lpldo_adj 
 */
void eFlashRead_LdoTrim(unsigned int* ldo_adj, unsigned int* lpldo_adj);
/**
 * @brief Read the relevant calibration values of the IRC
 * 
 * @param hirc_adj 
 * @param lirc_adj 
 */
void eFlashRead_IrcTrim(unsigned int* hirc_adj, unsigned int* lirc_adj);
/**
 * @brief Read the relevant calibration values of the ADC
 * 
 * @param ref_adj 
 * @param tempco_adj 
 */
void eFlashRead_AdcTrim(unsigned int* ref_adj, unsigned int* tempco_adj);
/**
 * @brief read params about eflash
 * 
 * @param para1 
 * @param para2 
 * @param para3 
 */
void eFlashRead_eFlashParams(unsigned int* para1, unsigned int* para2, unsigned int* para3);
/**
 * @brief Read the SN of the MCU(Total 128bit)
 *        4 int type value
 * 
 * @param sn 
 */
void eFlashRead_SN(unsigned int* sn);
/**
 * @brief Read whether the user code is locked
 * 
 * @return userCodeStatus 
 */
userCodeStatus eFlashRead_CodeLockState(void);
/**
 * @brief Configures whether to lock user code
 *        The interface checks whether the parameters to be configured are consistent with the current flash parameters. 
 *        If no, new parameters are written.
 * 
 * @param newState USERCODE_UNLOCKED or USERCODE_LOCKED
 */
void eFlashWrite_CodeLockState(userCodeStatus newState);
/**
 * @brief Read the configuration information of reset I/O
 * 
 * @param nrst_gpio NRST_CONFIG_GP00 - NRST_CONFIG_MAX
 * @param nrst_pullup 0 close nrst pullup, 1 open nrst pullup.
 */
void eFlashRead_NRST(unsigned int* nrst_gpio, unsigned int* nrst_pullup);
/**
 * @brief Writes new NRST configuration parameters.
 *        The interface checks whether the parameters to be configured are consistent with the current flash parameters. 
 *        If no, new parameters are written.
 * 
 * @param new_nrst_gpio NRST_CONFIG_GP00 - NRST_CONFIG_MAX
 * @param new_nrst_pullup 0 close nrst pullup, 1 open nrst pullup.
 */
void eFlashWrite_NRST(unsigned int new_nrst_gpio, unsigned int new_nrst_pullup);
/**
 * @brief Read the configuration information of boot I/O
 * 
 * @param boot0 
 * @param boot1 
 */
void eFlashRead_BOOT(unsigned int* boot0, unsigned int* boot1);
/**
 * @brief Writes new boot configuration parameters.
 *        The interface checks whether the parameters to be configured are consistent with the current flash parameters. 
 *        If no, new parameters are written.
 * 
 * @param new_boot0 BOOT_CONFIG_GP00 - BOOT_CONFIG_MAX
 * @param new_boot1 BOOT_CONFIG_GP00 - BOOT_CONFIG_MAX
 */
void eFlashWrite_BOOT(unsigned int new_boot0, unsigned int new_boot1);
/**
 * @brief Erase the eflash NVR area
 * 
 * @return eFlashErrorStatus 
 */
eFlashErrorStatus eFlashErase_NVR(void);
/**
 * @brief Data is written to the flash NVR area.
 *        This interface is not responsible for erasing sectors.
 * 
 * @param addr  To write to the address, he must be 4-byte aligned
 *              0 to 127 (a sector size)
 * @param data  buffer of data to be written
 * @param len   To write the length of the data, he must be 4-byte aligned
 * @return eFlashErrorStatus 
 */
eFlashErrorStatus eFlashWrite_NVR(unsigned int addr, unsigned int* data, unsigned int len);
/**
 * @brief Data is read to the flash NVR area.
 *        Operate by byte
 * 
 * @param addr  To read to the address
 * @param data  buffer of data to be written
 * @param len   To read the length of the data
 * @return eFlashErrorStatus 
 */
void eFlashRead_NVR(unsigned int addr, unsigned char* data, unsigned int len);

#endif
