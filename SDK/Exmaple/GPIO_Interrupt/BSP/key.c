#include "key.h"
#include "misc.h"

void Key_Init(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;

	NVIC_InitStruct.NVIC_IRQChannel = GPIO_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
	
	GPIO_SetPinMux(GPIO0_1, IO_GPIO);
	GPIO_SetPinDir(GPIO0_1, GPIO_Mode_IN);
	GPIO_ConfigPull(GPIO0_1, GPIO_PULL_DOWN);
	GPIO_EdgeITEnable(GPIO0_1, GPIO_IRQ_EDGE_FALLING);
	
	GPIO_SetPinMux(GPIO1_3, IO_GPIO);
	GPIO_SetPinDir(GPIO1_3, GPIO_Mode_IN);
	GPIO_ConfigPull(GPIO1_3, GPIO_PULL_DOWN);
	GPIO_EdgeITEnable(GPIO1_3, GPIO_IRQ_EDGE_RISING);
}
