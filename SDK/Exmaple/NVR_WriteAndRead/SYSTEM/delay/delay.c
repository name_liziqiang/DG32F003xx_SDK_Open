#include "delay.h"
#include "DG32F003xx_rcc.h"
#include "misc.h"

/**
 * @brief 初始化systick 用于精准延时
 * 
 * @param systickDiv systick 分频系数
 */
void delay_init(uint32_t systickDiv)
{
  RCC->SYSTICKCLKDIV = systickDiv;
  SysTick->LOAD = 0xffffff;//24bit 计数器
  SysTick->VAL   = 0; 
  SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;/* Enable and SysTick Timer */
  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);//AHB 96分频，systick工作在1Mhz
}
/**
 * @brief 实现1us的精准延时
 * 
 * @param nus 需要延时的时间，us为单位，最大值为 0xffffff
 */
void delay_us(uint32_t nus)
{
  if(nus > 2)nus -= 2;
  else nus = 0;
  if(nus == 0)return;
  //在1Mhz下，SysTick->VAL 每加1，就是1uS
  SysTick->CTRL = 0x00;
  SysTick->VAL = 0;
  SysTick->LOAD = nus;

  SysTick->CTRL=0x01;//开始倒数
  while(!(SysTick->CTRL & (1 << 16)));//等待事件到达
}
/**
 * @brief 实现1ms的精确延时
 * 
 * @param nms 需要延时的时间，ms为单位 最大值为16777ms
 */
void delay_ms(uint32_t nms)
{
  if(nms == 0)return;
  if(nms <= 16777){
    //在1Mhz下，SysTick->VAL 每加1，就是1uS
    SysTick->CTRL = 0x00;
    SysTick->VAL = 0;
    SysTick->LOAD = nms * 1000;

    SysTick->CTRL=0x01;//开始倒数
    while(!(SysTick->CTRL & (1 << 16)));//等待事件到达
  }
}

