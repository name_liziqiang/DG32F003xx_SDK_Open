/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"

/* Private define ------------------------------------------------------------*/
#define LED_PIN GPIO1_0
/* Private variables ---------------------------------------------------------*/

uint32_t temp;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void TIM1_Init(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	GPIO_SetPinMux( GPIO_Pin_2, IO_TIM1_ETR);
	GPIO_SetPinMux(LED_PIN, IO_GPIO);//tim mark signal
    GPIO_SetPinDir(LED_PIN, GPIO_Mode_OUT); 
    GPIO_ClearPin(LED_PIN);	
    TIM1_Init();
	while(1){

  }
}

static void TIM1_Init(void)
{
	//open triger source
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_TIM1);

	uint32_t TIM_Prescaler = 1-1;
	uint32_t TIM_Period = 8-1;
	TIM_TimeBaseInitTypeDef  TIM1_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM1_TimeBaseStructure);
	TIM1_TimeBaseStructure.TIM_Prescaler=TIM_Prescaler; //Frequency division coefficient
	TIM1_TimeBaseStructure.TIM_Period=TIM_Period;  //Automatic reload values
	TIM1_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//clock separation
	TIM1_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//count mode
	TIM1_TimeBaseStructure.TIM_RepetitionCounter = 0;//Repeated count value
	TIM_TimeBaseInit(TIM1, &TIM1_TimeBaseStructure);

	
	
	TIM_ARRPreloadConfig(TIM1, ENABLE); //Enable the TIMX preloaded register on the ARR
	TIM_SelectInputTrigger(TIM1, TIM_TS_ETRF);//Slave timer input signal set
	TIM_SelectSlaveMode(TIM1, TIM_SlaveMode_External1); //Slave timer input mode set
	TIM_ETRConfig(TIM1, TIM_ExtTRGPSC_DIV4, TIM_ExtTRGPolarity_NonInverted, 0);


	TIM_ClearITPendingBit(TIM1, TIM_IT_Update);     		
	TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE); 	//Enable TIM interrupt
	//Interrupt configuration
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM1_IRQn;	  
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;  
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_Init(&NVIC_InitStruct);
	
	TIM_Cmd(TIM1,ENABLE);
}
/**
 * @brief 
 * 
 */
void TIM1_IRQHandler(void)
{
  // Gets the timer update interrupt flag
  if ((TIM_GetITStatus(TIM1, TIM_IT_Update)) != RESET)
  { 
	  TIM_ClearITPendingBit(TIM1, TIM_IT_Update  );
	  GPIO_TogglePin(LED_PIN);		

  }

}
