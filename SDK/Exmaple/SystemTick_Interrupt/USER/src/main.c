/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint32_t gSysTickCount;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
  	RCC->SYSTICKCLKDIV = 24;	//sys tick =AHB/24=1M HZ
	SysTick_Config(1000000);		//1M/1000000=1hz=1s
	SysTick->CTRL &= (~SysTick_CTRL_CLKSOURCE_Msk);		//enable SYSTICKCLKDIV
	while(1){

  }
}
void SysTick_Handler(void)
{
  /* Decrement the TimingDelay variable */
  gSysTickCount++;
}