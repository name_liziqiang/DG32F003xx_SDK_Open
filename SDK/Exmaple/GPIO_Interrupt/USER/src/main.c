/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "misc.h"
#include "delay.h"


/* Private define ------------------------------------------------------------*/

#define KEY0  GPIO0_1
#define KEY1  GPIO1_3

#define LED1    GPIO0_0
#define LED2    GPIO1_2

#define TogglePin GPIO1_0

#define KEY0_READ  GPIO_ReadPin(KEY0)//读取按键0
#define KEY1_READ  GPIO_ReadPin(KEY1)//读取按键1
/* Private variables ---------------------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void APP_GpioInit(void);
/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */

int main()
{  
	delay_init(24);
	APP_GpioInit();
	while(1){
		delay_ms(200);
		GPIO_TogglePin(TogglePin);
  }
}
/**
  * @brief  GPIO中断
  * @param  无
  * @retval 无
  */
void GPIO_IRQHandler(void)
{

	if(GPIO_GetMaskITStatus(KEY0)){
		GPIO_ClearITFlag(KEY0);
		GPIO_TogglePin(LED1);
		
	}
	
	if(GPIO_GetMaskITStatus(KEY1)){
		GPIO_ClearITFlag(KEY1);
		GPIO_TogglePin(LED2);
	}	
}
/**
  * @brief  GPIO初始化
  * @param  无
  * @retval 无
  */
static void APP_GpioInit(void)
{
 
	NVIC_InitTypeDef NVIC_InitStruct;

	NVIC_InitStruct.NVIC_IRQChannel         = GPIO_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);	
	
	GPIO_SetPinMux(KEY0, IO_GPIO);
	GPIO_SetPinDir(KEY0, GPIO_Mode_IN);
	GPIO_ConfigPull(KEY0,GPIO_PULL_UP);
	GPIO_EdgeITEnable(KEY0, GPIO_IRQ_EDGE_FALLING);
	
	GPIO_SetPinMux(KEY1, IO_GPIO);
	GPIO_SetPinDir(KEY1, GPIO_Mode_IN);
	GPIO_ConfigPull(KEY1,GPIO_PULL_UP);
	GPIO_EdgeITEnable(GPIO1_3, GPIO_IRQ_EDGE_RISING);
	
	
	
	GPIO_SetPinMux(LED1, IO_GPIO);
	GPIO_SetPinDir(LED1, GPIO_Mode_OUT);
	GPIO_ClearPin(LED1);
	
	GPIO_SetPinMux(LED2, IO_GPIO);
	GPIO_SetPinDir(LED2, GPIO_Mode_OUT);
	GPIO_ClearPin(LED2);
	
	GPIO_SetPinMux(TogglePin, IO_GPIO);
	GPIO_SetPinDir(TogglePin, GPIO_Mode_OUT);
	GPIO_ClearPin(TogglePin);	
	
}

