/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "delay.h"

/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

uint32_t temp;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void TIM15_Init(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
	GPIO_SetPinMux( GPIO_Pin_2, IO_TIM15_CH1);
	delay_init(24);
  TIM15_Init();

	while(1){
	delay_ms(500);
	TIM_Cmd(TIM15,DISABLE);
	TIM_SelectOnePulseMode(TIM15, TIM_OPMode_Single);//Set to monopulse mode
	TIM_Cmd(TIM15,ENABLE);
  }
}

static void TIM15_Init(void)
{
	//open triger source
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_TIM15);

	uint32_t TIM_Prescaler = 0;
	uint32_t TIM_Period = 2000;
	TIM_TimeBaseInitTypeDef  TIM15_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM15_TimeBaseStructure);
	TIM15_TimeBaseStructure.TIM_Prescaler=TIM_Prescaler; //Frequency division coefficient
	TIM15_TimeBaseStructure.TIM_Period=TIM_Period;  //Automatic reload values
	TIM15_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//clock separation
	TIM15_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//count mode
	TIM15_TimeBaseStructure.TIM_RepetitionCounter = 8;//Repeated count value
	TIM_TimeBaseInit(TIM15, &TIM15_TimeBaseStructure);

	
	TIM_OCInitTypeDef  TIM15_OCInitStructure;
	TIM_OCStructInit(&TIM15_OCInitStructure);

	TIM15_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; 
	TIM15_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;        
	TIM15_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Reset;
	TIM15_OCInitStructure.TIM_OCNIdleState = TIM_OCNIdleState_Reset; 

	TIM15_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; 
	TIM15_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; 
	TIM15_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;                  
	TIM15_OCInitStructure.TIM_Pulse = TIM_Period/2; 
	TIM_OC1Init(TIM15, &TIM15_OCInitStructure);  //Output comparison channel 1 initialization
	TIM_OC1PreloadConfig(TIM15, TIM_OCPreload_Enable); //CH1 pre-loaded to enable	
	
  TIM_SelectOnePulseMode(TIM15, TIM_OPMode_Single);//Set to monopulse mode
	TIM_CtrlPWMOutputs(TIM15,ENABLE);	//MOE Pulse output
	TIM_ARRPreloadConfig(TIM15, ENABLE); //Enable the TIMX preloaded register on the ARR


	TIM_Cmd(TIM15,ENABLE);
}
