/**
 ******************************************************************************
 * @file    DG32F003xx_gpio.h
 * @author  Alpscale Software Team
 * @version V1.0.0
 * @date    12/5/2011
 * @brief   This file contains all the functions prototypes for the GPIO
 *          firmware library.
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, ALPSCALE SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2011 Alpscale</center></h2>
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DG32F003xx_GPIO_H
#define __DG32F003xx_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"

/** @addtogroup DG32F003xx_StdPeriph_Driver
 * @{
 */

/** @addtogroup GPIO
 * @{
 */

/** @defgroup GPIO_Exported_Constants
 * @{
 */
#define IS_GPIO_ALL_PERIPH(PERIPH) ((PERIPH) == GPIO0)

/** @defgroup GPIO_IT_Types
 * @{
 */

#define IS_GPIO_IT_ALL_PERIPH(IT) ((IT) == GPIO0_IT)

/**
 * @brief  function selection
 */

typedef enum {
  IO_GPIO        = 0,
  IO_SWD         = 1,
  IO_EFLASH_TEST = 2,
  IO_UART0_TX    = 3,
  IO_UART0_RX    = 4,
  IO_UART0_CLK   = 5,
  IO_I2C0_SDA    = 6,
  IO_I2C0_SCL    = 7,
  IO_SPI0_CS     = 8,
  IO_SPI0_CLK    = 9,
  IO_SPI0_MOSI   = 10,
  IO_SPI0_MISO   = 11,
  IO_TIM1_CH1    = 12,
  IO_TIM1_CH2    = 13,
  IO_TIM1_CH3    = 14,
  IO_TIM1_CH4    = 15,
  IO_TIM1_CH1N   = 16,
  IO_TIM1_CH2N   = 17,
  IO_TIM1_CH3N   = 18,
  IO_TIM1_BKIN   = 19,
  IO_TIM1_ETR    = 20,
  IO_TIM4_CH1    = 21,
  IO_TIM4_CH2    = 22,
  IO_TIM4_CH3    = 23,
  IO_TIM4_CH4    = 24,
  IO_TIM15_CH1    = 25,
  IO_TIM15_CH2    = 26,
  IO_TIM15_CH1N   = 27,
  IO_INCLK   		= 28,
  IO_OUTCLK      = 29,
  IO_ADC         = 30,
  //IO_OSC         = 31,
  //IO_NRST        = 32,
  IO_ADET        = 33,
  IO_UART1_TX    = 34,
  IO_UART1_RX    = 35,
  IO_CMP0        = 36,
  IO_CMP1        = 37,
} IO_TypeDef;

/**
 * @brief  Configuration Mode enumeration
 */

typedef enum {
  GPIO_Mode_IN  = 0x0,
  GPIO_Mode_OUT = 0x1
} GPIOMode_TypeDef;

#define IS_GPIO_MODE(MODE) (((MODE) == GPIO_Mode_OUT) || ((MODE) == GPIO_Mode_IN))

/**
 * @brief  Configuration Edge Action enumeration
 */

typedef enum {
  GPIO_IRQ_EDGE_FALLING = 0x0,
  GPIO_IRQ_EDGE_RISING,
  GPIO_IRQ_EDGE_DOUBLE
} EdgeAction;

#define IS_GPIO_EDGE_ACTION(ACTION) (((ACTION) == GPIO_IRQ_EDGE_FALLING) || ((ACTION) == GPIO_IRQ_EDGE_RISING) || \
                                     ((ACTION) == GPIO_IRQ_EDGE_DOUBLE))

/**
 * @brief  Configuration Level Action enumeration
 */

typedef enum {
  GPIO_IRQ_LEVEL_LOW = 0x0,
  GPIO_IRQ_LEVEL_HIGH
} LevelAction;

#define IS_GPIO_LEVEL_ACTION(ACTION) (((ACTION) == GPIO_IRQ_LEVEL_LOW) || ((ACTION) == GPIO_IRQ_LEVEL_HIGH))

/**
 * @brief  GPIO Init structure definition
 */

typedef struct
{
  uint32_t GPIO_Pin;          /*!< Specifies the GPIO pins to be configured.
                                  This parameter can be any value of @ref GPIO_pins_define */
  IO_TypeDef GPIO_Function;   /*!< Specifies the function for the selected pins.
                                       This parameter can be a value of @ref PIOFUNC_TypeDef */
  GPIOMode_TypeDef GPIO_Mode; /*!< Specifies the operating mode for the selected pins.
                                  This parameter can be a value of @ref GPIOMode_TypeDef */
} GPIO_InitTypeDef;

// bit[4:3]
typedef enum {
  GPIO_PULL_DISABLE = 0x00,
  GPIO_PULL_DOWN    = 0x08,
  GPIO_PULL_UP      = 0x10,
} GPIOPull_TypeDef;

// bit[8]
typedef enum {
  GPIO_DRIVER_0 = 0x000,
  GPIO_DRIVER_1 = 0x100,
} GPIODrive_TypeDef;

// bit[9]
typedef enum {
  GPIO_SLEWRATE_FAST = 0x000,
  GPIO_SLEWRATE_SLOW = 0x200,
} GPIOSLEWRATE_TypeDef;

typedef enum {
  GPIO_INPUT_SCHMITT = 0x00,
  GPIO_INPUT_CMOS    = 0x40,
} GPIOInput_TypeDef;

typedef enum {
  GPIO_OUTPUT_NORMAL     = 0x000,
  GPIO_OUTPUT_OPENSOURCE = 0x400,
  GPIO_OUTPUT_OPENDRAIN  = 0x800,
} GPIOOutput_TypeDef;

/**
 * @brief  Bit_SET and Bit_RESET enumeration
 */

typedef enum {
  Bit_RESET = 0,
  Bit_SET
} BitAction;

#define IS_GPIO_BIT_ACTION(ACTION) (((ACTION) == Bit_RESET) || ((ACTION) == Bit_SET))

/**
 * @}
 */

/** @defgroup GPIO_pins_define
 * @{
 */
#define GPIO_Pin_0 ((uint32_t)0x00000001) /*!< Pin 0 selected */
#define GPIO_Pin_1 ((uint32_t)0x00000002) /*!< Pin 1 selected */
#define GPIO_Pin_2 ((uint32_t)0x00000004) /*!< Pin 2 selected */
#define GPIO_Pin_3 ((uint32_t)0x00000008) /*!< Pin 3 selected */
#define GPIO_Pin_4 ((uint32_t)0x00000010) /*!< Pin 4 selected */
#define GPIO_Pin_5 ((uint32_t)0x00000020) /*!< Pin 5 selected */
#define GPIO_Pin_6 ((uint32_t)0x00000040) /*!< Pin 6 selected */
#define GPIO_Pin_7 ((uint32_t)0x00000080) /*!< Pin 7 selected */

#define GPIO_Pin_8  ((uint32_t)0x00000100) /*!< Pin 8 selected */
#define GPIO_Pin_9  ((uint32_t)0x00000200) /*!< Pin 9 selected */
#define GPIO_Pin_10 ((uint32_t)0x00000400) /*!< Pin 10 selected */
#define GPIO_Pin_11 ((uint32_t)0x00000800) /*!< Pin 11 selected */
#define GPIO_Pin_12 ((uint32_t)0x00001000) /*!< Pin 12 selected */
#define GPIO_Pin_13 ((uint32_t)0x00002000) /*!< Pin 13 selected */
#define GPIO_Pin_14 ((uint32_t)0x00004000) /*!< Pin 14 selected */
#define GPIO_Pin_15 ((uint32_t)0x00008000) /*!< Pin 15 selected */

#define GPIO_Pin_16  ((uint32_t)0x00010000) /*!< Pin 16 selected */
#define GPIO_Pin_17  ((uint32_t)0x00020000) /*!< Pin 17 selected */
#define GPIO_Pin_18  ((uint32_t)0x00040000) /*!< Pin 18 selected */
#define GPIO_Pin_19  ((uint32_t)0x00080000) /*!< Pin 19 selected */
#define GPIO_Pin_20  ((uint32_t)0x00100000) /*!< Pin 20 selected */
#define GPIO_Pin_21  ((uint32_t)0x00200000) /*!< Pin 21 selected */
#define GPIO_Pin_All ((uint32_t)0x003FFFFF) /*!< All pins selected */

#define GPIO0_0      GPIO_Pin_0
#define GPIO0_1      GPIO_Pin_1
#define GPIO0_2      GPIO_Pin_2
#define GPIO0_3      GPIO_Pin_3
#define GPIO0_4      GPIO_Pin_4
#define GPIO0_5      GPIO_Pin_5
#define GPIO0_6      GPIO_Pin_6
#define GPIO0_7      GPIO_Pin_7
#define GPIO1_0      GPIO_Pin_8
#define GPIO1_1      GPIO_Pin_9
#define GPIO1_2      GPIO_Pin_10
#define GPIO1_3      GPIO_Pin_11
#define GPIO1_4      GPIO_Pin_12
#define GPIO1_5      GPIO_Pin_13
#define GPIO1_6      GPIO_Pin_14
#define GPIO1_7      GPIO_Pin_15
#define GPIO2_0      GPIO_Pin_16
#define GPIO2_1      GPIO_Pin_17
#define GPIO2_2      GPIO_Pin_18
#define GPIO2_3      GPIO_Pin_19
#define GPIO2_4      GPIO_Pin_20
#define GPIO2_5      GPIO_Pin_21

#define IO_NUM 22

#define IS_GPIO_PIN(PIN) ((((PIN) & (uint32_t)0xFFC00000) == 0x00) && ((PIN) != (uint32_t)0x00))

#define IS_GET_GPIO_PIN(PIN) (((PIN) == GPIO_Pin_0) ||  \
                              ((PIN) == GPIO_Pin_1) ||  \
                              ((PIN) == GPIO_Pin_2) ||  \
                              ((PIN) == GPIO_Pin_3) ||  \
                              ((PIN) == GPIO_Pin_4) ||  \
                              ((PIN) == GPIO_Pin_5) ||  \
                              ((PIN) == GPIO_Pin_6) ||  \
                              ((PIN) == GPIO_Pin_7) ||  \
                              ((PIN) == GPIO_Pin_8) ||  \
                              ((PIN) == GPIO_Pin_9) ||  \
                              ((PIN) == GPIO_Pin_10) || \
                              ((PIN) == GPIO_Pin_11) || \
                              ((PIN) == GPIO_Pin_12) || \
                              ((PIN) == GPIO_Pin_13) || \
                              ((PIN) == GPIO_Pin_14) || \
                              ((PIN) == GPIO_Pin_15) || \
                              ((PIN) == GPIO_Pin_16) || \
                              ((PIN) == GPIO_Pin_17) || \
                              ((PIN) == GPIO_Pin_18) || \
                              ((PIN) == GPIO_Pin_19) || \
                              ((PIN) == GPIO_Pin_20) || \
                              ((PIN) == GPIO_Pin_21) || \
                              ((PIN) == GPIO_Pin_All))

/**
 * @}
 */

/**
 * @}
 */

void GPIO_Reset(void);
void GPIO_Init(GPIO_InitTypeDef* GPIO_InitStruct);
void GPIO_StructInit(GPIO_InitTypeDef* GPIO_InitStruct);
void GPIO_SetPinMux(uint32_t GPIO_Pin, IO_TypeDef IO_Function);
void GPIO_SetPinDir(uint32_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode);
void GPIO_WritePort(uint32_t PortVal);
uint32_t GPIO_ReadPort(void);
void GPIO_WritePin(uint32_t GPIO_Pin, BitAction BitVal);
uint8_t GPIO_ReadPin(uint32_t GPIO_Pin);
void GPIO_SetPin(uint32_t GPIO_Pin);
void GPIO_ClearPin(uint32_t GPIO_Pin);
void GPIO_TogglePin(uint32_t GPIO_Pin);
void GPIO_PinMask(uint32_t GPIO_Pin);
void GPIO_PinUnmask(uint32_t GPIO_Pin);
/******************interrupt config****************************************/
void GPIO_EdgeITEnable(uint32_t GPIO_Pin, EdgeAction GPIO_Irq);
void GPIO_LevelITEnable(uint32_t GPIO_Pin, LevelAction GPIO_Irq);
void GPIO_MaskIT(uint32_t GPIO_Pin);
void GPIO_UnmaskIT(uint32_t GPIO_Pin);
void GPIO_ClearITFlag(uint32_t GPIO_Pin);
uint8_t GPIO_GetOriginalITStatus(uint32_t GPIO_Pin);
uint8_t GPIO_GetMaskITStatus(uint32_t GPIO_Pin);
void GPIO_ConfigPull(uint32_t GPIO_Pin, GPIOPull_TypeDef NewPull);
void GPIO_ConfigDriver(uint32_t GPIO_Pin, GPIODrive_TypeDef NewDriver);
void GPIO_ConfigSlewRate(uint32_t GPIO_Pin, GPIOSLEWRATE_TypeDef NewSlewRate);
void GPIO_ConfigInput(uint32_t GPIO_Pin, GPIOInput_TypeDef GPIO_InputMode);
void GPIO_ConfigOutput(uint32_t GPIO_Pin, GPIOOutput_TypeDef GPIO_OutputMode);

#ifdef __cplusplus
}
#endif

#endif /* __DG32F003xx_GPIO_H */
/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2011 Alpscale *****END OF FILE****/
