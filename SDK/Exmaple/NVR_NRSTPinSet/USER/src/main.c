/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_adc.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_flash.h"
#include "DG32F003xx_flash_nvr.h"
#include "usart.h"	  
#include "delay.h"
//#include "DG32F003xx_utility_flash.h"

/* Private define ------------------------------------------------------------*/
#define  NRSTPULLUP_OPEN		1	
#define  NRSTPULLUP_CLOSE	  0
/* Private variables ---------------------------------------------------------*/
uint32_t NRSTIO;
uint32_t NRSTIOPULL;
//uint32_t BOOT1IO;
//uint32_t BOOT2IO;

/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{   
	delay_init(24);
	
	eFlashWrite_NRST(NRST_CONFIG_GP02,NRSTPULLUP_OPEN);
	eFlashRead_NRST(&NRSTIO,&NRSTIOPULL);
	
	while(1){
		
  }
}

