/**
 ******************************************************************************
 * @file    DG32F003xx.h
 * @author  MCD Application Team
 * @version V1.0.1
 * @date    20-April-2012
 * @brief   CMSIS Cortex-M0+ Device Peripheral Access Layer Header File.
 *          This file contains all the peripheral register's definitions, bits
 *          definitions and memory mapping for DG32F003xx devices.
 *
 *          The file is the unique include file that the application programmer
 *          is using in the C source code, usually in main.c. This file contains:
 *           - Configuration section that allows to select:
 *              - The device used in the target application
 *              - To use or not the peripheral’s drivers in application code(i.e.
 *                code will be based on direct access to peripheral’s registers
 *                rather than drivers API), this option is controlled by
 *                "#define USE_STDPERIPH_DRIVER"
 *              - To change few application-specific parameters such as the HSE
 *                crystal frequency
 *           - Data structures and the address mapping for all peripherals
 *           - Peripheral's registers declarations and bits definition
 *           - Macros to access peripheral’s registers hardware
 *
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2012 ALPSCALE</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/** @addtogroup CMSIS
 * @{
 */

/** @addtogroup alpscale
 * @{
 */

#ifndef __DG32F003xx_H
#define __DG32F003xx_H

#ifdef __cplusplus
extern "C" {
#endif

/** @addtogroup Library_configuration_section
 * @{
 */

#if !defined USE_STDPERIPH_DRIVER
/**
 * @brief Comment the line below if you will not use the peripherals drivers.
   In this case, these drivers will not be included and the application code will
   be based on direct access to peripherals registers
   */
/*#define USE_STDPERIPH_DRIVER*/
#endif /* USE_STDPERIPH_DRIVER */

/**
 * @brief DG32F003xx Standard Peripheral Library version number V1.0.1
 */
#define __DG32F003xx_STDPERIPH_VERSION_MAIN (0x01) /*!< [31:24] main version */
#define __DG32F003xx_STDPERIPH_VERSION_SUB1 (0x00) /*!< [23:16] sub1 version */
#define __DG32F003xx_STDPERIPH_VERSION_SUB2 (0x00) /*!< [15:8]  sub2 version */
#define __DG32F003xx_STDPERIPH_VERSION_RC   (0x00) /*!< [7:0]  release candidate */
#define __DG32F003xx_STDPERIPH_VERSION      ((__DG32F003xx_STDPERIPH_VERSION_MAIN << 24) | (__DG32F003xx_STDPERIPH_VERSION_SUB1 << 16) | (__DG32F003xx_STDPERIPH_VERSION_SUB2 << 8) | (__DG32F003xx_STDPERIPH_VERSION_RC))


/**
 * @brief DG32F003xx Interrupt Number Definition, according to the selected device
 *        in @ref Library_configuration_section
 */
#define __CM0_REV              0 /*!< Core Revision r0p0                            */
#define __MPU_PRESENT          0 /*!< ALPSCALE do not provide MPU                  */
#define __NVIC_PRIO_BITS       2 /*!< ALPSCALE uses 2 Bits for the Priority Levels */
#define __Vendor_SysTickConfig 0 /*!< Set to 1 if different SysTick Config is used  */

/*!< Interrupt Number Definition */
typedef enum IRQn {
  /******  Cortex-M0+ Processor Exceptions Numbers ******************************************************/
  NonMaskableInt_IRQn = -14, /*!< 2 Non Maskable Interrupt                                */
  HardFault_IRQn      = -13, /*!< 3 Cortex-M0+ Hard Fault Interrupt                        */
  SVC_IRQn            = -5,  /*!< 11 Cortex-M0+ SV Call Interrupt                          */
  PendSV_IRQn         = -2,  /*!< 14 Cortex-M0+ Pend SV Interrupt                          */
  SysTick_IRQn        = -1,  /*!< 15 Cortex-M0+ System Tick Interrupt                      */

  /******  DG32F003xx specific Interrupt Numbers *********************************************************/
  GPIO_IRQn  = 1,  /*!< GPIO Interrupt                                        */
  I2C0_IRQn  = 2,  /*!< I2C0 Interrupt										   */
  UART0_IRQn = 3,  /*!< UART0 Interrupt										   */
  SPI_IRQn   = 4,  /*!< SPI Interrupt 								   */
  TIM1_IRQn  = 5,  /*!< TIM1 Interrupt                                         */
  TIM4_IRQn                 = 6,      /*!< TIM2 Interrupt                                         */
  TIM15_IRQn					= 7,	 /*!< TIM3 Interrupt										   */
  BOD_IRQn   = 8,  /*!< BOD  Interrupt										   */
  ADC0_IRQn  = 9,  /*!< ADC0 Interrupt										   */
  UART1_IRQn = 10, /*!< UART1 Interrupt                                           */
} IRQn_Type;


#include "core_cm0plus.h"
#include "system_DG32F003xx.h"
#include <stdint.h>

/** @addtogroup Exported_types
 * @{
 */

typedef enum { RESET = 0,
               SET   = !RESET } FlagStatus,
  ITStatus;

typedef enum { DISABLE = 0,
               ENABLE  = !DISABLE } FunctionalState;
#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))

typedef enum { ERROR   = 0,
               SUCCESS = !ERROR } ErrorStatus;

/** @addtogroup Peripheral_registers_structures
 * @{
 */

/**
 * @brief UART
 */

typedef struct
{
  __IO uint32_t CTRL0;
  __IO uint32_t CTRL0_SET;
  __IO uint32_t CTRL0_CLR;
  __IO uint32_t CTRL0_TOG;
 uint32_t RESERVED3[4];
  __IO uint32_t CTRL2;
  __IO uint32_t CTRL2_SET;
  __IO uint32_t CTRL2_CLR;
  __IO uint32_t CTRL2_TOG;
  __IO uint32_t LINECTRL;
  __IO uint32_t LINECTRL_SET;
  __IO uint32_t LINECTRL_CLR;
  __IO uint32_t LINECTRL_TOG;
  __IO uint32_t INTR;
  __IO uint32_t INTR_SET;
  __IO uint32_t INTR_CLR;
  __IO uint32_t INTR_TOG;
  __IO uint32_t DATA;
  uint32_t RESERVED0[3];
  __IO uint32_t STAT; // 0x60
  uint32_t RESERVED1[7];
  __IO uint8_t ILPR;//0x80
  uint32_t RESERVED2[15];
  __IO uint16_t AUTOBAUD; // 0xc0
  uint16_t RESERVED20;
  __IO uint16_t AUTOBAUD_SET;
  uint16_t RESERVED21;
  __IO uint16_t AUTOBAUD_CLR;
  uint16_t RESERVED22;
  __IO uint16_t AUTOBAUD_TOG;
  uint16_t RESERVED23;
  __IO uint32_t CTRL3;     // 0x0d0
  __IO uint32_t CTRL3_SET; // 0x0d4
  __IO uint32_t CTRL3_CLR; // 0x0d8
  __IO uint32_t CTRL3_TOG; // 0x0dc
  uint32_t RESERVED24[20];
  __IO uint32_t STAT2; // 0x130
} UART_TypeDef;

typedef struct
{
  __IO uint32_t CON;                // 0X00
  __IO uint32_t TAR;                // 0X04
  __IO uint32_t SAR;                // 0X08
  __IO uint32_t HS_MADDR;           // 0X0C
  __IO uint32_t DATA_CMD;           // 0X10
  __IO uint32_t SS_SCL_HCNT;        // 0X14
  __IO uint32_t SS_SCL_LCNT;        // 0X18
  __IO uint32_t FS_SCL_HCNT;        // 0X1C
  __IO uint32_t FS_SCL_LCNT;        // 0X20
  __IO uint32_t HS_SCL_HCNT;        // 0X24
  __IO uint32_t HS_SCL_LCNT;        // 0X28
  __IO uint32_t INTR_STAT;          // 0X2C
  __IO uint32_t INTR_MASK;          // 0X30
  __IO uint32_t RAW_INTR_STAT;      // 0X34
  __IO uint32_t RX_TL;              // 0X38
  __IO uint32_t TX_TL;              // 0X3C
  __IO uint32_t CLR_INTR;           // 0X40
  __IO uint32_t CLR_RX_UNDER;       // 0X44
  __IO uint32_t CLR_RX_OVER;        // 0X48
  __IO uint32_t CLR_TX_OVER;        // 0X4C
  __IO uint32_t CLR_RD_REQ;         // 0X50
  __IO uint32_t CLR_TX_ABRT;        // 0X54
  __IO uint32_t CLR_RX_DONE;        // 0X58
  __IO uint32_t CLR_ACTIVITY;       // 0X5C
  __IO uint32_t CLR_STOP_DET;       // 0X60
  __IO uint32_t CLR_START_DET;      // 0X64
  __IO uint32_t CLR_GEN_CALL;       // 0X68
  __IO uint32_t ENABLE;             // 0X6C
  __IO uint32_t STAT;               // 0X70
  __IO uint32_t TXFLR;              // 0X74
  __IO uint32_t RXFLR;              // 0X78
  uint32_t RESERVED1;                // 0X7C
  __IO uint32_t TX_ABRT_SOURCE;     // 0X80
  __IO uint32_t SLV_DATA_NACK_ONLY; // 0X84
  uint32_t RESERVED2;             // 0X88
  uint32_t RESERVED3;           // 0X8C
  uint32_t RESERVED4;           // 0X90
  __IO uint32_t SDA_HOLD;           // 0X94
  __IO uint32_t ACK_GENERAL_CALL;   // 0X98
  __IO uint32_t ENABLE_STATUS;      // 0X9C
} I2C_TypeDef;

/**
 * @brief Independent WATCHDOG
 */
typedef struct
{
  __IO uint32_t WDMOD;
  __IO uint32_t WDTC;
  __IO uint32_t WDFEED;
  __IO uint32_t WDTV;
} WDG_TypeDef;

/**
 * @brief Reset and Clock Control
 */
typedef struct
{
  __IO uint32_t PRESETCTRL0; // 0x00
  __IO uint32_t PRESETCTRL0_SET;
  __IO uint32_t PRESETCTRL0_CLR;
  __IO uint32_t PRESETCTRL0_TOG;
  __IO uint32_t AHBCLKCTRL0; // 0x10
  __IO uint32_t AHBCLKCTRL0_SET;
  __IO uint32_t AHBCLKCTRL0_CLR;
  __IO uint32_t AHBCLKCTRL0_TOG;
  __IO uint32_t ADDRESS_REMAP; // 0x20
  __IO uint32_t IRC48M_CTRL;
  __IO uint32_t IRC10K_CTRL;
  uint32_t RESERVED0;       // add 0x2C
  __IO uint32_t LDO_CTRL; // 0x30
  __IO uint32_t SYSRSTSTAT;
  __IO uint32_t EFLASHSTAT;
  uint32_t RESERVED1;       // add 0x3C
  __IO uint32_t MAINCLKSEL; // 0x40
  __IO uint32_t MAINCLKUEN;
  __IO uint32_t OUTCLKSEL;
  __IO uint32_t OUTCLKUEN;
  __IO uint32_t SYSAHBCLKDIV; // 0x50
  __IO uint32_t SYSTICKCLKDIV;
  __IO uint32_t UART0CLKDIV;
  __IO uint32_t UART1CLKDIV;
  __IO uint32_t SPI0CLKDIV; // 0x60
  __IO uint32_t WDTCLKDIV;
  __IO uint32_t OUTCLKDIV;
  __IO uint32_t PWMCLKDIV;
  __IO uint32_t SYSTICKCAL; // 0x70
  __IO uint32_t BODCTRL;
  __IO uint32_t NRSTCTRL;
	uint32_t RESERVED2;
  uint32_t RESERVED3[4];    // add 0x80~0x8C
  __IO uint32_t PDSLEEPCFG; // 0x90
  __IO uint32_t PDAWAKECFG;
  __IO uint32_t PDRUNCFG;
  uint32_t RESERVED4;     // add 0x9C
  __IO uint32_t DEVICEID; // 0xA0
  uint32_t RESERVED5; 		 // 0xA4
  __IO uint32_t PCON;
  __IO uint32_t ADCREF_ADJ;    //0xAC
  uint32_t RESERVED6;     //0xB0
  __IO uint32_t EFLASH_SPACE;  //0xB4
  uint32_t RESERVED7[2];   // 
  __IO uint32_t PRNG_CTRL; // 0xC0
  __IO uint32_t PRNG_DATA;
} RCC_TypeDef;

/**
 * @brief General Purpose I/O
 */

typedef struct
{
  __IO uint32_t DT;
  __IO uint32_t DT_SET;
  __IO uint32_t DT_CLR;
  __IO uint32_t DT_TOG;
} GPIO_TypeDef;

typedef struct
{
  __IO uint32_t DIR;
  __IO uint32_t DIR_SET;
  __IO uint32_t DIR_CLR;
  __IO uint32_t DIR_TOG;
  __IO uint32_t IS;
  __IO uint32_t IS_SET;
  __IO uint32_t IS_CLR;
  __IO uint32_t IS_TOG;
  __IO uint32_t IBE;
  __IO uint32_t IBE_SET;
  __IO uint32_t IBE_CLR;
  __IO uint32_t IBE_TOG;
  __IO uint32_t IEV;
  __IO uint32_t IEV_SET;
  __IO uint32_t IEV_CLR;
  __IO uint32_t IEV_TOG;
  __IO uint32_t IE;
  __IO uint32_t IE_SET;
  __IO uint32_t IE_CLR;
  __IO uint32_t IE_TOG;
  __IO uint32_t RIS;
  uint32_t RESERVED0[3];
  __IO uint32_t MIS;
  uint32_t RESERVED1[3];
  __IO uint32_t IC;
  __IO uint32_t IC_SET;
  __IO uint32_t IC_CLR;
  __IO uint32_t IC_TOG;
  __IO uint32_t DATAMASK;
  __IO uint32_t DATAMASK_SET;
  __IO uint32_t DATAMASK_CLR;
  __IO uint32_t DATAMASK_TOG;
} GPIO_IT_TypeDef;

typedef struct
{
  __IO uint32_t CON;
} IOCON_TypeDef;

/**
 * @brief IOCON Interface
 */
typedef struct
{
  __IO uint32_t PIN0;
  __IO uint32_t PIN1;
  __IO uint32_t PIN2;
  __IO uint32_t PIN3;
  __IO uint32_t PIN4;
  __IO uint32_t PIN5;
  __IO uint32_t PIN6;
  __IO uint32_t PIN7;
  __IO uint32_t PIN8;
  __IO uint32_t PIN9;
  __IO uint32_t PIN10;
  __IO uint32_t PIN11;
  __IO uint32_t PIN12;
  __IO uint32_t PIN13;
  __IO uint32_t PIN14;
  __IO uint32_t PIN15;
  __IO uint32_t PIN16;
  __IO uint32_t PIN17;
  __IO uint32_t PIN18;
  __IO uint32_t PIN19;
  __IO uint32_t PIN20;
  __IO uint32_t PIN21;
} PIO_TypeDef;

/**
 * @brief TIM Interface
 */
typedef struct
{
  __IO uint32_t CR1;    
  //  uint16_t  RESERVED0;
  __IO uint16_t CR2;
  uint16_t RESERVED1;
  __IO uint16_t SMCR;
  uint16_t RESERVED2;
  __IO uint16_t DIER;
  uint16_t RESERVED3;
  __IO uint16_t SR;
  uint16_t RESERVED4;
  __IO uint16_t EGR;
  uint16_t RESERVED5;
  __IO uint16_t CCMR1;
  uint16_t RESERVED6;
  __IO uint16_t CCMR2;
  uint16_t RESERVED7;
  __IO uint16_t CCER;
  uint16_t RESERVED8;
  __IO uint16_t CNT;
  uint16_t RESERVED9;
  __IO uint16_t PSC;
  uint16_t RESERVED10;
  __IO uint16_t ARR;
  uint16_t RESERVED11;
  __IO uint16_t RCR;
  uint16_t RESERVED12;
  __IO uint16_t CCR1;
  uint16_t RESERVED13;
  __IO uint16_t CCR2;
  uint16_t RESERVED14;
  __IO uint16_t CCR3;
  uint16_t RESERVED15;
  __IO uint16_t CCR4;
  uint16_t RESERVED16;
  __IO uint16_t BDTR;
  uint16_t RESERVED17;
  uint16_t RESERVED18;
  uint16_t RESERVED19;
  uint16_t RESERVED20;
  uint16_t RESERVED21;
  __IO uint32_t CP;
} TIM_TypeDef;

typedef struct
{
  __IO uint32_t CR;
  __IO uint32_t SR;
  __IO uint32_t ERASE_NUM;
  uint32_t RESERVED1;
  __IO uint32_t KEY_MAIN;
  uint32_t RESERVED2;
  uint32_t RESERVED3;
  uint32_t RESERVED4;
  __IO uint32_t ACR;
} FLASH_TypeDef;

typedef struct
{
  __IO uint32_t CTRL0;     // 0x0
  __IO uint32_t CTRL0_SET; // 0x4
  __IO uint32_t CTRL0_CLR; // 0x8
  __IO uint32_t CTRL0_TOG; // 0xc
  __IO uint32_t CTRL1;     // 0x10
  __IO uint32_t CTRL1_SET; // 0x14
  __IO uint32_t CTRL1_CLR; // 0x18
  __IO uint32_t CTRL1_TOG; // 0x1c
  __IO uint32_t TIMING;    // 0x20
  uint32_t RESERVED1[3];   //
  __IO uint32_t DATA;      // 0x30
  uint32_t RESERVED2[3];   //
  __IO uint32_t STATUS;    // 0x40
  uint32_t RESERVED3[3];   //
  __IO uint32_t DEBUG;     // 0x50
  uint32_t RESERVED4[3];   //
  __IO uint32_t XFER;      // 0x60
  uint32_t RESERVED5[27];    //
  __IO uint32_t BSY_DLY;   // 0xD0
} SPI_TypeDef;

typedef struct
{
  __IO uint32_t CTRL0;     //0x0
  __IO uint32_t CTRL0_SET; //0x4
  __IO uint32_t CTRL0_CLR; //0x8
  __IO uint32_t CTRL0_TOG; //0xc
  __IO uint32_t CTRL1;     //0x10
  __IO uint32_t CTRL1_SET; //0x14
  __IO uint32_t CTRL1_CLR; //0x18
  __IO uint32_t CTRL1_TOG; //0x1c
  __IO uint32_t CTRL2;     //0x20
  __IO uint32_t CTRL2_SET; //0x24
  __IO uint32_t CTRL2_CLR; //0x28
  __IO uint32_t CTRL2_TOG; //0x2c
  __IO uint32_t CTRL3;     //0x30
  __IO uint32_t CTRL3_SET; //0x34
  __IO uint32_t CTRL3_CLR; //0x38
  __IO uint32_t CTRL3_TOG; //0x3c
  __IO uint32_t CTRL4;     //0x40
  __IO uint32_t CTRL4_SET; //0x44
  __IO uint32_t CTRL4_CLR; //0x48
  __IO uint32_t CTRL4_TOG; //0x4c
  __IO uint32_t CTRL5;     //0x50
  __IO uint32_t CTRL5_SET; //0x54
  __IO uint32_t CTRL5_CLR; //0x58
  __IO uint32_t CTRL5_TOG; //0x5c
  __IO uint32_t STATUS;    //0x60
  uint32_t RESERVED6[3];
  __IO uint32_t DEBUG;     //0x70
  uint32_t RESERVED7[3]; //

  uint32_t RESERVED8[4];   //0x80
  uint32_t RESERVED9[28];  //0x90

  __IO uint32_t CH0;       //0x100
  uint32_t RESERVED10[3];
  __IO uint32_t CH1;       //0x110
  uint32_t RESERVED11[3]; 
  __IO uint32_t CH2;       //0x120
  uint32_t RESERVED12[3]; 
  __IO uint32_t CH3;       //0x130
  uint32_t RESERVED13[3];  
  __IO uint32_t CH4;       //0x140
  uint32_t RESERVED14[3];   
  __IO uint32_t CH5;       //0x150
  uint32_t RESERVED15[3];  
  __IO uint32_t CH6;       //0x160
  uint32_t RESERVED16[3]; 
  __IO uint32_t CH7;       //0x170
  uint32_t RESERVED17[3];  
  __IO uint32_t CH8;       //0x180
  uint32_t RESERVED18[3];  
  __IO uint32_t CH9;       //0x190
  uint32_t RESERVED19[3];  
  __IO uint32_t CH10;      //0x1a0
  uint32_t RESERVED20[3];   
  __IO uint32_t CH11;      //0x1b0
  uint32_t RESERVED21[3]; 
  __IO uint32_t CH12;      //0x1c0
  uint32_t RESERVED22[3];  
  __IO uint32_t CH13;      //0x1d0
  uint32_t RESERVED23[3];  
  __IO uint32_t CH14;      //0x1e0
  uint32_t RESERVED24[3];   
  __IO uint32_t CH15;      //0x1f0
  uint32_t RESERVED25[3];  
  __IO uint32_t CH16;      //0x200
  uint32_t RESERVED26[3];   
  __IO uint32_t CH17;      //0x210
  uint32_t RESERVED27[3];  
  __IO uint32_t CH18;      //0x220
  uint32_t RESERVED28[3]; 
  __IO uint32_t CH19;      //0x230
  uint32_t RESERVED29[3]; 
  __IO uint32_t CH20;      //0x240
  uint32_t RESERVED30[3]; 
  __IO uint32_t CH21;      //0x250
  uint32_t RESERVED31[3]; 
  __IO uint32_t CH22;      //0x260
  uint32_t RESERVED32[3];  
} ADC_TypeDef;

/**
 * @}
 */
/** @addtogroup Peripheral_memory_map
 * @{
 */
/*!< Peripheral memory map */
#define PERIPH_BASE ((uint32_t)0x40000000) /*!< Peripheral base address in the alias region */

#define APB0PERIPH_BASE (PERIPH_BASE)
#define RCC_BASE        (APB0PERIPH_BASE)
#define IOCON_PIO0_BASE (APB0PERIPH_BASE + 0x1000)
#define UART0_BASE      (APB0PERIPH_BASE + 0x3000)
#define UART1_BASE      (APB0PERIPH_BASE + 0xF000)
#define I2C0_BASE       (APB0PERIPH_BASE + 0x4000)
#define TIM1_BASE       (APB0PERIPH_BASE + 0xB800)
#define TIM4_BASE                           (APB0PERIPH_BASE + 0xC000)
#define TIM15_BASE                           (APB0PERIPH_BASE + 0xC800)
#define EFLASH_BASE     (APB0PERIPH_BASE + 0xD000)
#define WDG_BASE        (APB0PERIPH_BASE + 0xE000)
#define ADC_BASE        (APB0PERIPH_BASE + 0x8000)
#define GPIO_BASE       (PERIPH_BASE + 0x20000)
#define GPIO0_BASE      (GPIO_BASE)
#define GPIO0_IT_BASE   (GPIO_BASE + 0x8000)
#define SPI_BASE        (APB0PERIPH_BASE + 0x6000)

/**
 * @}
 */

/** @addtogroup Peripheral_declaration
 * @{
  */ 
#define RCC                                 ((RCC_TypeDef *) RCC_BASE)
#define PIO0                                ((PIO_TypeDef *) IOCON_PIO0_BASE)
#define UART0                               ((UART_TypeDef *) UART0_BASE)
#define UART1                               ((UART_TypeDef *) UART1_BASE)
#define I2C0                                ((I2C_TypeDef *) I2C0_BASE)
#define TIM1                               ((TIM_TypeDef *) TIM1_BASE)
#define TIM4                               ((TIM_TypeDef *) TIM4_BASE)
#define TIM15                               ((TIM_TypeDef *) TIM15_BASE)
#define WDG                                 ((WDG_TypeDef *) WDG_BASE)
#define GPIO0                               ((GPIO_TypeDef *) GPIO0_BASE)
#define GPIO0_IT                            ((GPIO_IT_TypeDef *) GPIO0_IT_BASE)
#define FLASH                               ((FLASH_TypeDef *) EFLASH_BASE)
#define SPI0     ((SPI_TypeDef *)SPI_BASE)
#define ADC0     ((ADC_TypeDef *)ADC_BASE)
/**
 * @}
 */

/**
 * @}
 */

#ifdef USE_STDPERIPH_DRIVER
#include "DG32F003xx_conf.h"
#endif

/******************************************************************************/
/*                                                                            */
/*         Universal Asynchronous Receiver Transmitter                    */
/*                                                                            */
/******************************************************************************/

/*******************  Bit definition for HW_UARTAPP_CTRL0 register  *******************/
#define UART_CTRL0_XFER_COUNT  ((uint32_t)0x0000FFFF) /*!<Number of bytes to receive. */
#define UART_CTRL0_RXTIMEOUT   ((uint32_t)0x00FF0000) /*!<Receive Timeout Counter Value: */
#define UART_CTRL0_RXTO_ENABLE ((uint32_t)0x01000000) /*!<RXTIMEOUT Enable */
#define UART_CTRL0_RX_SOURCE   ((uint32_t)0x02000000) /*!<Source of Receive Data */
#define UART_CTRL0_RUN         ((uint32_t)0x10000000) /*!<Tell the UART to execute the RX DMA Command. */
#define UART_CTRL0_CLKGATE     ((uint32_t)0x40000000) /*!<Gates of clock */
#define UART_CTRL0_SFTRST      ((uint32_t)0x80000000) /*!<Set to zero for normal operation,set to one to reset entire block */

/*******************  Bit definition for HW_UARTAPP_CTRL1 register  *******************/
#define UART_CTRL1_XFER_COUNT ((uint32_t)0x0000FFFF) /*!<Number of bytes to transmit. */
#define UART_CTRL1_RUN        ((uint32_t)0x10000000) /*!<Tell the UART to execute the TX DMA Command. */

/******************  Bit definition for HW_UARTAPP_CTRL2 register  *******************/
#define UART_CTRL2_UARTEN     ((uint32_t)0x00000001) /*!<UART Enable. */
#define UART_CTRL2_SIREN      ((uint32_t)0x00000002) /*!<SIR Enable. */
#define UART_CTRL2_SIRLP      ((uint32_t)0x00000004) /*!<IrDA SIR Low Power Mode. */
#define UART_CTRL2_LBE        ((uint32_t)0x00000080) /*!<Loop Back Enable. */
#define UART_CTRL2_TXE        ((uint32_t)0x00000100) /*!<Transmit Enable. */
#define UART_CTRL2_RXE        ((uint32_t)0x00000200) /*!<Receive Enable. */
#define UART_CTRL2_DTR        ((uint32_t)0x00000400) /*!<Data Transmit Ready.*/
#define UART_CTRL2_RTS        ((uint32_t)0x00000800) /*!<Request To Send. */
#define UART_CTRL2_OUT1       ((uint32_t)0x00001000) /*!<This bit is the complement of the UART Out1 (nUARTOut1) modem status output. */
#define UART_CTRL2_OUT2       ((uint32_t)0x00002000) /*!<This bit is the complement of the UART Out2 (nUARTOut2) modem status output. */
#define UART_CTRL2_RTSEN      ((uint32_t)0x00004000) /*!<RTS Hardware Flow Control Enable. */
#define UART_CTRL2_CTSEN      ((uint32_t)0x00008000) /*!<CTS Hardware Flow Control Enable.*/
#define UART_CTRL2_TXIFLSEL   ((uint32_t)0x000F0000) /*!<Transmit Interrupt FIFO Level Select. */
#define UART_CTRL2_TXIFLSEL_0 ((uint32_t)0x00010000) /*!<Transmit Interrupt FIFO Level Select. */
#define UART_CTRL2_TXIFLSEL_1 ((uint32_t)0x00020000) /*!<Transmit Interrupt FIFO Level Select. */
#define UART_CTRL2_TXIFLSEL_2 ((uint32_t)0x00040000) /*!<Transmit Interrupt FIFO Level Select. */
#define UART_CTRL2_RXIFLSEL   ((uint32_t)0x00F00000) /*!<Receive Interrupt FIFO Level Select. */
#define UART_CTRL2_RXIFLSEL_0 ((uint32_t)0x00100000) /*!<Receive Interrupt FIFO Level Select. */
#define UART_CTRL2_RXIFLSEL_1 ((uint32_t)0x00200000) /*!<Receive Interrupt FIFO Level Select. */
#define UART_CTRL2_RXIFLSEL_2 ((uint32_t)0x00400000) /*!<Receive Interrupt FIFO Level Select. */
#define UART_CTRL2_RXDMAE     ((uint32_t)0x01000000) /*!<Receive DMA Enable.*/
#define UART_CTRL2_TXDMAE     ((uint32_t)0x02000000) /*!<Transmit DMA Enable. */
#define UART_CTRL2_DMAONERROR ((uint32_t)0x04000000) /*!<DMA On Error. */

/******************  Bit definition for HW_UARTAPP_LINECTRL register  *******************/
#define UART_LINECTRL_BRK         ((uint32_t)0x00000001) /*!<Send Break */
#define UART_LINECTRL_PEN         ((uint32_t)0x00000002) /*!<Parity Enable. */
#define UART_LINECTRL_EPS         ((uint32_t)0x00000004) /*!<Even Parity Select. */
#define UART_LINECTRL_STP2        ((uint32_t)0x00000008) /*!<Even Parity Select. */
#define UART_LINECTRL_FEN         ((uint32_t)0x00000010) /*!<Enable FIFOs. */
#define UART_LINECTRL_WLEN        ((uint32_t)0x00000060) /*!<Word length [1:0]. */
#define UART_LINECTRL_WLEN_0      ((uint32_t)0x00000020) /*!<Word length [1:0]. */
#define UART_LINECTRL_WLEN_1      ((uint32_t)0x00000040) /*!<Word length [1:0]. */
#define UART_LINECTRL_SPS         ((uint32_t)0x00000080) /*!<Stick Parity Select. */
#define UART_LINECTRL_BAUD_DIVFRA ((uint32_t)0x00003F00) /*!<Baud Rate Fraction [5:0]. */
#define UART_LINECTRL_BAUD_DIVINT ((uint32_t)0xFFFF0000) /*!<Baud Rate Integer [15:0]. */

/******************  Bit definition for HW_UARTAPP_INTR register  *******************/
#define UART_INTR_RIMIS   ((uint32_t)0x00000001) /*!<nUARTRI Modem Interrupt Status. */
#define UART_INTR_CTSMIS  ((uint32_t)0x00000002) /*!<nUARTCTS Modem Interrupt Status. */
#define UART_INTR_DCDMIS  ((uint32_t)0x00000004) /*!<nUARTDCD Modem Interrupt Status. */
#define UART_INTR_DSRMIS  ((uint32_t)0x00000008) /*!<nUARTDSR Modem Interrupt Status. */
#define UART_INTR_RXIS    ((uint32_t)0x00000010) /*!<nUARTDSR Modem Interrupt Status. */
#define UART_INTR_TXIS    ((uint32_t)0x00000020) /*!<Transmit Interrupt Status. */
#define UART_INTR_RTIS    ((uint32_t)0x00000040) /*!<Receive Timeout Interrupt Status. */
#define UART_INTR_FEIS    ((uint32_t)0x00000080) /*!<Framing Error Interrupt Status. */
#define UART_INTR_PEIS    ((uint32_t)0x00000100) /*!<Parity Error Interrupt Status. */
#define UART_INTR_BEIS    ((uint32_t)0x00000200) /*!<Break Error Interrupt Status. */
#define UART_INTR_OEIS    ((uint32_t)0x00000400) /*!<Break Error Interrupt Status. */
#define UART_INTR_TFEIS   ((uint32_t)0x00000800) /*!<Tx FIFO EMPTY Raw Interrupt status*/
#define UART_INTR_ABEO    ((uint32_t)0x00001000) /*!<Auto Buadrate End Interrupt Status. */
#define UART_INTR_ABTO    ((uint32_t)0x00002000) /*!<Auto Buadrate TimeOut Interrupt Status. */
#define UART_INTR_RIMIEN  ((uint32_t)0x00010000) /*!<nUARTRI Modem Interrupt Enable. */
#define UART_INTR_CTSMIEN ((uint32_t)0x00020000) /*!<nUARTCTS Modem Interrupt Enable.*/
#define UART_INTR_DCDMIEN ((uint32_t)0x00040000) /*!<nUARTDCD Modem Interrupt Enable. */
#define UART_INTR_DSRMIEN ((uint32_t)0x00080000) /*!<nUARTDSR Modem Interrupt Enable. */
#define UART_INTR_RXIEN   ((uint32_t)0x00100000) /*!<Receive Interrupt Enable. */
#define UART_INTR_TXIEN   ((uint32_t)0x00200000) /*!<Transmit Interrupt Enable. */
#define UART_INTR_RTIEN   ((uint32_t)0x00400000) /*!<Receive Timeout Interrupt Enable. */
#define UART_INTR_FEIEN   ((uint32_t)0x00800000) /*!<Framing Error Interrupt Enable.*/
#define UART_INTR_PEIEN   ((uint32_t)0x01000000) /*!<Parity Error Interrupt Enable.*/
#define UART_INTR_BEIEN   ((uint32_t)0x02000000) /*!<Break Error Interrupt Enable. */
#define UART_INTR_OEIEN   ((uint32_t)0x04000000) /*!<Overrun Error Interrupt Enable. */
#define UART_INTR_TFEIEN  ((uint32_t)0x08000000) /*!<Overrun Error Interrupt Enable.*/
#define UART_INTR_ABEOEN  ((uint32_t)0x10000000) /*!<Auto Buadrate End Interrupt Enable.*/
#define UART_INTR_ABTOEN  ((uint32_t)0x20000000) /*!<Auto Buadrate TimeOut Interrupt Enable.*/

/******************  Bit definition for HW_UARTAPP_DATA register  *******************/
#define UART_DATA ((uint32_t)0xFFFFFFFF) /*!<The status register contains the receive data flags and valid bits. */

/******************  Bit definition for HW_UARTAPP_STAT register  ******************/
#define UART_STAT_RXCOUNT        ((uint32_t)0x0000FFFF) /*!<Number of bytes received during a Receive DMA command. */
#define UART_STAT_FERR           ((uint32_t)0x00010000) /*!<Framing Error. */
#define UART_STAT_PERR           ((uint32_t)0x00020000) /*!<Parity Error. */
#define UART_STAT_BERR           ((uint32_t)0x00040000) /*!<Break Error. */
#define UART_STAT_OERR           ((uint32_t)0x00080000) /*!<Overrun Error.*/
#define UART_STAT_RXBYTE_INVALID ((uint32_t)0x00F00000) /*!<The invalid state of the last read of Receive Data. */
#define UART_STAT_RXFE           ((uint32_t)0x01000000) /*!<Receive FIFO Empty. */
#define UART_STAT_TXFF           ((uint32_t)0x02000000) /*!<Receive FIFO Empty. */
#define UART_STAT_RXFF           ((uint32_t)0x04000000) /*!<Receive FIFO Full. */
#define UART_STAT_TXFE           ((uint32_t)0x08000000) /*!<Transmit FIFO Empty. */
#define UART_STAT_CTS            ((uint32_t)0x10000000) /*!<Clear To Send. */
#define UART_STAT_BUSY           ((uint32_t)0x20000000) /*!<UART Busy. */
#define UART_STAT_HISPEED        ((uint32_t)0x40000000) /*!<indicates that the high-speed function is present. */
#define UART_STAT_PRESENT        ((uint32_t)0x80000000) /*!<indicates that the Application UART function is present. */

/******************  Bit definition for HW_UARTAPP_DEBUG register  ******************/
#define UART_DEBUG_RXDMARQ  ((uint32_t)0x00000001) /*!<UART Receive DMA Request Status. */
#define UART_DEBUG_TXDMARQ  ((uint32_t)0x00000002) /*!<UART Transmit DMA Request Status. */
#define UART_DEBUG_RXCMDEND ((uint32_t)0x00000004) /*!<UART Receive DMA Command End Status. */
#define UART_DEBUG_TXCMDEND ((uint32_t)0x00000008) /*!<UART Transmit DMA Command End Status. */
#define UART_DEBUG_RXDMARUN ((uint32_t)0x00000010) /*!<UART Receive DMA Command Run Status. */
#define UART_DEBUG_TXDMARUN ((uint32_t)0x00000020) /*!<UART Transmit DMA Command Run Status. */

/******************  Bit definition for HW_UARTAPP_ILPR register  ******************/
#define UART_ILPDVSR ((uint32_t)0x000000FF) /*!<The baud rate divisor of the IRDA_LowPower */

/******************  Bit definition for HW_UARTAPP_RS485CTRL register  ******************/
#define UART_RS485CTRL_NMMEN ((uint32_t)0x00000001) /*!<Multidrop Mode. */
#define UART_RS485CTRL_RXDIS ((uint32_t)0x00000002) /*!<Enable receiver. */
#define UART_RS485CTRL_AADEN ((uint32_t)0x00000004) /*!<Auto Address Detect. */
#define UART_RS485CTRL_SEL   ((uint32_t)0x00000008) /*!<Direction control select. */
#define UART_RS485CTRL_DCTRL ((uint32_t)0x00000010) /*!<Auto Direction Control. */
#define UART_RS485CTRL_ONIV  ((uint32_t)0x00000020) /*!<reverses the polarity of the direction control signal. */

/******************  Bit definition for HW_UARTAPP_RS485ADRMATCH register  ******************/
#define UART_ADRMATCH ((uint32_t)0x000000FF) /*!<Contains the address match value. */

/******************  Bit definition for HW_UARTAPP_RS485DLY register  ******************/
#define UART_DLY ((uint32_t)0x000000FF) /*!<Contains the direction control (RTS or DTR) delay value. */

/******************  Bit definition for HW_UARTAPP_AUTOBAUD register  ******************/
#define UART_AUTOBAUD_START       ((uint32_t)0x00000001) /*!<Start auto baudrate. */
#define UART_AUTOBAUD_MODE        ((uint32_t)0x00000002) /*!<Auto-baud mode select. */
#define UART_AUTOBAUD_AUTORESTART ((uint32_t)0x00000004) /*!<Auto Restart. */
#define UART_AUTOBAUD_ABEOIntClr  ((uint32_t)0x00000100) /*!<End of auto-baud interrupt clear bit. */
#define UART_AUTOBAUD_ABTOIntClr  ((uint32_t)0x00000200) /*!<End of auto-baud interrupt clear bit. */

/******************************************************************************/
/*                                                                            */
/*                General Purpose and Alternate Function I/O                  */
/*                                                                            */
/******************************************************************************/

/*******************  Bit definition for GPIO_DATA register  *******************/
#define GPIO_DATA_BIT0  ((uint32_t)0x00000001) /*!< Pin data, bit 0 */
#define GPIO_DATA_BIT1  ((uint32_t)0x00000002) /*!< Pin data, bit 1 */
#define GPIO_DATA_BIT2  ((uint32_t)0x00000004) /*!< Pin data, bit 2 */
#define GPIO_DATA_BIT3  ((uint32_t)0x00000008) /*!< Pin data, bit 3 */
#define GPIO_DATA_BIT4  ((uint32_t)0x00000010) /*!< Pin data, bit 4 */
#define GPIO_DATA_BIT5  ((uint32_t)0x00000020) /*!< Pin data, bit 5 */
#define GPIO_DATA_BIT6  ((uint32_t)0x00000040) /*!< Pin data, bit 6 */
#define GPIO_DATA_BIT7  ((uint32_t)0x00000080) /*!< Pin data, bit 7 */
#define GPIO_DATA_BIT8  ((uint32_t)0x00000100) /*!< Pin data, bit 8 */
#define GPIO_DATA_BIT9  ((uint32_t)0x00000200) /*!< Pin data, bit 9 */
#define GPIO_DATA_BIT10 ((uint32_t)0x00000400) /*!< Pin data, bit 10 */
#define GPIO_DATA_BIT11 ((uint32_t)0x00000800) /*!< Pin data, bit 11 */
#define GPIO_DATA_BIT12 ((uint32_t)0x00001000) /*!< Pin data, bit 12 */
#define GPIO_DATA_BIT13 ((uint32_t)0x00002000) /*!< Pin data, bit 13 */
#define GPIO_DATA_BIT14 ((uint32_t)0x00004000) /*!< Pin data, bit 14 */
#define GPIO_DATA_BIT15 ((uint32_t)0x00008000) /*!< Pin data, bit 15 */
#define GPIO_DATA_BIT16 ((uint32_t)0x00010000) /*!< Pin data, bit 16 */
#define GPIO_DATA_BIT17 ((uint32_t)0x00020000) /*!< Pin data, bit 17 */
#define GPIO_DATA_BIT18 ((uint32_t)0x00040000) /*!< Pin data, bit 18 */
#define GPIO_DATA_BIT19 ((uint32_t)0x00080000) /*!< Pin data, bit 19 */
#define GPIO_DATA_BIT20 ((uint32_t)0x00100000) /*!< Pin data, bit 20 */
#define GPIO_DATA_BIT21 ((uint32_t)0x00200000) /*!< Pin data, bit 21 */
#define GPIO_DATA_BIT22 ((uint32_t)0x00400000) /*!< Pin data, bit 22 */
#define GPIO_DATA_BIT23 ((uint32_t)0x00800000) /*!< Pin data, bit 23 */
#define GPIO_DATA_BIT24 ((uint32_t)0x01000000) /*!< Pin data, bit 24 */
#define GPIO_DATA_BIT25 ((uint32_t)0x02000000) /*!< Pin data, bit 25 */
#define GPIO_DATA_BIT26 ((uint32_t)0x04000000) /*!< Pin data, bit 26 */
#define GPIO_DATA_BIT27 ((uint32_t)0x08000000) /*!< Pin data, bit 27 */
#define GPIO_DATA_BIT28 ((uint32_t)0x10000000) /*!< Pin data, bit 28 */
#define GPIO_DATA_BIT29 ((uint32_t)0x20000000) /*!< Pin data, bit 29 */
#define GPIO_DATA_BIT30 ((uint32_t)0x40000000) /*!< Pin data, bit 30 */
#define GPIO_DATA_BIT31 ((uint32_t)0x80000000) /*!< Pin data, bit 31 */

/*******************  Bit definition for GPIO_DIR register  *******************/
#define GPIO_DIR_BIT0  ((uint32_t)0x00000001) /*!< Pin dir, bit 0 */
#define GPIO_DIR_BIT1  ((uint32_t)0x00000002) /*!< Pin dir, bit 1 */
#define GPIO_DIR_BIT2  ((uint32_t)0x00000004) /*!< Pin dir, bit 2 */
#define GPIO_DIR_BIT3  ((uint32_t)0x00000008) /*!< Pin dir, bit 3 */
#define GPIO_DIR_BIT4  ((uint32_t)0x00000010) /*!< Pin dir, bit 4 */
#define GPIO_DIR_BIT5  ((uint32_t)0x00000020) /*!< Pin dir, bit 5 */
#define GPIO_DIR_BIT6  ((uint32_t)0x00000040) /*!< Pin dir, bit 6 */
#define GPIO_DIR_BIT7  ((uint32_t)0x00000080) /*!< Pin dir, bit 7 */
#define GPIO_DIR_BIT8  ((uint32_t)0x00000100) /*!< Pin dir, bit 8 */
#define GPIO_DIR_BIT9  ((uint32_t)0x00000200) /*!< Pin dir, bit 9 */
#define GPIO_DIR_BIT10 ((uint32_t)0x00000400) /*!< Pin dir, bit 10 */
#define GPIO_DIR_BIT11 ((uint32_t)0x00000800) /*!< Pin dir, bit 11 */
#define GPIO_DIR_BIT12 ((uint32_t)0x00001000) /*!< Pin dir, bit 12 */
#define GPIO_DIR_BIT13 ((uint32_t)0x00002000) /*!< Pin dir, bit 13 */
#define GPIO_DIR_BIT14 ((uint32_t)0x00004000) /*!< Pin dir, bit 14 */
#define GPIO_DIR_BIT15 ((uint32_t)0x00008000) /*!< Pin dir, bit 15 */
#define GPIO_DIR_BIT16 ((uint32_t)0x00010000) /*!< Pin dir, bit 16 */
#define GPIO_DIR_BIT17 ((uint32_t)0x00020000) /*!< Pin dir, bit 17 */
#define GPIO_DIR_BIT18 ((uint32_t)0x00040000) /*!< Pin dir, bit 18 */
#define GPIO_DIR_BIT19 ((uint32_t)0x00080000) /*!< Pin dir, bit 19 */
#define GPIO_DIR_BIT20 ((uint32_t)0x00100000) /*!< Pin dir, bit 20 */
#define GPIO_DIR_BIT21 ((uint32_t)0x00200000) /*!< Pin dir, bit 21 */
#define GPIO_DIR_BIT22 ((uint32_t)0x00400000) /*!< Pin dir, bit 22 */
#define GPIO_DIR_BIT23 ((uint32_t)0x00800000) /*!< Pin dir, bit 23 */
#define GPIO_DIR_BIT24 ((uint32_t)0x01000000) /*!< Pin dir, bit 24 */
#define GPIO_DIR_BIT25 ((uint32_t)0x02000000) /*!< Pin dir, bit 25 */
#define GPIO_DIR_BIT26 ((uint32_t)0x04000000) /*!< Pin dir, bit 26 */
#define GPIO_DIR_BIT27 ((uint32_t)0x08000000) /*!< Pin dir, bit 27 */
#define GPIO_DIR_BIT28 ((uint32_t)0x10000000) /*!< Pin dir, bit 28 */
#define GPIO_DIR_BIT29 ((uint32_t)0x20000000) /*!< Pin dir, bit 29 */
#define GPIO_DIR_BIT30 ((uint32_t)0x40000000) /*!< Pin dir, bit 30 */
#define GPIO_DIR_BIT31 ((uint32_t)0x80000000) /*!< Pin dir, bit 31 */

/*******************  Bit definition for GPIO_IS register  *******************/
#define GPIO_IS_BIT0  ((uint32_t)0x00000001) /*!< Pin is, bit 0 */
#define GPIO_IS_BIT1  ((uint32_t)0x00000002) /*!< Pin is, bit 1 */
#define GPIO_IS_BIT2  ((uint32_t)0x00000004) /*!< Pin is, bit 2 */
#define GPIO_IS_BIT3  ((uint32_t)0x00000008) /*!< Pin is, bit 3 */
#define GPIO_IS_BIT4  ((uint32_t)0x00000010) /*!< Pin is, bit 4 */
#define GPIO_IS_BIT5  ((uint32_t)0x00000020) /*!< Pin is, bit 5 */
#define GPIO_IS_BIT6  ((uint32_t)0x00000040) /*!< Pin is, bit 6 */
#define GPIO_IS_BIT7  ((uint32_t)0x00000080) /*!< Pin is, bit 7 */
#define GPIO_IS_BIT8  ((uint32_t)0x00000100) /*!< Pin is, bit 8 */
#define GPIO_IS_BIT9  ((uint32_t)0x00000200) /*!< Pin is, bit 9 */
#define GPIO_IS_BIT10 ((uint32_t)0x00000400) /*!< Pin is, bit 10 */
#define GPIO_IS_BIT11 ((uint32_t)0x00000800) /*!< Pin is, bit 11 */
#define GPIO_IS_BIT12 ((uint32_t)0x00001000) /*!< Pin is, bit 12 */
#define GPIO_IS_BIT13 ((uint32_t)0x00002000) /*!< Pin is, bit 13 */
#define GPIO_IS_BIT14 ((uint32_t)0x00004000) /*!< Pin is, bit 14 */
#define GPIO_IS_BIT15 ((uint32_t)0x00008000) /*!< Pin is, bit 15 */
#define GPIO_IS_BIT16 ((uint32_t)0x00010000) /*!< Pin is, bit 16 */
#define GPIO_IS_BIT17 ((uint32_t)0x00020000) /*!< Pin is, bit 17 */
#define GPIO_IS_BIT18 ((uint32_t)0x00040000) /*!< Pin is, bit 18 */
#define GPIO_IS_BIT19 ((uint32_t)0x00080000) /*!< Pin is, bit 19 */
#define GPIO_IS_BIT20 ((uint32_t)0x00100000) /*!< Pin is, bit 20 */
#define GPIO_IS_BIT21 ((uint32_t)0x00200000) /*!< Pin is, bit 21 */
#define GPIO_IS_BIT22 ((uint32_t)0x00400000) /*!< Pin is, bit 22 */
#define GPIO_IS_BIT23 ((uint32_t)0x00800000) /*!< Pin is, bit 23 */
#define GPIO_IS_BIT24 ((uint32_t)0x01000000) /*!< Pin is, bit 24 */
#define GPIO_IS_BIT25 ((uint32_t)0x02000000) /*!< Pin is, bit 25 */
#define GPIO_IS_BIT26 ((uint32_t)0x04000000) /*!< Pin is, bit 26 */
#define GPIO_IS_BIT27 ((uint32_t)0x08000000) /*!< Pin is, bit 27 */
#define GPIO_IS_BIT28 ((uint32_t)0x10000000) /*!< Pin is, bit 28 */
#define GPIO_IS_BIT29 ((uint32_t)0x20000000) /*!< Pin is, bit 29 */
#define GPIO_IS_BIT30 ((uint32_t)0x40000000) /*!< Pin is, bit 30 */
#define GPIO_IS_BIT31 ((uint32_t)0x80000000) /*!< Pin is, bit 31 */

/*******************  Bit definition for GPIO_IBE register  *******************/
#define GPIO_IBE_BIT0  ((uint32_t)0x00000001) /*!< Pin ibe, bit 0 */
#define GPIO_IBE_BIT1  ((uint32_t)0x00000002) /*!< Pin ibe, bit 1 */
#define GPIO_IBE_BIT2  ((uint32_t)0x00000004) /*!< Pin ibe, bit 2 */
#define GPIO_IBE_BIT3  ((uint32_t)0x00000008) /*!< Pin ibe, bit 3 */
#define GPIO_IBE_BIT4  ((uint32_t)0x00000010) /*!< Pin ibe, bit 4 */
#define GPIO_IBE_BIT5  ((uint32_t)0x00000020) /*!< Pin ibe, bit 5 */
#define GPIO_IBE_BIT6  ((uint32_t)0x00000040) /*!< Pin ibe, bit 6 */
#define GPIO_IBE_BIT7  ((uint32_t)0x00000080) /*!< Pin ibe, bit 7 */
#define GPIO_IBE_BIT8  ((uint32_t)0x00000100) /*!< Pin ibe, bit 8 */
#define GPIO_IBE_BIT9  ((uint32_t)0x00000200) /*!< Pin ibe, bit 9 */
#define GPIO_IBE_BIT10 ((uint32_t)0x00000400) /*!< Pin ibe, bit 10 */
#define GPIO_IBE_BIT11 ((uint32_t)0x00000800) /*!< Pin ibe, bit 11 */
#define GPIO_IBE_BIT12 ((uint32_t)0x00001000) /*!< Pin ibe, bit 12 */
#define GPIO_IBE_BIT13 ((uint32_t)0x00002000) /*!< Pin ibe, bit 13 */
#define GPIO_IBE_BIT14 ((uint32_t)0x00004000) /*!< Pin ibe, bit 14 */
#define GPIO_IBE_BIT15 ((uint32_t)0x00008000) /*!< Pin ibe, bit 15 */
#define GPIO_IBE_BIT16 ((uint32_t)0x00010000) /*!< Pin ibe, bit 16 */
#define GPIO_IBE_BIT17 ((uint32_t)0x00020000) /*!< Pin ibe, bit 17 */
#define GPIO_IBE_BIT18 ((uint32_t)0x00040000) /*!< Pin ibe, bit 18 */
#define GPIO_IBE_BIT19 ((uint32_t)0x00080000) /*!< Pin ibe, bit 19 */
#define GPIO_IBE_BIT20 ((uint32_t)0x00100000) /*!< Pin ibe, bit 20 */
#define GPIO_IBE_BIT21 ((uint32_t)0x00200000) /*!< Pin ibe, bit 21 */
#define GPIO_IBE_BIT22 ((uint32_t)0x00400000) /*!< Pin ibe, bit 22 */
#define GPIO_IBE_BIT23 ((uint32_t)0x00800000) /*!< Pin ibe, bit 23 */
#define GPIO_IBE_BIT24 ((uint32_t)0x01000000) /*!< Pin ibe, bit 24 */
#define GPIO_IBE_BIT25 ((uint32_t)0x02000000) /*!< Pin ibe, bit 25 */
#define GPIO_IBE_BIT26 ((uint32_t)0x04000000) /*!< Pin ibe, bit 26 */
#define GPIO_IBE_BIT27 ((uint32_t)0x08000000) /*!< Pin ibe, bit 27 */
#define GPIO_IBE_BIT28 ((uint32_t)0x10000000) /*!< Pin ibe, bit 28 */
#define GPIO_IBE_BIT29 ((uint32_t)0x20000000) /*!< Pin ibe, bit 29 */
#define GPIO_IBE_BIT30 ((uint32_t)0x40000000) /*!< Pin ibe, bit 30 */
#define GPIO_IBE_BIT31 ((uint32_t)0x80000000) /*!< Pin ibe, bit 31 */

/*******************  Bit definition for GPIO_IEV register  *******************/
#define GPIO_IEV_BIT0  ((uint32_t)0x00000001) /*!< Pin iev, bit 0 */
#define GPIO_IEV_BIT1  ((uint32_t)0x00000002) /*!< Pin iev, bit 1 */
#define GPIO_IEV_BIT2  ((uint32_t)0x00000004) /*!< Pin iev, bit 2 */
#define GPIO_IEV_BIT3  ((uint32_t)0x00000008) /*!< Pin iev, bit 3 */
#define GPIO_IEV_BIT4  ((uint32_t)0x00000010) /*!< Pin iev, bit 4 */
#define GPIO_IEV_BIT5  ((uint32_t)0x00000020) /*!< Pin iev, bit 5 */
#define GPIO_IEV_BIT6  ((uint32_t)0x00000040) /*!< Pin iev, bit 6 */
#define GPIO_IEV_BIT7  ((uint32_t)0x00000080) /*!< Pin iev, bit 7 */
#define GPIO_IEV_BIT8  ((uint32_t)0x00000100) /*!< Pin iev, bit 8 */
#define GPIO_IEV_BIT9  ((uint32_t)0x00000200) /*!< Pin iev, bit 9 */
#define GPIO_IEV_BIT10 ((uint32_t)0x00000400) /*!< Pin iev, bit 10 */
#define GPIO_IEV_BIT11 ((uint32_t)0x00000800) /*!< Pin iev, bit 11 */
#define GPIO_IEV_BIT12 ((uint32_t)0x00001000) /*!< Pin iev, bit 12 */
#define GPIO_IEV_BIT13 ((uint32_t)0x00002000) /*!< Pin iev, bit 13 */
#define GPIO_IEV_BIT14 ((uint32_t)0x00004000) /*!< Pin iev, bit 14 */
#define GPIO_IEV_BIT15 ((uint32_t)0x00008000) /*!< Pin iev, bit 15 */
#define GPIO_IEV_BIT16 ((uint32_t)0x00010000) /*!< Pin iev, bit 16 */
#define GPIO_IEV_BIT17 ((uint32_t)0x00020000) /*!< Pin iev, bit 17 */
#define GPIO_IEV_BIT18 ((uint32_t)0x00040000) /*!< Pin iev, bit 18 */
#define GPIO_IEV_BIT19 ((uint32_t)0x00080000) /*!< Pin iev, bit 19 */
#define GPIO_IEV_BIT20 ((uint32_t)0x00100000) /*!< Pin iev, bit 20 */
#define GPIO_IEV_BIT21 ((uint32_t)0x00200000) /*!< Pin iev, bit 21 */
#define GPIO_IEV_BIT22 ((uint32_t)0x00400000) /*!< Pin iev, bit 22 */
#define GPIO_IEV_BIT23 ((uint32_t)0x00800000) /*!< Pin iev, bit 23 */
#define GPIO_IEV_BIT24 ((uint32_t)0x01000000) /*!< Pin iev, bit 24 */
#define GPIO_IEV_BIT25 ((uint32_t)0x02000000) /*!< Pin iev, bit 25 */
#define GPIO_IEV_BIT26 ((uint32_t)0x04000000) /*!< Pin iev, bit 26 */
#define GPIO_IEV_BIT27 ((uint32_t)0x08000000) /*!< Pin iev, bit 27 */
#define GPIO_IEV_BIT28 ((uint32_t)0x10000000) /*!< Pin iev, bit 28 */
#define GPIO_IEV_BIT29 ((uint32_t)0x20000000) /*!< Pin iev, bit 29 */
#define GPIO_IEV_BIT30 ((uint32_t)0x40000000) /*!< Pin iev, bit 30 */
#define GPIO_IEV_BIT31 ((uint32_t)0x80000000) /*!< Pin iev, bit 31 */

/*******************  Bit definition for GPIO_IE register  *******************/
#define GPIO_IE_BIT0  ((uint32_t)0x00000001) /*!< Pin ie, bit 0 */
#define GPIO_IE_BIT1  ((uint32_t)0x00000002) /*!< Pin ie, bit 1 */
#define GPIO_IE_BIT2  ((uint32_t)0x00000004) /*!< Pin ie, bit 2 */
#define GPIO_IE_BIT3  ((uint32_t)0x00000008) /*!< Pin ie, bit 3 */
#define GPIO_IE_BIT4  ((uint32_t)0x00000010) /*!< Pin ie, bit 4 */
#define GPIO_IE_BIT5  ((uint32_t)0x00000020) /*!< Pin ie, bit 5 */
#define GPIO_IE_BIT6  ((uint32_t)0x00000040) /*!< Pin ie, bit 6 */
#define GPIO_IE_BIT7  ((uint32_t)0x00000080) /*!< Pin ie, bit 7 */
#define GPIO_IE_BIT8  ((uint32_t)0x00000100) /*!< Pin ie, bit 8 */
#define GPIO_IE_BIT9  ((uint32_t)0x00000200) /*!< Pin ie, bit 9 */
#define GPIO_IE_BIT10 ((uint32_t)0x00000400) /*!< Pin ie, bit 10 */
#define GPIO_IE_BIT11 ((uint32_t)0x00000800) /*!< Pin ie, bit 11 */
#define GPIO_IE_BIT12 ((uint32_t)0x00001000) /*!< Pin ie, bit 12 */
#define GPIO_IE_BIT13 ((uint32_t)0x00002000) /*!< Pin ie, bit 13 */
#define GPIO_IE_BIT14 ((uint32_t)0x00004000) /*!< Pin ie, bit 14 */
#define GPIO_IE_BIT15 ((uint32_t)0x00008000) /*!< Pin ie, bit 15 */
#define GPIO_IE_BIT16 ((uint32_t)0x00010000) /*!< Pin ie, bit 16 */
#define GPIO_IE_BIT17 ((uint32_t)0x00020000) /*!< Pin ie, bit 17 */
#define GPIO_IE_BIT18 ((uint32_t)0x00040000) /*!< Pin ie, bit 18 */
#define GPIO_IE_BIT19 ((uint32_t)0x00080000) /*!< Pin ie, bit 19 */
#define GPIO_IE_BIT20 ((uint32_t)0x00100000) /*!< Pin ie, bit 20 */
#define GPIO_IE_BIT21 ((uint32_t)0x00200000) /*!< Pin ie, bit 21 */
#define GPIO_IE_BIT22 ((uint32_t)0x00400000) /*!< Pin ie, bit 22 */
#define GPIO_IE_BIT23 ((uint32_t)0x00800000) /*!< Pin ie, bit 23 */
#define GPIO_IE_BIT24 ((uint32_t)0x01000000) /*!< Pin ie, bit 24 */
#define GPIO_IE_BIT25 ((uint32_t)0x02000000) /*!< Pin ie, bit 25 */
#define GPIO_IE_BIT26 ((uint32_t)0x04000000) /*!< Pin ie, bit 26 */
#define GPIO_IE_BIT27 ((uint32_t)0x08000000) /*!< Pin ie, bit 27 */
#define GPIO_IE_BIT28 ((uint32_t)0x10000000) /*!< Pin ie, bit 28 */
#define GPIO_IE_BIT29 ((uint32_t)0x20000000) /*!< Pin ie, bit 29 */
#define GPIO_IE_BIT30 ((uint32_t)0x40000000) /*!< Pin ie, bit 30 */
#define GPIO_IE_BIT31 ((uint32_t)0x80000000) /*!< Pin ie, bit 31 */

/*******************  Bit definition for GPIO_IRS register  *******************/
#define GPIO_IRS_BIT0  ((uint32_t)0x00000001) /*!< Pin irs, bit 0 */
#define GPIO_IRS_BIT1  ((uint32_t)0x00000002) /*!< Pin irs, bit 1 */
#define GPIO_IRS_BIT2  ((uint32_t)0x00000004) /*!< Pin irs, bit 2 */
#define GPIO_IRS_BIT3  ((uint32_t)0x00000008) /*!< Pin irs, bit 3 */
#define GPIO_IRS_BIT4  ((uint32_t)0x00000010) /*!< Pin irs, bit 4 */
#define GPIO_IRS_BIT5  ((uint32_t)0x00000020) /*!< Pin irs, bit 5 */
#define GPIO_IRS_BIT6  ((uint32_t)0x00000040) /*!< Pin irs, bit 6 */
#define GPIO_IRS_BIT7  ((uint32_t)0x00000080) /*!< Pin irs, bit 7 */
#define GPIO_IRS_BIT8  ((uint32_t)0x00000100) /*!< Pin irs, bit 8 */
#define GPIO_IRS_BIT9  ((uint32_t)0x00000200) /*!< Pin irs, bit 9 */
#define GPIO_IRS_BIT10 ((uint32_t)0x00000400) /*!< Pin irs, bit 10 */
#define GPIO_IRS_BIT11 ((uint32_t)0x00000800) /*!< Pin irs, bit 11 */
#define GPIO_IRS_BIT12 ((uint32_t)0x00001000) /*!< Pin irs, bit 12 */
#define GPIO_IRS_BIT13 ((uint32_t)0x00002000) /*!< Pin irs, bit 13 */
#define GPIO_IRS_BIT14 ((uint32_t)0x00004000) /*!< Pin irs, bit 14 */
#define GPIO_IRS_BIT15 ((uint32_t)0x00008000) /*!< Pin irs, bit 15 */
#define GPIO_IRS_BIT16 ((uint32_t)0x00010000) /*!< Pin irs, bit 16 */
#define GPIO_IRS_BIT17 ((uint32_t)0x00020000) /*!< Pin irs, bit 17 */
#define GPIO_IRS_BIT18 ((uint32_t)0x00040000) /*!< Pin irs, bit 18 */
#define GPIO_IRS_BIT19 ((uint32_t)0x00080000) /*!< Pin irs, bit 19 */
#define GPIO_IRS_BIT20 ((uint32_t)0x00100000) /*!< Pin irs, bit 20 */
#define GPIO_IRS_BIT21 ((uint32_t)0x00200000) /*!< Pin irs, bit 21 */
#define GPIO_IRS_BIT22 ((uint32_t)0x00400000) /*!< Pin irs, bit 22 */
#define GPIO_IRS_BIT23 ((uint32_t)0x00800000) /*!< Pin irs, bit 23 */
#define GPIO_IRS_BIT24 ((uint32_t)0x01000000) /*!< Pin irs, bit 24 */
#define GPIO_IRS_BIT25 ((uint32_t)0x02000000) /*!< Pin irs, bit 25 */
#define GPIO_IRS_BIT26 ((uint32_t)0x04000000) /*!< Pin irs, bit 26 */
#define GPIO_IRS_BIT27 ((uint32_t)0x08000000) /*!< Pin irs, bit 27 */
#define GPIO_IRS_BIT28 ((uint32_t)0x10000000) /*!< Pin irs, bit 28 */
#define GPIO_IRS_BIT29 ((uint32_t)0x20000000) /*!< Pin irs, bit 29 */
#define GPIO_IRS_BIT30 ((uint32_t)0x40000000) /*!< Pin irs, bit 30 */
#define GPIO_IRS_BIT31 ((uint32_t)0x80000000) /*!< Pin irs, bit 31 */

/*******************  Bit definition for GPIO_MIS register  *******************/
#define GPIO_MIS_BIT0  ((uint32_t)0x00000001) /*!< Pin mis, bit 0 */
#define GPIO_MIS_BIT1  ((uint32_t)0x00000002) /*!< Pin mis, bit 1 */
#define GPIO_MIS_BIT2  ((uint32_t)0x00000004) /*!< Pin mis, bit 2 */
#define GPIO_MIS_BIT3  ((uint32_t)0x00000008) /*!< Pin mis, bit 3 */
#define GPIO_MIS_BIT4  ((uint32_t)0x00000010) /*!< Pin mis, bit 4 */
#define GPIO_MIS_BIT5  ((uint32_t)0x00000020) /*!< Pin mis, bit 5 */
#define GPIO_MIS_BIT6  ((uint32_t)0x00000040) /*!< Pin mis, bit 6 */
#define GPIO_MIS_BIT7  ((uint32_t)0x00000080) /*!< Pin mis, bit 7 */
#define GPIO_MIS_BIT8  ((uint32_t)0x00000100) /*!< Pin mis, bit 8 */
#define GPIO_MIS_BIT9  ((uint32_t)0x00000200) /*!< Pin mis, bit 9 */
#define GPIO_MIS_BIT10 ((uint32_t)0x00000400) /*!< Pin mis, bit 10 */
#define GPIO_MIS_BIT11 ((uint32_t)0x00000800) /*!< Pin mis, bit 11 */
#define GPIO_MIS_BIT12 ((uint32_t)0x00001000) /*!< Pin mis, bit 12 */
#define GPIO_MIS_BIT13 ((uint32_t)0x00002000) /*!< Pin mis, bit 13 */
#define GPIO_MIS_BIT14 ((uint32_t)0x00004000) /*!< Pin mis, bit 14 */
#define GPIO_MIS_BIT15 ((uint32_t)0x00008000) /*!< Pin mis, bit 15 */
#define GPIO_MIS_BIT16 ((uint32_t)0x00010000) /*!< Pin mis, bit 16 */
#define GPIO_MIS_BIT17 ((uint32_t)0x00020000) /*!< Pin mis, bit 17 */
#define GPIO_MIS_BIT18 ((uint32_t)0x00040000) /*!< Pin mis, bit 18 */
#define GPIO_MIS_BIT19 ((uint32_t)0x00080000) /*!< Pin mis, bit 19 */
#define GPIO_MIS_BIT20 ((uint32_t)0x00100000) /*!< Pin mis, bit 20 */
#define GPIO_MIS_BIT21 ((uint32_t)0x00200000) /*!< Pin mis, bit 21 */
#define GPIO_MIS_BIT22 ((uint32_t)0x00400000) /*!< Pin mis, bit 22 */
#define GPIO_MIS_BIT23 ((uint32_t)0x00800000) /*!< Pin mis, bit 23 */
#define GPIO_MIS_BIT24 ((uint32_t)0x01000000) /*!< Pin mis, bit 24 */
#define GPIO_MIS_BIT25 ((uint32_t)0x02000000) /*!< Pin mis, bit 25 */
#define GPIO_MIS_BIT26 ((uint32_t)0x04000000) /*!< Pin mis, bit 26 */
#define GPIO_MIS_BIT27 ((uint32_t)0x08000000) /*!< Pin mis, bit 27 */
#define GPIO_MIS_BIT28 ((uint32_t)0x10000000) /*!< Pin mis, bit 28 */
#define GPIO_MIS_BIT29 ((uint32_t)0x20000000) /*!< Pin mis, bit 29 */
#define GPIO_MIS_BIT30 ((uint32_t)0x40000000) /*!< Pin mis, bit 30 */
#define GPIO_MIS_BIT31 ((uint32_t)0x80000000) /*!< Pin mis, bit 31 */

/*******************  Bit definition for GPIO_IC register  *******************/
#define GPIO_IC_BIT0  ((uint32_t)0x00000001) /*!< Pin ic, bit 0 */
#define GPIO_IC_BIT1  ((uint32_t)0x00000002) /*!< Pin ic, bit 1 */
#define GPIO_IC_BIT2  ((uint32_t)0x00000004) /*!< Pin ic, bit 2 */
#define GPIO_IC_BIT3  ((uint32_t)0x00000008) /*!< Pin ic, bit 3 */
#define GPIO_IC_BIT4  ((uint32_t)0x00000010) /*!< Pin ic, bit 4 */
#define GPIO_IC_BIT5  ((uint32_t)0x00000020) /*!< Pin ic, bit 5 */
#define GPIO_IC_BIT6  ((uint32_t)0x00000040) /*!< Pin ic, bit 6 */
#define GPIO_IC_BIT7  ((uint32_t)0x00000080) /*!< Pin ic, bit 7 */
#define GPIO_IC_BIT8  ((uint32_t)0x00000100) /*!< Pin ic, bit 8 */
#define GPIO_IC_BIT9  ((uint32_t)0x00000200) /*!< Pin ic, bit 9 */
#define GPIO_IC_BIT10 ((uint32_t)0x00000400) /*!< Pin ic, bit 10 */
#define GPIO_IC_BIT11 ((uint32_t)0x00000800) /*!< Pin ic, bit 11 */
#define GPIO_IC_BIT12 ((uint32_t)0x00001000) /*!< Pin ic, bit 12 */
#define GPIO_IC_BIT13 ((uint32_t)0x00002000) /*!< Pin ic, bit 13 */
#define GPIO_IC_BIT14 ((uint32_t)0x00004000) /*!< Pin ic, bit 14 */
#define GPIO_IC_BIT15 ((uint32_t)0x00008000) /*!< Pin ic, bit 15 */
#define GPIO_IC_BIT16 ((uint32_t)0x00010000) /*!< Pin ic, bit 16 */
#define GPIO_IC_BIT17 ((uint32_t)0x00020000) /*!< Pin ic, bit 17 */
#define GPIO_IC_BIT18 ((uint32_t)0x00040000) /*!< Pin ic, bit 18 */
#define GPIO_IC_BIT19 ((uint32_t)0x00080000) /*!< Pin ic, bit 19 */
#define GPIO_IC_BIT20 ((uint32_t)0x00100000) /*!< Pin ic, bit 20 */
#define GPIO_IC_BIT21 ((uint32_t)0x00200000) /*!< Pin ic, bit 21 */
#define GPIO_IC_BIT22 ((uint32_t)0x00400000) /*!< Pin ic, bit 22 */
#define GPIO_IC_BIT23 ((uint32_t)0x00800000) /*!< Pin ic, bit 23 */
#define GPIO_IC_BIT24 ((uint32_t)0x01000000) /*!< Pin ic, bit 24 */
#define GPIO_IC_BIT25 ((uint32_t)0x02000000) /*!< Pin ic, bit 25 */
#define GPIO_IC_BIT26 ((uint32_t)0x04000000) /*!< Pin ic, bit 26 */
#define GPIO_IC_BIT27 ((uint32_t)0x08000000) /*!< Pin ic, bit 27 */
#define GPIO_IC_BIT28 ((uint32_t)0x10000000) /*!< Pin ic, bit 28 */
#define GPIO_IC_BIT29 ((uint32_t)0x20000000) /*!< Pin ic, bit 29 */
#define GPIO_IC_BIT30 ((uint32_t)0x40000000) /*!< Pin ic, bit 30 */
#define GPIO_IC_BIT31 ((uint32_t)0x80000000) /*!< Pin ic, bit 31 */

/*******************  Bit definition for GPIO_PinMask register  *******************/
#define GPIO_PinMask_BIT0  ((uint32_t)0x00000001) /*!< Data mask, bit 0 */
#define GPIO_PinMask_BIT1  ((uint32_t)0x00000002) /*!< Data mask, bit 1 */
#define GPIO_PinMask_BIT2  ((uint32_t)0x00000004) /*!< Data mask, bit 2 */
#define GPIO_PinMask_BIT3  ((uint32_t)0x00000008) /*!< Data mask, bit 3 */
#define GPIO_PinMask_BIT4  ((uint32_t)0x00000010) /*!< Data mask, bit 4 */
#define GPIO_PinMask_BIT5  ((uint32_t)0x00000020) /*!< Data mask, bit 5 */
#define GPIO_PinMask_BIT6  ((uint32_t)0x00000040) /*!< Data mask, bit 6 */
#define GPIO_PinMask_BIT7  ((uint32_t)0x00000080) /*!< Data mask, bit 7 */
#define GPIO_PinMask_BIT8  ((uint32_t)0x00000100) /*!< Data mask, bit 8 */
#define GPIO_PinMask_BIT9  ((uint32_t)0x00000200) /*!< Data mask, bit 9 */
#define GPIO_PinMask_BIT10 ((uint32_t)0x00000400) /*!< Data mask, bit 10 */
#define GPIO_PinMask_BIT11 ((uint32_t)0x00000800) /*!< Data mask, bit 11 */
#define GPIO_PinMask_BIT12 ((uint32_t)0x00001000) /*!< Data mask, bit 12 */
#define GPIO_PinMask_BIT13 ((uint32_t)0x00002000) /*!< Data mask, bit 13 */
#define GPIO_PinMask_BIT14 ((uint32_t)0x00004000) /*!< Data mask, bit 14 */
#define GPIO_PinMask_BIT15 ((uint32_t)0x00008000) /*!< Data mask, bit 15 */
#define GPIO_PinMask_BIT16 ((uint32_t)0x00010000) /*!< Data mask, bit 16 */
#define GPIO_PinMask_BIT17 ((uint32_t)0x00020000) /*!< Data mask, bit 17 */
#define GPIO_PinMask_BIT18 ((uint32_t)0x00040000) /*!< Data mask, bit 18 */
#define GPIO_PinMask_BIT19 ((uint32_t)0x00080000) /*!< Data mask, bit 19 */
#define GPIO_PinMask_BIT20 ((uint32_t)0x00100000) /*!< Data mask, bit 20 */
#define GPIO_PinMask_BIT21 ((uint32_t)0x00200000) /*!< Data mask, bit 21 */
#define GPIO_PinMask_BIT22 ((uint32_t)0x00400000) /*!< Data mask, bit 22 */
#define GPIO_PinMask_BIT23 ((uint32_t)0x00800000) /*!< Data mask, bit 23 */
#define GPIO_PinMask_BIT24 ((uint32_t)0x01000000) /*!< Data mask, bit 24 */
#define GPIO_PinMask_BIT25 ((uint32_t)0x02000000) /*!< Data mask, bit 25 */
#define GPIO_PinMask_BIT26 ((uint32_t)0x04000000) /*!< Data mask, bit 26 */
#define GPIO_PinMask_BIT27 ((uint32_t)0x08000000) /*!< Data mask, bit 27 */
#define GPIO_PinMask_BIT28 ((uint32_t)0x10000000) /*!< Data mask, bit 28 */
#define GPIO_PinMask_BIT29 ((uint32_t)0x20000000) /*!< Data mask, bit 29 */
#define GPIO_PinMask_BIT30 ((uint32_t)0x40000000) /*!< Data mask, bit 30 */
#define GPIO_PinMask_BIT31 ((uint32_t)0x80000000) /*!< Data mask, bit 31 */

/*******************  Bit definition for GPIO_DMA_CTRL register  *******************/
#define GPIO_XFER_COUNT ((uint32_t)0x0000FFFF) /*!< Data counter */
#define GPIO_XFER_END   ((uint32_t)0x00010000) /*!< Transmit data end */
#define GPIO_FIFO_EMPTY ((uint32_t)0x00020000) /*!< Transmit fifo empty */
#define GPIO_DMA_EN     ((uint32_t)0x80000000) /*!< DMA enable */

/*******************  Bit definition for GPIO_DMA_PADCTRL0 register  *******************/
#define GPIO_XFER_COUNT ((uint32_t)0x0000FFFF) /*!< Select AHB clock cycle */

/*******************  Bit definition for GPIO_DMA_PADCTRL1 register  *******************/
#define GPIO_DMA_PADCTRL1_SEL0 ((uint32_t)0x0000000F) /*!< Select GPIO0[7:0] */
#define GPIO_DMA_PADCTRL1_SEL1 ((uint32_t)0x000000F0) /*!< Select GPIO0[15:8] */
#define GPIO_DMA_PADCTRL1_SEL2 ((uint32_t)0x00000F00) /*!< Select GPIO0[23:16] */
#define GPIO_DMA_PADCTRL1_SEL3 ((uint32_t)0x0000F000) /*!< Select GPIO0[31:24] */
#define GPIO_DMA_PADCTRL1_SEL4 ((uint32_t)0x000F0000) /*!< Select GPIO1[7:0] */
#define GPIO_DMA_PADCTRL1_SEL5 ((uint32_t)0x00F00000) /*!< Select GPIO1[15:8] */
#define GPIO_DMA_PADCTRL1_SEL6 ((uint32_t)0x0F000000) /*!< Select GPIO1[23:16] */
#define GPIO_DMA_PADCTRL1_SEL7 ((uint32_t)0xF0000000) /*!< Select GPIO1[31:24] */

/*******************  Bit definition for GPIO_DMA_PADCTRL2 register  *******************/
#define GPIO_DMA_PADCTRL1_SEL8  ((uint32_t)0x0000000F) /*!< Select GPIO2[7:0] */
#define GPIO_DMA_PADCTRL1_SEL9  ((uint32_t)0x000000F0) /*!< Select GPIO2[15:8] */
#define GPIO_DMA_PADCTRL1_SEL10 ((uint32_t)0x00000F00) /*!< Select GPIO2[23:16] */

/******************************************************************************/
/*                                                                                                 */
/*                      Inter-integrated Circuit Interface                                                                             */
/*                                                                                                                                                       */
/******************************************************************************/
/*******************  Bit definition for I2C_CON register  ********************/
#define I2C_CON_MASTER_MODE         ((uint16_t)0x0001)
#define I2C_CON_SPEED1_2            ((uint16_t)0x0006)
#define I2C_CON_IC_10BITADDR_SLAVE  ((uint16_t)0x0008)
#define I2C_CON_IC_10BITADDR_MASTER ((uint16_t)0x0010)
#define I2C_CON_IC_RESTART_EN       ((uint16_t)0x0020)
#define I2C_CON_IC_SLAVE_DISABLE    ((uint16_t)0x0040)

/*******************  Bit definition for IC_TAR register  ********************/
#define I2C_TAR_IC_TAR0_9   ((uint16_t)0x03FF)
#define I2C_TAR_GC_OR_START ((uint16_t)0x0400)
#define I2C_TAR_IC_SPECIAL  ((uint16_t)0x0800)

/*******************  Bit definition for IC_SAR register  ********************/
#define I2C_SAR_IC_SAR0_9 ((uint16_t)0x03FF)

/*******************  Bit definition for IC_HS_MADDR register  ********************/
#define I2C_IC_HS_MADDR_IC_HS_MAR0_2 ((uint16_t)0x0007)

/*******************  Bit definition for IC_DATA_CMD register  ********************/
#define I2C_IC_DATA_CMD_DAT0_7 ((uint16_t)0x00FF)
#define I2C_IC_DATA_CMD_CMD    ((uint16_t)0x0100)

/*******************  Bit definition for IC_SS_SCL_HCNT register  ********************/
#define I2C_IC_SS_SCL_HCNT_IC_SS_SCL_HCNT0_15 ((uint16_t)0xFFFF)

/*******************  Bit definition for IC_SS_SCL_LCNT register  ********************/
#define I2C_IC_SS_SCL_LCNT_IC_SS_SCL_LCNT0_15 ((uint16_t)0xFFFF)

/*******************  Bit definition for IC_FS_SCL_HCNT register  ********************/
#define I2C_IC_FS_SCL_HCNT_IC_FS_SCL_HCNT0_15 ((uint16_t)0xFFFF)

/*******************  Bit definition for IC_FS_SCL_LCNT register  ********************/
#define I2C_IC_FS_SCL_LCNT_IC_FS_SCL_LCNT0_15 ((uint16_t)0xFFFF)

/*******************  Bit definition for IC_HS_SCL_HCNT register  ********************/
#define I2C_IC_HS_SCL_HCNT_IC_HS_SCL_HCNT0_15 ((uint16_t)0xFFFF)

/*******************  Bit definition for IC_HS_SCL_LCNT register  ********************/
#define I2C_IC_HS_SCL_LCNT_IC_HS_SCL_LCNT0_15 ((uint16_t)0xFFFF)

/*******************  Bit definition for IC_INTR_STAT register  ********************/
#define I2C_IC_INTR_STAT_R_RX_UNDER  ((uint16_t)0x0001)
#define I2C_IC_INTR_STAT_R_RX_OVER   ((uint16_t)0x0002)
#define I2C_IC_INTR_STAT_R_RX_FULL   ((uint16_t)0x0004)
#define I2C_IC_INTR_STAT_R_TX_OVER   ((uint16_t)0x0008)
#define I2C_IC_INTR_STAT_R_TX_EMPTY  ((uint16_t)0x0010)
#define I2C_IC_INTR_STAT_R_RD_REQ    ((uint16_t)0x0020)
#define I2C_IC_INTR_STAT_R_TX_ABRT   ((uint16_t)0x0040)
#define I2C_IC_INTR_STAT_R_RX_DONE   ((uint16_t)0x0080)
#define I2C_IC_INTR_STAT_R_ACTIVITY  ((uint16_t)0x0100)
#define I2C_IC_INTR_STAT_R_STOP_DET  ((uint16_t)0x0200)
#define I2C_IC_INTR_STAT_R_START_DET ((uint16_t)0x0400)
#define I2C_IC_INTR_STAT_R_GEN_CALL  ((uint16_t)0x0800)

/*******************  Bit definition for IC_INTR_MASK register  ********************/
#define I2C_IC_INTR_MASK_M_RX_UNDER  ((uint16_t)0x0001)
#define I2C_IC_INTR_MASK_M_RX_OVER   ((uint16_t)0x0002)
#define I2C_IC_INTR_MASK_M_RX_FULL   ((uint16_t)0x0004)
#define I2C_IC_INTR_MASK_M_TX_OVER   ((uint16_t)0x0008)
#define I2C_IC_INTR_MASK_M_TX_EMPTY  ((uint16_t)0x0010)
#define I2C_IC_INTR_MASK_M_RD_REQ    ((uint16_t)0x0020)
#define I2C_IC_INTR_MASK_M_TX_ABRT   ((uint16_t)0x0040)
#define I2C_IC_INTR_MASK_M_ACTIVITY  ((uint16_t)0x0100)
#define I2C_IC_INTR_MASK_M_STOP_DET  ((uint16_t)0x0200)
#define I2C_IC_INTR_MASK_M_START_DET ((uint16_t)0x0400)
#define I2C_IC_INTR_MASK_M_GEN_CALL  ((uint16_t)0x0800)

/*******************  Bit definition for IC_RAW_INTR_STAT register  ********************/
#define I2C_IC_RAW_INTR_STAT_RX_UNDER  ((uint16_t)0x0001)
#define I2C_IC_RAW_INTR_STAT_RX_OVER   ((uint16_t)0x0002)
#define I2C_IC_RAW_INTR_STAT_RX_FULL   ((uint16_t)0x0004)
#define I2C_IC_RAW_INTR_STAT_TX_OVER   ((uint16_t)0x0008)
#define I2C_IC_RAW_INTR_STAT_TX_EMPTY  ((uint16_t)0x0010)
#define I2C_IC_RAW_INTR_STAT_RD_REQ    ((uint16_t)0x0020)
#define I2C_IC_RAW_INTR_STAT_TX_ABRT   ((uint16_t)0x0040)
#define I2C_IC_RAW_INTR_STAT_RX_DONE   ((uint16_t)0x0080)
#define I2C_IC_RAW_INTR_STAT_ACTIVITY  ((uint16_t)0x0100)
#define I2C_IC_RAW_INTR_STAT_STOP_DET  ((uint16_t)0x0200)
#define I2C_IC_RAW_INTR_STAT_START_DET ((uint16_t)0x0400)
#define I2C_IC_RAW_INTR_STAT_GEN_CALL  ((uint16_t)0x0800)

/*******************  Bit definition for IC_RX_TL register  ********************/
#define I2C_IC_RX_TL_RX_TL0_7 ((uint16_t)0x00ff)

/*******************  Bit definition for IC_TX_TL register  ********************/
#define I2C_IC_TX_TL_TX_TL0_7 ((uint16_t)0x00ff)

/*******************  Bit definition for IC_CLR_INTR register  ********************/
#define I2C_IC_CLR_INTR_CLR_INTR ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_RX_UNDER register  ********************/
#define I2C_IC_CLR_RX_UNDER_CLR_RX_UNDER ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_RX_OVER register  ********************/
#define I2C_IC_CLR_RX_OVER_CLR_RX_OVER ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_TX_OVER register  ********************/
#define I2C_IC_CLR_TX_OVER_CLR_TX_OVER ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_RD_REQ register  ********************/
#define I2C_IC_CLR_RD_REQ_CLR_RD_REQ ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_TX_ABRT register  ********************/
#define I2C_IC_CLR_TX_ABRT_CLR_TX_ABRT ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_RX_DONE register  ********************/
#define I2C_IC_CLR_RX_DONE_CLR_RX_DONE ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_ACTIVITY register  ********************/
#define I2C_IC_CLR_ACTIVITY_CLR_ACTIVITY ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_STOP_DET register  ********************/
#define I2C_IC_CLR_STOP_DET_CLR_STOP_DET ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_START_DET register  ********************/
#define I2C_IC_CLR_START_DET_CLR_START_DET ((uint16_t)0x0001)

/*******************  Bit definition for IC_CLR_GEN_CALL register  ********************/
#define I2C_IC_CLR_GEN_CALL_CLR_GEN_CALL ((uint16_t)0x0001)

/*******************  Bit definition for IC_ENABLE register  ********************/
#define I2C_IC_ENABLE_ENABLE ((uint16_t)0x0001)

/*******************  Bit definition for IC_STATUS register  ********************/
#define I2C_IC_STATUS_ACTIVITY     ((uint16_t)0x0001)
#define I2C_IC_STATUS_TFNF         ((uint16_t)0x0002)
#define I2C_IC_STATUS_TFE          ((uint16_t)0x0004)
#define I2C_IC_STATUS_RFNE         ((uint16_t)0x0008)
#define I2C_IC_STATUS_RFF          ((uint16_t)0x0010)
#define I2C_IC_STATUS_MST_ACTIVITY ((uint16_t)0x0020)
#define I2C_IC_STATUS_SLV_ACTIVITY ((uint16_t)0x0040)

/*******************  Bit definition for IC_TXFLR register  ********************/
#define I2C_IC_TXFLR_TXFLR0_3 ((uint16_t)0x000f)

/*******************  Bit definition for IC_RXFLR register  ********************/
#define I2C_IC_RXFLR_RXFLR0_3 ((uint16_t)0x000f)

/*******************  Bit definition for IC_TX_ABRT_SOURCE register  ********************/
#define I2C_IC_TX_ABRT_SOURCE_ABRT_7B_ADDR_NOACK   ((uint16_t)0x0001)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_10ADDR1_NOACK   ((uint16_t)0x0002)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_10ADDR2_NOACK   ((uint16_t)0x0004)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_TXDATA_NOACK    ((uint16_t)0x0008)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_GCALL_NOACK     ((uint16_t)0x0010)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_GCALL_READ      ((uint16_t)0x0020)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_HS_ACKDET       ((uint16_t)0x0040)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_SBYTE_ACKDET    ((uint16_t)0x0080)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_HS_NORSTRT      ((uint16_t)0x0100)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_SBYTE_NORSTRT   ((uint16_t)0x0200)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_10B_RD_NORSTRT  ((uint16_t)0x0400)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_MASTER_DIS      ((uint16_t)0x0800)
#define I2C_IC_TX_ABRT_SOURCE_ARB_LOST             ((uint16_t)0x1000)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_SLVFLUSH_TXFIFO ((uint16_t)0x2000)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_SLV_ARBLOST     ((uint16_t)0x4000)
#define I2C_IC_TX_ABRT_SOURCE_ABRT_SLVRD_INTX      ((uint16_t)0x8000)

/*******************  Bit definition for IC_SLV_DATA_NACK_ONLY register  ********************/
#define I2C_IC_SLV_DATA_NACK_ONLY_NACK ((uint16_t)0x0001)



/*******************  Bit definition for IC_SDA_SETUP register  ********************/
#define I2C_IC_SDA_SETUP_SDA_SETUP0_7 ((uint16_t)0x00ff)

/*******************  Bit definition for I2C_CON register  ********************/
#define I2C_IC_ACK_GENERAL_CALL_ACK_GEN_CALL ((uint16_t)0x0001)

/*******************  Bit definition for IC_ENABLE_STATUS register  ********************/
#define I2C_IC_ENABLE_STATUS_IC_EN                       ((uint16_t)0x0001)
#define I2C_IC_ENABLE_STATUS_SLV_RX_ABORTED              ((uint16_t)0x0002)
#define I2C_IC_ENABLE_STATUS_SLV_FIFO_FILLED_AND_FLUSHED ((uint16_t)0x0004)

/*******************  Bit definition for RCC_SYSPLLCTRL register  *******************/
#define RCC_SYSPLLCTRL_FORCELOCK    ((uint32_t)0x80000000) /*!< System pll lock */
#define RCC_SYSPLLCTRL_DEFAULT_BIAS ((uint32_t)0x20000000) /*!< pll default bias */

/*******************  Bit definition for RCC_SYSPLLSTAT register  *******************/
#define RCC_SYSPLLSTAT_LOCK ((uint8_t)0x01)

/*******************  Bit definition for RCC_ANASTATUS register  *******************/
#define RCC_ANASTATUS_RTC_POR ((uint8_t)0x01) /*!< RTC analog signal */

/*******************  Bit definition for RCC_PIOPORCAP0 register  *******************/
#define RCC_PIOPORCAP0_CAPPIO0 ((uint32_t)0x000000FF) /*!< Raw reset status input GPIO0 */
#define RCC_PIOPORCAP0_CAPPIO1 ((uint32_t)0x0000FF00) /*!< Raw reset status input GPIO1 */
#define RCC_PIOPORCAP0_CAPPIO2 ((uint32_t)0x00FF0000) /*!< Raw reset status input GPIO2 */
#define RCC_PIOPORCAP0_CAPPIO3 ((uint32_t)0xFF000000) /*!< Raw reset status input GPIO3 */

/*******************  Bit definition for RCC_PIOPORCAP1 register  *******************/
#define RCC_PIOPORCAP1_CAPPIO0 ((uint32_t)0x000000FF) /*!< Raw reset status input GPIO4 */
#define RCC_PIOPORCAP1_CAPPIO1 ((uint32_t)0x0000FF00) /*!< Raw reset status input GPIO5 */
#define RCC_PIOPORCAP1_CAPPIO2 ((uint32_t)0x00FF0000) /*!< Raw reset status input GPIO6 */
#define RCC_PIOPORCAP1_CAPPIO3 ((uint32_t)0xFF000000) /*!< Raw reset status input GPIO7 */

/*******************  Bit definition for RCC_PIOPORCAP2 register  *******************/
#define RCC_PIOPORCAP2_CAPPIO0 ((uint32_t)0x000000FF) /*!< Raw reset status input GPIO8 */
#define RCC_PIOPORCAP2_CAPPIO1 ((uint32_t)0x0000FF00) /*!< Raw reset status input GPIO9 */
#define RCC_PIOPORCAP2_CAPPIO2 ((uint32_t)0x00FF0000) /*!< Raw reset status input GPIO10 */

/*******************  Bit definition for RCC_BODCTRL register  *******************/
#define RCC_PIOPORCAP3_BODINTVAL ((uint8_t)0x03) /*!< BOD reset level */
#define RCC_PIOPORCAP3_BODRSTENA ((uint8_t)0x04) /*!< BOD reset enable */

/*******************  Bit definition for RCC_SYSTICKCAL register  *******************/
#define RCC_SYSTICKCAL_CAL ((uint32_t)0x03FFFFFF) /*!< System tick timer calibration value */

/*******************  Bit definition for RCC_STARTAPRP0 register  *******************/
#define RCC_STARTAPRP0_APRPIO0 ((uint32_t)0x000000FF) /*!< Edge select for start logic input GPIO0 */
#define RCC_STARTAPRP0_APRPIO1 ((uint32_t)0x0000FF00) /*!< Edge select for start logic input GPIO1 */
#define RCC_STARTAPRP0_APRPIO2 ((uint32_t)0x00FF0000) /*!< Edge select for start logic input GPIO2 */
#define RCC_STARTAPRP0_APRPIO3 ((uint32_t)0xFF000000) /*!< Edge select for start logic input GPIO3 */

/*******************  Bit definition for RCC_STARTERP0 register  *******************/
#define RCC_STARTERP0_ERPIO0 ((uint32_t)0x000000FF) /*!< Enable start signal for start logic input GPIO1 */
#define RCC_STARTERP0_ERPIO1 ((uint32_t)0x0000FF00) /*!< Enable start signal for start logic input GPIO2 */
#define RCC_STARTERP0_ERPIO2 ((uint32_t)0x00FF0000) /*!< Enable start signal for start logic input GPIO3 */
#define RCC_STARTERP0_ERPIO3 ((uint32_t)0xFF000000) /*!< Enable start signal for start logic input GPIO4 */

/*******************  Bit definition for RCC_STARTRSRP0 register  *******************/
#define RCC_STARTRSRP0_RSRPIO0 ((uint32_t)0x000000FF) /*!< Start signal reset for start logic input GPIO1 */
#define RCC_STARTRSRP0_RSRPIO1 ((uint32_t)0x0000FF00) /*!< Start signal reset for start logic input GPIO2 */
#define RCC_STARTRSRP0_RSRPIO2 ((uint32_t)0x00FF0000) /*!< Start signal reset for start logic input GPIO3 */
#define RCC_STARTRSRP0_RSRPIO3 ((uint32_t)0xFF000000) /*!< Start signal reset for start logic input GPIO4 */

/*******************  Bit definition for RCC_STARTSRP0 register  *******************/
#define RCC_STARTSRP0_SRPIO0 ((uint32_t)0x000000FF) /*!< Start signal status for start logic input GPIO0 */
#define RCC_STARTSRP0_SRPIO1 ((uint32_t)0x0000FF00) /*!< Start signal status for start logic input GPIO1 */
#define RCC_STARTSRP0_SRPIO2 ((uint32_t)0x00FF0000) /*!< Start signal status for start logic input GPIO2 */
#define RCC_STARTSRP0_SRPIO3 ((uint32_t)0xFF000000) /*!< Start signal status for start logic input GPIO3 */

/*******************  Bit definition for RCC_STARTAPRP1 register  *******************/
#define RCC_STARTAPRP1_APRPIO0 ((uint8_t)0xFF) /*!< Edge select for start logic input GPIO4 */

/*******************  Bit definition for RCC_STARTERP1 register  *******************/
#define RCC_STARTERP1_ERPIO0 ((uint8_t)0xFF) /*!< Enable start signal for start logic input GPIO4 */

/*******************  Bit definition for RCC_STARTRSRP1 register  *******************/
#define RCC_STARTRSRP1_RSRPIO0 ((uint8_t)0xFF) /*!< Start signal reset for start logic input GPIO4 */

/*******************  Bit definition for RCC_STARTSRP1 register  *******************/
#define RCC_STARTSRP1_SRPIO0 ((uint8_t)0xFF) /*!< Start signal status for start logic input GPIO4 */

/*******************  Bit definition for RCC_PDSLEEPCFG register  *******************/
#define RCC_PDSLEEPCFG_IRC10K_PD ((uint16_t)0x0002) /*!< IRC oscillator power-down control in Deep-sleep mode */
#define RCC_PDSLEEPCFG_ADC0_PD   ((uint16_t)0x0004) /*!< ADC0 power-down control in Deep-sleep mode */
#define RCC_PDSLEEPCFG_SYSPLL_PD ((uint16_t)0x0010) /*!< System PLL power-down control in Deep-sleep mode */
#define RCC_PDSLEEPCFG_BOD_PD    ((uint16_t)0x0020) /*!< BOD power-down control in Deep-sleep mode */
#define RCC_PDSLEEPCFG_BOR_PD    ((uint16_t)0x0040) /*!< BOD power-down control in Deep-sleep mode */
#define RCC_PDSLEEPCFG_IRC12M_PD ((uint16_t)0x0100) /*!< RTC oscillator power-down control in Deep-sleep mode */

/*******************  Bit definition for RCC_PDAWAKECFG register  *******************/
#define RCC_PDAWAKECFG_IRC10K_PD ((uint16_t)0x0002) /*!< IRC oscillator power-down wake-up configuration */
#define RCC_PDAWAKECFG_ADC0_PD   ((uint16_t)0x0004) /*!< ADC0 wake-up configuration */
#define RCC_PDAWAKECFG_SYSPLL_PD ((uint16_t)0x0010) /*!< System PLL wake-up configuration */
#define RCC_PDAWAKECFG_BOD_PD    ((uint16_t)0x0020) /*!< BOD wake-up configuration */
#define RCC_PDAWAKECFG_BOR_PD    ((uint16_t)0x0040) /*!< BOD wake-up configuration */
#define RCC_PDAWAKECFG_IRC12M_PD ((uint16_t)0x0100) /*!< BOD wake-up configuration */

/*******************  Bit definition for RCC_PDRUNCFG register  *******************/
#define RCC_PDRUNCFG_IRC10K_PD ((uint16_t)0x0002) /*!< IRC10K oscillator power-down */
#define RCC_PDRUNCFG_ADC0_PD   ((uint16_t)0x0004) /*!< ADC0 power-down */
#define RCC_PDRUNCFG_SYSPLL_PD ((uint16_t)0x0010) /*!< System PLL power-down */
#define RCC_PDRUNCFG_BOD_PD    ((uint16_t)0x0020) /*!< BOD power-down */
#define RCC_PDRUNCFG_BOR_PD    ((uint16_t)0x0040) /*!< BOR power-down */
#define RCC_PDRUNCFG_IRC12M_PD ((uint16_t)0x0100) /*!< IRC48M oscillator power-down */
#define RCC_PDRUNCFG_EFLASH_PD ((uint16_t)0x0200) /*!< EFLASH power-down */

/*******************  Bit definition for RCC_DEVICEID register  *******************/
#define RCC_DEVICEID ((uint32_t)0xFFFFFFFF) /*!< Device ID */

/*******************  Bit definition for RCC_PCON register  *******************/
#define RCC_PCON_ADDR_DPDEN     ((uint16_t)0x0002) /*!< Deep-sleep enable */
#define RCC_PCON_ADDR_SLEEPFLAG ((uint16_t)0x0100) /*!< Sleep mode flag */
#define RCC_PCON_ADDR_DPDFLAG   ((uint16_t)0x0800) /*!< Deep-sleep mode flag */

/******************************************************************************/
/*                                                                            */
/*                               Timers (TIM)                                 */
/*                                                                            */
/******************************************************************************/
/*******************  Bit definition for TIM_CR1 register  ********************/
#define TIM_CR1_CEN  ((uint16_t)0x0001) /*!<Counter enable */
#define TIM_CR1_UDIS ((uint16_t)0x0002) /*!<Update disable */
#define TIM_CR1_URS  ((uint16_t)0x0004) /*!<Update request source */
#define TIM_CR1_OPM  ((uint16_t)0x0008) /*!<One pulse mode */
#define TIM_CR1_DIR  ((uint16_t)0x0010) /*!<Direction */

#define TIM_CR1_CMS   ((uint16_t)0x0060) /*!<CMS[1:0] bits (Center-aligned mode selection) */
#define TIM_CR1_CMS_0 ((uint16_t)0x0020) /*!<Bit 0 */
#define TIM_CR1_CMS_1 ((uint16_t)0x0040) /*!<Bit 1 */

#define TIM_CR1_ARPE ((uint16_t)0x0080) /*!<Auto-reload preload enable */

#define TIM_CR1_CKD   ((uint16_t)0x0300) /*!<CKD[1:0] bits (clock division) */
#define TIM_CR1_CKD_0 ((uint16_t)0x0100) /*!<Bit 0 */
#define TIM_CR1_CKD_1 ((uint16_t)0x0200) /*!<Bit 1 */

/*******************  Bit definition for TIM_CR2 register  ********************/
#define TIM_CR2_CCPC ((uint16_t)0x0001) /*!<Capture/Compare Preloaded Control */
#define TIM_CR2_CCUS ((uint16_t)0x0004) /*!<Capture/Compare Control Update Selection */
#define TIM_CR2_CCDS ((uint16_t)0x0008) /*!<Capture/Compare DMA Selection */

#define TIM_CR2_MMS   ((uint16_t)0x0070) /*!<MMS[2:0] bits (Master Mode Selection) */
#define TIM_CR2_MMS_0 ((uint16_t)0x0010) /*!<Bit 0 */
#define TIM_CR2_MMS_1 ((uint16_t)0x0020) /*!<Bit 1 */
#define TIM_CR2_MMS_2 ((uint16_t)0x0040) /*!<Bit 2 */

#define TIM_CR2_TI1S ((uint16_t)0x0080) /*!<TI1 Selection */
#define TIM_CR2_OIS1  ((uint16_t)0x0100) /*!<Output Idle state 1 (OC1 output) */
#define TIM_CR2_OIS1N ((uint16_t)0x0200) /*!<Output Idle state 1 (OC1N output) */
#define TIM_CR2_OIS2  ((uint16_t)0x0400) /*!<Output Idle state 2 (OC2 output) */
#define TIM_CR2_OIS2N ((uint16_t)0x0800) /*!<Output Idle state 2 (OC2N output) */
#define TIM_CR2_OIS3  ((uint16_t)0x1000) /*!<Output Idle state 3 (OC3 output) */
#define TIM_CR2_OIS3N ((uint16_t)0x2000) /*!<Output Idle state 3 (OC3N output) */
#define TIM_CR2_OIS4  ((uint16_t)0x4000) /*!<Output Idle state 4 (OC4 output) */

/*******************  Bit definition for TIM_SMCR register  *******************/
#define TIM_SMCR_SMS   ((uint16_t)0x0007) /*!<SMS[2:0] bits (Slave mode selection) */
#define TIM_SMCR_SMS_0 ((uint16_t)0x0001) /*!<Bit 0 */
#define TIM_SMCR_SMS_1 ((uint16_t)0x0002) /*!<Bit 1 */
#define TIM_SMCR_SMS_2 ((uint16_t)0x0004) /*!<Bit 2 */

#define TIM_SMCR_TS   ((uint16_t)0x0070) /*!<TS[2:0] bits (Trigger selection) */
#define TIM_SMCR_TS_0 ((uint16_t)0x0010) /*!<Bit 0 */
#define TIM_SMCR_TS_1 ((uint16_t)0x0020) /*!<Bit 1 */
#define TIM_SMCR_TS_2 ((uint16_t)0x0040) /*!<Bit 2 */

#define TIM_SMCR_MSM ((uint16_t)0x0080) /*!<Master/slave mode */

#define TIM_SMCR_ETF   ((uint16_t)0x0F00) /*!<ETF[3:0] bits (External trigger filter) */
#define TIM_SMCR_ETF_0 ((uint16_t)0x0100) /*!<Bit 0 */
#define TIM_SMCR_ETF_1 ((uint16_t)0x0200) /*!<Bit 1 */
#define TIM_SMCR_ETF_2 ((uint16_t)0x0400) /*!<Bit 2 */
#define TIM_SMCR_ETF_3 ((uint16_t)0x0800) /*!<Bit 3 */

#define TIM_SMCR_ETPS   ((uint16_t)0x3000) /*!<ETPS[1:0] bits (External trigger prescaler) */
#define TIM_SMCR_ETPS_0 ((uint16_t)0x1000) /*!<Bit 0 */
#define TIM_SMCR_ETPS_1 ((uint16_t)0x2000) /*!<Bit 1 */

#define TIM_SMCR_ECE ((uint16_t)0x4000) /*!<External clock enable */
#define TIM_SMCR_ETP ((uint16_t)0x8000) /*!<External trigger polarity */

/*******************  Bit definition for TIM_DIER register  *******************/
#define TIM_DIER_UIE   ((uint16_t)0x0001) /*!<Update interrupt enable */
#define TIM_DIER_CC1IE ((uint16_t)0x0002) /*!<Capture/Compare 1 interrupt enable */
#define TIM_DIER_CC2IE ((uint16_t)0x0004) /*!<Capture/Compare 2 interrupt enable */
#define TIM_DIER_CC3IE ((uint16_t)0x0008) /*!<Capture/Compare 3 interrupt enable */
#define TIM_DIER_CC4IE ((uint16_t)0x0010) /*!<Capture/Compare 4 interrupt enable */
#define TIM_DIER_COMIE ((uint16_t)0x0020) /*!<COM interrupt enable */
#define TIM_DIER_TIE   ((uint16_t)0x0040) /*!<Trigger interrupt enable */
#define TIM_DIER_BIE   ((uint16_t)0x0080) /*!<Break interrupt enable */
#define TIM_DIER_UDE   ((uint16_t)0x0100) /*!<Update DMA request enable */
#define TIM_DIER_CC1DE ((uint16_t)0x0200) /*!<Capture/Compare 1 DMA request enable */
#define TIM_DIER_CC2DE ((uint16_t)0x0400) /*!<Capture/Compare 2 DMA request enable */
#define TIM_DIER_CC3DE ((uint16_t)0x0800) /*!<Capture/Compare 3 DMA request enable */
#define TIM_DIER_CC4DE ((uint16_t)0x1000) /*!<Capture/Compare 4 DMA request enable */
#define TIM_DIER_COMDE ((uint16_t)0x2000) /*!<COM DMA request enable */
#define TIM_DIER_TDE   ((uint16_t)0x4000) /*!<Trigger DMA request enable */

/********************  Bit definition for TIM_SR register  ********************/
#define TIM_SR_UIF   ((uint16_t)0x0001) /*!<Update interrupt Flag */
#define TIM_SR_CC1IF ((uint16_t)0x0002) /*!<Capture/Compare 1 interrupt Flag */
#define TIM_SR_CC2IF ((uint16_t)0x0004) /*!<Capture/Compare 2 interrupt Flag */
#define TIM_SR_CC3IF ((uint16_t)0x0008) /*!<Capture/Compare 3 interrupt Flag */
#define TIM_SR_CC4IF ((uint16_t)0x0010) /*!<Capture/Compare 4 interrupt Flag */
#define TIM_SR_COMIF ((uint16_t)0x0020) /*!<COM interrupt Flag */
#define TIM_SR_TIF   ((uint16_t)0x0040) /*!<Trigger interrupt Flag */
#define TIM_SR_BIF   ((uint16_t)0x0080) /*!<Break interrupt Flag */
#define TIM_SR_CC1OF ((uint16_t)0x0200) /*!<Capture/Compare 1 Overcapture Flag */
#define TIM_SR_CC2OF ((uint16_t)0x0400) /*!<Capture/Compare 2 Overcapture Flag */
#define TIM_SR_CC3OF ((uint16_t)0x0800) /*!<Capture/Compare 3 Overcapture Flag */
#define TIM_SR_CC4OF ((uint16_t)0x1000) /*!<Capture/Compare 4 Overcapture Flag */

/*******************  Bit definition for TIM_EGR register  ********************/
#define TIM_EGR_UG   ((uint8_t)0x01) /*!<Update Generation */
#define TIM_EGR_CC1G ((uint8_t)0x02) /*!<Capture/Compare 1 Generation */
#define TIM_EGR_CC2G ((uint8_t)0x04) /*!<Capture/Compare 2 Generation */
#define TIM_EGR_CC3G ((uint8_t)0x08) /*!<Capture/Compare 3 Generation */
#define TIM_EGR_CC4G ((uint8_t)0x10) /*!<Capture/Compare 4 Generation */
#define TIM_EGR_COMG ((uint8_t)0x20) /*!<Capture/Compare Control Update Generation */
#define TIM_EGR_TG   ((uint8_t)0x40) /*!<Trigger Generation */
#define TIM_EGR_BG   ((uint8_t)0x80) /*!<Break Generation */

/******************  Bit definition for TIM_CCMR1 register  *******************/
#define TIM_CCMR1_CC1S   ((uint16_t)0x0003) /*!<CC1S[1:0] bits (Capture/Compare 1 Selection) */
#define TIM_CCMR1_CC1S_0 ((uint16_t)0x0001) /*!<Bit 0 */
#define TIM_CCMR1_CC1S_1 ((uint16_t)0x0002) /*!<Bit 1 */

#define TIM_CCMR1_OC1FE ((uint16_t)0x0004) /*!<Output Compare 1 Fast enable */
#define TIM_CCMR1_OC1PE ((uint16_t)0x0008) /*!<Output Compare 1 Preload enable */

#define TIM_CCMR1_OC1M   ((uint16_t)0x0070) /*!<OC1M[2:0] bits (Output Compare 1 Mode) */
#define TIM_CCMR1_OC1M_0 ((uint16_t)0x0010) /*!<Bit 0 */
#define TIM_CCMR1_OC1M_1 ((uint16_t)0x0020) /*!<Bit 1 */
#define TIM_CCMR1_OC1M_2 ((uint16_t)0x0040) /*!<Bit 2 */

#define TIM_CCMR1_OC1CE ((uint16_t)0x0080) /*!<Output Compare 1Clear Enable */

#define TIM_CCMR1_CC2S   ((uint16_t)0x0300) /*!<CC2S[1:0] bits (Capture/Compare 2 Selection) */
#define TIM_CCMR1_CC2S_0 ((uint16_t)0x0100) /*!<Bit 0 */
#define TIM_CCMR1_CC2S_1 ((uint16_t)0x0200) /*!<Bit 1 */

#define TIM_CCMR1_OC2FE ((uint16_t)0x0400) /*!<Output Compare 2 Fast enable */
#define TIM_CCMR1_OC2PE ((uint16_t)0x0800) /*!<Output Compare 2 Preload enable */

#define TIM_CCMR1_OC2M   ((uint16_t)0x7000) /*!<OC2M[2:0] bits (Output Compare 2 Mode) */
#define TIM_CCMR1_OC2M_0 ((uint16_t)0x1000) /*!<Bit 0 */
#define TIM_CCMR1_OC2M_1 ((uint16_t)0x2000) /*!<Bit 1 */
#define TIM_CCMR1_OC2M_2 ((uint16_t)0x4000) /*!<Bit 2 */

#define TIM_CCMR1_OC2CE ((uint16_t)0x8000) /*!<Output Compare 2 Clear Enable */

/*----------------------------------------------------------------------------*/

#define TIM_CCMR1_IC1PSC   ((uint16_t)0x000C) /*!<IC1PSC[1:0] bits (Input Capture 1 Prescaler) */
#define TIM_CCMR1_IC1PSC_0 ((uint16_t)0x0004) /*!<Bit 0 */
#define TIM_CCMR1_IC1PSC_1 ((uint16_t)0x0008) /*!<Bit 1 */

#define TIM_CCMR1_IC1F   ((uint16_t)0x00F0) /*!<IC1F[3:0] bits (Input Capture 1 Filter) */
#define TIM_CCMR1_IC1F_0 ((uint16_t)0x0010) /*!<Bit 0 */
#define TIM_CCMR1_IC1F_1 ((uint16_t)0x0020) /*!<Bit 1 */
#define TIM_CCMR1_IC1F_2 ((uint16_t)0x0040) /*!<Bit 2 */
#define TIM_CCMR1_IC1F_3 ((uint16_t)0x0080) /*!<Bit 3 */

#define TIM_CCMR1_IC2PSC   ((uint16_t)0x0C00) /*!<IC2PSC[1:0] bits (Input Capture 2 Prescaler) */
#define TIM_CCMR1_IC2PSC_0 ((uint16_t)0x0400) /*!<Bit 0 */
#define TIM_CCMR1_IC2PSC_1 ((uint16_t)0x0800) /*!<Bit 1 */

#define TIM_CCMR1_IC2F   ((uint16_t)0xF000) /*!<IC2F[3:0] bits (Input Capture 2 Filter) */
#define TIM_CCMR1_IC2F_0 ((uint16_t)0x1000) /*!<Bit 0 */
#define TIM_CCMR1_IC2F_1 ((uint16_t)0x2000) /*!<Bit 1 */
#define TIM_CCMR1_IC2F_2 ((uint16_t)0x4000) /*!<Bit 2 */
#define TIM_CCMR1_IC2F_3 ((uint16_t)0x8000) /*!<Bit 3 */

/******************  Bit definition for TIM_CCMR2 register  *******************/
#define TIM_CCMR2_CC3S   ((uint16_t)0x0003) /*!<CC3S[1:0] bits (Capture/Compare 3 Selection) */
#define TIM_CCMR2_CC3S_0 ((uint16_t)0x0001) /*!<Bit 0 */
#define TIM_CCMR2_CC3S_1 ((uint16_t)0x0002) /*!<Bit 1 */

#define TIM_CCMR2_OC3FE ((uint16_t)0x0004) /*!<Output Compare 3 Fast enable */
#define TIM_CCMR2_OC3PE ((uint16_t)0x0008) /*!<Output Compare 3 Preload enable */

#define TIM_CCMR2_OC3M   ((uint16_t)0x0070) /*!<OC3M[2:0] bits (Output Compare 3 Mode) */
#define TIM_CCMR2_OC3M_0 ((uint16_t)0x0010) /*!<Bit 0 */
#define TIM_CCMR2_OC3M_1 ((uint16_t)0x0020) /*!<Bit 1 */
#define TIM_CCMR2_OC3M_2 ((uint16_t)0x0040) /*!<Bit 2 */

#define TIM_CCMR2_OC3CE ((uint16_t)0x0080) /*!<Output Compare 3 Clear Enable */

#define TIM_CCMR2_CC4S   ((uint16_t)0x0300) /*!<CC4S[1:0] bits (Capture/Compare 4 Selection) */
#define TIM_CCMR2_CC4S_0 ((uint16_t)0x0100) /*!<Bit 0 */
#define TIM_CCMR2_CC4S_1 ((uint16_t)0x0200) /*!<Bit 1 */

#define TIM_CCMR2_OC4FE ((uint16_t)0x0400) /*!<Output Compare 4 Fast enable */
#define TIM_CCMR2_OC4PE ((uint16_t)0x0800) /*!<Output Compare 4 Preload enable */

#define TIM_CCMR2_OC4M   ((uint16_t)0x7000) /*!<OC4M[2:0] bits (Output Compare 4 Mode) */
#define TIM_CCMR2_OC4M_0 ((uint16_t)0x1000) /*!<Bit 0 */
#define TIM_CCMR2_OC4M_1 ((uint16_t)0x2000) /*!<Bit 1 */
#define TIM_CCMR2_OC4M_2 ((uint16_t)0x4000) /*!<Bit 2 */

#define TIM_CCMR2_OC4CE ((uint16_t)0x8000) /*!<Output Compare 4 Clear Enable */

/*----------------------------------------------------------------------------*/

#define TIM_CCMR2_IC3PSC   ((uint16_t)0x000C) /*!<IC3PSC[1:0] bits (Input Capture 3 Prescaler) */
#define TIM_CCMR2_IC3PSC_0 ((uint16_t)0x0004) /*!<Bit 0 */
#define TIM_CCMR2_IC3PSC_1 ((uint16_t)0x0008) /*!<Bit 1 */

#define TIM_CCMR2_IC3F   ((uint16_t)0x00F0) /*!<IC3F[3:0] bits (Input Capture 3 Filter) */
#define TIM_CCMR2_IC3F_0 ((uint16_t)0x0010) /*!<Bit 0 */
#define TIM_CCMR2_IC3F_1 ((uint16_t)0x0020) /*!<Bit 1 */
#define TIM_CCMR2_IC3F_2 ((uint16_t)0x0040) /*!<Bit 2 */
#define TIM_CCMR2_IC3F_3 ((uint16_t)0x0080) /*!<Bit 3 */

#define TIM_CCMR2_IC4PSC   ((uint16_t)0x0C00) /*!<IC4PSC[1:0] bits (Input Capture 4 Prescaler) */
#define TIM_CCMR2_IC4PSC_0 ((uint16_t)0x0400) /*!<Bit 0 */
#define TIM_CCMR2_IC4PSC_1 ((uint16_t)0x0800) /*!<Bit 1 */

#define TIM_CCMR2_IC4F   ((uint16_t)0xF000) /*!<IC4F[3:0] bits (Input Capture 4 Filter) */
#define TIM_CCMR2_IC4F_0 ((uint16_t)0x1000) /*!<Bit 0 */
#define TIM_CCMR2_IC4F_1 ((uint16_t)0x2000) /*!<Bit 1 */
#define TIM_CCMR2_IC4F_2 ((uint16_t)0x4000) /*!<Bit 2 */
#define TIM_CCMR2_IC4F_3 ((uint16_t)0x8000) /*!<Bit 3 */

/*******************  Bit definition for TIM_CCER register  *******************/
#define TIM_CCER_CC1E ((uint16_t)0x0001) /*!<Capture/Compare 1 output enable */
#define TIM_CCER_CC1P ((uint16_t)0x0002) /*!<Capture/Compare 1 output Polarity */
#define TIM_CCER_CC1NE ((uint16_t)0x0004) /*!<Capture/Compare 1 Complementary output enable */
#define TIM_CCER_CC1NP ((uint16_t)0x0008) /*!<Capture/Compare 1 Complementary output Polarity */
#define TIM_CCER_CC2E ((uint16_t)0x0010) /*!<Capture/Compare 2 output enable */
#define TIM_CCER_CC2P ((uint16_t)0x0020) /*!<Capture/Compare 2 output Polarity */
#define TIM_CCER_CC2NE ((uint16_t)0x0040) /*!<Capture/Compare 2 Complementary output enable */
#define TIM_CCER_CC2NP ((uint16_t)0x0080) /*!<Capture/Compare 2 Complementary output Polarity */
#define TIM_CCER_CC3E  ((uint16_t)0x0100) /*!<Capture/Compare 3 output enable */
#define TIM_CCER_CC3P  ((uint16_t)0x0200) /*!<Capture/Compare 3 output Polarity */
#define TIM_CCER_CC3NE ((uint16_t)0x0400) /*!<Capture/Compare 3 Complementary output enable */
#define TIM_CCER_CC3NP ((uint16_t)0x0800) /*!<Capture/Compare 3 Complementary output Polarity */
#define TIM_CCER_CC4E  ((uint16_t)0x1000) /*!<Capture/Compare 4 output enable */
#define TIM_CCER_CC4P  ((uint16_t)0x2000) /*!<Capture/Compare 4 output Polarity */
#define TIM_CCER_CC4NP ((uint16_t)0x8000) /*!<Capture/Compare 4 Complementary output Polarity */

/*******************  Bit definition for TIM_CNT register  ********************/
#define TIM_CNT_CNT ((uint16_t)0xFFFF) /*!<Counter Value */

/*******************  Bit definition for TIM_PSC register  ********************/
#define TIM_PSC_PSC ((uint16_t)0xFFFF) /*!<Prescaler Value */

/*******************  Bit definition for TIM_ARR register  ********************/
#define TIM_ARR_ARR ((uint16_t)0xFFFF) /*!<actual auto-reload Value */

/*******************  Bit definition for TIM_RCR register  ********************/
#define TIM_RCR_REP ((uint8_t)0xFF) /*!<Repetition Counter Value */

/*******************  Bit definition for TIM_CCR1 register  *******************/
#define TIM_CCR1_CCR1 ((uint16_t)0xFFFF) /*!<Capture/Compare 1 Value */

/*******************  Bit definition for TIM_CCR2 register  *******************/
#define TIM_CCR2_CCR2 ((uint16_t)0xFFFF) /*!<Capture/Compare 2 Value */

/*******************  Bit definition for TIM_CCR3 register  *******************/
#define TIM_CCR3_CCR3 ((uint16_t)0xFFFF) /*!<Capture/Compare 3 Value */

/*******************  Bit definition for TIM_CCR4 register  *******************/
#define TIM_CCR4_CCR4 ((uint16_t)0xFFFF) /*!<Capture/Compare 4 Value */

/*******************  Bit definition for TIM_BDTR register  *******************/
#define TIM_BDTR_DTG   ((uint16_t)0x00FF) /*!<DTG[0:7] bits (Dead-Time Generator set-up) */
#define TIM_BDTR_DTG_0 ((uint16_t)0x0001) /*!<Bit 0 */
#define TIM_BDTR_DTG_1 ((uint16_t)0x0002) /*!<Bit 1 */
#define TIM_BDTR_DTG_2 ((uint16_t)0x0004) /*!<Bit 2 */
#define TIM_BDTR_DTG_3 ((uint16_t)0x0008) /*!<Bit 3 */
#define TIM_BDTR_DTG_4 ((uint16_t)0x0010) /*!<Bit 4 */
#define TIM_BDTR_DTG_5 ((uint16_t)0x0020) /*!<Bit 5 */
#define TIM_BDTR_DTG_6 ((uint16_t)0x0040) /*!<Bit 6 */
#define TIM_BDTR_DTG_7 ((uint16_t)0x0080) /*!<Bit 7 */

#define TIM_BDTR_LOCK   ((uint16_t)0x0300) /*!<LOCK[1:0] bits (Lock Configuration) */
#define TIM_BDTR_LOCK_0 ((uint16_t)0x0100) /*!<Bit 0 */
#define TIM_BDTR_LOCK_1 ((uint16_t)0x0200) /*!<Bit 1 */

#define TIM_BDTR_OSSI ((uint16_t)0x0400) /*!<Off-State Selection for Idle mode */
#define TIM_BDTR_OSSR ((uint16_t)0x0800) /*!<Off-State Selection for Run mode */
#define TIM_BDTR_BKE  ((uint16_t)0x1000) /*!<Break enable */
#define TIM_BDTR_BKP  ((uint16_t)0x2000) /*!<Break Polarity */
#define TIM_BDTR_AOE  ((uint16_t)0x4000) /*!<Automatic Output enable */
#define TIM_BDTR_MOE  ((uint16_t)0x8000) /*!<Main Output enable */

/*******************  Bit definition for TIM_DCR register  ********************/
#define TIM_DCR_DBA   ((uint16_t)0x001F) /*!<DBA[4:0] bits (DMA Base Address) */
#define TIM_DCR_DBA_0 ((uint16_t)0x0001) /*!<Bit 0 */
#define TIM_DCR_DBA_1 ((uint16_t)0x0002) /*!<Bit 1 */
#define TIM_DCR_DBA_2 ((uint16_t)0x0004) /*!<Bit 2 */
#define TIM_DCR_DBA_3 ((uint16_t)0x0008) /*!<Bit 3 */
#define TIM_DCR_DBA_4 ((uint16_t)0x0010) /*!<Bit 4 */

#define TIM_DCR_DBL   ((uint16_t)0x1F00) /*!<DBL[4:0] bits (DMA Burst Length) */
#define TIM_DCR_DBL_0 ((uint16_t)0x0100) /*!<Bit 0 */
#define TIM_DCR_DBL_1 ((uint16_t)0x0200) /*!<Bit 1 */
#define TIM_DCR_DBL_2 ((uint16_t)0x0400) /*!<Bit 2 */
#define TIM_DCR_DBL_3 ((uint16_t)0x0800) /*!<Bit 3 */
#define TIM_DCR_DBL_4 ((uint16_t)0x1000) /*!<Bit 4 */

/*******************  Bit definition for TIM_DMAR register  *******************/
#define TIM_DMAR_DMAB ((uint16_t)0xFFFF) /*!<DMA register for burst accesses */

/*******************  Bit definition for TIM_OR register  *********************/
#define TIM14_OR_TI1_RMP   ((uint16_t)0x0003) /*!<TI1_RMP[1:0] bits (TIM14 Input 4 remap) */
#define TIM14_OR_TI1_RMP_0 ((uint16_t)0x0001) /*!<Bit 0 */
#define TIM14_OR_TI1_RMP_1 ((uint16_t)0x0002) /*!<Bit 1 */



/*******************  Bit definition for HW_SPI_CTRL0 register  ********************/
#define SPI_CTRL0_MEMAP       ((uint32_t)0x02000000)
#define SPI_CTRL0_READ        ((uint32_t)0x04000000)
#define SPI_CTRL0_LOCK_CS     ((uint32_t)0x08000000)
#define SPI_CTRL0_HALF_DUPLEX ((uint32_t)0x10000000)
#define SPI_CTRL0_RUN         ((uint32_t)0x20000000)

/*******************  Bit definition for HW_SPI_CTRL1 register  ********************/
#define SPI_CTRL1_MODE                ((uint32_t)0x00000007)
#define SPI_CTRL1_WORD                ((uint32_t)0x000000f8)
#define SPI_CTRL1_SLAVE_MODE          ((uint32_t)0x00000100)
#define SPI_CTRL1_POLARITY            ((uint32_t)0x00000200)
#define SPI_CTRL1_PHASE               ((uint32_t)0x00000400)
#define SPI_CTRL1_SLAVE_OUT_DISABLE   ((uint32_t)0x00000800)
#define SPI_CTRL1_LOOP_BACK           ((uint32_t)0x00001000)
#define SPI_CTRL1_DMA_ENABLE          ((uint32_t)0x00002000)
#define SPI_CTRL1_RECV_OVRFLW_IRQ_EN  ((uint32_t)0x00004000)
#define SPI_CTRL1_RECV_OVRFLW_IRQ     ((uint32_t)0x00008000)
#define SPI_CTRL1_RECV_TIMEOUT_IRQ_EN ((uint32_t)0x00010000)
#define SPI_CTRL1_RECV_TIMEOUT_IRQ    ((uint32_t)0x00020000)
#define SPI_CTRL1_RECV_IRQ_EN         ((uint32_t)0x00040000)
#define SPI_CTRL1_RECV_IRQ            ((uint32_t)0x00080000)
#define SPI_CTRL1_XMIT_IRQ_EN         ((uint32_t)0x00100000)
#define SPI_CTRL1_XMIT_IRQ            ((uint32_t)0x00200000)
#define SPI_CTRL1_LSB                 ((uint32_t)0x00400000)
#define SPI_CTRL1_QPI                 ((uint32_t)0x00800000)

/*******************  Bit definition for HW_SPI_XFER register  ********************/
#define SPI_CMD ((uint32_t)0x00FF)

/*******************  Bit definition for HW_SPI_TIMING register  ********************/
#define SPI_TIMING_CLOCK_RATE   ((uint32_t)0x000000FF)
#define SPI_TIMING_CLOCK_DIVIDE ((uint32_t)0x0000FF00)

/*******************  Bit definition for HW_SPI_DATA register  ********************/
#define SPI_DATA ((uint32_t)0xFFFFFFFF)

/*******************  Bit definition for HW_SPI_STATUS register  ********************/
#define SPI_STATUS_SPI_BUSY          ((uint32_t)0x00000001)
#define SPI_STATUS_XMIT_UNDRFLW      ((uint32_t)0x00000010)
#define SPI_STATUS_XMIT_EMPTY        ((uint32_t)0x00000020)
#define SPI_STATUS_XMIT_NOT_FULL     ((uint32_t)0x00000040)
#define SPI_STATUS_RECV_NOT_EMPTY    ((uint32_t)0x00000080)
#define SPI_STATUS_RECV_FULL         ((uint32_t)0x00000100)
#define SPI_STATUS_RECV_OVRFLW       ((uint32_t)0x00000200)
#define SPI_STATUS_RECV_DATA_STAT    ((uint32_t)0x00000400)
#define SPI_STATUS_RECV_TIMEOUT_STAT ((uint32_t)0x00000800)
#define SPI_STATUS_TXDAVL            ((uint32_t)0x00F00000)
#define SPI_STATUS_RXDAVL            ((uint32_t)0x0F000000)

/*******************  Bit definition for HW_SPI_DEBUG register  ********************/
#define SPI_DEBUG0_SOEN            ((uint32_t)0x00000001)
#define SPI_DEBUG0_MOEN            ((uint32_t)0x00000002)
#define SPI_DEBUG0_SSN_OEN         ((uint32_t)0x00000004)
#define SPI_DEBUG0_CLK_OEN         ((uint32_t)0x00000008)
#define SPI_DEBUG0_RX_CNT_ONE      ((uint32_t)0x00000010)
#define SPI_DEBUG0_RFF4            ((uint32_t)0x00000020)
#define SPI_DEBUG0_FORCE_TX        ((uint32_t)0x00000040)
#define SPI_DEBUG0_LGNORE_RX       ((uint32_t)0x00000080)
#define SPI_DEBUG0_TXD_BUSY        ((uint32_t)0x00000100)
#define SPI_DEBUG0_FULL_CMD_DONE   ((uint32_t)0x00000200)
#define SPI_DEBUG0_HALF_CMD_DONE   ((uint32_t)0x00000400)
#define SPI_DEBUG0_FULL_DATA_STALL ((uint32_t)0x00000800)
#define SPI_DEBUG0_HALF_DATA_STALL ((uint32_t)0x00001000)
#define SPI_DEBUG0_FULL_DMA_ACK    ((uint32_t)0x00002000)
#define SPI_DEBUG0_HALF_DMA_ACK    ((uint32_t)0x00004000)
#define SPI_DEBUG0_FULL_DMA_STATE  ((uint32_t)0x00038000)
#define SPI_DEBUG0_HALF_DMA_STATE  ((uint32_t)0x001C0000)
#define SPI_DEBUG0_SLAVE_STATE     ((uint32_t)0x03E00000)
#define SPI_DEBUG0_MASTER_STATE    ((uint32_t)0xFC000000)

/*******************  Bit definition for HW_SPI_XFER register  ********************/
#define SPI_XFER ((uint32_t)0xFFFFFFFF)

void writeReg(uint32_t addr, uint32_t value);
uint32_t readReg(uint32_t addr);

#define outl(v, p) *(__IO uint32_t *)(p) = (v) // write 32bit
#define inl(p)     (*(__IO uint32_t *)(p))     // read 32bit
#define outs(v, p) *(__IO uint16_t *)(p) = (v) // write 16bit
#define ins(p)     (*(__IO uint16_t *)(p))     // read 16bit
#define outb(v, p) *(__IO uint8_t *)(p) = (v)  // write 8bit
#define inb(p)     (*(__IO uint8_t *)(p))      // read 8bit


#define Byte2Bit(addr) (((uint32_t)addr & 0x03FFFFFF) << 3) // Byte Addr to Bit Addr

void testReg32(uint32_t addr, uint32_t val);
void testReg16(uint32_t addr, uint16_t val);
void testReg8(uint32_t addr, uint8_t val);

void testMask32(uint32_t addr, uint32_t mask, uint32_t val);
void testMask16(uint32_t addr, uint16_t mask, uint16_t val);
void testMask8(uint32_t addr, uint8_t mask, uint8_t val);

void commonDelay(volatile unsigned long time);

#ifdef __cplusplus
}
#endif

#endif /* __DG32F003xx_H */

/************************ (C) COPYRIGHT ALPSCALE *****END OF FILE****/
