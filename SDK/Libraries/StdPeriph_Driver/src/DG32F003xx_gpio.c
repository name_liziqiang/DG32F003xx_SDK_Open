/**
 ******************************************************************************
 * @file    DG32F003xx_gpio.c
 * @author  Alpscale Software Team
 * @version V1.0.0
 * @date    10/14/2013
 * @brief   This file provides all the GPIO firmware functions.
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"

/** @addtogroup DG32F003xx_StdPeriph_Driver
 * @{
 */

/** @defgroup GPIO
 * @brief GPIO driver modules
 * @{
 */

/** @defgroup GPIO_Private_TypesDefinitions
 * @{
 */

/**
 * @}
 */

/** @defgroup GPIO_Private_Defines
 * @{
 */

#define GPIO_DMA_ENABLE  ((uint32_t)0x80000000)
#define GPIO_DMA_DISABLE ((uint32_t)0x00000000)
/**
 * @}
 */

/** @defgroup GPIO_Private_Macros
 * @{
 */

/**
 * @}
 */

/** @defgroup GPIO_Private_Variables
 * @{
 */

/**
 * @}
 */

/** @defgroup GPIO_Private_FunctionPrototypes
 * @{
 */

/**
 * @}
 */

/** @defgroup GPIO_Private_Functions
 * @{
 */

/**
 * @brief  Deinitializes the GPIO peripheral registers to their default reset values.
 * @retval None
 */
void GPIO_Reset(void)
{
  RCC_SetPRESETCTRL(1 << PRESET_BIT_GPIO, DISABLE);
  RCC_SetPRESETCTRL(1 << PRESET_BIT_GPIO, ENABLE);
}

/**
 * @brief  Initializes the GPIOx peripheral according to the specified
 *   parameters in the GPIO_InitStruct.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_InitStruct: pointer to a GPIO_InitTypeDef structure that
 *   contains the configuration information for the specified GPIO peripheral.
 * @retval None
 */
void GPIO_Init(GPIO_InitTypeDef *GPIO_InitStruct)
{
  uint32_t pinnum, temp;
  IOCON_TypeDef *PINCON;

  if (GPIO_InitStruct->GPIO_Function == IO_GPIO)
    assert_param(IS_GPIO_MODE(GPIO_InitStruct->GPIO_Mode));

  assert_param(IS_GPIO_PIN(GPIO_InitStruct->GPIO_Pin));

  for (pinnum = 0; pinnum < IO_NUM; pinnum++) {
    temp = (GPIO_InitStruct->GPIO_Pin) >> pinnum;
    temp &= 0x1;
    if (temp == 0x1) {
      PINCON = (IOCON_TypeDef *)(IOCON_PIO0_BASE + 0x4 * pinnum);
      PINCON->CON &= 0xFFC0FFFF;                             /* Clear IO function */
      PINCON->CON |= (GPIO_InitStruct->GPIO_Function << 16); /* Set IO function */
    }
  }
  if (GPIO_InitStruct->GPIO_Function == IO_GPIO) {
    if (GPIO_InitStruct->GPIO_Mode == GPIO_Mode_OUT)
      GPIO0_IT->DIR_SET = GPIO_InitStruct->GPIO_Pin;
    else
      GPIO0_IT->DIR_CLR = GPIO_InitStruct->GPIO_Pin;
  }
}

/**
 * @brief  Fills each GPIO_InitStruct member with its default value.
 * @param  GPIO_InitStruct : pointer to a GPIO_InitTypeDef structure which will
 *   be initialized.
 * @retval None
 */
void GPIO_StructInit(GPIO_InitTypeDef *GPIO_InitStruct)
{
  /* Reset GPIO init structure parameters values */
  GPIO_InitStruct->GPIO_Pin      = GPIO_Pin_All;
  GPIO_InitStruct->GPIO_Function = IO_GPIO;  
  GPIO_InitStruct->GPIO_Mode     = GPIO_Mode_IN;
}

uint32_t unused_gpio=GPIO_Pin_All;
void GPIO_SetPinMux(uint32_t GPIO_Pin, IO_TypeDef IO_Function)
{
  uint32_t pinnum, temp;
  IOCON_TypeDef *PINCON;
  assert_param(IS_GPIO_PIN(GPIO_Pin));

	unused_gpio&=(~GPIO_Pin);
  for (pinnum = 0; pinnum < IO_NUM; pinnum++) {
    temp = GPIO_Pin >> pinnum;
    temp &= 0x1;
    if (temp == 0x1) {
      PINCON = (IOCON_TypeDef *)(IOCON_PIO0_BASE + 0x4 * pinnum);
      PINCON->CON &= 0xFFC0FFFF;          /* Clear IO function */
      PINCON->CON |= (IO_Function << 16); /* Set IO function */
    }
  }
}

/**
 * @brief  Configure GPIO internal pull-dwon,pull-up or disable.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 *   This parameter can be one of GPIO_Pin_x where x can be (0...18).
 * @param  NewPull: specifies function mode.
 *   This parameter can be one of the GPIOPull_TypeDef enum values:
 *     @arg GPIO_PULL_DISABLE: disable.
 *     @arg GPIO_PULL_DOWN: pull-down.
 *     @arg GPIO_PULL_UP: pull-up.
 * @retval None.
 */
void GPIO_ConfigPull(uint32_t GPIO_Pin, GPIOPull_TypeDef NewPull)
{
  uint32_t pinnum, temp;
  IOCON_TypeDef *PINCON;

  for (pinnum = 0; pinnum < IO_NUM; pinnum++) {
    temp = GPIO_Pin >> pinnum;
    temp &= 0x1;
    if (temp == 0x1) {
      PINCON = (IOCON_TypeDef *)(IOCON_PIO0_BASE + 0x4 * pinnum);
      PINCON->CON &= 0xFFFFFFE7; /* Clear PULL function */
      PINCON->CON |= NewPull;    /* Set PULL function */
    }
  }
}

/**
 * @brief  Configuring IO driver capabilities.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 *   This parameter can be one of GPIO_Pin_x where x can be (0...18).
 * @param  NewDriver: specifies driver capabilities.
 *   This parameter can be one of the GPIODrive_TypeDef enum values:
 *     @arg GPIO_DRIVER_0: 12mA.
 *     @arg GPIO_DRIVER_1: 8mA.
 * @retval None.
 */
void GPIO_ConfigDriver(uint32_t GPIO_Pin, GPIODrive_TypeDef NewDriver)
{
  uint32_t pinnum, temp;
  IOCON_TypeDef *PINCON;

  for (pinnum = 0; pinnum < IO_NUM; pinnum++) {
    temp = GPIO_Pin >> pinnum;
    temp &= 0x1;
    if (temp == 0x1) {
      PINCON = (IOCON_TypeDef *)(IOCON_PIO0_BASE + 0x4 * pinnum);
      PINCON->CON &= 0xFFFFFEFF; /* Clear Driver function */
      PINCON->CON |= NewDriver;  /* Set Driver function */
    }
  }
}

/**
 * @brief  Configure GPIO slew rate.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 *   This parameter can be one of GPIO_Pin_x where x can be (0...18).
 * @param  NewSlewRate: specifies the slew rate is fast or slow.
 *   This parameter can be one of the GPIOSLEWRATE_TypeDef enum values:
 *     @arg GPIO_SLEWRATE_FAST: fast.
 *     @arg GPIO_SLEWRATE_SLOW: slow.
 * @retval
 */
void GPIO_ConfigSlewRate(uint32_t GPIO_Pin, GPIOSLEWRATE_TypeDef NewSlewRate)
{
  uint32_t pinnum, temp;
  IOCON_TypeDef *PINCON;

  for (pinnum = 0; pinnum < IO_NUM; pinnum++) {
    temp = GPIO_Pin >> pinnum;
    temp &= 0x1;
    if (temp == 0x1) {
      PINCON = (IOCON_TypeDef *)(IOCON_PIO0_BASE + 0x4 * pinnum);
      PINCON->CON &= 0xFFFFFDFF;  /* Clear Driver function */
      PINCON->CON |= NewSlewRate; /* Set Driver function */
    }
  }
}

/**
 * @brief  Sets the selected pins direction.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @param  GPIO_Mode: specifies the direction mode.
 *   This parameter can be one of the GPIOMode_TypeDef enum values:
 *     @arg GPIO_Mode_IN: to set pin input.
 *     @arg GPIO_Mode_OUT: to set pin output.
 * @retval None
 */
void GPIO_SetPinDir(uint32_t GPIO_Pin, GPIOMode_TypeDef GPIO_Mode)
{
  assert_param(IS_GPIO_PIN(GPIO_Pin));
  assert_param(IS_GPIO_MODE(GPIO_Mode));

  if (GPIO_Mode == GPIO_Mode_OUT) {
    GPIO0_IT->DIR_SET = GPIO_Pin;
  } else if (GPIO_Mode == GPIO_Mode_IN) {
    GPIO0_IT->DIR_CLR = GPIO_Pin;
  }
}

/**
 * @brief  Configure GPIO input trigger.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 *   This parameter can be one of GPIO_Pin_x where x can be (0...18).
 * @param  GPIO_InputMode: specifies the input trigger mode.
 *   This parameter can be one of the GPIOInput_TypeDef enum values:
 *     @arg GPIO_INPUT_SCHMITT: schmitt trigger input.
 *     @arg GPIO_INPUT_CMOS: CMOS trigger input.
 * @retval
 */
void GPIO_ConfigInput(uint32_t GPIO_Pin, GPIOInput_TypeDef GPIO_InputMode)
{
  uint32_t pinnum, temp, temreg;
  IOCON_TypeDef *PINCON;

  for (pinnum = 0; pinnum < IO_NUM; pinnum++) {
    temp = GPIO_Pin >> pinnum;
    temp &= 0x1;
    if (temp == 0x1) {
      PINCON = (IOCON_TypeDef *)(IOCON_PIO0_BASE + 0x4 * pinnum);
      temreg = PINCON->CON;
      temreg &= ~((uint32_t)1 << 6);
      temreg |= GPIO_InputMode;
      PINCON->CON = temreg;
    }
  }
}

/**
 * @brief  Configure GPIO output type.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 *   This parameter can be one of GPIO_Pin_x where x can be (0..18).
 * @param  GPIO_OutputMode: specifies the input trigger mode.
 *   This parameter can be one of the GPIOInput_TypeDef enum values:
 *     @arg GPIO_OUTPUT_NORMAL: normal putput
 *     @arg GPIO_OUTPUT_OPENSOURCE: open source output.
 *     @arg GPIO_OUTPUT_OPENDRAIN: open drain output.
 * @retval
 */
void GPIO_ConfigOutput(uint32_t GPIO_Pin, GPIOOutput_TypeDef GPIO_OutputMode)
{
  uint32_t pinnum, temp, temreg;
  IOCON_TypeDef *PINCON;

  for (pinnum = 0; pinnum < IO_NUM; pinnum++) {
    temp = GPIO_Pin >> pinnum;
    temp &= 0x1;
    if (temp == 0x1) {
      PINCON = (IOCON_TypeDef *)(IOCON_PIO0_BASE + 0x4 * pinnum);
      temreg = PINCON->CON;
      temreg &= ~((uint32_t)3 << 10);
      temreg |= GPIO_OutputMode;
      PINCON->CON = temreg;
    }
  }
}

/**
 * @brief  Writes the specified port.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  PortVal:  specifies the value to be written to the selected port.
 * @retval none.
 */
void GPIO_WritePort(uint32_t PortVal)
{
  GPIO0->DT = PortVal;
}

/**
 * @brief  Reads the specified port all pins data value.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @retval GPIO port all pins data value.
 */
uint32_t GPIO_ReadPort(void)
{
  return (GPIO0->DT);
}

/**
 * @brief  Sets or clears the selected data port bit.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bit to be written.
 *   This parameter can be one of GPIO_Pin_x where x can be (0...18).
 * @param  BitVal: specifies the value to be written to the selected bit.
 *   This parameter can be one of the BitAction enum values:
 *     @arg Bit_RESET: to clear the port pin
 *     @arg Bit_SET: to set the port pin
 * @retval None
 */
void GPIO_WritePin(uint32_t GPIO_Pin, BitAction BitVal)
{
  /* Check the parameters */
  assert_param(IS_GET_GPIO_PIN(GPIO_Pin));
  assert_param(IS_GPIO_BIT_ACTION(BitVal));

  if (BitVal != Bit_RESET) {
    GPIO0->DT_SET = GPIO_Pin;
  } else {
    GPIO0->DT_CLR = GPIO_Pin;
  }
}

/**
 * @brief  Reads the specified port bit data value.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin:  specifies the port bit to read.
 *   This parameter can be GPIO_Pin_x where x can be (0...18).
 * @retval The specified port pin value.
 */
uint8_t GPIO_ReadPin(uint32_t GPIO_Pin)
{
  uint8_t bitstatus = 0x00;
  /* Check the parameters */
  assert_param(IS_GET_GPIO_PIN(GPIO_Pin));

  if ((GPIO0->DT & GPIO_Pin) != (uint32_t)Bit_RESET) {
    bitstatus = (uint8_t)Bit_SET;
  } else {
    bitstatus = (uint8_t)Bit_RESET;
  }
  return bitstatus;
}

/**
 * @brief  Sets the selected data port bits.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @retval None
 */
void GPIO_SetPin(uint32_t GPIO_Pin)
{
  /* Check the parameters */
  assert_param(IS_GPIO_PIN(GPIO_Pin));
  GPIO0->DT_SET = GPIO_Pin;
}

/**
 * @brief  Clears the selected data port bits.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @retval None
 */
void GPIO_ClearPin(uint32_t GPIO_Pin)
{
  assert_param(IS_GPIO_PIN(GPIO_Pin));
  GPIO0->DT_CLR = GPIO_Pin;
}

/**
 * @brief  Toggle the selected data port bits.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bits to be written.
 * @retval None
 */
void GPIO_TogglePin(uint32_t GPIO_Pin)
{
  assert_param(IS_GPIO_PIN(GPIO_Pin));
  GPIO0->DT_TOG = GPIO_Pin;
}

/**
 * @brief  Masks the selected port pin data register bits.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bit to be written.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @retval None
 */
void GPIO_PinMask(uint32_t GPIO_Pin)
{
  /* Check the parameters */
  assert_param(IS_GPIO_PIN(GPIO_Pin));

  /* Set mask bit */
  GPIO0_IT->DATAMASK_SET = GPIO_Pin;
}

/**
 * @brief  Unmasks the selected port pin data register bits.
 * @param  GPIOx: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin: specifies the port bit to be written.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @retval None
 */
void GPIO_PinUnmask(uint32_t GPIO_Pin)
{
  /* Check the parameters */
  assert_param(IS_GPIO_PIN(GPIO_Pin));

  /* Clear mask bit */
  GPIO0_IT->DATAMASK_CLR = GPIO_Pin;
}

/**
 * @brief  Enables edge IT on the specified port pins.
 * @param  GPIOx_IT: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin:  specifies the port bit to set.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @param  GPIO_Irq: specifies the irq type.
 *   This parameter can be one of the EdgeAction enum values:
 *     @arg GPIO_IRQ_EDGE_FALLING: to select falling edge trigger.
 *     @arg GPIO_IRQ_EDGE_RISING: to select rising edge trigger.
 *     @arg GPIO_IRQ_EDGE_DOUBLE: to select double edge trigger.
 * @retval none.
 */
void GPIO_EdgeITEnable(uint32_t GPIO_Pin, EdgeAction GPIO_Irq)
{
  /* Check the parameters */
  assert_param(IS_GPIO_PIN(GPIO_Pin));
  assert_param(IS_GPIO_EDGE_ACTION(GPIO_Irq));

  GPIO0_IT->IS_CLR = GPIO_Pin; /* Clear IS register bits,select edge trigger */

  if (GPIO_Irq == GPIO_IRQ_EDGE_FALLING) {
    GPIO0_IT->IEV_CLR = GPIO_Pin; /* Choose falling edge */
  } else if (GPIO_Irq == GPIO_IRQ_EDGE_RISING) {
    GPIO0_IT->IEV_SET = GPIO_Pin; /* Choose rising edge */
  } else if (GPIO_Irq == GPIO_IRQ_EDGE_DOUBLE) {
    GPIO0_IT->IBE_SET = GPIO_Pin; /* Choose double edge */
  }
  GPIO0_IT->IC_SET = GPIO_Pin;
  GPIO0_IT->IE_SET = GPIO_Pin; /* Unmask IRQ */
}

/**
 * @brief  Enables level IT on the specified port pins.
 * @param  GPIOx_IT: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin:  specifies the port bit to set.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @param  GPIO_Irq: specifies the irq type.
 *   This parameter can be one of the LevelAction enum values:
 *     @arg GPIO_IRQ_LEVEL_LOW: to select low level trigger.
 *     @arg GPIO_IRQ_LEVEL_HIGH: to select high level trigger.
 * @retval none.
 */
void GPIO_LevelITEnable(uint32_t GPIO_Pin, LevelAction GPIO_Irq)
{
  /* Check the parameters */
  assert_param(IS_GPIO_PIN(GPIO_Pin));
  assert_param(IS_GPIO_LEVEL_ACTION(GPIO_Irq));

  if (GPIO_Irq == GPIO_IRQ_LEVEL_LOW) {
    GPIO0_IT->IEV_CLR = GPIO_Pin; /* Choose low level */
  } else if (GPIO_Irq == GPIO_IRQ_LEVEL_HIGH) {
    GPIO0_IT->IEV_SET = GPIO_Pin; /* Choose high level */
  }
  GPIO0_IT->IS_SET = GPIO_Pin; /* Set IS register bits,select level trigger */
  GPIO0_IT->IC_SET = GPIO_Pin;
  GPIO0_IT->IE_SET = GPIO_Pin; /* Unmask IRQ */
}

/**
 * @brief  Mask IT on the specified port pins.
 * @param  GPIOx_IT: where x can be (0..3) to select the GPIO peripheral.
 * @param  GPIO_Pin:  specifies the port bit to set.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @retval none.
 */
void GPIO_MaskIT(uint32_t GPIO_Pin)
{
  /* Check the parameters */
  assert_param(IS_GPIO_PIN(GPIO_Pin));

  GPIO0_IT->IE_CLR = GPIO_Pin;
}

/**
 * @brief  Unmask IT on the specified port pins.
 * @param  GPIOx_IT: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin:  specifies the port bit to set.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @retval none.
 */
void GPIO_UnmaskIT(uint32_t GPIO_Pin)
{
  assert_param(IS_GPIO_PIN(GPIO_Pin));

  GPIO0_IT->IE_SET = GPIO_Pin;
}

/**
 * @brief  Clear edge IT on the specified port pins.
 * @param  GPIOx_IT: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin:  specifies the port bit to set.
 *   This parameter can be any combination of GPIO_Pin_x where x can be (0...18).
 * @retval none.
 */
void GPIO_ClearITFlag(uint32_t GPIO_Pin)
{
  /* Check the parameters */
  assert_param(IS_GPIO_PIN(GPIO_Pin));

  GPIO0_IT->IC_SET = GPIO_Pin;
}

/**
 * @brief  Gets the original IT status on the specified port pin.
 * @param  GPIOx_IT: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin:  specifies the port bit to read.
 *   This parameter can be GPIO_Pin_x where x can be (0...18).
 * @retval The original IT status.
 *         @arg  Bit_SET: have interrupt
 *         @arg  Bit_RESET: no interrupt
 */
uint8_t GPIO_GetOriginalITStatus(uint32_t GPIO_Pin)
{
  uint8_t bitstatus = 0x00;
  /* Check the parameters */
  assert_param(IS_GET_GPIO_PIN(GPIO_Pin));

  if ((GPIO0_IT->RIS & GPIO_Pin) != (uint32_t)Bit_RESET) {
    bitstatus = (uint8_t)Bit_SET;
  } else {
    bitstatus = (uint8_t)Bit_RESET;
  }
  return bitstatus;
}

/**
 * @brief  Gets the mask IT status on the specified port pin.
 * @param  GPIOx_IT: where x can be (0) to select the GPIO peripheral.
 * @param  GPIO_Pin:  specifies the port bit to read.
 *   This parameter can be GPIO_Pin_x where x can be (0...18).
 * @retval The mask IT status.
 *         @arg  Bit_SET: have interrupt
 *         @arg  Bit_RESET: no interrupt
 */
uint8_t GPIO_GetMaskITStatus(uint32_t GPIO_Pin)
{
  uint8_t bitstatus = 0x00;
  /* Check the parameters */
  assert_param(IS_GET_GPIO_PIN(GPIO_Pin));

  if ((GPIO0_IT->MIS & GPIO_Pin) != (uint32_t)Bit_RESET) {
    bitstatus = (uint8_t)Bit_SET;
  } else {
    bitstatus = (uint8_t)Bit_RESET;
  }
  return bitstatus;
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/
