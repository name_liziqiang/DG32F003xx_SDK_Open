/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "misc.h"
#include "delay.h"
#include "usart.h"
#include "DG32F003xx_i2c.h"
#include "DG32F003xx_utility_i2c.h"
	  

/* Private define ------------------------------------------------------------*/
#define IIC_SLAVER_ADDRESS  0X55


//LCNT min = 8  actual quantity = LCNT+1
//HCNT min = 6  actual quantity = HCNT+8
//HOLD set 0  actual quantity = 1   set other  actual quantity = HOLD + 5 mast smaller then LCNT
//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
#define IIC_LCNT    115   //I2C_Speedmode_Stand 100k  115  116   400k   25 26
#define IIC_HCNT    116
#define IIC_HOLD    0
#define IIC_TEST_BUFFER_LEN     32
/* Private variables ---------------------------------------------------------*/

static char Slaver_RxBuffer[IIC_TEST_BUFFER_LEN];
static volatile int8_t Slaver_RxLen = 0;
static volatile uint8_t stopCount;
static volatile uint8_t startCount;//Used to count Start_DET 
static volatile uint8_t reqCount;
static volatile uint8_t Slaver_RxFlag;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void IIC_Config_Slaver(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{ 
	Debug_uart_init(115200);
	delay_init(24);	
	printf("start test\r\n");
	IIC_Config_Slaver();
    
	while(1){
		  if(Slaver_RxFlag == 1){
				Slaver_RxFlag = 0;
				printf("\r\n");
				printf("SLAVE Rec len= %d\r\n",Slaver_RxLen);
				printf("SLAVE Rec Data:  \r\n");
				for (int i = 0; i < IIC_TEST_BUFFER_LEN; i++){
				  printf("0x%02x,  ", Slaver_RxBuffer[i]);
				}
				  for (int i = 0; i < IIC_TEST_BUFFER_LEN; i++)Slaver_RxBuffer[i] = 0;
				Slaver_RxLen = 0;

		  }
		  delay_ms(1);
  }
}


/**
 * @brief 
 * 
 * @param I2Cx 
 * @param ioGroup 
 */
static void IIC_Config_Slaver(void)
{
	RCC_ResetAHBCLK(1<<AHBCLK_BIT_I2C0);
	I2C_InitTypeDef I2C_InitStruct;

		//配置复用IO
	GPIO_SetPinMux(GPIO2_0, IO_I2C0_SCL); //I2C_SCL
    GPIO_SetPinMux(GPIO2_1, IO_I2C0_SDA); //I2C_SDA	
	
	I2C_InitStruct.I2C_Mode = I2C_SLAVE_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = I2C_Speedmode_Stand;
	//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
	I2C_InitStruct.I2C_SCL_HCNT = IIC_HCNT;
	I2C_InitStruct.I2C_SCL_LCNT = IIC_LCNT;
	I2C_InitStruct.I2C_SDA_HOLD = 0;

	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
	I2C_InitStruct.I2C_OwnAddress = IIC_SLAVER_ADDRESS;       //casually specify 
	I2C_InitStruct.I2C_HS_MADDR = 0X01;
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
	I2C_InitStruct.I2C_RX_TL = 0;//0 = RX buffer = 1
	I2C_InitStruct.I2C_TX_TL = 0;
  	I2C_Init(I2C0, &I2C_InitStruct);

	I2C_ClearITPendingBit(I2C0);

	/* i2c interrupt request */
	/*Enable I2C interrupt here*/
    //Interrupt configuration
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = I2C0_IRQn;	  
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;  
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_Init(&NVIC_InitStruct);
    I2C_ITConfig(I2C0, I2C_IT_RX_FULL | I2C_IT_RD_REQ | I2C_IT_STOP_DET, ENABLE);
    I2C_Cmd(I2C0, ENABLE, 1);//
}

static uint8_t sendData = 0;
 void I2C0_IRQHandler(void)
{
	__IO unsigned int stat = 0;
	stat = I2C_GetITFlag(I2C0);
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_RD_REQ)){
    //I2C Master Request data
    I2C0->DATA_CMD = sendData & 0x00ff;
    I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_RD_REQ);
  }
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_RX_FULL)){
    //RX BUF full RX_TL 1BYTES
    Slaver_RxBuffer[Slaver_RxLen] = I2C0->DATA_CMD&0xff;
    Slaver_RxLen ++;
    if(Slaver_RxLen >= IIC_TEST_BUFFER_LEN)Slaver_RxLen = 0;
  }
  
    
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_START_DET)){
    //iic start
    I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_START_DET);

  }
  
    if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_STOP_DET)){
    //iic stop
    Slaver_RxFlag = 1;
    I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_STOP_DET);
  }
	 
  
}
