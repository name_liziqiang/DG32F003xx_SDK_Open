
#include "usart.h"	  
#include "DG32F003xx.h"
#include "DG32F003xx_uart.h"
#include "DG32F003xx_gpio.h"




UART_TypeDef *Debug_Uart = NULL;
//////////////////////////////////////////////////////////////////
//加入以下代码,支持printf函数,而不需要选择use MicroLIB
#if 1
/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
	  

PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the UART */
  if(Debug_Uart != 0)UART_SendData(Debug_Uart, (uint8_t)ch);

  /* Loop until the end of transmission */
  if(Debug_Uart != 0)while (UART_GetFlagStatus(Debug_Uart, UART_FLAG_TXFE) == RESET) {}

  return ch;
}
#endif 

 

/**
 * @brief 初始化串口 用于串口debug输出
 * 
 * @param BaudRate 波特率
 */
void Debug_uart_init(uint32_t BaudRate){
	
    UART_InitTypeDef UART_InitStructure;
	Debug_Uart = UART0;
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
	RCC->UART0CLKDIV                = 2;
	GPIO_SetPinMux(GPIO2_0, IO_UART0_TX);
	GPIO_SetPinMux(GPIO0_1, IO_UART0_RX);


	UART_Reset(UART0);
	UART_StructInit(&UART_InitStructure);
	UART_InitStructure.UART_BaudRate   = BaudRate;           // Baud rate
	UART_InitStructure.UART_TXIFLSEL   = UART_TXIFLSEL_14;   // Send FIFO depth   TXFIFO LEVEL
	UART_InitStructure.UART_RXIFLSEL   = UART_RXIFLSEL_14;   // Receive FIFO depth  RXFIFO LEVEL
	UART_InitStructure.UART_IdleNum    = UART_IdleNum_4Byte; // Idle detects word length
	UART_InitStructure.UART_IdleEN     = UART_IdleEN_Enable; // Idle detection enabled
	UART_Init(UART0, &UART_InitStructure);
	/* set timeout to 8 uart clk cycle len */
	UART0->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
	UART0->CTRL0_SET = 8 << 16;

	UART_Cmd(UART0, ENABLE); //使能串口0
}

 

