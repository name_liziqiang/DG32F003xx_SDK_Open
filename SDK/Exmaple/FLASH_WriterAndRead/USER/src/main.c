/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_adc.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_flash.h"
#include "usart.h"
#include "delay.h"
#include "DG32F003xx_utility_flash.h"

/* Private define ------------------------------------------------------------*/
#define  FLASH_ADDR_TEST1  0x0000C800
#define  FLASH_ADDR_TEST2  0x0000CA00
/* Private variables ---------------------------------------------------------*/
uint8_t Write8Buf[64];
uint8_t Read8Buf[64];

uint32_t Write32Buf[64];
uint32_t Read32Buf[64];
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/


/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{
    delay_init(24);

    for (unsigned int i = 0; i < 64; i++)
    {
        Write8Buf[i] = i;
				Write32Buf[i] = i;
    }

    flah_writeData8(FLASH_ADDR_TEST1,Write8Buf,sizeof(Write8Buf));
	flah_readData8(FLASH_ADDR_TEST1,Read8Buf,64);
		
	flah_writeData32(FLASH_ADDR_TEST2,Write32Buf,sizeof(Write32Buf));
	flah_readData32(FLASH_ADDR_TEST2,Read32Buf,64*4);

    while (1)
    {

    }
}

