/**
  ******************************************************************************
  * @file    DG32F003xx_flash.c
  * @author  Alpscale Software Team
  * @version V1.0.0
  * @date    12/20/2013
  * @brief   This file provides all the EFLASH firmware functions.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  */ 

/* Includes ------------------------------------------------------------------*/

#include "stdio.h"
#include "stdlib.h"
#include "DG32F003xx.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_flash.h"

#define EFLASH_MAIN_SECTOR_MAX             (192) 

void Flash_Unlock(void)
{
	FLASH->KEY_MAIN = 0x12345678;
	FLASH->KEY_MAIN = 0x9abcdef0;
}

void Flash_Lock(void)
{
	FLASH->CR |= CR_LOCK_MAIN;
}

//this sector must be different sector from code sector
void Flash_EraseSector(uint32_t sectorIndex)
{
	if(sectorIndex>=EFLASH_MAIN_SECTOR_MAX)
		return;
	uint32_t Eraseaddr=sectorIndex*512;
	uint8_t* pEraseRead = (uint8_t*)Eraseaddr; 
	for(unsigned char i=0;i<2;i++){
		FLASH->CR &= (~CR_NVR);			//0 is normal
		FLASH->CR &= (~CR_CHIP);		//0 is sector
		FLASH->ERASE_NUM = sectorIndex;
		FLASH->CR |= CR_STRT;			//start erase
		while((FLASH->SR&SR_EOE) == 0);	//wait EOE
		FLASH->SR |= SR_EOE;			//clean EOE
		for(uint32_t j=0;j<512;j++){
			if(pEraseRead[j] != 0xFF)
				break;
			if(j==511)
				return;
		}
	}
}

int Flash_Write32(uint32_t adr, uint32_t* buf, uint32_t byteCount)
{
	if(adr%4 !=0){
		printf("Flash_Write32:adr must align 4\r\n");
		return -1;
	}
	if(byteCount%4 !=0){
		printf("Flash_Write32:byteCount must align 4\r\n");
		return -1;
	}
	
	uint8_t* pWriteRead = (uint8_t*)adr;
	for(unsigned char i=0;i<2;i++){
		while(byteCount){
			outl(*buf, adr);

			byteCount -= 4;
			buf++;
			adr+=4;
		}
		for(uint32_t j=0;j<byteCount;j++){
			if(pWriteRead[j] != buf[j])
				break;
			if(j==(byteCount-1))
				return 0;
		}
	}
	return -1; 
}

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/
