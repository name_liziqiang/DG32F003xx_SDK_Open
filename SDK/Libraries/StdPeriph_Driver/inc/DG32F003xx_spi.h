/**
  ******************************************************************************
  * @file    DG32F003xx_SPI.h
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    27-November-2013
  * @brief   This file contains all the functions prototypes for the SPI firmware
  *          library.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, Alpscale SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DG32F003xx_SPI_H
#define __DG32F003xx_SPI_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"

/** @addtogroup DG32F003xx_StdPeriph_Driver
  * @{
  */

/** @addtogroup SPI
  * @{
  */

/** @defgroup SPI_Exported_Types
  * @{
  */

typedef enum {SPI_STD, SPI_SSI, SPI_MICROWIRE} SPIModeSelect;
typedef struct
{
                                  
  uint32_t SPI_CPOL;                /*!< Specifies the serial clock steady state.
                                         This parameter can be a value of @ref SPI_Clock_Polarity */

  uint32_t SPI_CPHA;                /*!< Specifies the clock active edge for the bit capture.
                                         This parameter can be a value of @ref SPI_Clock_Phase */            

  uint32_t SPI_FirstBit;            /*!< Specifies whether data transfers start from MSB or LSB bit.
                                         This parameter can be a value of @ref SPI_MSB_LSB_transmission */

  uint32_t SPI_ClockDiv;              /*!< Specifies the DIVIDE parameter for clock frequency of the SPI controller.
                                                      This even number can be a value between 0x2 and 0xFE. */

  uint32_t SPI_ClockRate;            /*!<Specifies the RATE parameter for clock frequency of the SPI controller.
                        This parameter can be a value between 0x0 and 0xFF.  */
  uint32_t SPI_SlaveMode;             /*!< Specifies the SPI is master mode or slaver mode.
                                             This parameter can be a value of @ref SPI_SLAVE_MODE_Select */
  SPIModeSelect SPI_ModeSelect;     /*!< Specifies the SPI MODE:SPI standard mode/SPI Dual mode/SPI Quad mode. */
  uint32_t SPI_FrameLength;           /*!< Specifies the SPI frame length. 
                                           This parameter can be a value of @ref SPI_Wrod_Length_setting*/
} SPI_InitTypeDef;



typedef struct
{
  uint32_t SPI_DUPLEX;      /*!< Specifies SPI work mode:.Full duplex /half duplex.
                                 This parameter can be a value of @ref SPI_data_Duplex  */

  uint32_t SPI_DataLength;     /*!< Specifies the number of data bytes to be transferred. */
 
  uint32_t SPI_TransferDir;    /*!< Specifies the data transfer direction, whether the transfer is a read or write.
                                             This parameter can be a value of @ref SPI_Transfer_Direction */
} SPI_DataInitTypeDef;


typedef enum {CAP_DLY_NONE=0, CAP_DLY_1CLK=1, CAP_DLY_2CLK=2, CAP_DLY_3CLK=3} SPICapDly;

/**
  * @}
  */



/** @defgroup SPI_PERIPH_CHECK
  * @{
  */
#define IS_SPI_MODE(MODE)     ( (MODE==SPI_STD)||(MODE==SPI_SSI)||(MODE==SPI_MICROWIRE))
/**
  * @}
  */


/** @addtogroup SPI_SLAVE_MODE_Select
  * @{
  */
#define SPI_SLAVE_MODE       ((uint32_t)0x00000100) 
#define SPI_MASTER_MODE      ((uint32_t)0x00000000) 
#define IS_SPI_MASTER_OR_SLAVE_MODE(mode)  (((mode) == SPI_SLAVE_MODE) || \
                                             ((mode) == SPI_MASTER_MODE))
/**
  * @}
  */

/** @defgroup SPI_Hardware_Flow_Control 
  * @{
  */
#define SPI_PowerState_ON                   ((uint32_t)0x00000001)
#define SPI_PowerState_OFF                  ((uint32_t)0x00000000)
#define IS_SPI_POWER_STATE(STATE) (((STATE) == SPI_PowerState_ON) || ((STATE) == SPI_PowerState_OFF)) 
/**
  * @}
  */ 



/** @defgroup SPI_Clock_Polarity 
  * @{
  */
#define SPI_CPOL_Low                    ((uint32_t)0x00000000)
#define SPI_CPOL_High                   ((uint32_t)0x00000200)
#define IS_SPI_CPOL(CPOL) (((CPOL) == SPI_CPOL_Low) || \
                           ((CPOL) == SPI_CPOL_High))
/**
  * @}
  */


/** @defgroup SPI_Clock_Phase 
  * @{
  */
#define SPI_CPHA_1Edge                  ((uint32_t)0x00000000)
#define SPI_CPHA_2Edge                  ((uint32_t)0x00000400)
#define IS_SPI_CPHA(CPHA) (((CPHA) == SPI_CPHA_1Edge) || \
                           ((CPHA) == SPI_CPHA_2Edge))
/**
  * @}
  */


/** @defgroup SPI_MSB_LSB_transmission 
  * @{
  */
#define SPI_FirstBit_MSB                ((uint32_t)0x00000000)
#define SPI_FirstBit_LSB                ((uint32_t)0x00400000)
#define IS_SPI_FIRST_BIT(BIT) (((BIT) == SPI_FirstBit_MSB) || \
                               ((BIT) == SPI_FirstBit_LSB))
/**
  * @}
  */

/** @defgroup SPI_ClockDiv_setting 
  * @{
  */
#define IS_SPI_ClockDiv_setting(DIV) ( (((DIV)&0xFFFF00FF)==0x0)&&((DIV)!=0x0) )
/**
  * @}
  */

/** @defgroup SPI_ClockRate_setting 
  * @{
  */
// #define IS_SPI_ClockRate_setting(RATE) ( (((RATE)&0xFFFFFF00)==0x0)&&((RATE)!=0x0) )
#define IS_SPI_ClockRate_setting(RATE) ( (((RATE)&0xFFFFFF00)==0x0) )
/**
  * @}
  */

/** @addtogroup SPI_Wrod_Length_setting
  * @{
  */
#define SPI_FRAME_LENGTH_8Bit       ((uint32_t)0x00000038)
#define SPI_FRAME_LENGTH_16Bit      ((uint32_t)0x00000078)
#define SPI_FRAME_LENGTH_24Bit      ((uint32_t)0x000000b8)
#define SPI_FRAME_LENGTH_32Bit      ((uint32_t)0x000000f8)
#define IS_SPI_FRAME_LENGTH(wl)     (((wl) == SPI_FRAME_LENGTH_8Bit) || \
                                     ((wl) == SPI_FRAME_LENGTH_16Bit) || \
                                     ((wl) == SPI_FRAME_LENGTH_24Bit) || \
                                     ((wl) == SPI_FRAME_LENGTH_32Bit))
/**
  * @}
  */



/** @defgroup SPI_data_Duplex 
  * @{
  */  
#define SPI_FullDuplex  ((uint32_t)0x00000000)
#define SPI_HalfDuplex  ((uint32_t)0x10000000)
#define IS_SPI_Duplex(DIRECTION) (((DIRECTION) == SPI_FullDuplex) || \
                                         ((DIRECTION) == SPI_HalfDuplex))
/**
  * @}
  */


/** @defgroup SPI_Transfer_Direction 
  * @{
  */  
#define SPI_Transfer_Write  ((uint32_t)0x00000000)
#define SPI_Transfer_Read  ((uint32_t)0x04000000)
#define IS_SPI_SPI_Direction(DIRECTION) (((DIRECTION) == SPI_Transfer_Write) || \
                                         ((DIRECTION) == SPI_Transfer_Read))
/**
  * @}
  */


/** @defgroup SPI_Interrupt_soucres 
  * @{
  */
#define SPI_IT_RXOVEFLW                   ((uint32_t)0x00008000)
#define SPI_IT_RXTIMEOUT          ((uint32_t)0x00020000)
#define SPI_IT_RXFIFOHF          ((uint32_t)0x00080000)
#define SPI_IT_TXFIFOHE          ((uint32_t)0x00200000)


#define IS_SPI_IT(IT) ( ((IT) == SPI_IT_RXOVEFLW) ||((IT) == SPI_IT_RXTIMEOUT) ||((IT) == SPI_IT_RXFIFOHF) ||((IT) == SPI_IT_TXFIFOHE) )

#define IS_SPI_GET_IT(IT) ( ((IT) == SPI_IT_RXOVEFLW) ||((IT) == SPI_IT_RXTIMEOUT) ||((IT) == SPI_IT_RXFIFOHF) ||((IT) == SPI_IT_TXFIFOHE) )
                            
#define IS_SPI_CLEAR_IT(IT) ( ((IT) == SPI_IT_RXOVEFLW) ||((IT) == SPI_IT_RXTIMEOUT) ||((IT) == SPI_IT_RXFIFOHF) ||((IT) == SPI_IT_TXFIFOHE) )
/**
  * @}
  */ 


/** @defgroup SPI_Status_flag 
  * @{
  */  
#define IS_SPI_STATUS_FLAG(FLAG) ( (FLAG)&0x0ff00ff1 )
/**
  * @}
  */ 



/** @defgroup SPI_Exported_Functions
  * @{
  */
void SPI_SetPowerState(SPI_TypeDef* SPIx, uint32_t SPI_PowerState);
void SPI_HwInit(SPI_TypeDef* SPIx, SPI_InitTypeDef* SPI_InitStruct);
void SPI_ModeSet(SPI_TypeDef* SPIptr, SPIModeSelect ModeSelect);
uint32_t SPI_GetPowerState(SPI_TypeDef* SPIx);
void SPI_ITConfig(SPI_TypeDef* SPIx, uint32_t SPI_IT, FunctionalState NewState);
void SPI_CS_Low(SPI_TypeDef* SPIx);
void SPI_CS_High(SPI_TypeDef* SPIx);
void SPI_DataConfig(SPI_TypeDef* SPIx, SPI_DataInitTypeDef* SPI_DataInitStruct);
uint32_t SPI_ReadData(SPI_TypeDef* SPIx);
void SPI_WriteData(SPI_TypeDef* SPIx, uint32_t Data);
void SPI_FirstBitSet(SPI_TypeDef *SPIptr, uint32_t SPI_FirstBit);
FlagStatus SPI_GetFlagStatus(SPI_TypeDef* SPIx, uint32_t SPI_FLAG);        
ITStatus SPI_GetITStatus(SPI_TypeDef* SPIx, uint32_t SPI_IT);
void SPI_ClearITPendingBit(SPI_TypeDef* SPIx, uint32_t SPI_IT);
void SPI_FrameLengthSet(SPI_TypeDef* SPIptr, uint8_t wl);



#ifdef __cplusplus
}
#endif

#endif /* __DG32F003xx_SPI_H */
/**
  * @
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/
