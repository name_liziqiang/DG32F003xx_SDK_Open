#include "DG32F003xx_utility_iic.h"
#include "DG32F003xx_gpio.h"
#include "string.h"


#define TIMEOUT               0x400000

#define I2C_DEBUG             1
#if I2C_DEBUG
  #include "stdio.h"
  #define i2c_debug(x)        printf("i2c error: %s\r\n", x);
#else
  #define i2c_debug(x)
#endif

I2C_IRQFunction  I2C0_IRQFunction = 0;

/**
 * @brief init I2C pinmux
 * 
 * @param I2Cx 
 * @param i2c_io I2C pins see: enum I2C_GPIO
 */
void I2C_PinsInit(I2C_TypeDef *I2Cx, I2C_GPIO i2c_io)
{
  if(I2Cx == I2C0){
    switch (i2c_io) {
      case I2C_GP0_0:
        GPIO_SetPinMux(GPIO_Pin_0, IO_I2C0_SDA); //gp0_0 sda
        GPIO_SetPinMux(GPIO_Pin_18, IO_I2C0_SCL); //gp0_1 scl
				GPIO_ConfigPull(GPIO_Pin_0, GPIO_PULL_UP);
				GPIO_ConfigPull(GPIO_Pin_18, GPIO_PULL_UP);
        break;      
      case I2C_GP0_1:
        GPIO_SetPinMux(GPIO_Pin_1, IO_I2C0_SDA); //gp0_0 sda
        GPIO_SetPinMux(GPIO_Pin_2, IO_I2C0_SCL); //gp0_1 scl
				GPIO_ConfigPull(GPIO_Pin_1, GPIO_PULL_UP);
				GPIO_ConfigPull(GPIO_Pin_2, GPIO_PULL_UP);
        break;
      case I2C_GP0_5:
        GPIO_SetPinMux(GPIO_Pin_5, IO_I2C0_SDA); //gp0_3 sda
        GPIO_SetPinMux(GPIO_Pin_6, IO_I2C0_SCL); //gp0_4 scl
				GPIO_ConfigPull(GPIO_Pin_5, GPIO_PULL_UP);
				GPIO_ConfigPull(GPIO_Pin_6, GPIO_PULL_UP);
        break;
      case I2C_GP0_8:
        GPIO_SetPinMux(GPIO_Pin_7, IO_I2C0_SDA); //gp7_4 sda
        GPIO_SetPinMux(GPIO_Pin_8, IO_I2C0_SCL); //gp7_5 scl
				GPIO_ConfigPull(GPIO_Pin_7, GPIO_PULL_UP);
				GPIO_ConfigPull(GPIO_Pin_8, GPIO_PULL_UP);
        break;
      default:
        break;
    }
	}
}
/**
 * @brief init I2C controller
 * 
 * @param I2Cx 
 * @param I2C_InitStruct I2C work parameters
 * @param i2c_io I2C pins
 * @return int 0 = ok  other = error
 */
int I2C_UtilityInit(I2C_TypeDef* I2Cx, I2C_InitTypeDef* I2C_InitStruct, I2C_GPIO i2c_io)
{
  I2C_PinsInit(I2Cx, i2c_io);
	I2C_Reset(I2Cx);
  return I2C_Init(I2Cx, I2C_InitStruct);
}
/**
 * @brief Regist irq handler Function  to   i2c
 * 
 * @param I2Cx wihtch i2c?
 * @param function irq handler
 */
void I2C_RegistIRQHandler(I2C_TypeDef* I2Cx, I2C_IRQFunction function)
{
  if(I2Cx == I2C0)I2C0_IRQFunction = function;
}
/** 
  * @brief  read bytes in pio mode
  * @param  dev_addr: device address on i2c bus
  *         internal_addr: i2c device internal address to write into
  *         addr_size: internal address byte length  0 = no internal address
  *         buf: mem to store read data
  *         read_size: read size in bytes
  * @retval 0 = success
  *         -1= failed
  */
int I2C_Read(I2C_TypeDef* I2Cx, uint8_t dev_addr, uint32_t internal_addr, int addr_size, uint8_t *buf,  int read_size)
{
  int i;
  uint32_t timeout;
  uint32_t temreg;
  uint32_t len, readCount;
  uint8_t *recBuf;
  
  /*disable I2C module*/
  timeout = TIMEOUT;
  I2Cx->ENABLE = 0;
  while ((I2Cx->STAT & I2C_IC_STATUS_ACTIVITY) || (I2Cx->ENABLE_STATUS & I2C_IC_ENABLE_STATUS_IC_EN)){
    I2Cx->ENABLE = 0;
    timeout--;
    if(timeout == 0){
      i2c_debug("init failed");
      return -1;
    }
  }
  
  I2Cx->CON |= 1 << 5;  //restart enable
  
  temreg = I2Cx->TAR;
  temreg &= ~0x000003ff;
  temreg |= 1 << 10; //generate START BYTE
  temreg |= dev_addr;
  I2Cx->TAR = temreg;
  
  I2Cx->TX_TL = 0;
  I2Cx->RX_TL = 0;
  I2Cx->ENABLE = 1;
  
  /*send device internal addr*/
  if(addr_size != 0){
    for (i = addr_size - 1; i >= 0; i--){
      timeout = TIMEOUT;
      I2C_SendData(I2Cx, (internal_addr >> i*8) & 0x00ff);
      while (((I2Cx->STAT & (1 << 2)) == 0) && timeout)timeout--;
      if (timeout == 0){
        i2c_debug("send internal address timeout");
        I2Cx->ENABLE = 0;
        return -1;
      }
    }
  }
  //read data
  len = read_size;
  readCount = 0;
	recBuf = buf;
  timeout = TIMEOUT;
  while(timeout != 0){
    while((len != 0) && ((I2Cx->STAT & (1 << 1)) != 0)){
      //TX FIFO is not full
      I2C_TriggerRecData(I2Cx);
      len--;
    }
    if ((I2Cx->STAT & (1 << 3)) != 0){
      //rx fifo is not empty
      *recBuf = I2C_ReceiveData(I2Cx);
      recBuf++;
      readCount++;
    }
    if(readCount == read_size)break;
    timeout --;
  }
  if (timeout == 0){
    i2c_debug("read data timeout");
    I2Cx->ENABLE = 0;
    return -1;
  }
  return 0;
}
/** 
  * @brief  read bytes in irq mode
  * @param  dev_addr: device address on i2c bus
  *         internal_addr: i2c device internal address to write into
  *         addr_size: internal address byte length  0 = no internal address
  *         buf: mem to store read data
  *         read_size: read size in bytes
  * @retval 0 = success
  *         -1= failed
  */
int I2C_ReadIRQ(I2C_TypeDef* I2Cx, uint8_t dev_addr, uint32_t internal_addr, int addr_size)
{
  int i;
  uint32_t timeout;
  uint32_t temreg;
  
  /*disable I2C module*/
  timeout = TIMEOUT;
  I2Cx->ENABLE = 0;
  while ((I2Cx->STAT & I2C_IC_STATUS_ACTIVITY) || (I2Cx->ENABLE_STATUS & I2C_IC_ENABLE_STATUS_IC_EN)){
    I2Cx->ENABLE = 0;
    timeout--;
    if(timeout == 0){
      i2c_debug("init failed");
      return -1;
    }
  }
  
  I2Cx->CON |= 1 << 5;  //restart enable
  
  temreg = I2Cx->TAR;
  temreg &= ~0x000003ff;
  temreg |= 1 << 10; //generate START BYTE
  temreg |= dev_addr;
  I2Cx->TAR = temreg;
  
  I2Cx->TX_TL = I2C_TX_FIFO_SIZE;
  I2Cx->RX_TL = 0;
  I2Cx->ENABLE = 1;
  
  /*send device internal addr*/
  if(addr_size != 0){
    for (i = addr_size - 1; i >= 0; i--){
      timeout = TIMEOUT;
      I2C_SendData(I2Cx, (internal_addr >> i*8) & 0x00ff);
      while (((I2Cx->STAT & (1 << 2)) == 0) && timeout)timeout--;
      if (timeout == 0){
        i2c_debug("send internal address timeout");
        I2Cx->ENABLE = 0;
        return -1;
      }
    }
  }
  I2C_ITConfig(I2Cx, I2C_IT_RX_FULL | I2C_IT_TX_EMPTY, ENABLE);
  return 0;
}
/** 
  * @brief  write bytes in pio mode
  * @param  dev_addr: device 7bit address on i2c bus
  *         internal_addr: i2c device internal address to write into   0 = no internal address
  *         addr_size: internal address byte length
  *         buf: written data address
  *         write_size: write size in bytes
  * @retval 0 = success
  *         -1= failed
  */
int I2C_Write(I2C_TypeDef* I2Cx, uint8_t dev_addr, uint32_t internal_addr, int addr_size, uint8_t *buf,  int write_size)
{
  int i;
  uint32_t timeout;
  uint32_t temreg;
  uint32_t SpeedMode;
  uint32_t len;
  
  /*disable I2C module*/
  timeout = TIMEOUT;
  I2Cx->ENABLE = 0;
  while ((I2Cx->STAT & I2C_IC_STATUS_ACTIVITY) || (I2Cx->ENABLE_STATUS & I2C_IC_ENABLE_STATUS_IC_EN)){
    I2Cx->ENABLE = 0;
    timeout--;
    if(timeout == 0){
      i2c_debug("init failed");
      return -1;
    }
  }
  
  SpeedMode = I2Cx->CON;
  SpeedMode = (SpeedMode>>1) & 0X00000003;
  //if work in SpeedHigh  the restart signal mast enbale
  if(SpeedMode == 3)I2Cx->CON |= 1 << 5;  //restart enable
  else I2Cx->CON &= ~(1 << 5);  //restart disable
  
  temreg = I2Cx->TAR;
  temreg &= ~0x000003ff;
  temreg |= 1 << 10; //generate START BYTE
  temreg |= dev_addr;
  I2Cx->TAR = temreg;
  I2Cx->TX_TL = 0;
  I2Cx->RX_TL = 0;
  I2Cx->ENABLE = 1;
  
  /*send device internal addr*/
  if(addr_size != 0){
    for (i = addr_size - 1; i >= 0; i--){
      timeout = TIMEOUT;
      I2C_SendData(I2Cx, (internal_addr >> i*8) & 0x00ff);
      while (((I2Cx->STAT & (1 << 2)) == 0) && timeout)timeout--;
      if (timeout == 0){
        i2c_debug("send internal address failed");
        return -1;
      }
    }
  }
  //write data
  len = write_size;
  timeout = TIMEOUT;
  
  while(1){
    while((len != 0) && ((I2Cx->STAT & (1 << 1)) != 0)){
      //TX FIFO is not full
      I2C_SendData(I2Cx, (*buf++) & 0x00ff);
      len--;
    }
    if(len == 0)break;
    // while((I2Cx->STAT & (1 << 1)) == 0);//wait TX FIFO is not full
  }
  timeout = TIMEOUT;
  while (((I2Cx->STAT & (1 << 2)) == 0) && timeout)timeout--;//wait TX FIFO is empty
  if (timeout == 0){
    i2c_debug("wait data write end timeout");
    return -1;
  }
  return 0;
}
/** 
  * @brief  write bytes in pio mode
  * @param  dev_addr: device 7bit address on i2c bus
  *         internal_addr: i2c device internal address to write into   0 = no internal address
  *         addr_size: internal address byte length
  *         buf: written data address
  *         write_size: write size in bytes
  * @retval 0 = success
  *         -1= failed
  */
int I2C_WriteIRQ(I2C_TypeDef* I2Cx, uint8_t dev_addr, uint32_t internal_addr, int addr_size)
{
  int i;
  uint32_t timeout;
  uint32_t temreg;
  uint32_t SpeedMode;
  
  /*disable I2C module*/
  timeout = TIMEOUT;
  I2Cx->ENABLE = 0;
  while ((I2Cx->STAT & I2C_IC_STATUS_ACTIVITY) || (I2Cx->ENABLE_STATUS & I2C_IC_ENABLE_STATUS_IC_EN)){
    I2Cx->ENABLE = 0;
    timeout--;
    if(timeout == 0){
      i2c_debug("init failed");
      return -1;
    }
  }
  
  SpeedMode = I2Cx->CON;
  SpeedMode = (SpeedMode>>1) & 0X00000003;
  //if work in SpeedHigh  the restart signal mast enbale
  if(SpeedMode == 3)I2Cx->CON |= 1 << 5;  //restart enable
  else I2Cx->CON &= ~(1 << 5);  //restart disable
  
  temreg = I2Cx->TAR;
  temreg &= ~0x000003ff;
  temreg |= 1 << 10; //generate START BYTE
  temreg |= dev_addr;
  I2Cx->TAR = temreg;
  I2Cx->TX_TL = I2C_TX_FIFO_SIZE;
  I2Cx->RX_TL = 0;
  I2Cx->ENABLE = 1;
  
  /*send device internal addr*/
  if(addr_size != 0){
    for (i = addr_size - 1; i >= 0; i--){
      timeout = TIMEOUT;
      I2C_SendData(I2Cx, (internal_addr >> i*8) & 0x00ff);
      while (((I2Cx->STAT & (1 << 2)) == 0) && timeout)timeout--;
      if (timeout == 0){
        i2c_debug("send internal address failed");
        return -1;
      }
    }
  }
  //write data
  I2C_ITConfig(I2Cx, I2C_IT_TX_EMPTY, ENABLE);
  return 0;
}
/**
 * @brief I2C0 IRQ handler
 * 
 */
void I2C0_IRQHandler(void)
{
  if(I2C0_IRQFunction != 0)I2C0_IRQFunction();
  // I2C_ClearITPendingBit(I2C0);
  I2C0->CLR_INTR = 0;
}


