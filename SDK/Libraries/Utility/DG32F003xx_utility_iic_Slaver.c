#include "DG32F003xx_utility_iic_Slaver.h"
#include "string.h"
#include "stdio.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_uart.h"
#include "DG32F003xx_utility_usart.h"
#include "stdio.h"
#include "stdlib.h"

#define IIC_SLAVE 
#define I2C_IRQ_ENABLE      				
#define IIC_SPEED_MODE  		 		I2C_Speedmode_FAST 
#define IIC_Use_10bits_Addr
#define IIC_SLAVER_ADDRESS  0X55

#define EEPROM_DEVADDR  				(0xa0 >> 1)
#define DEV_ADDR_SIZE           1

//LCNT min = 8  actual quantity = LCNT+1
//HCNT min = 6  actual quantity = HCNT+8
//HOLD set 0  actual quantity = 1   set other  actual quantity = HOLD + 5 mast smaller then LCNT
//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
#define IIC_LCNT    25   //I2C_Speedmode_Stand 100k  115  116   400k   25 26
#define IIC_HCNT    26
#define IIC_HOLD    0





#define BYTENUM  16
//#define SIM_FLAG 0x4000fffc


#define  IIC_TEST_BUFFER_LEN     32
static char Slaver_RxBuffer[IIC_TEST_BUFFER_LEN];
static volatile int8_t Slaver_RxLen = 0;
static volatile uint8_t stopCount;
static volatile uint8_t startCount;//Used to count Start_DET 
static volatile uint8_t reqCount;
static volatile uint8_t Slaver_RxFlag;





/**
 * @brief delay function
 * 
 * @param ms 
 */
static void IIC_TestDelay(uint32_t ms)
{
  uint32_t i, j;
  for(i=0; i < ms; i++){
    for(j=0; j<100; j++){
      __nop();
    }
  }
}

#ifdef I2C_IRQ_ENABLE
static uint8_t sendData = 0;
 void I2C0_IRQHandler(void)
{
	__IO unsigned int stat = 0;
	stat = I2C_GetITFlag(I2C0);
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_RD_REQ)){
    //I2C Master Request data
    I2C0->DATA_CMD = sendData & 0x00ff;
    sendData++;
    reqCount++;
    I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_RD_REQ);
  }
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_RX_FULL)){
    //RX BUF full RX_TL 1BYTES
    Slaver_RxBuffer[Slaver_RxLen] = I2C0->DATA_CMD&0xff;
    Slaver_RxLen ++;
    if(Slaver_RxLen >= IIC_TEST_BUFFER_LEN)Slaver_RxLen = 0;
  }
  
    
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_START_DET)){
    //iic start
    I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_START_DET);
	startCount++;

  }
  
    if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_STOP_DET)){
    //iic stop
    Slaver_RxFlag = 1;
	stopCount++;
    I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_STOP_DET);
  }
	 
  
}

#endif
/**
 * @brief 
 * 
 * @param I2Cx 
 * @param ioGroup 
 */
static void IIC_Config_Slaver(void)
{
	RCC_ResetAHBCLK(1<<AHBCLK_BIT_I2C0);
	I2C_InitTypeDef I2C_InitStruct;

	I2C_InitStruct.I2C_Mode = I2C_SLAVE_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = IIC_SPEED_MODE;
	//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
	I2C_InitStruct.I2C_SCL_HCNT = IIC_HCNT;
	I2C_InitStruct.I2C_SCL_LCNT = IIC_LCNT;
	I2C_InitStruct.I2C_SDA_HOLD = 0;
#ifndef IIC_Use_10bits_Addr
	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
#else
  I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_10_BITADDR_SLAVE;
#endif
	I2C_InitStruct.I2C_OwnAddress = IIC_SLAVER_ADDRESS;       //casually specify 
	I2C_InitStruct.I2C_HS_MADDR = 0X01;
#ifndef IIC_Use_10bits_Addr
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
#else
  I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_10_BITADDR_MASTER;
#endif
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
	I2C_InitStruct.I2C_RX_TL = 0;//0 = RX buffer = 1
	I2C_InitStruct.I2C_TX_TL = 0;
  	I2C_Init(I2C0, &I2C_InitStruct);

  I2C_ClearITPendingBit(I2C0);

	/* i2c interrupt request */
#ifdef I2C_IRQ_ENABLE
	/*Enable I2C interrupt here*/
    //Interrupt configuration
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = I2C0_IRQn;	  
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;  
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_Init(&NVIC_InitStruct);
#endif
    I2C_ITConfig(I2C0, I2C_IT_RX_FULL | I2C_IT_RD_REQ | I2C_IT_STOP_DET, ENABLE);
    I2C_Cmd(I2C0, ENABLE, 1);//
}
/**
 * @brief 
 * 
 */
void I2C_TestSlave(void)
{
  GPIO_SetPinMux(GPIO_Pin_2, IO_GPIO);//tim mark signal
  GPIO_SetPinDir(GPIO_Pin_2, GPIO_Mode_OUT); 
  GPIO_ClearPin(GPIO_Pin_2);
	
  GPIO_SetPinMux(GPIO_Pin_0, IO_I2C0_SCL); //I2C_SCL
  GPIO_SetPinMux(GPIO_Pin_1, IO_I2C0_SDA); //I2C_SDA	
  IIC_Config_Slaver();
	
  while(1){
	  
	  if(Slaver_RxFlag == 1){
		  Slaver_RxFlag = 0;
			printf("\r\n");
			printf("startCount = %d,  stopCount = %d, reqCount = %d\r\n",startCount, stopCount, reqCount);
		    printf("SLAVE Rec Data:  \r\n");
				for (int i = 0; i < IIC_TEST_BUFFER_LEN; i++){
				  printf("0x%02x,  ", Slaver_RxBuffer[i]);
				}
			  for (int i = 0; i < IIC_TEST_BUFFER_LEN; i++)Slaver_RxBuffer[i] = 0;
			Slaver_RxLen = 0;
			startCount = 0;
			stopCount = 0;
			reqCount = 0;
	  }
      IIC_TestDelay(10);
  }
}


