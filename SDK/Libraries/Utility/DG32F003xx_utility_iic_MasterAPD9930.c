/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "DG32F003xx_i2c.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_utility_i2c.h"
#include "string.h"
#include "stdio.h"






static __align(32) uint8_t readBuf[32];
static __align(32) uint8_t writeBuf[32];

#define APD9930_DEVADDR  				(0x39)
#define DEV_ADDR_SIZE           1

//LCNT min = 8  actual quantity = LCNT+1
//HCNT min = 6  actual quantity = HCNT+8
//HOLD set 0  actual quantity = 1   set other  actual quantity = HOLD + 5 mast smaller then LCNT
#define IIC_LCNT    25   //I2C_Speedmode_Stand 100k  115  116   400k   25 26
#define IIC_HCNT    26
#define IIC_HOLD    0


/**
 * @brief delay function
 * 
 * @param ms 
 */
static void IIC_TestDelay(uint32_t ms)
{
  uint32_t i, j;
  for(i=0; i < ms; i++){
    for(j=0; j<100; j++){
      __nop();
    }
  }
}
/**
 * @brief 
 * 
 * @param internal_addr 
 * @param buf 
 * @param write_size 
 */
void WriteRegData(uint32_t internal_addr,uint8_t *buf,int write_size)
{

	I2C_Write(APD9930_DEVADDR, internal_addr|0x80, DEV_ADDR_SIZE, buf, write_size);

	IIC_TestDelay(100);
}
/**
 * @brief 
 * 
 * @param internal_addr 
 * @param buf 
 * @param read_size 
 * @return uint16_t 
 */
uint16_t Read_Word(uint32_t internal_addr, uint8_t *buf, int read_size)
{
	I2C_Read(APD9930_DEVADDR, internal_addr|0xA0, DEV_ADDR_SIZE, buf, read_size);
	IIC_TestDelay(100);
	return (uint16_t)((buf[1]<<8) + buf[0]);
}
/**
 * @brief 
 * 
 * @param I2Cx 
 * @param ioGroup 
 */
static void IIC_Config_Master(void )
{
	I2C_InitTypeDef I2C_InitStruct;

	I2C_InitStruct.I2C_Mode = I2C_MASTER_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = I2C_Speedmode_FAST;
	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
	I2C_InitStruct.I2C_OwnAddress = 0x73;       //casually specify 
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
	I2C_InitStruct.I2C_SCL_HCNT = IIC_HCNT;
	I2C_InitStruct.I2C_SCL_LCNT = IIC_LCNT;
	I2C_InitStruct.I2C_SDA_HOLD = IIC_HOLD;

	I2C_InitStruct.I2C_HS_MADDR = 12;
	I2C_InitStruct.I2C_RX_TL = 0;
	I2C_InitStruct.I2C_TX_TL = 0;
    
	I2C_Reset(I2C0);

	I2C_Init(I2C0, &I2C_InitStruct);
}
/**
 * @brief 
 * 
 */
void I2C_TestReadAPD9930(void)
{
	uint8_t PERIVE,PDIODE,PGAIN,AGAIN;
	uint8_t WEN,PEN,AEN,PON;
	int CH0_data,CH1_data,PROX_data;
	
  GPIO_SetPinMux(GPIO_Pin_0, IO_I2C0_SCL); //I2C_SCL
  GPIO_SetPinMux(GPIO_Pin_1, IO_I2C0_SDA); //I2C_SDA
	IIC_Config_Master();
	
	writeBuf[0] = 0x00;
	writeBuf[1] = 0xff;
	writeBuf[2] = 0xff;
	writeBuf[3] = 0xff;
	WriteRegData(0, writeBuf, 4);
	writeBuf[0] = 1;	
	WriteRegData(0x0e, writeBuf, 1);	
	
	PERIVE = 0;
	PDIODE = 0x20;
	PGAIN = 0;
	AGAIN = 0;
	writeBuf[0] = PERIVE | PDIODE | PGAIN | AGAIN;
	WriteRegData(0x0f, writeBuf, 1);
	
	WEN = 8;
	PEN = 4;
	AEN = 2;
	PON = 1; 
	writeBuf[0]=WEN | PEN | AEN | PON;
	WriteRegData(0, writeBuf, 1);
	IIC_TestDelay(600);

	while(1){
		memset(readBuf, 0, sizeof(readBuf));
		Read_Word(0x14, readBuf, 6);
		CH0_data = (readBuf[1] << 8) + readBuf[0];
		CH1_data = (readBuf[3] << 8) + readBuf[2];
		PROX_data = (readBuf[5] << 8) + readBuf[4];
		printf("\r\nCH0_data = %d\r\nCH1_data = %d\r\nPROX_data = %d\r\n",CH0_data,CH1_data,PROX_data);
		IIC_TestDelay(60000);
	}
}
