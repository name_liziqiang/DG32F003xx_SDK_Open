================================================================================
                                样例使用说明
================================================================================
功能描述：
本样例主要展示深度休眠功能。

================================================================================
测试环境：
测试用板：DG32F003_STK
MDK版本： 5.20
================================================================================
使用步骤：
1、下载程序进入深度休眠

================================================================================
注意事项：
没有用到的IO配置上拉或者下拉

================================================================================