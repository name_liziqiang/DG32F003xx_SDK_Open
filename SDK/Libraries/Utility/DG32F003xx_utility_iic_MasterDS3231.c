/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "DG32F003xx_utility_iic_MasterDS3231.h"

#include "DG32F003xx_gpio.h"

static uint8_t twdata[7]={0x58,0x59,0x23,0x04,0x31,0x12,0x20};//Initialization time  second,minute,hour,week,date,month,year
static uint8_t trdata[7];//Time data read
static int asc[7];//Time data--ASCII

#define I2C_IRQ_ENABLE

#define DS3231_DEVADDR  				0xD0>>1 
#define DEV_ADDR_SIZE           1
//LCNT min = 8  actual quantity = LCNT+1
//HCNT min = 6  actual quantity = HCNT+8
//HOLD set 0  actual quantity = 1   set other  actual quantity = HOLD + 5��mast smaller then LCNT
//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
#define IIC_LCNT    8
#define IIC_HCNT    6
#define IIC_HOLD    0

//bcd to ascii
static void Bcd2asc(uint8_t* bcd)
{
	uint8_t i,j;
	for(j=0,i=0;i<7;i++){
		asc[j]=(bcd[i]>>4)*10;//second minute hour week dat month year
		asc[j]+=(bcd[i]&0x0f);
		j++;
	}
}


static void delay(uint16_t us)
{
  int i;
  for (; us > 0; us--)
    for (i = 0; i < 100; i++);
}

//Mask independent bits of time data
static void datajust(void)
{
	trdata[0] = trdata[0] & 0x7F;
	trdata[1] = trdata[1] & 0x7F;
	trdata[2] = trdata[2] & 0x3F;
	trdata[3] = trdata[3] & 0x07;
	trdata[4] = trdata[4] & 0x3F;
	trdata[5] = trdata[5] & 0x1F;
	trdata[6] = trdata[6] & 0xFF;
}

static void IIC_Config_Master(void)
{
#ifdef I2C_IRQ_ENABLE
  NVIC_InitTypeDef NVIC_InitStructure;
#endif
	I2C_InitTypeDef I2C_InitStruct;

	I2C_InitStruct.I2C_Mode = I2C_MASTER_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = I2C_Speedmode_High;
	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
	I2C_InitStruct.I2C_OwnAddress = 0x73;       //casually specify 
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
	I2C_InitStruct.I2C_SCL_HCNT = IIC_HCNT;
	I2C_InitStruct.I2C_SCL_LCNT = IIC_LCNT;
	I2C_InitStruct.I2C_SDA_HOLD = IIC_HOLD;

	I2C_InitStruct.I2C_HS_MADDR = 12;
	I2C_InitStruct.I2C_RX_TL = 0;
	I2C_InitStruct.I2C_TX_TL = 0;
    
	I2C_Reset(I2C0);

	/* i2c interrupt request */
#ifdef I2C_IRQ_ENABLE
/*Enable I2C interrupt here*/
	NVIC_InitStructure.NVIC_IRQChannel = I2C0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//set_IRQ_state(STATUS_IDLE);
		
#endif

	I2C_Init(I2C0, &I2C_InitStruct);
}

void I2C_TestReadDS3231(void)
{
	uint8_t buff[2]={0x1c,0x00};
	 GPIO_SetPinMux(GPIO_Pin_0, IO_I2C0_SCL); //I2C_SCL
	 GPIO_SetPinMux(GPIO_Pin_1, IO_I2C0_SDA); //I2C_SDA	
	 IIC_Config_Master();
  // i2cOpen(I2C_GP1,I2C_Speedmode_FAST);

	Bcd2asc((twdata));
	printf("Set time:%d-%d-%d  %d:%d:%d  week(%d)\r\n",(asc[6]+2000),asc[5],asc[4],asc[2],asc[1],asc[0],asc[3]);

  I2C_Write(DS3231_DEVADDR, 0x0e,1,buff,2);
	delay(1000);
  I2C_Write(DS3231_DEVADDR, 0x00,1,twdata,7);	
	delay(60000);

	while(1){
    I2C_Read(DS3231_DEVADDR, 0x00,1,trdata,7);
		datajust();
		Bcd2asc(trdata);
		printf("Read time:%d-%d-%d  %d:%d:%d  week(%d)\r\n",(asc[6]+2000),asc[5],asc[4],asc[2],asc[1],asc[0],asc[3]);
		delay(60000);
	}		
}


