/**
  ******************************************************************************
  * @file    DG32F003xx_wdg.h
  * @author  Alpscale Software Team
  * @version V1.0.0
  * @date    12/20/2013
  * @brief   This file contains all the functions prototypes for the WDG 
  *          firmware library.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 Alpscale</center></h2>
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DG32F003xx_FLASH_H
#define __DG32F003xx_FLASH_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"

#define  CR_LOCK_MAIN	0x80000000
//#define  CR_LOCK_NVR1	0x40000000
//#define  CR_LOCK_NVR0	0x20000000
#define  CR_STRT		0x00010000
#define  CR_NVR			0x00000002
#define  CR_CHIP		0x00000001


#define  SR_BUSY		0x00010000
#define  SR_EOE			0x00000001


void Flash_EraseSector(uint32_t sectorIndex);
int Flash_Write32(uint32_t adr, uint32_t* buf, uint32_t byteCount);

void Flash_Unlock(void);
void Flash_Lock(void);
void Flash_EraseMain(void);



#ifdef __cplusplus
}
#endif

#endif /* __DG32F003xx_FLASH_H*/
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/

