/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_wdg.h"
#include "delay.h"

/* Private define ------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

void WDG_Configuration(void)
{
  WDT_CLKSET(ENABLE);
  WDT_Reset();
  commonDelay(10);

  RCC->WDTCLKDIV = 1;
  WDG_SetMode(WDG_SETMODE_RESET, WDG_FAULT_ENABLE);      //rest
  
  WDG_SetReload(50000);                      /* reset time : 0.1ms*50K=5S */
  WDG_ReloadCounter();

}

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
  WDG_Configuration();
	delay_init(24);
	while(1){
	  delay_ms(100);
	  WDG_ReloadCounter();
  }
}

