/**
  ******************************************************************************
  * @file    DG32F003xx_SPI.c
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    27-November-2013
  * @brief   This file provides all the SPI firmware functions.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, Alpscale SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  */

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx_spi.h"
#include "DG32F003xx_rcc.h"

#include "stdio.h"
/** @addtogroup DG32F003xx_StdPeriph_Driver
  * @{
  */

/** @defgroup SPI
  * @brief SPI driver modules
  * @{
  */

/** @defgroup SPI_Private_Typedefinitions
  * @{
  */

/**
  * @}
  */

/** @defgroup SPI_Private_Functions
  * @{
  */

/**
  * @brief  Sets the power status of the SPI controller.
  * @param SPIptr: the QUAD spi controller base address.
  * @param  SPI_PowerState: new state of the Power state. 
  *   This parameter can be one of the following values:
  *     @arg SPI_PowerState_OFF
  *     @arg SPI_PowerState_ON
  * @retval None
  */
void SPI_SetPowerState(SPI_TypeDef *SPIptr, uint32_t SPI_PowerState)
{

  /* Check the parameters */
  assert_param(IS_SPI_POWER_STATE(SPI_PowerState));

  if (SPI_PowerState == SPI_PowerState_ON)
  {
    if (SPIptr == SPI0)
    {
      RCC_ResetAHBCLK(1 << AHBCLK_BIT_SPI);
    }
  }
  else
  {
    if (SPIptr == SPI0)
      RCC_SetAHBCLK(1 << AHBCLK_BIT_SPI, DISABLE);
  }
}

/**
  * @brief  Initializes the SPI peripheral according to the specified 
  *   parameters in the SPI_InitStruct.  
  * @param SPIptr: the QUAD spi controller base address.
  * @param  SPI_InitStruct : pointer to a SPI_InitTypeDef structure 
  *   that contains the configuration information for the SPI peripheral.
  * @retval None
  */
void SPI_HwInit(SPI_TypeDef *SPIptr, SPI_InitTypeDef *SPI_InitStruct)
{
  volatile int i;
  /* Check the parameters */
  assert_param(IS_SPI_CPOL(SPI_InitStruct->SPI_CPOL));
  assert_param(IS_SPI_CPHA(SPI_InitStruct->SPI_CPHA));
  assert_param(IS_SPI_FIRST_BIT(SPI_InitStruct->SPI_FirstBit));
  assert_param(IS_SPI_ClockDiv_setting(SPI_InitStruct->SPI_ClockDiv));
  //assert_param(IS_SPI_ClockRate_setting(SPI_InitStruct->SPI_ClockRate));
  assert_param(IS_SPI_MASTER_OR_SLAVE_MODE(SPI_InitStruct->SPI_SlaveMode));
  assert_param(IS_SPI_MODE(SPI_InitStruct->SPI_ModeSelect));
  assert_param(IS_SPI_FRAME_LENGTH(SPI_InitStruct->SPI_FrameLength));
  
  SPI_SetPowerState(SPIptr, SPI_PowerState_ON);

#if 1
  /**Vital notice, must delay for a while**/
  for (i = 0; i < 0x1000; i++)
    ;
#endif

  SPI_ModeSet(SPIptr,SPI_InitStruct->SPI_ModeSelect);
  SPI_FrameLengthSet(SPIptr,SPI_InitStruct->SPI_FrameLength);
  SPIptr->CTRL1_SET = SPI_InitStruct->SPI_CPOL | SPI_InitStruct->SPI_CPHA | \
                       SPI_InitStruct->SPI_SlaveMode;
  SPI_FirstBitSet(SPIptr, SPI_InitStruct->SPI_FirstBit);
  SPIptr->TIMING = 0xFFFF0000 | SPI_InitStruct->SPI_ClockDiv | SPI_InitStruct->SPI_ClockRate;

  SPIptr->CTRL1_SET = 0x0;
}

/**
   * @brief  Specifies the SPI mode.
   * @param  SPIptr: the QUAD spi controller base address.  
   * @param  ModeSelect: the selected spi mode.
   *        This parameter can be one of the following values: 
   *         @arg SPI_STD: standard spi mode 
   *         @arg SPI_DUAL: Dual spi mode 
   *         @arg SPI_QUAD: quad spi mode 
   * @retval None 
   */
void SPI_ModeSet(SPI_TypeDef *SPIptr, SPIModeSelect ModeSelect)
{
  /* Check the parameters */
  assert_param(IS_SPI_MODE(ModeSelect));

  if (ModeSelect == SPI_STD)
  {
    SPIptr->CTRL1 = (SPIptr->CTRL1 & 0xfffffff8) | 0x00;
  }
  else if (ModeSelect == SPI_SSI)
  {
    SPIptr->CTRL1 = (SPIptr->CTRL1 & 0xfffffff8) | 0x01;
  }
  else
  {
    SPIptr->CTRL1 = (SPIptr->CTRL1 & 0xfffffff8) | 0x02;
  }
}

/**
  * @brief  Set Number of bits in each frame of data
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  wl:word length
  *    This parameter can be one of the following values: 
  *         @arg SPI_FRAME_LENGTH_4Bit 
  *         @arg SPI_FRAME_LENGTH_8Bit
  *         @arg SPI_FRAME_LENGTH_16Bit
  *         @arg SPI_FRAME_LENGTH_24Bit
  *         @arg SPI_FRAME_LENGTH_32Bit
  * 
  * @retval None 
  */
void SPI_FrameLengthSet(SPI_TypeDef *SPIptr, uint8_t wl)
{
  assert_param(IS_SPI_FRAME_LENGTH(wl));
  switch (wl)
  {
    case SPI_FRAME_LENGTH_8Bit:
      SPIptr->CTRL1 = (SPIptr->CTRL1 & 0xffffff07) | SPI_FRAME_LENGTH_8Bit;
      break;
    case SPI_FRAME_LENGTH_16Bit:
      SPIptr->CTRL1 = (SPIptr->CTRL1 & 0xffffff07) | SPI_FRAME_LENGTH_16Bit;
      break;
    case SPI_FRAME_LENGTH_24Bit:
      SPIptr->CTRL1 = (SPIptr->CTRL1 & 0xffffff07) | SPI_FRAME_LENGTH_24Bit;
      break;
    case SPI_FRAME_LENGTH_32Bit:
      SPIptr->CTRL1 = (SPIptr->CTRL1 & 0xffffff07) | SPI_FRAME_LENGTH_32Bit;
      break;
    default:
      break;
  }
}


/**
  * @brief  Gets the power status of the QUAD SPI controller.
  * @param SPIptr: the QUAD spi controller base address.
  * @retval Power status of the controller. The returned value can
  *   be one of the following:
  * - 0x0: Power OFF   
  * - !0x0: Power ON 
  */
uint32_t SPI_GetPowerState(SPI_TypeDef *SPIptr)
{
   return ((RCC->PRESETCTRL0 & (1 << PRESET_BIT_SPI)) && (RCC->AHBCLKCTRL0 & (1 << AHBCLK_BIT_SPI)));
}

/**
  * @brief  Enables or disables the SPI interrupts.  
  * @param SPIptr: the QUAD spi controller base address.
  * @param  SPI_IT: specifies the SPI interrupt source to be enabled or disabled.
  *   This parameter can be one or a combination of the following values: 
  *     @arg SPI_IT_RXOVEFLW:  Received FIFO overrun error interrupt 
  *     @arg SPI_IT_RXTIMEOUT:  data not be read in time 
  *     @arg SPI_IT_RXFIFOHF: Receive FIFO Half Full interrupt
  *     @arg SPI_IT_TXFIFOHE: Transmit FIFO Half Empty interrupt
  *
  * @param  NewState: new state of the specified SPI interrupts.
  *   This parameter can be: ENABLE or DISABLE.
  * @note: once, only one kind of interrupts can be set.   
  * @retval None 
  */
void SPI_ITConfig(SPI_TypeDef *SPIptr, uint32_t SPI_IT, FunctionalState NewState)
{
  /* Check the parameters */
  assert_param(IS_SPI_IT(SPI_IT));
  assert_param(IS_FUNCTIONAL_STATE(NewState));

  if (NewState != DISABLE)
  {
    /* Enable the SPI interrupts */
    SPIptr->CTRL1_CLR = SPI_IT;
    SPIptr->CTRL1_SET = SPI_IT >> 1;
  }
  else
  {
    /* Disable the SPI interrupts */
    SPIptr->CTRL1_CLR = SPI_IT >> 1;
    SPIptr->CTRL1_CLR = SPI_IT;
  }
}


/**
  * @brief  when transmission starts, assert the CS signal.
  * @param SPIptr: the QUAD spi controller base address.
  * @retval None
  */
void SPI_CS_Low(SPI_TypeDef *SPIptr)
{
  SPIptr->CTRL0 |= SPI_CTRL0_LOCK_CS;
}

/** 
  * @brief  After transmission, deassert the CS signal..
  * @param SPIptr: the QUAD spi controller base address.
  * @retval None
  */
void SPI_CS_High(SPI_TypeDef *SPIptr)
{
  SPIptr->CTRL0 &= ~(SPI_CTRL0_LOCK_CS|SPI_CTRL0_RUN);
}

/**
  * @brief  Trigger the SPI controller for data transfer according to the specified 
  *             parameters in the SPI_DataInitStruct.
  * @param SPIptr: the QUAD spi controller base address.
  * @param SPI_DataInitStruct : pointer to a SPI_DataInitTypeDef structure that
  *   contains the configuration information for the SPI controller.
  * @retval None
  */
void SPI_DataConfig(SPI_TypeDef *SPIptr, SPI_DataInitTypeDef *SPI_DataInitStruct)
{
  uint32_t tmepReg;
  int curTransferDir;

  /* Check the parameters */
  assert_param(IS_SPI_Duplex(SPI_DataInitStruct->SPI_DUPLEX));
  assert_param(IS_SPI_SPI_Direction(SPI_DataInitStruct->SPI_TransferDir));

  if(SPIptr->CTRL0 & 0x04000000)
    curTransferDir = SPI_Transfer_Read;
  else
    curTransferDir = SPI_Transfer_Write;

  SPIptr->XFER = SPI_DataInitStruct->SPI_DataLength;
  if(curTransferDir == SPI_DataInitStruct->SPI_TransferDir){
    if (SPIptr->CTRL0 & SPI_CTRL0_LOCK_CS)
    {
      tmepReg = 0x20000000|SPI_DataInitStruct->SPI_DUPLEX | SPI_DataInitStruct->SPI_TransferDir | SPI_CTRL0_LOCK_CS;
    }
    else
    {
      tmepReg = 0x20000000|SPI_DataInitStruct->SPI_DUPLEX | SPI_DataInitStruct->SPI_TransferDir;
    }
    SPIptr->CTRL0 = tmepReg;
  }else{
    if (SPIptr->CTRL0 & SPI_CTRL0_LOCK_CS)
    {
      tmepReg = SPI_DataInitStruct->SPI_DUPLEX | SPI_DataInitStruct->SPI_TransferDir | SPI_CTRL0_LOCK_CS;
    }
    else
    {
      tmepReg = SPI_DataInitStruct->SPI_DUPLEX | SPI_DataInitStruct->SPI_TransferDir;
    }
    SPIptr->CTRL0 = tmepReg;
    SPIptr->CTRL0 |= 0x20000000;
    }
}

/**
  * @brief  Read one data word from Rx FIFO.
  * @param SPIptr: the QUAD spi controller base address.
  * @retval Data received
  */
uint32_t SPI_ReadData(SPI_TypeDef *SPIptr)
{
  return SPIptr->DATA;
}

/**
  * @brief  Write one data word to Tx FIFO.
  * @param SPIptr: the QUAD spi controller base address.
  * @param Data: data to be written.
  * @retval None
  */
void SPI_WriteData(SPI_TypeDef *SPIptr, uint32_t Data)
{
  SPIptr->DATA = Data;
}


/**
  * @brief  Set SPI First Bit mode MSB/LSB.
  * @param  SPIptr: the QUAD spi controller base address.
  * @param  SPI_FirstBit: specifies the flag to set MSB/LSB.
  *   This parameter can be one of the following values:
  *     @arg SPI_FirstBit_MSB
  *     @arg SPI_FirstBit_LSB
  * @retval None 
  */
void SPI_FirstBitSet(SPI_TypeDef *SPIptr, uint32_t SPI_FirstBit)
{
  assert_param(IS_SPI_FIRST_BIT(SPI_FirstBit));
  if(SPI_FirstBit == SPI_FirstBit_LSB){
    SPIptr->CTRL1_SET = SPI_FirstBit_LSB;
  }else{
    SPIptr->CTRL1_CLR = SPI_FirstBit_LSB;
  }
}

/**
  * @brief  Checks whether the specified SPI flag is set or not.  
  * @param SPIptr: the QUAD spi controller base address.
  * @param  SPI_FLAG: specifies the flag to check. 
  *   This parameter can be one of the following values:
  *     @arg SPI_STATUS_RXDAVL
  *     @arg SPI_STATUS_TXDAVL
  *     @arg SPI_STATUS_RECV_TIMEOUT_STAT
  *     @arg SPI_STATUS_RECV_DATA_STAT
  *     @arg SPI_STATUS_RECV_OVRFLW
  *     @arg SPI_STATUS_RECV_FULL
  *     @arg SPI_STATUS_RECV_NOT_EMPTY
  *     @arg SPI_STATUS_XMIT_NOT_FULL
  *     @arg SPI_STATUS_XMIT_EMPTY
  *     @arg SPI_STATUS_XMIT_UNDRFLW
  *     @arg SPI_STATUS_SPI_BUSY:  SPI controller busy       
  *
  * @note once, only one kind of FLAG can be checked.
  * @retval The new state of FlagStatus (SET or RESET).
  */
FlagStatus SPI_GetFlagStatus(SPI_TypeDef *SPIptr, uint32_t SPI_FLAG)
{
  FlagStatus bitstatus = RESET;

  /* Check the parameters */
  assert_param(IS_SPI_STATUS_FLAG(SPI_FLAG));

  if ((SPIptr->STATUS & SPI_FLAG) != (uint32_t)RESET)
  {
    bitstatus = SET;
  }
  else
  {
    bitstatus = RESET;
  }

  return bitstatus;
}

/**
  * @brief  Checks whether the specified SPI interrupt has occurred or not.
  * @param SPIptr: the QUAD spi controller base address.
  * @param  SPI_IT: specifies the SPI interrupt source to check. 
  *     @arg SPI_IT_RXOVEFLW
  *     @arg SPI_IT_RXTIMEOUT
  *     @arg SPI_IT_RXFIFOHF
  *     @arg SPI_IT_TXFIFOHE
  * @retval The specified interrupt status state (SET or RESET).
  */
ITStatus SPI_GetITStatus(SPI_TypeDef *SPIptr, uint32_t SPI_IT)
{
  ITStatus bitstatus;

  /* Check the parameters */
  assert_param(IS_SPI_GET_IT(SPI_IT));

  if ((SPIptr->CTRL1 & SPI_IT) != (uint32_t)RESET)
  {
    bitstatus = SET;
  }
  else
  {
    bitstatus = RESET;
  }
  return bitstatus;
}

/**
  * @brief  Clears the SPI's interrupt pending bits.  
  * @param SPIptr: the QUAD spi controller base address.
  * @param  SPI_IT: specifies the interrupt pending bit to clear. 
  *     @arg SPI_IT_RXOVEFLW
  *     @arg SPI_IT_RXTIMEOUT
  *     @arg SPI_IT_RXFIFOHF
  *     @arg SPI_IT_TXFIFOHE   
  * @retval None
  */
void SPI_ClearITPendingBit(SPI_TypeDef *SPIptr, uint32_t SPI_IT)
{
  /* Check the parameters */
  assert_param(IS_SPI_CLEAR_IT(SPI_IT));

  SPIptr->CTRL1_CLR = SPI_IT;
}



/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/
