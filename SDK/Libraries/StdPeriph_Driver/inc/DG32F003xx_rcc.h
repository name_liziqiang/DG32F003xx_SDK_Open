/**
 ******************************************************************************
 * @file    DG32F003xx_rcc.h
 * @author  Alpscale Software Team
 * @version V1.0.0
 * @date    10/12/2013
 * @brief   This file contains all the functions prototypes for the RCC firmware
 *          library.
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2012 Alphascale</center></h2>
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DG32F003xx_RCC_H
#define __DG32F003xx_RCC_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"

/** @addtogroup DG32F003xx_StdPeriph_Driver
 * @{
 */

/** @addtogroup RCC
 * @{
 */

#define PRESET_BIT_ROM    0
#define PRESET_BIT_RAM0   1
#define PRESET_BIT_EFLASH 2
#define PRESET_BIT_GPIO   3
#define PRESET_BIT_UART0  4
#define PRESET_BIT_UART1  5
#define PRESET_BIT_I2C0   6
#define PRESET_BIT_ADC0   7
#define PRESET_BIT_IOCON  8
#define PRESET_BIT_WDT    9
#define PRESET_BIT_TIM1   10
#define PRESET_BIT_TIM4   11
#define PRESET_BIT_TIM15   12
#define PRESET_BIT_SPI    13

#define AHBCLK_BIT_ROM    0
#define AHBCLK_BIT_RAM0   1
#define AHBCLK_BIT_EFLASH 2
#define AHBCLK_BIT_GPIO   3
#define AHBCLK_BIT_UART0  4
#define AHBCLK_BIT_UART1  5
#define AHBCLK_BIT_I2C0   6
#define AHBCLK_BIT_ADC0   7
#define AHBCLK_BIT_IOCON  8
#define AHBCLK_BIT_WDT    9
#define AHBCLK_BIT_TIM1   10
#define AHBCLK_BIT_TIM4   11
#define AHBCLK_BIT_TIM15   12
#define AHBCLK_BIT_SPI    13

/** @defgroup MAINCLK_SOURCE
 * @{
 */
#define RCC_MAINCLK_SOURCE_48MIRC 0
#define RCC_MAINCLK_SOURCE_10KIRC 2
#define RCC_MAINCLK_SOURCE_TEST   3

#define IS_RCC_MAINCLK_SOURCE(SOURCE) (((SOURCE) == RCC_MAINCLK_SOURCE_48MIRC) || \
                                       ((SOURCE) == RCC_MAINCLK_SOURCE_10KIRC) || \
                                       ((SOURCE) == RCC_MAINCLK_SOURCE_TEST))


/** @defgroup OUTCLK_SOURCE
 * @{
 */
#define RCC_OUTCLK_SOURCE_48MIRC 0
#define RCC_OUTCLK_SOURCE_10KIRC 2
#define RCC_OUTCLK_SOURCE_AHBCLK 3

#define IS_RCC_OUTCLK_SOURCE(SOURCE) (((SOURCE) == RCC_OUTCLK_SOURCE_48MIRC) || \
                                      ((SOURCE) == RCC_OUTCLK_SOURCE_10KIRC) || \
                                      ((SOURCE) == RCC_OUTCLK_SOURCE_AHBCLK))

/** @defgroup EFLASHSTAT
 * @{
 */
#define RCC_EFLASHSTAT_DONE 1
#define RCC_EFLASHSTAT_FAIL 2


/** @defgroup PDCFG_MOD
 * @{
 */
#define RCC_PDCFG_10KIRC                  (1 << 1)
#define RCC_PDCFG_ADC0                    (1 << 2)
#define RCC_PDCFG_BOD                     (1 << 5)
#define RCC_PDCFG_BOR                     (1 << 6)
#define RCC_PDCFG_48MIRC                  (1 << 8)
#define RCC_PDCFG_EFLASH                  (1 << 9)
#define IS_RCC_PDCFG_ANALOG_BLOCK(MODULE) (((MODULE)&0x366) != 0)


/** @defgroup PDCFG_MODE
 * @{
 */
#define RCC_PDCFG_POWER_ON      0
#define RCC_PDCFG_POWER_DOWN    1
#define IS_RCC_PDCFG_MODE(MODE) (((MODE) == RCC_PDCFG_POWER_ON) || ((MODE) == RCC_PDCFG_POWER_DOWN))


/** @defgroup SYSRST_FLAG
 * @{
 */
#define RCC_SYSRSTSTATE_POR      0x01
#define RCC_SYSRSTSTATE_EXERSTN  0x02
#define RCC_SYSRSTSTATE_WDT      0x04
#define RCC_SYSRSTSTATE_BOD      0x08
#define RCC_SYSRSTSTATE_SYSRST   0x10
#define IS_RCC_SYSRST_FLAG(FLAG) (((FLAG) == RCC_SYSRSTSTATE_POR) ||     \
                                  ((FLAG) == RCC_SYSRSTSTATE_EXERSTN) || \
                                  ((FLAG) == RCC_SYSRSTSTATE_WDT) ||     \
                                  ((FLAG) == RCC_SYSRSTSTATE_BOD) ||     \
                                  ((FLAG) == RCC_SYSRSTSTATE_SYSRST))


/** @addtogroup PCON
 * @{
 */
#define RCC_SLEEPFLAG                       (1 << 8)
#define RCC_DEEPSLEEPFLAG                   (1 << 11)
#define IS_RCC_PCON_FLAG(FLAG)              (((FLAG) == RCC_SLEEPFLAG) ||\
                                             ((FLAG) == RCC_DEEPSLEEPFLAG))


#define RCC_LDOMODE                         0x0002
#define LDO_ACTIVE_MODE                     0
#define LDO_LOWPOWER_MODE 									1
#define IS_LDO_MODE(mode)   								(((mode) == LDO_ACTIVE_MODE) || ((mode) == LDO_LOWPOWER_MODE))

/** @defgroup SYSTICK_CAL
 * @{
 */
#define IS_RCC_SYSTICK_CAL(CAL) (((CAL)&0xFC000000) == 0)




#define IRC_10K_STATE      0x80000000
#define IRC_48M_STATE      0x80000000

/**
 * @}
 */

/** @defgroup BODINT_VAL
 * @{
 */
#define RCC_BODINT_VAL_200     0x00000000
#define RCC_BODINT_VAL_220     0x00008000
#define RCC_BODINT_VAL_240     0x00010000
#define RCC_BODINT_VAL_270     0x00018000
#define RCC_BODINT_VAL_300     0x00020000
#define RCC_BODINT_VAL_370     0x00028000
#define RCC_BODINT_VAL_400     0x00030000
#define RCC_BODINT_VAL_430     0x00038000
#define IS_RCC_BODINT_VAL(VAL) (((VAL) == RCC_BODINT_VAL_200) || \
                                ((VAL) == RCC_BODINT_VAL_220) || \
                                ((VAL) == RCC_BODINT_VAL_240) || \
                                ((VAL) == RCC_BODINT_VAL_270) || \
                                ((VAL) == RCC_BODINT_VAL_300) || \
                                ((VAL) == RCC_BODINT_VAL_370) || \
                                ((VAL) == RCC_BODINT_VAL_400) || \
                                ((VAL) == RCC_BODINT_VAL_430))

#define RCC_BODCTRL_CLEAR_Mask 0xFFFC7FFF

/** @defgroup BORINT_VAL
 * @{
 */
#define RCC_BORINT_VAL_18     0x00000000
#define RCC_BORINT_VAL_20     0x00000001
#define RCC_BORINT_VAL_25     0x00000002
#define RCC_BORINT_VAL_35     0x00000003
#define IS_RCC_BORINT_VAL(VAL) (((VAL) == RCC_BORINT_VAL_18) || \
                                ((VAL) == RCC_BORINT_VAL_20) || \
                                ((VAL) == RCC_BORINT_VAL_25) || \
                                ((VAL) == RCC_BORINT_VAL_35))

#define RCC_BORCTRL_CLEAR_Mask 0xFFFFFFFC

/** @defgroup Peripheral_CLK
 * @{
 */
typedef enum {
  RCC_CLOCKFREQ_SYSAHBCLK = 0x1,
  RCC_CLOCKFREQ_SYSTICKCLK,
  RCC_CLOCKFREQ_CLKOUTCLK,
  RCC_CLOCKFREQ_PWMCLK,
  RCC_CLOCKFREQ_UART0CLK,
  RCC_CLOCKFREQ_UART1CLK,
} CLOCK_TypeDef;

void RCC_SetPRESETCTRL(uint32_t PRESETCTRL, FunctionalState NewState);
void RCC_SetAHBCLK(uint32_t AHBCLK, FunctionalState NewState);
void RCC_ResetAHBCLK(uint32_t AHBCLK);
void RCC_SetSystickCal(uint32_t SystickCalibration);

void RCC_MAINCLKSel(uint8_t RCC_MAINCLKSource);
void RCC_OUTCLKSel(uint8_t RCC_OUTCLKSource);

uint32_t RCC_GetClocksFreq(CLOCK_TypeDef RCC_Clocks);

void RCC_BORConfig(uint32_t BORINTVal, FunctionalState NewState);
void RCC_BODConfig(uint32_t BODINTVal, FunctionalState NewState);
FlagStatus RCC_CAPPIO(uint32_t RCC_PortNum, uint32_t RCC_PinNum);

FlagStatus RCC_GetSYSRSTFlagStatus(uint16_t RCC_FLAG);
void RCC_ClearSYSRSTFlag(uint16_t RCC_FLAG);

FlagStatus RCC_GetPCONFlagStatus(uint16_t RCC_FLAG);
void RCC_ClearPCONFlag(uint16_t RCC_FLAG);

void RCC_PDRUNConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE);
void RCC_PDAWAKEConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE);
void RCC_PDSLEEPConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE);

FlagStatus RCC_GetIRC10KState(void);
FlagStatus RCC_GetIRC48MState(void);
void RCC_InitPRNG(void);

typedef enum {
  SM_SLEEP     = 0,
  SM_DEEPSLEEP = 1,
} SleepMode;

typedef enum {
  LM_NORMAL     = 0,
  LM_LOW 		= 1,
} LdoMode;

void goSleep(SleepMode mode, LdoMode ldo_mode);

#ifdef __cplusplus
}
#endif

#endif /* __DG32F003xx_RCC_H */
/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2012 Alphascale *****END OF FILE****/
