/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_tim.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_uart.h"
#include <string.h>
/* Private define ------------------------------------------------------------*/
#define LED_PIN GPIO2_4
#define TxPin GPIO0_0
#define RxPin GPIO0_1
#define BUFF_SIZE 64

/* Private variables ---------------------------------------------------------*/
static uint8_t UartRxBuf[BUFF_SIZE];
static uint32_t rx_ptr          = 0;
static uint32_t rx_len          = 0;
static uint8_t rx_irq_flag    = 0;
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void UART0_Init(void);
int UART0_Send_data(UART_TypeDef *UARTx, uint8_t *buf, uint32_t len);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{  
    GPIO_SetPinMux(LED_PIN, IO_GPIO);//tim mark signal
    GPIO_SetPinDir(LED_PIN, GPIO_Mode_OUT); 
    GPIO_ClearPin(LED_PIN);	
    UART0_Init();
	while(1){
         if(rx_irq_flag == 1){
			 rx_irq_flag = 0;
			 UART0_Send_data(UART0,UartRxBuf,rx_len);
			 memset(UartRxBuf, 0, sizeof(UartRxBuf));
		 }
  }
}

static void UART0_Init(void)
{
	
  UART_InitTypeDef UART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStruct;

 
 
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
    RCC->UART0CLKDIV                = 2;
    NVIC_InitStruct.NVIC_IRQChannel = UART0_IRQn;
    GPIO_SetPinMux(TxPin, IO_UART0_TX);
    GPIO_SetPinMux(RxPin, IO_UART0_RX);

    NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;
    NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
    NVIC_Init(&NVIC_InitStruct);

  UART_Reset(UART0);
  UART_StructInit(&UART_InitStructure);
  UART_InitStructure.UART_BaudRate   = 115200;           // Baud rate
  UART_InitStructure.UART_TXIFLSEL   = UART_TXIFLSEL_14;   // Send FIFO depth   TXFIFO LEVEL
  UART_InitStructure.UART_RXIFLSEL   = UART_RXIFLSEL_14;   // Receive FIFO depth  RXFIFO LEVEL
  UART_InitStructure.UART_IdleNum    = UART_IdleNum_4Byte; // Idle detects word length
  UART_InitStructure.UART_IdleEN     = UART_IdleEN_Enable; // Idle detection enabled
  UART_Init(UART0, &UART_InitStructure);
  /* set timeout to 8 uart clk cycle len */
  UART0->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UART0->CTRL0_SET = 8 << 16;

  UART_ITConfig(UART0, UART_IT_RXIEN | UART_IT_RTIEN, ENABLE);

  UART_Cmd(UART0, ENABLE);
}
/**
 * @brief 
 * 
 */
void UART0_IRQHandler(void)
{
  if (UART_GetITStatus(UART0, UART_IT_RXIS)) {
    while (!UART_GetFlagStatus(UART0, UART_FLAG_RXFE))
      UartRxBuf[rx_ptr++] = UART0->DATA;

      UART_ClearITPendingBit(UART0, UART_IT_RXIS);
  }

  if (UART0->INTR & UART_IT_RTIEN) {
    if (UART_GetITStatus(UART0, UART_IT_RTIS)) {
      while (!UART_GetFlagStatus(UART0, UART_FLAG_RXFE))
        UartRxBuf[rx_ptr++] = UART0->DATA;
		  rx_len       = rx_ptr;
		  rx_irq_flag  = 1;
		  rx_ptr       = 0;
		  UART_ClearITPendingBit(UART0, UART_INTR_RTIS);
    }
  }

}

/**
 * @brief 
 * 
 */

int UART0_Send_data(UART_TypeDef *UARTx, uint8_t *buf, uint32_t len)
{
  while (len) {
    if ((UARTx->STAT & UART_FLAG_TXFF) == 0) { // fifo is not full
      UARTx->DATA = (uint8_t)*buf;
      buf++;
      len--;
    } 
  }

  while (UART_GetFlagStatus(UARTx, UART_FLAG_TXFE) == RESET)
    ; // Waiting for the end of sending
	
	return 0;
}

