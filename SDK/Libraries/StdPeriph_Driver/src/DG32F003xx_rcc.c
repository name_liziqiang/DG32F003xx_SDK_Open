/**
 ******************************************************************************
 * @file    DG32F003xx_rcc.c
 * @author  Alpscale Software Team
 * @version V1.0.0
 * @date    10/12/2013
 * @brief   This file provides all the RCC(reset and clock control) firmware functions.
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_gpio.h"
/** @addtogroup DG32F003xx_StdPeriph_Driver
 * @{
 */



/** @defgroup RCC_Private_Defines
 * @{
 */

#define RCC_SYSPLLSTATE_TIMEOUT       0x10000
#define RCC_SYSPLLUEN_DISABLE         0
#define RCC_SYSPLLUEN_ENABLE          1
#define RCC_SYSPLLCTRL_CLEAR_Mask     0xFFE0
#define RCC_SYSPLLSTATE_LOCK          1

#define RCC_MAINCLKUEN_DISABLE        0
#define RCC_MAINCLKUEN_ENABLE         1

#define RCC_UARTCLKUEN_DISABLE        0
#define RCC_UARTCLKUEN_ENABLE         1

#define RCC_WDTCLKUEN_DISABLE         0
#define RCC_WDTCLKUEN_ENABLE          1

#define RCC_OUTCLKUEN_DISABLE         0
#define RCC_OUTCLKUEN_ENABLE          1

// #define RCC_I2S0CLKUEN_DISABLE        0
// #define RCC_I2S0CLKUEN_ENABLE         1



/**
 * @brief  Set Peripheral clock on AHB
 * @param  AHBCLK: module clock enable signal bit.
 *     @arg 1 << AHBCLK_BIT_ROM
 *     ...
 *     @arg 1 << AHBCLK_BIT_UART2
 * @param  NewState: the new state of the clock,ENABLE/DISABLE
 * @retval None
 */
void RCC_SetAHBCLK(uint32_t AHBCLK, FunctionalState NewState)
{
  if (NewState != DISABLE)
    RCC->AHBCLKCTRL0_SET = AHBCLK;
  else
    RCC->AHBCLKCTRL0_CLR = AHBCLK;
}
/**
 * @brief  Reset Peripheral module.
 * @param  PRESETCTRL: Module software reset signal bit.
 *     @arg 1 << PRESET_BIT_ROM
 *     ...
 *     @arg 1 << PRESET_BIT_UART2
 * @param  NewState: the new state of reset,ENABLE/DISABLE.
 * @retval None
 */
void RCC_SetPRESETCTRL(uint32_t PRESETCTRL, FunctionalState NewState)
{
  if (NewState != DISABLE)
    RCC->PRESETCTRL0_SET = PRESETCTRL;
  else
    RCC->PRESETCTRL0_CLR = PRESETCTRL;
}
/**
 * @brief  Reset Peripheral clock on AHB
 * @param  AHBCLK: module clock enable signal bit.
 *     @arg 1 << AHBCLK_BIT_ROM
 *     ...
 *     @arg 1 << AHBCLK_BIT_UART2
 * @retval None
 */
void RCC_ResetAHBCLK(uint32_t AHBCLK)
{
  RCC_SetAHBCLK(AHBCLK, ENABLE);
  RCC_SetPRESETCTRL(AHBCLK, DISABLE);
  RCC_SetPRESETCTRL(AHBCLK, ENABLE);
}
/**
 * @brief  Select MAIN Clock Source
 * @param  RCC_MAINCLKSource: specifies MAIN Clock Source .
 *   This parameter can be the following values:
 *     @arg RCC_MAINCLK_SOURCE_48MIRC
 *     @arg RCC_MAINCLK_SOURCE_10KIRC
 *     @arg RCC_MAINCLK_SOURCE_TEST
 *
 * @retval None
 */
void RCC_MAINCLKSel(uint8_t RCC_MAINCLKSource)
{
  assert_param(IS_RCC_MAINCLK_SOURCE(RCC_MAINCLKSource));
  RCC->MAINCLKSEL = RCC_MAINCLKSource;
  RCC->MAINCLKUEN = RCC_MAINCLKUEN_DISABLE;
  RCC->MAINCLKUEN = RCC_MAINCLKUEN_ENABLE;
}
/**
 * @brief  Select OUT Clock Source
 * @param  RCC_OUTCLKSource: specifies OUT Clock Source .
 *   This parameter can be the following values:
 *     @arg RCC_OUTCLK_SOURCE_48MIRC
 *     @arg RCC_OUTCLK_SOURCE_10KIRC
 *     @arg RCC_OUTCLK_SOURCE_AHBCLK
 * @retval None
 */
void RCC_OUTCLKSel(uint8_t RCC_OUTCLKSource)
{
  assert_param(IS_RCC_OUTCLK_SOURCE(RCC_OUTCLKSource));
  RCC->OUTCLKSEL = RCC_OUTCLKSource;
  RCC->OUTCLKUEN = RCC_OUTCLKUEN_DISABLE;
  RCC->OUTCLKUEN = RCC_OUTCLKUEN_ENABLE;
}
/**
 * @brief  Configures the state the chip must enter when the Deep-sleep mode is asserted by the ARM.
 * @param  RCC_Analog_Block: specifies Module which will be power on or down.
 *   This parameter can be any combination of the following values:
 *     @arg RCC_PDCFG_10KIRC
 *     @arg RCC_PDCFG_ADC0
 *     @arg RCC_PDCFG_SYSPLL
 *     @arg RCC_PDCFG_BOD
 *     @arg RCC_PDCFG_BOR
 *     @arg RCC_PDCFG_12MIRC
 *
 * @param  PD_MODE: new mode of the Oscillator.
 *   This parameter can be one of the following values:
 *     @arg RCC_PDCFG_POWER_ON
 *     @arg RCC_PDCFG_POWER_DOWN
 * @retval None
 */
void RCC_PDSLEEPConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE)
{
  assert_param(IS_RCC_PDCFG_ANALOG_BLOCK(RCC_Analog_Block));
  assert_param(IS_RCC_PDCFG_MODE(PD_MODE));
  if (PD_MODE != RCC_PDCFG_POWER_DOWN)
    RCC->PDSLEEPCFG &= ~RCC_Analog_Block;
  else
    RCC->PDSLEEPCFG |= RCC_Analog_Block;
}

/**
 * @brief  Configures the state the chip must enter when it is waking up from Deep-sleep mode.
 * @param  RCC_Analog_Block: specifies Module which will be power on or down.
 *   This parameter can be any combination of the following values:
 *     @arg RCC_PDCFG_10KIRC
 *     @arg RCC_PDCFG_ADC0
 *     @arg RCC_PDCFG_SYSPLL
 *     @arg RCC_PDCFG_BOD
 *     @arg RCC_PDCFG_BOR
 *     @arg RCC_PDCFG_12MIRC
 *
 * @param  PD_MODE: new mode of the Oscillator.
 *   This parameter can be one of the following values:
 *     @arg RCC_PDCFG_POWER_ON
 *     @arg RCC_PDCFG_POWER_DOWN
 * @retval None
 */
void RCC_PDAWAKEConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE)
{
  assert_param(IS_RCC_PDCFG_ANALOG_BLOCK(RCC_Analog_Block));
  assert_param(IS_RCC_PDCFG_MODE(PD_MODE));
  if (PD_MODE != RCC_PDCFG_POWER_DOWN)
    RCC->PDAWAKECFG &= ~RCC_Analog_Block;
  else
    RCC->PDAWAKECFG |= RCC_Analog_Block;
}

/**
 * @brief  Configures the power to the various analog blocks.
 * @param  RCC_Analog_Block: specifies Module which will be power on or down.
 *   This parameter can be any combination of the following values:
 *     @arg RCC_PDCFG_10KIRC
 *     @arg RCC_PDCFG_ADC0
 *     @arg RCC_PDCFG_SYSPLL
 *     @arg RCC_PDCFG_BOD
 *     @arg RCC_PDCFG_BOR
 *     @arg RCC_PDCFG_12MIRC
 *
 * @param  PD_MODE: new mode of the Oscillator.
 *   This parameter can be one of the following values:
 *     @arg RCC_PDCFG_POWER_ON
 *     @arg RCC_PDCFG_POWER_DOWN
 * @retval None
 */
void RCC_PDRUNConfig(uint16_t RCC_Analog_Block, uint8_t PD_MODE)
{
  assert_param(IS_RCC_PDCFG_ANALOG_BLOCK(RCC_Analog_Block));
  assert_param(IS_RCC_PDCFG_MODE(PD_MODE));
  if (PD_MODE != RCC_PDCFG_POWER_DOWN)
    RCC->PDRUNCFG &= ~RCC_Analog_Block;
  else
    RCC->PDRUNCFG |= RCC_Analog_Block;
}


/**
 * @brief  Checks whether the specified RCC flag is set or not.
 * @param  RCC_FLAG: specifies the flag to check.
 *   This parameter can be one of the following values:
 *     @arg RCC_SYSRSTSTATE_POR
 *     @arg RCC_SYSRSTSTATE_EXERSTN
 *     @arg RCC_SYSRSTSTATE_WDT
 *     @arg RCC_SYSRSTSTATE_BOD
 *     @arg RCC_SYSRSTSTATE_SYSRST
 * @retval The new state of RCC_FLAG (SET or RESET).
 */
FlagStatus RCC_GetSYSRSTFlagStatus(uint16_t RCC_FLAG)
{
  FlagStatus bitstatus = RESET;
  assert_param(IS_RCC_SYSRST_FLAG(RCC_FLAG));
  if ((RCC->SYSRSTSTAT & RCC_FLAG) != (uint8_t)RESET)
    bitstatus = SET;
  else
    bitstatus = RESET;
  /* Return the flag status */
  return bitstatus;
}

/**
 * @brief  Checks whether the specified RCC flag is set or not.
 * @param  RCC_FLAG: specifies the flag to check.
 *   This parameter can be one of the following values:
 *     @arg RCC_DPDEN
 *     @arg RCC_SLEEPFLAG
 *     @arg RCC_DPDFLAG
 * @retval  The new state of RCC_FLAG (SET or RESET).
 */
FlagStatus RCC_GetPCONFlagStatus(uint16_t RCC_FLAG)
{
  FlagStatus bitstatus = RESET;
  assert_param(IS_RCC_PCON_FLAG(RCC_FLAG));
  if ((RCC->PCON & RCC_FLAG) != (uint16_t)RESET)
    bitstatus = SET;
  else
    bitstatus = RESET;
  /* Return the flag status */
  return bitstatus;
}

/**
 * @brief  Clears the RCC reset flags.
 * @param  RCC_FLAG: specifies the flag to clear.
 *   This parameter can be one of the following values:
 *     @arg RCC_SYSRSTSTATE_POR
 *     @arg RCC_SYSRSTSTATE_EXERSTN
 *     @arg RCC_SYSRSTSTATE_WDT
 *     @arg RCC_SYSRSTSTATE_BOD
 *     @arg RCC_SYSRSTSTATE_SYSRST
 * @retval None
 */
void RCC_ClearSYSRSTFlag(uint16_t RCC_FLAG)
{
  assert_param(IS_RCC_SYSRST_FLAG(RCC_FLAG));
  RCC->SYSRSTSTAT |= RCC_FLAG;
}

/**
 * @brief  Clears the RCC power config flags.
 * @param  RCC_FLAG: specifies the flag to clear.
 *   This parameter can be one of the following values:
 *     @arg RCC_SLEEPFLAG
 *     @arg RCC_DEEPSLEEPFLAG
 * @retval None
 */
void RCC_ClearPCONFlag(uint16_t RCC_FLAG)
{
  assert_param(IS_RCC_PCON_FLAG(RCC_FLAG));
  RCC->PCON |= RCC_FLAG;
}

/**
 * @brief  Set the systick Calibration.
 * @param  SystickCalibration: specifies the Calibration.
 *   This parameter can be any value of 26 bits data.
 * @retval None
 */
void RCC_SetSystickCal(uint32_t SystickCalibration)
{
  assert_param(IS_RCC_SYSTICK_CAL(SystickCalibration));
  RCC->SYSTICKCAL = SystickCalibration;
}

/**
 * @brief  Configures the BOD interrupt electrical level.
 * @param  BODINTVal: specifies BOD interrupt electrical level.
 *   This parameter can be one of the following values:
 *     @arg RCC_BODINT_VAL_2_56
 *     @arg RCC_BODINT_VAL_2_65
 *     @arg RCC_BODINT_VAL_2_74
 *     @arg RCC_BODINT_VAL_2_85
 * @param  NewState: new mode of the BOD reset.
 *   This parameter can be: ENABLE or DISABLE.
 * @retval None
 */
void RCC_BODConfig(uint32_t BODINTVal, FunctionalState NewState)
{
  uint32_t tmpreg;
  NVIC_InitTypeDef NVIC_InitStruct;
  assert_param(IS_FUNCTIONAL_STATE(NewState));
  assert_param(IS_RCC_BODINT_VAL(BODINTVal));

  tmpreg = RCC->BODCTRL;
  tmpreg &= RCC_BODCTRL_CLEAR_Mask;
  tmpreg |= BODINTVal;
  RCC->BODCTRL = tmpreg;

  if (NewState != DISABLE){
	  NVIC_InitStruct.NVIC_IRQChannel		  = BOD_IRQn;
	  NVIC_InitStruct.NVIC_IRQChannelCmd	  = ENABLE;
	  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	  NVIC_Init(&NVIC_InitStruct);

	  RCC_PDRUNConfig(RCC_PDCFG_BOD, RCC_PDCFG_POWER_ON);
  }else{
	  NVIC_InitStruct.NVIC_IRQChannel		  = BOD_IRQn;
	  NVIC_InitStruct.NVIC_IRQChannelCmd	  = DISABLE;
	  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	  NVIC_Init(&NVIC_InitStruct);
	  
	  RCC_PDRUNConfig(RCC_PDCFG_BOD, RCC_PDCFG_POWER_DOWN);
  }
}

void RCC_BORConfig(uint32_t BORINTVal, FunctionalState NewState)
{
  uint32_t tmpreg;
  assert_param(IS_FUNCTIONAL_STATE(NewState));
  assert_param(IS_RCC_BORINT_VAL(BORINTVal));

  tmpreg = RCC->BODCTRL;
  tmpreg &= RCC_BORCTRL_CLEAR_Mask;
  tmpreg |= BORINTVal;
  RCC->BODCTRL = tmpreg;

  if (NewState != DISABLE){
	  RCC->BODCTRL |= 4;	//bit2
	  RCC_PDRUNConfig(RCC_PDCFG_BOR, RCC_PDCFG_POWER_ON);
  }else{
	  RCC->BODCTRL &= 0xFFFFFFFB;	//bit2
	  RCC_PDRUNConfig(RCC_PDCFG_BOR, RCC_PDCFG_POWER_DOWN);
  }
}

FlagStatus RCC_GetIRC10KState(void)
{
	if(RCC->IRC10K_CTRL & IRC_10K_STATE)
		return SET;
	else
		return RESET;
}

FlagStatus RCC_GetIRC48MState(void)
{
	if(RCC->IRC48M_CTRL & IRC_48M_STATE)
		return SET;
	else
		return RESET;
}

/**
 * @brief  Get clock frequence
 * @param  RCC_Clocks: specifies the clock.
 *   This parameter can be one of the following values:
 *     @arg RCC_CLOCKFREQ_SYSAHBCLK
 *     @arg RCC_CLOCKFREQ_UART0CLK
 *     @arg RCC_CLOCKFREQ_SYSTICKCLK
 *     @arg RCC_CLOCKFREQ_WDTCLK
 *     @arg RCC_CLOCKFREQ_CLKOUTCLK
 * @retval the clock frequence.
 */
uint32_t RCC_GetClocksFreq(CLOCK_TypeDef RCC_Clocks)
{
  uint32_t tmpclk, mainclk, hclk, outclk;
  uint8_t mainclk_sel, outclk_sel;

  mainclk_sel = RCC->MAINCLKSEL;

  if (mainclk_sel == RCC_MAINCLK_SOURCE_48MIRC)
    mainclk = 48000000;
  else if (mainclk_sel == RCC_MAINCLK_SOURCE_10KIRC)
    mainclk = 10000;

  hclk = mainclk / RCC->SYSAHBCLKDIV;

  if (RCC_Clocks == RCC_CLOCKFREQ_SYSAHBCLK)
    tmpclk = hclk;
  else if ((RCC_Clocks == RCC_CLOCKFREQ_UART0CLK)) {
    if (RCC->UART0CLKDIV == 0)
      return 0;
    tmpclk = mainclk / RCC->UART0CLKDIV;
  } else if ((RCC_Clocks == RCC_CLOCKFREQ_UART1CLK)) {
    if (RCC->UART1CLKDIV == 0)
      return 0;
    tmpclk = mainclk / RCC->UART1CLKDIV;
  } else if (RCC_Clocks == RCC_CLOCKFREQ_CLKOUTCLK) {
    if (RCC->OUTCLKDIV == 0)
      return 0;
    if (outclk_sel == RCC_MAINCLK_SOURCE_48MIRC)
      outclk = 48000000 / RCC->OUTCLKDIV;
    else if (outclk_sel == RCC_OUTCLK_SOURCE_10KIRC)
      outclk = 10000 / RCC->OUTCLKDIV;
    tmpclk = outclk;
  } else if (outclk_sel == RCC_CLOCKFREQ_SYSTICKCLK) {
    if (RCC->SYSTICKCLKDIV == 0)
      return 0;
    tmpclk = hclk / (RCC->SYSTICKCLKDIV);
  } else {
    /*can't get clock*/
    while (1)
      ;
  }

  return tmpclk;
}

void RCC_InitPRNG(void)
{
	RCC_PDRUNConfig(RCC_PDCFG_10KIRC, RCC_PDCFG_POWER_ON);
	while(RCC_GetIRC10KState()==RESET);
	RCC_PDRUNConfig(RCC_PDCFG_48MIRC, RCC_PDCFG_POWER_ON);
	while(RCC_GetIRC48MState()==RESET);
	
	RCC->PRNG_CTRL |= 1;
	while((RCC->PRNG_CTRL&0x80000000)==0);
}
/**
 * @brief  Set sleep mode.
 * @param  mode: specifies sleep mode value
 *   This parameter can be one of the following values:
 *     @arg SM_SLEEP: sleep mode
 *     @arg SM_DEEPSLEEP: deep sleep mode
 * @retval none.
 */

extern uint32_t unused_gpio;
void goSleep(SleepMode mode, LdoMode ldo_mode)
{
  if(ldo_mode == LM_NORMAL){
    RCC->PCON       = 0x900;
  }else{
	  RCC->PCON 	  = 0x902;
  }

  if(mode == SM_DEEPSLEEP){
	  //PDSLEEPCFG:RCC_PDCFG_EFLASH=1
		GPIO_ConfigPull(unused_gpio, GPIO_PULL_DOWN);
		RCC->PRESETCTRL0_CLR=1;
		RCC->PRESETCTRL0_SET=1;
	  RCC->PDSLEEPCFG |= RCC_PDCFG_EFLASH;
  }
	
  if (mode == SM_SLEEP) {
    uint32_t scr = *((uint32_t *)0xE000ED10);
    scr &= 0xFFFFFFFB;
    *((uint32_t *)0xE000ED10) = scr;
  } else {
    uint32_t scr = *((uint32_t *)0xE000ED10);
    scr |= 0x00000004;
    *((uint32_t *)0xE000ED10) = scr;
  }

  __WFI();
}

/******************* (C) COPYRIGHT 2010 ALPHASCALE *****END OF FILE****/
