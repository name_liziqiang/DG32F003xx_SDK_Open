#include "key.h"

void Key_Init(void)
{
	GPIO_SetPinMux(GPIO0_1, IO_GPIO);
	GPIO_SetPinDir(GPIO0_1, GPIO_Mode_IN);
	GPIO_ConfigPull(GPIO0_1,GPIO_PULL_DOWN);
	
	GPIO_SetPinMux(GPIO1_3, IO_GPIO);
	GPIO_SetPinDir(GPIO1_3, GPIO_Mode_IN);
	GPIO_ConfigPull(GPIO1_3,GPIO_PULL_DOWN);
}
