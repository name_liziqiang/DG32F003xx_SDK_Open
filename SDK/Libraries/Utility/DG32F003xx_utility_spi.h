/**
  ******************************************************************************
  * @file    m1130_eval_SPI.h
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    27-November-2013
  * @brief   This file contains all the functions prototypes for the 
  *              m1130_eval_SPI.c driver.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  ******************************************************************************  
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DG32F003xx_EVAL_SPI_H
#define __DG32F003xx_EVAL_SPI_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx_gpio.h"

/** @addtogroup Utilities
  * @{
  */ 

/** @addtogroup A4800_EVAL
  * @{
  */
  
/** @addtogroup A4800_EVAL_SPI
  * @{
  */  


  
//ESMT EN25Q80
#define SPI_FLASH_CMD_WRITE_STATUS                  0x01
#define SPI_FLASH_CMD_WRITE                         0x02
#define SPI_FLASH_CMD_READ                          0x03
#define SPI_FLASH_CMD_WRITE_DISABLE                 0x04
#define SPI_FLASH_CMD_WRITE_ENABLE                  0x06
#define SPI_FLASH_CMD_READ_STATUS1                  0x05
#define SPI_FLASH_CMD_READ_STATUS2                  0x35

#define SPI_FLASH_CMD_ERASE_4K                      0x20
#define SPI_FLASH_CMD_ERASE_32K                     0x52
#define SPI_FLASH_CMD_ERASE_64K                     0xD8
#define SPI_FLASH_CMD_ERASE_ALL                     0x60 // 0xC7

#define SPI_FLASH_CMD_READ_ID                       0x9F
#define SPI_FLASH_CMD_FAST_READ                     0x0B

#define SPI_FLASH_PAGESHIFT  8 // 256 Byte


#define  __ALIGN4        __align(4)

/**
  * @}
  */ 
  



/** @defgroup A4800_EVAL_SPI_FLASH_Functions
  * @{
  */


void spi_initPins(uint32_t GPIO_CS, uint32_t GPIO_CLK, uint32_t GPIO_MOSI, uint32_t GPIO_MISO);
int spi_flash_init(SPI_TypeDef* SPIptr,
                    uint8_t rcc_clk_div,
                    uint32_t slaveMode,
                    int internal_clk_div,
                    int internal_clk_rate);
void spi_flash_read_status1(SPI_TypeDef* SPIptr, char * status);
void spi_flash_read_status2(SPI_TypeDef* SPIptr, char * status);
int spi_flash_read_id(SPI_TypeDef* SPIptr,uint8_t* buf);
int spi_flash_read(SPI_TypeDef* SPIptr, uint8_t * buf, uint32_t faddr, int count);
void spi_flash_write_enable(SPI_TypeDef* SPIptr, int enable);
void spi_flash_write_status(SPI_TypeDef* SPIptr, uint8_t* buf, int n);
void spi_flash_global_unprotect(SPI_TypeDef* SPIptr);
int spi_flash_erase_block_64k(SPI_TypeDef* SPIptr, uint32_t faddr);
int spi_flash_erase_block_4k(SPI_TypeDef* SPIptr, uint32_t faddr);
int spi_flash_write(SPI_TypeDef* SPIptr, uint8_t * buf, uint32_t faddr, int count);
int spi_flash_write_buf(SPI_TypeDef* SPIptr, uint8_t * buf, int n);
int spi_flash_read_buf(SPI_TypeDef* SPIptr, uint8_t * buf, int n);
int SpiRX(SPI_TypeDef* SPIptr,uint8_t *data,uint32_t num);
int SpiTX(SPI_TypeDef* SPIptr,uint8_t *data,uint32_t num);

void SPI_IRQInit(void);
void SPI_IRQDeInit(void);

#ifdef __cplusplus
}
#endif

#endif /* __DG32F003xx_EVAL_SPI_H */
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/

