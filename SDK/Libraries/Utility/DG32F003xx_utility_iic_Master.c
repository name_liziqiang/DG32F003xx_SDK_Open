/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx_utility_iic_Master.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_uart.h"
#include "DG32F003xx_utility_usart.h"
#include "DG32F003xx_utility_i2c.h"
#include "DG32F003xx_i2c.h"
#include "stdio.h"
#include "stdlib.h"

//#define IIC_SLAVE 
#define I2C_IRQ_ENABLE      				
#define IIC_SPEED_MODE  		 		I2C_Speedmode_FAST 
#define IIC_Use_10bits_Addr
#define IIC_SLAVER_ADDRESS  0X55

#define EEPROM_DEVADDR  				(0xa0 >> 1)
#define DEV_ADDR_SIZE           1

//LCNT min = 8  actual quantity = LCNT+1
//HCNT min = 6  actual quantity = HCNT+8
//HOLD set 0  actual quantity = 1   set other  actual quantity = HOLD + 5 mast smaller then LCNT
//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
#define IIC_LCNT    25   //I2C_Speedmode_Stand 100k  115  116   400k   25 26
#define IIC_HCNT    26
#define IIC_HOLD    0





static uint8_t senddata;

#ifdef I2C_IRQ_ENABLE
 void I2C0_IRQHandler(void)
{
	__IO unsigned int stat = 0;
	stat ++ ;
	stat = I2C_GetITFlag(I2C0);
	if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_RX_FULL)){
    //RX BUF full
    while((I2C0->STAT & (1<<3)) != 0){
      //RX_FIFO is not empty

    }
  }
	
	if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_RX_OVER)){
	   I2C_ClearRawITPendingBit(I2C0, I2C_IC_RAW_INTR_STAT_RX_OVER);
  }
	  
  
  
  if(I2C_CheckITFlag(I2C0, stat, I2C_IC_RAW_INTR_STAT_TX_EMPTY)){
    //TX BUF is not full
    while(((I2C0->STAT & (1 << 1)) != 0)){
		  I2C_SendData(I2C0, senddata & 0xff);
		  senddata++;

	 }
  }
}

#endif
/**
 * @brief Test Function  Init I2C0 Master
 * 
 */
static void IIC_Config_Master(void)
{
	I2C_InitTypeDef I2C_InitStruct;
	
	I2C_InitStruct.I2C_Mode = I2C_MASTER_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = IIC_SPEED_MODE;
	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
	I2C_InitStruct.I2C_OwnAddress = 0x73;       //casually specify 
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
	I2C_InitStruct.I2C_SCL_HCNT = IIC_HCNT;
	I2C_InitStruct.I2C_SCL_LCNT = IIC_LCNT;
	I2C_InitStruct.I2C_SDA_HOLD = IIC_HOLD;

	I2C_InitStruct.I2C_HS_MADDR = 12;
	I2C_InitStruct.I2C_RX_TL = 0;
	I2C_InitStruct.I2C_TX_TL = 0;
    
	I2C_Reset(I2C0);

	/* i2c interrupt request */
#ifdef I2C_IRQ_ENABLE
	/*Enable I2C interrupt here*/
    //Interrupt configuration
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = I2C0_IRQn;	  
	NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;  
	NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
	NVIC_Init(&NVIC_InitStruct);
#endif

	I2C_Init(I2C0, &I2C_InitStruct);
}


/**
 * @brief 
 * 
 */
void I2C_TestMasterWR(void)
{

  GPIO_SetPinMux(GPIO_Pin_2, IO_GPIO);//I2C mark signal
  GPIO_SetPinDir(GPIO_Pin_2, GPIO_Mode_OUT); 
  GPIO_ClearPin(GPIO_Pin_2);
	
  GPIO_SetPinMux(GPIO_Pin_0, IO_I2C0_SCL); //I2C_SCL
  GPIO_SetPinMux(GPIO_Pin_1, IO_I2C0_SDA); //I2C_SDA	
  IIC_Config_Master();
  I2C_WriteIRQ(EEPROM_DEVADDR, 0, DEV_ADDR_SIZE);	
  while(1){

  
  }
}


