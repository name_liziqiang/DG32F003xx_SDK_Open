#include "led.h"

void Led_Init(void)
{
	GPIO_SetPinMux(GPIO0_0, IO_GPIO);
	GPIO_SetPinDir(GPIO0_0, GPIO_Mode_OUT);
	GPIO_ClearPin(GPIO0_0);
	
	GPIO_SetPinMux(GPIO1_2, IO_GPIO);
	GPIO_SetPinDir(GPIO1_2, GPIO_Mode_OUT);
	GPIO_ClearPin(GPIO1_2);
	
	GPIO_SetPinMux(GPIO2_4, IO_GPIO);
	GPIO_SetPinDir(GPIO2_4, GPIO_Mode_OUT);
	GPIO_ClearPin(GPIO2_4);
}
