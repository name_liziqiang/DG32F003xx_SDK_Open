#ifndef  __DG32F003xx_UTILITY_FLASH_H
#define  __DG32F003xx_UTILITY_FLASH_H
#include <stdint.h>

void flah_writeData8(uint32_t addr, uint8_t* buf, uint32_t byteCount);
void flah_writeData16(uint32_t addr, uint16_t* buf, uint32_t byteCount);
void flah_writeData32(uint32_t addr, uint32_t* buf, uint32_t byteCount);

void flah_readData8(uint32_t addr, uint8_t* buf, uint32_t byteCount);
void flah_readData16(uint32_t addr, uint16_t* buf, uint32_t byteCount);
void flah_readData32(uint32_t addr, uint32_t* buf, uint32_t byteCount);
#endif 
