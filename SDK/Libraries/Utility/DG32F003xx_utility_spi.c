/**
  ******************************************************************************
  * @file    a4800_eval_SPI.c
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    27-November-2013
  * @brief   This file provides a set of functions needed to manage the SPI FLASH 
  *              mounted on A4800-EVAL evaluation board.
  *         
  *              
  *          It implements a high level communication layer for read and write 
  *          from/to this memory. The needed Alpscale hardware resources (SPI and 
  *          GPIO) are defined in m1130_eval.h file, and the initialization is 
  *          performed in SPI_Dma_Init() function declared in a4800_eval.c 
  *          file.
  *          You can easily tailor this driver to any other development board, 
  *          by just adapting the defines for hardware resources and 
  *          SPI_Dma_Init() function. 
  *               
  *
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  ******************************************************************************  
  */ 

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx_utility_spi.h"
#include "DG32F003xx_spi.h"
#include "DG32F003xx_gpio.h"
#include <stdio.h>
#include "DG32F003xx_rcc.h"   


/** @addtogroup Utilities
  * @{
  */
  
/** @addtogroup A4800_EVAL
  * @{
  */
  
/** @addtogroup A4800_EVAL_SPI
  * @brief      This file includes the SPI FLASH driver of A4800-EVAL boards.
  * @{
  */ 

/**
  * @}
  */

/** @defgroup A4800_EVAL_SPI_Private_Defines
  * @{
  */   
#define MAX_BUSY_WAIT 0x00100000
#define FLASH_FIRST_BIT_MODE        SPI_FirstBit_MSB //SPI_FirstBit_MSB:MSB first, SPI_FirstBit_LSB:LSB first
//#define TEST_SPI_MSB_TX_LSB_RX      1 //1:MSB TX,LSB RX;0:LSB TX，MSB RX.测试用单线测试,在Dual/Quad SPI模式，只支持MSB first。

#define SPI_CLK_MODE                3 //0/3: set spi clk mode 0/3
/**
  * @}
  */ 


/**
  * @brief  initialize SPI function pin.
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  SPIxpos: SPI GPIO pin init. 
  *     @arg SPI0_GP4: QSPSI0 gpio pin
  *     @arg SPI1_GP1, SPI1_GP3: SPI1 gpio pin 
  *     @arg SPI2_GP5: SPI2 gpio pin 
  * @retval  
  */
void spi_initPins(uint32_t GPIO_CS, uint32_t GPIO_CLK, uint32_t GPIO_MOSI, uint32_t GPIO_MISO)
{
    GPIO_SetPinMux(GPIO_CS, IO_SPI0_CS);
    GPIO_SetPinMux(GPIO_CLK, IO_SPI0_CLK);
    GPIO_SetPinMux(GPIO_MOSI, IO_SPI0_MOSI);
    GPIO_SetPinMux(GPIO_MISO, IO_SPI0_MISO);
}


/**
  * @brief  Initializes the SPI controller and allocate the resource.
  * @param   SPIptr: the QUAD spi controller base address.
  * @param   rcc_clk_div: config the SPIx clock divider.
  * @param   slaveMode: Specifies SPI is master mode or slave mode.
  *     @arg SPI_MASTER_MODE:SPI master mode
  *     @arg SPI_SLAVE_MODE:SPI slave mode
  * @param  internal_clk_div: the SPI clock internal clock setting coefficient.  
  * @param  internal_clk_rate: the SPI clock internal clock setting coefficient.
  *     Bit_rate= SPI_CLK/(internal_clk_div* (1+internal_clk_rate))
  * @retval 0: okay; else: fail.
  */
int spi_flash_init(SPI_TypeDef* SPIptr,
                    uint8_t rcc_clk_div,
                    uint32_t slaveMode,
                    int internal_clk_div,
                    int internal_clk_rate)
{
  SPI_InitTypeDef SPI_InitStructure;

  if(SPI_CLK_MODE == 0){
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  }else if(SPI_CLK_MODE == 1){
	  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  }else if(SPI_CLK_MODE == 2){
	  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
	  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  }else{
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  }
  if(FLASH_FIRST_BIT_MODE == SPI_FirstBit_MSB){
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  }else{
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_LSB;//只支持单线
  }
  SPI_InitStructure.SPI_ClockDiv = (internal_clk_div<<8);
  SPI_InitStructure.SPI_ClockRate = internal_clk_rate;
  SPI_InitStructure.SPI_SlaveMode = slaveMode;
  SPI_InitStructure.SPI_ModeSelect = SPI_STD;
  SPI_InitStructure.SPI_FrameLength = SPI_FRAME_LENGTH_8Bit;

  RCC->SPI0CLKDIV = rcc_clk_div;

  SPI_HwInit(SPIptr, &SPI_InitStructure);

  return 0;
}

/**
  * @brief  SPI no dma send data.
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  data: pointer to the buffer containing the data to be written. 
  * @param  num: write data length.
  * @retval 0: ok, other: fail. 
  */
int SpiTX(SPI_TypeDef *SPIptr, uint8_t *data, uint32_t num)
{
  uint32_t i;
  uint8_t *pData;
  volatile int waitnum;
  SPI_DataInitTypeDef SPI_DataInitStruct;

  pData = (uint8_t *)&SPIptr->DATA;
  SPI_DataInitStruct.SPI_DataLength = num;
  SPI_DataInitStruct.SPI_DUPLEX = SPI_HalfDuplex;
  SPI_DataInitStruct.SPI_TransferDir = SPI_Transfer_Write;
  SPI_DataConfig(SPIptr, &SPI_DataInitStruct);

  for (i = 0; i < num-1;) {
    if (SPI_GetFlagStatus(SPIptr, SPI_STATUS_XMIT_NOT_FULL)) {
      *pData = *((uint8_t *)data + i++);
    }
  }
	commonDelay(10);			//solve 8/2/0 issue
  if (SPI_GetFlagStatus(SPIptr, SPI_STATUS_XMIT_NOT_FULL)) {
	*pData = *((uint8_t *)data + i++);
  }
  
  /*wait spi busy to be cleared*/
  waitnum = MAX_BUSY_WAIT;
  while (SPI_GetFlagStatus(SPIptr, SPI_STATUS_SPI_BUSY) && waitnum) waitnum--;
  if (waitnum == 0) {
    printf("spi busy timeout\r\n");
    return -1;
  }
  
  return 0;
}

/**
  * @brief  SPI no dma receive data
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  data: pointer to the buffer containing the data to be read. 
  * @param  num: read data length. 
  * @retval 0: ok, other: fail. 
  */
int SpiRX(SPI_TypeDef* SPIptr, uint8_t *data, uint32_t num)
{
  uint32_t i;
  int waitnum;
  SPI_DataInitTypeDef SPI_DataInitStruct;
  
  SPI_DataInitStruct.SPI_DataLength = num;
  SPI_DataInitStruct.SPI_DUPLEX = SPI_HalfDuplex;
  SPI_DataInitStruct.SPI_TransferDir = SPI_Transfer_Read;
  SPI_DataConfig(SPIptr, &SPI_DataInitStruct);

  for (i = 0; i < num;) {
    if (SPI_GetFlagStatus(SPIptr, SPI_STATUS_RECV_NOT_EMPTY)) {
      *((uint8_t *)data + i) = (uint8_t)SPIptr->DATA;
      i++;
    }
  }

  /*wait spi busy to be cleared*/
  waitnum = MAX_BUSY_WAIT;
  while (SPI_GetFlagStatus(SPIptr, SPI_STATUS_SPI_BUSY) && waitnum) waitnum--;
  if (waitnum == 0) {
    printf("spi busy timeout\r\n");
    return -1;
  }

  return 0;
}

/**
  * @brief  start data write.  
  * @param  SPIptr: the QUAD spi controller base address.
  * @param  buf : the source address. 
  * @param  n : the bytes number.
  *  
  * @retval 0,okay; else, error.
  */
int spi_flash_write_buf(SPI_TypeDef* SPIptr, uint8_t * buf, int n)
{
  return SpiTX(SPIptr, buf, (uint32_t)n);
}


/**
  * @brief  start data read.  
  * @param SPIptr: the QUAD spi controller base address.
  * @param  buf : the target store address. 
  * @param  n : the bytes number.
  *  
  * @retval 0,okay; else, error.
  */

int spi_flash_read_buf(SPI_TypeDef* SPIptr, uint8_t * buf, int n)
{
  return SpiRX(SPIptr, buf, (uint32_t)n);
}


__ALIGN4 static uint8_t SPI_cmd[4];
static int spi_flash_send_cmd(SPI_TypeDef *SPIptr, uint8_t cmd)
{
  SPI_cmd[0] = cmd;
  return spi_flash_write_buf(SPIptr, SPI_cmd, 1);
}

__ALIGN4 static uint8_t SPI_addr[4];
static int spi_flash_send_addr(SPI_TypeDef* SPIptr,uint32_t faddr)
{
  int ret;
    SPI_addr[0] = faddr >> 16;
    SPI_addr[1] = ((faddr >> 8) & 0xff);
    SPI_addr[2] = (faddr & 0xff);
    ret = spi_flash_write_buf(SPIptr, SPI_addr, 3);
  return ret;
}


__ALIGN4 static uint8_t spi_flash_status[4];
/**
  * @brief  Read SPI flash Status Register-1 
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  status: get the SPI flash status1 value. 
  * @retval None 
  */
void spi_flash_read_status1(SPI_TypeDef* SPIptr, char * status)
{
  SPI_CS_Low(SPIptr);

#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_READ_STATUS1);
  spi_flash_read_buf(SPIptr, spi_flash_status, 1);
#else
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_READ_STATUS1);
  spi_flash_read_buf(SPIptr, spi_flash_status, 1);
#endif
  *status = spi_flash_status[0];
  SPI_CS_High(SPIptr);

  return;
}

/**
  * @brief  Read SPI flash Status Register-2 
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  status: get the SPI flash status2 value. 
  * @retval None 
  */
void spi_flash_read_status2(SPI_TypeDef* SPIptr, char * status)
{
  SPI_CS_Low(SPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_READ_STATUS2);
  spi_flash_read_buf(SPIptr, spi_flash_status, 1);
#else
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_READ_STATUS2);
  spi_flash_read_buf(SPIptr, spi_flash_status, 1);
#endif
  *status = spi_flash_status[0];
  SPI_CS_High(SPIptr);

  return;
}

/**
  * @brief   Read SPI flash JEDEC ID 
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  buf: read buffer to get the JEDEC ID. 
  * @retval 0: ok, others: fail
  */
int spi_flash_read_id(SPI_TypeDef* SPIptr,uint8_t* buf)
{
  int ret;

  SPI_CS_Low(SPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_READ_ID);  

  if (TEST_SPI_MSB_TX_LSB_RX == 1) //tx msb, rx lsb
    SPI_FirstBitSet(SPIptr, SPI_FirstBit_LSB);
  else
    SPI_FirstBitSet(SPIptr, SPI_FirstBit_MSB);

  ret = spi_flash_read_buf(SPIptr, buf,4);
#else
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_READ_ID);  
  ret = spi_flash_read_buf(SPIptr, buf,4);
#endif
  SPI_CS_High(SPIptr);

  return ret;
}

//DMA limit 256 Bytes
int spi_flash_read_page(SPI_TypeDef *SPIptr, uint8_t *buf, uint32_t faddr, int count)
{
  SPI_CS_Low(SPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_READ);
  spi_flash_send_addr(SPIptr, faddr);

  if (TEST_SPI_MSB_TX_LSB_RX == 1) //tx msb, rx lsb
    SPI_FirstBitSet(SPIptr, SPI_FirstBit_LSB);
  else
    SPI_FirstBitSet(SPIptr, SPI_FirstBit_MSB);

  spi_flash_read_buf(SPIptr, buf, count);
#else
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_READ);
  spi_flash_send_addr(SPIptr, faddr);
  spi_flash_read_buf(SPIptr, buf, count);
#endif
  SPI_CS_High(SPIptr);
  return 0;
}

/**
  * @brief  Read Data instruction read data. 
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  buf: read buffer to  contain read data.
  * @param  faddr: SPI flash internal address. 
  * @param  count: read data length. 
  * @retval 0: ok,other: fail  
  */
int spi_flash_read(SPI_TypeDef *SPIptr, uint8_t *buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  remains = (1 << SPI_FLASH_PAGESHIFT) - (faddr & ((1 << SPI_FLASH_PAGESHIFT) - 1));
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> SPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  spi_flash_read_page(SPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << SPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << SPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    spi_flash_read_page(SPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}


/**
  * @brief  SPI flash write Enable/Disable
  * @param  SPIptr: the QUAD spi controller base address.   
  * @param  enable: set 0/1 to disable/enable SPI flash write. 
  * @retval None 
  */
void spi_flash_write_enable(SPI_TypeDef* SPIptr, int enable)
{
  int cmd = enable ? SPI_FLASH_CMD_WRITE_ENABLE : SPI_FLASH_CMD_WRITE_DISABLE;

  SPI_CS_Low(SPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, cmd);
#else
  spi_flash_send_cmd(SPIptr, cmd);
#endif
  SPI_CS_High(SPIptr);

  return;
}

/**
  * @brief  Write SPI flash status 
  * @param  SPIptr: the QUAD spi controller base address.  
  * @param  buf: write buffer.
  * @param  n: write buffer length. 
  * @retval None 
  */
void spi_flash_write_status(SPI_TypeDef* SPIptr, uint8_t* buf, int n)
{
  volatile uint8_t status = 1;

  //spi_flash_write_enable will use SPI_cmd, so buf cannot be SPI_cmd
  spi_flash_write_enable(SPIptr, 1);

  SPI_CS_Low(SPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_WRITE_STATUS);
  spi_flash_write_buf(SPIptr, buf, n);
#else
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_WRITE_STATUS);
  spi_flash_write_buf(SPIptr, buf, n);
#endif
  SPI_CS_High(SPIptr);

  status = 1;
  while (status & 1) //is SPI flash busy
  {
    spi_flash_read_status1(SPIptr, (char *)&status);
    commonDelay(1000);
  }
}


/**
  * @brief  Erase SPI flash block protect
  * @param  SPIptr: the QUAD spi controller base address.   
  * @note   status&0xC3 is fully compatible:
  *     (1)ISSI    BP3~BP0
  *     (2)GD    BP4~BP0    All can be written when BP2~BP0 is 0
  *     (3)ESMT    BP3~BP0
  *     (4)WB    TB,BP2~BP0  All can be written when BP2~BP0 is 0
  *      (5)MXIC    BP3~BP0
  * @retval None 
  */
void spi_flash_global_unprotect(SPI_TypeDef* SPIptr)
{
  volatile uint8_t status1 = 1;
  volatile uint8_t status2 = 1;
  spi_flash_read_status1(SPIptr, (char *)&status1);
  spi_flash_read_status2(SPIptr, (char *)&status2);
  spi_flash_status[0] = status1 & 0xC3;
  spi_flash_status[1] = status2;
  spi_flash_write_status(SPIptr, spi_flash_status, 2);
  return;
}

/**
  * @brief  64k block erase
  * @param  SPIptr: the QUAD spi controller base address.   
  * @param  faddr: SPI flash internal address. 
  * @retval 0: ok,other: fail  
  */
int spi_flash_erase_block_64k(SPI_TypeDef* SPIptr, uint32_t faddr)
{
  volatile uint8_t status = 1;

  spi_flash_write_enable(SPIptr, 1);
  SPI_CS_Low(SPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_ERASE_64K);
  spi_flash_send_addr(SPIptr, faddr);
#else
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_ERASE_64K);
  spi_flash_send_addr(SPIptr, faddr);
#endif
  SPI_CS_High(SPIptr);

  while (status & 1)
  {
    spi_flash_read_status1(SPIptr, (char *)&status);
    commonDelay(1000);
  }

  return 0;
}

int spi_flash_erase_block_4k(SPI_TypeDef *SPIptr, uint32_t faddr)
{
  volatile uint8_t status = 1;

  spi_flash_write_enable(SPIptr, 1);
  SPI_CS_Low(SPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_ERASE_4K);
  spi_flash_send_addr(SPIptr, faddr);
#else
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_ERASE_4K);
  spi_flash_send_addr(SPIptr, faddr);
#endif
  SPI_CS_High(SPIptr);

  while (status & 1)
  {
    spi_flash_read_status1(SPIptr, (char *)&status);
    commonDelay(1000);
  }

  return 0;
}

/**
  * @brief  Write pazesize data to the SPI flash
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  buf: pointer to the buffer containing the data to be written 
  *         to the SPI flash. 
  * @param  faddr: SPI flash internel address. 
  * @param  count: SPI flash pagesize. 
  * @retval 0: ok, other: fail 
  */
int spi_flash_write_page(SPI_TypeDef* SPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  volatile uint8_t status = 1;

  spi_flash_write_enable(SPIptr, 1);
  SPI_CS_Low(SPIptr);
#ifdef TEST_SPI_MSB_TX_LSB_RX
  SPI_FirstBitSet(SPIptr, FLASH_FIRST_BIT_MODE);
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_WRITE);
  spi_flash_send_addr(SPIptr, faddr);

  if (TEST_SPI_MSB_TX_LSB_RX == 1) //tx msb, rx lsb
    SPI_FirstBitSet(SPIptr, SPI_FirstBit_MSB);
  else
    SPI_FirstBitSet(SPIptr, SPI_FirstBit_LSB);

  spi_flash_write_buf(SPIptr, buf, count);
#else
  spi_flash_send_cmd(SPIptr, SPI_FLASH_CMD_WRITE);
  spi_flash_send_addr(SPIptr, faddr);
  spi_flash_write_buf(SPIptr, buf, count);
#endif
  SPI_CS_High(SPIptr);

  while (status & 1)
  {
    spi_flash_read_status1(SPIptr, (char *)&status);
    commonDelay(1000);
  }

  return count;
}

/**
  * @brief  Write data to the SPI flash
  * @param  SPIptr: the QUAD spi controller base address. 
  * @param  buf: pointer to the buffer containing the data to be written 
  *         to the SPI flash. 
  * @param  faddr: SPI flash internel address. 
  * @param  count: write data length. 
  * @retval 0: ok, other: fail 
  */
int spi_flash_write(SPI_TypeDef* SPIptr, uint8_t * buf, uint32_t faddr, int count)
{
  int i;
  int pages;
  int remains; // first page remains
  int w_bytes;

  spi_flash_global_unprotect(SPIptr);
  remains = (1 << SPI_FLASH_PAGESHIFT) - (faddr & ((1 << SPI_FLASH_PAGESHIFT) - 1)); //faddr最后一个page剩余字节
  if (count <= remains)
  {
    remains = count;
  }

  // count left
  count -= remains;
  // total pages going to write
  pages = ((count - 1) >> SPI_FLASH_PAGESHIFT) + 1;
  // send first page remains
  spi_flash_write_page(SPIptr, buf, faddr, remains);

  faddr += remains;
  buf += remains;
  for (i = 0; i < pages; i++)
  {
    if (count > (1 << SPI_FLASH_PAGESHIFT))
    {
      w_bytes = (1 << SPI_FLASH_PAGESHIFT);
    }
    else
    {
      w_bytes = count;
    }
    // send remain pages
    spi_flash_write_page(SPIptr, buf, faddr, w_bytes);

    faddr += w_bytes;
    buf += w_bytes;
    count -= w_bytes;
  }
  return 0;
}

void SPI_IRQInit(void)
{
  NVIC_InitTypeDef NVIC_InitStruct;

  NVIC_InitStruct.NVIC_IRQChannel         = SPI_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelCmd      = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
  NVIC_Init(&NVIC_InitStruct);
}

void SPI_IRQDeInit(void)
{
  NVIC_InitTypeDef NVIC_InitStruct;

  NVIC_InitStruct.NVIC_IRQChannel         = SPI_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelCmd      = DISABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
  NVIC_Init(&NVIC_InitStruct);
}

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/

