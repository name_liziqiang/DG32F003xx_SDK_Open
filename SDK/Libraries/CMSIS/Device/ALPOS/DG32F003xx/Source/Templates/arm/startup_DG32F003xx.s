;******************** (C) COPYRIGHT 2012 ALPSCALE ********************
;* File Name          : startup_DG32F003xx.s
;* Author             : MCD Application Team
;* Version            : V1.0.1
;* Date               : 20-April-2012
;* Description        : DG32F003xx Devices vector table for MDK-ARM toolchain.
;*                      This module performs:
;*                      - Set the initial SP
;*                      - Set the initial PC == Reset_Handler
;*                      - Set the vector table entries with the exceptions ISR address
;*                      - Branches to __main in the C library (which eventually
;*                        calls main()).
;*                      After Reset the CortexM0 processor is in Thread mode,
;*                      priority is Privileged, and the Stack is set to Main.
;* <<< Use Configuration Wizard in Context Menu >>>   
;*******************************************************************************
;  @attention
; 
;  Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
;  You may not use this file except in compliance with the License.
;  You may obtain a copy of the License at:
; 
;         http://www.st.com/software_license_agreement_liberty_v2
; 
;  Unless required by applicable law or agreed to in writing, software 
;  distributed under the License is distributed on an "AS IS" BASIS, 
;  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;  See the License for the specific language governing permissions and
;  limitations under the License.
; 
;*******************************************************************************
;
; Amount of memory (in bytes) allocated for Stack
; Tailor this value to your application needs
; <h> Stack Configuration
;   <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Stack_Size      EQU     0x00000180

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp


; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>

Heap_Size       EQU     0x00000000

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit

                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset
                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size

__Vectors       DCD     __initial_sp                   ; Top of Stack
                DCD     Reset_Handler                  ; Reset Handler
                DCD     NMI_Handler                    ; NMI Handler
                DCD     HardFault_Handler              ; Hard Fault Handler
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     SVC_Handler                    ; SVCall Handler
                DCD     0                              ; Reserved
                DCD     0                              ; Reserved
                DCD     PendSV_Handler                 ; PendSV Handler
                DCD     SysTick_Handler                ; SysTick Handler

                ; External Interrupts
                DCD     0                              ; 0
                DCD     GPIO_IRQHandler               ; 1
                DCD     I2C0_IRQHandler               ; 2
                DCD     UART0_IRQHandler               ; 3
                DCD     SPI_IRQHandler                ; 4
                DCD     TIM1_IRQHandler                ; 5
                DCD     TIM4_IRQHandler               ; 6
                DCD     TIM15_IRQHandler                ; 7
                DCD     BOD_IRQHandler                 ; 8
                DCD     ADC0_IRQHandler         ; 9
                DCD     UART1_IRQHandler         ; 10

__Vectors_End

__Vectors_Size  EQU  __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY

; Reset handler routine
Reset_Handler    PROC
                 EXPORT  Reset_Handler                 [WEAK]
        IMPORT  __main
        IMPORT  SystemInit
                 LDR     R0, =SystemInit
                 BLX     R0
                 LDR     R0, =__main
                 BX      R0
                 ENDP

; Dummy Exception Handlers (infinite loops which can be modified)

NMI_Handler     PROC
                EXPORT  NMI_Handler                    [WEAK]
                B       .
                ENDP
HardFault_Handler\
                PROC
                EXPORT  HardFault_Handler              [WEAK]
                B       .
                ENDP
SVC_Handler     PROC
                EXPORT  SVC_Handler                    [WEAK]
                B       .
                ENDP
PendSV_Handler  PROC
                EXPORT  PendSV_Handler                 [WEAK]
                B       .
                ENDP
SysTick_Handler PROC
                EXPORT  SysTick_Handler                [WEAK]
                B       .
                ENDP

Default_Handler PROC
                EXPORT  GPIO_IRQHandler               [WEAK]
                EXPORT  I2C0_IRQHandler               [WEAK]
                EXPORT  UART0_IRQHandler               [WEAK]
                EXPORT  SPI_IRQHandler               [WEAK]
                EXPORT  TIM1_IRQHandler               [WEAK]
                EXPORT  TIM4_IRQHandler                [WEAK]
                EXPORT  TIM15_IRQHandler                [WEAK]
                EXPORT  BOD_IRQHandler                [WEAK]
                EXPORT  ADC0_IRQHandler                 [WEAK]
                EXPORT  UART1_IRQHandler             [WEAK]

GPIO_IRQHandler
I2C0_IRQHandler
UART0_IRQHandler
SPI_IRQHandler
TIM1_IRQHandler
TIM4_IRQHandler
TIM15_IRQHandler
BOD_IRQHandler
ADC0_IRQHandler
UART1_IRQHandler

                B       .

                ENDP

                ALIGN

;*******************************************************************************
; User Stack and Heap initialization
;*******************************************************************************
                 IF      :DEF:__MICROLIB
                
                 EXPORT  __initial_sp
                 EXPORT  __heap_base
                 EXPORT  __heap_limit
                
                 ELSE
                
                 IMPORT  __use_two_region_memory
                 EXPORT  __user_initial_stackheap
                 
__user_initial_stackheap

                 LDR     R0, =  Heap_Mem
                 LDR     R1, =(Stack_Mem + Stack_Size)
                 LDR     R2, = (Heap_Mem +  Heap_Size)
                 LDR     R3, = Stack_Mem
                 BX      LR

                 ALIGN

                 ENDIF

                 END

;************************ (C) COPYRIGHT ALPSCALE *****END OF FILE*****
