/**
  ******************************************************************************
  * @file    DG32F003xx_adc.c
  * @author  Alpscale Application Team
  * @version V1.0.1
  * @date    20-April-2014
  * @brief   This file provides firmware functions to manage the following 
  *          functionalities of the Analog to Digital Convertor (ADC) peripheral:
  *           + Initialization and Configuration
  *           + Power saving
  *           + Analog Watchdog configuration
  *           + Temperature Sensor, Vrefint (Internal Reference Voltage) and 
  *             Vbat (Voltage battery) management 
  *           + ADC Channels Configuration
  *           + Interrupts and flags management
  *
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 Alpscale</center></h2>
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_adc.h"
#include "DG32F003xx_rcc.h"
#include "DG32F003xx_gpio.h"


/**
  * @brief  Power_off the ADC peripheral
  * @param  ADCx:pointer to an ADC_InitTypeDef structure that contains 
  * @retval None 
  */
void ADC_DeInit(ADC_TypeDef *ADCx)
{
  /* ADC peripheral registers to their default reset values*/	
  RCC_SetPRESETCTRL(1 << PRESET_BIT_ADC0, DISABLE);
  RCC_SetPRESETCTRL(1 << PRESET_BIT_ADC0, ENABLE);
  /* ADC Periph clock disable */
  RCC_SetAHBCLK(1 << AHBCLK_BIT_ADC0,	DISABLE);
  /* power-offer ADC */
  RCC_PDRUNConfig(RCC_PDCFG_ADC0, RCC_PDCFG_POWER_DOWN);
}
/**
 * @brief  Power-on the ADC peripheral,initializes the ADC clock and reset
 *		   the ADC registers to their default value. 
 * @param  ADCx: pointer to an ADC_InitTypeDef structure that contains  
 * @retval None
 */
void ADC_RCC_Init(ADC_TypeDef *ADCx)
{
  /* power-on ADC */
  RCC_PDRUNConfig(RCC_PDCFG_ADC0, RCC_PDCFG_POWER_ON);
  /* ADC Periph clock enable */
  RCC_ResetAHBCLK(1 << AHBCLK_BIT_ADC0);
}
/**
  * @brief  Initializes the ADC peripheral according to the specified parameters
  *         in the ADC_InitStruct.
  * @note   This function is used to configure the global features of the ADC ( 
  *         Resolution, Data Alignment, continuous mode activation, External 
  *         trigger source and edge, Sequence Scan Direction).   
 * @param   ADCx: pointer to an ADC_InitTypeDef structure that contains  
  * @param  ADC_InitStruct: pointer to an ADC_InitTypeDef structure that contains 
  *         the configuration information for the specified ADC peripheral.
  * @retval None
  */
void ADC_Init(ADC_TypeDef *ADCx, ADC_InitTypeDef *ADC_InitStruct)
{
  /* Check the parameters */
  assert_param(IS_ADC_CHANNEL(ADC_InitStruct->ADC_Channel));
  assert_param(IS_ADC_ConversionMode(ADC_InitStruct->ADC_ConvMode));
  assert_param(IS_ADC_REFSOURCE(ADC_InitStruct->ADC_ReferenceSrcSel));
  assert_param(IS_ADC_OffsetErrorComp(ADC_InitStruct->ADC_OffsetErrorComp));
  assert_param(IS_ADC_TRIGSRC(ADC_InitStruct->ADC_TrigConvSrcSel));
  assert_param(IS_ADC_TRIG_EDGE(ADC_InitStruct->ADC_TrigConvEdge));
  assert_param(IS_ADC_FIRST_DISSAMPLENUM(ADC_InitStruct->ADC_First_DisSampleNum));
  assert_param(IS_ADC_EXCH_DISSAMPLENUM(ADC_InitStruct->ADC_Exch_DisSampleNum));
  assert_param(IS_ADC_CLOCK_DIV(ADC_InitStruct->ADC_ClockDiv));

  ADC_RCC_Init(ADCx);


  /* config the ADC sample frequence */
  ADCx->CTRL2 &= ~(ADC_REF_MASK | ADC_CLKDIV_MASK);
  ADCx->CTRL2 |= (ADC_InitStruct->ADC_ReferenceSrcSel | ADC_InitStruct->ADC_ClockDiv);
  if(ADC_InitStruct->ADC_OffsetErrorComp == ADC_OffsetErrorComp_DISABLE){
    ADCx->CTRL3 &= ~(ADC_OffsetErrorComp_ENABLE);
  }
  else{
    ADCx->CTRL3 |= (ADC_OffsetErrorComp_ENABLE);
  }
  ADCx->CTRL4 = (ADC_InitStruct->ADC_Count_SCycle) & 0x00ffffff;

  ADCx->CTRL5 &= ((uint32_t)3 << 30);
  ADCx->CTRL5 |= ADC_InitStruct->ADC_First_DisSampleNum | ADC_InitStruct->ADC_Exch_DisSampleNum | \
                 ADC_InitStruct->ADC_ConvMode | ADC_InitStruct->ADC_TrigConvEdge | \
                 ADC_InitStruct->ADC_TrigConvSrcSel;

  /* set conversion channel ADCx->CTRL0*/
  ADC_SetConvChannel(ADCx, ADC_InitStruct->ADC_Channel, ENABLE);

  /* set anoalog pin pinmux*/
	ADC_AnaEn(ADC_InitStruct->ADC_Channel);
}
/**
  * @brief  Start ADC Trigger Conversion.
  * @param  ADCx: pointer to an ADC_InitTypeDef structure that contains  
  * @param  NewState: new state of ADC trigger conversion. 
  *   This parameter can be: ENABLE or DISABLE. 
  * @retval None. 
  */
void ADC_Run(ADC_TypeDef *ADCx, FunctionalState NewState)
{
	if(NewState == ENABLE){
	  /* start ADC trigger conversion */
	  ADCx->CTRL5_CLR = (uint32_t)1 << 31;
    while((ADCx->STATUS & (1 << 1)) == 0); // wait adc init end
	}else{
	  /* stop ADC trigger conversion */
	  ADCx->CTRL5_SET = (uint32_t)1 << 31;
	}
}
/**
  * @brief  select pin mux and enable analog function.
  * @param  channel: ADC channel number,channel0~channel7
  * @retval None 
  */
void ADC_AnaEn(uint32_t channel)
{
  /* Check the parameters */
  assert_param(IS_ADC_CHANNEL(channel));

  //the last channel is avdd/3, so it do not config the pinmux.
  GPIO_SetPinMux(channel &= ~(ADC_CHANNEL22), IO_ADC);
  GPIO_ConfigPull(channel &= ~(ADC_CHANNEL22), GPIO_PULL_DISABLE);
}
/**
  * @brief  Enables or disables the specified ADC interrupts.
  * @param  ADCx: pointer to an ADC_InitTypeDef structure that contains 
  * @param  NewState: new state of the specified ADC interrupts.
  *          This parameter can be: ENABLE or DISABLE. 
  * @retval None 
  */
void ADC_ITConfig(ADC_TypeDef *ADCx, FunctionalState NewState)
{
  /* Check the parameters */
  assert_param(IS_FUNCTIONAL_STATE(NewState));

  if (NewState == ENABLE)
  {
    /* Enable Interrupt */
    ADCx->CTRL1_SET = ADC_Conv_IRQ;
  }
  else
  {
    /* Disable Interrupt */
    ADCx->CTRL1_CLR = ADC_Conv_IRQ;
  }
}
/**
  * @brief  Returns the last ADC conversion result data for ADC channel.
  * @param  ADCx: pointer to an ADC_InitTypeDef structure that contains 
  * @param  channel: ADC channel number,channel0~channel7 
  * @retval None 
  */
uint32_t ADC_GetConvValue(ADC_TypeDef *ADCx, uint32_t channel)
{
  uint32_t channelDataBaseAddress = (uint32_t)(&ADCx->CH0);
  uint32_t i = 0;
	uint32_t value = 0;
  /* Check the parameters */
  assert_param(IS_ADC_CHANNEL(channel));

  for (i = 0; i < ADC_CHANNEL_MAX; i++){
    if(channel & (1 << i)){
			value = *((__IO uint32_t *)(channelDataBaseAddress + i * 0x10));
			*((__IO uint32_t *)(channelDataBaseAddress + i * 0x10)) &= ~0x0000ffff; //clear chx value
      return (value & 0x0000ffff);
    }
  }
	return 0;
}
/**
 * @brief 
 * 
 * @param ADCx get the adc's offset error value
 * @return uint32_t 
 */
uint32_t ADC_GetCalibrationValue(ADC_TypeDef *ADCx)
{
  return (ADCx->CTRL3 & 0x000003ff);
}
/**
  * @brief  Set the ADC conversion channels.
  * @param  ADCx: pointer to an ADC_InitTypeDef structure that contains 
  * @param  channel: ADC channel number,channel0~channel7 
  * @param  NewState: new state of the specified ADC channels.
  *          This parameter can be: ENABLE or DISABLE. 
  * @retval None 
  */
void ADC_SetConvChannel(ADC_TypeDef *ADCx, uint32_t channel, FunctionalState NewState)
{
  /* Check the parameters */
  assert_param(IS_ADC_CHANNEL(channel));

  if (NewState == ENABLE)
  {
    /* start channel convertion */
    ADCx->CTRL0_SET = channel;
  }else
  {
    /* stop channel0 convertion */
    ADCx->CTRL0_CLR = channel;
  }
}
/**
  * @brief  Checks whether the specified ADC channel conversion completed interrupt
  *     has occurred or not.
  * @param  ADCx: pointer to an ADC_InitTypeDef structure that contains 
  * @param  channel: ADC channel number,channel0~channel22 
  * @retval  
  */
ITStatus ADC_GetITStatus(ADC_TypeDef *ADCx, uint32_t channel)
{
  ITStatus bitstatus = RESET;
  uint32_t i = 0;
  /* Check the parameters */
  assert_param(IS_ADC_CHANNEL(channel));

  for (i = 0; i < ADC_CHANNEL_MAX; i++){
    if(channel & (1 << i)){
      return ((ITStatus)((ADCx->CTRL1 & (1 << i)) >> i));
    }
  }

  /* Return the ADC_IT status */
  return bitstatus;
}
/**
  * @brief  Clears the ADC channel conversion completed interrupt flags.
  * @param  ADCx: pointer to an ADC_InitTypeDef structure that contains 
  * @param  channel: ADC channel number,channel0~channel7 
  * @retval None 
  */
void ADC_ClearITFlag(ADC_TypeDef *ADCx, uint32_t channel)
{
  /* Check the parameters */
  assert_param(IS_ADC_CHANNEL(channel));

  /* Clear the selected ADC flags */
  ADCx->CTRL1_CLR = channel;
}
/**
  * @brief  Start ADC software trigger conversion when select @ADC_TrigEdge_Software
  * @param  ADC_InitStruct: pointer to an ADC_InitTypeDef structure that contains 
  *         the configuration information for the specified ADC peripheral. 
  * @retval None 
  */
void ADC_StartSoftwareTrigConv(ADC_TypeDef *ADCx)
{
  /* start software trigger conversion mode  */
  ADCx->CTRL5_SET = 1 << 30;
}
/**
 * @brief 
 * 
 */
void ADC_StartCalibration(ADC_TypeDef *ADCx)
{
  ADCx->CTRL2 |= ((uint32_t)1 << 31); //start calibration

  while (ADCx->CTRL2 & ((uint32_t)1 << 31)){
    //wait calibration end
  }
}

/************************ (C) COPYRIGHT Alpscale *****END OF FILE****/
