/**
  ******************************************************************************
  * @file    DG32F003xx_utility_I2C_Slaver.h
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    16-October-2013
  * @brief   This file contains all the functions prototypes for the 
  *              DG32F003xx_utility_I2C_Slaver.c driver.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  ******************************************************************************  
  */ 
 #ifndef _DG32F003xx_utility_I2C_Slaver_h
 #define _DG32F003xx_utility_I2C_Slaver_h
 #include "stdint.h"
 #include "DG32F003xx.h"
 #include "DG32F003xx_i2c.h"
 #include "DG32F003xx_utility_i2c.h"

 
#ifdef __cplusplus
 extern "C" {
#endif


void I2C_TestSlave(void);

#ifdef __cplusplus
}
#endif

#endif

