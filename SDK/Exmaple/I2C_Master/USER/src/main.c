/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_gpio.h"
#include "DG32F003xx_rcc.h"
#include "misc.h"
#include "delay.h"
#include "DG32F003xx_i2c.h"
#include "DG32F003xx_utility_i2c.h"

/* Private define ------------------------------------------------------------*/
#define EEPROM_DEVADDR  		(0xa0 >> 1)
#define DEV_ADDR_SIZE           2

//LCNT min = 8  actual quantity = LCNT+1
//HCNT min = 6  actual quantity = HCNT+8
//HOLD set 0  actual quantity = 1   set other  actual quantity = HOLD + 5 mast smaller then LCNT
//fscl = systemAHB / ((IIC_LCNT+1)  +  (HCNT + 8))
#define IIC_LCNT    115   //I2C_Speedmode_Stand 100k  115  116   400k   25 26
#define IIC_HCNT    116
#define IIC_HOLD    0
#define BYTENUM  16

/* Private variables ---------------------------------------------------------*/
static __align(32) uint8_t wbuf[BYTENUM];
static __align(32) uint8_t rbuf[BYTENUM];
/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void IIC_Config_Master(void);

/**
  * @brief  应用程序入口函数.
  * @param  无
  * @retval int
  */
int main()
{ 
	
	delay_init(24);	
    IIC_Config_Master();    
	
	for (int i = 0; i < BYTENUM; i++)wbuf[i] = (char)( (i + 1) & 0xff);
    for (int i = 0; i < BYTENUM; i++)rbuf[i] = 0;

    I2C_Write(EEPROM_DEVADDR, 0, DEV_ADDR_SIZE, wbuf, BYTENUM);
    delay_ms(1000);
    I2C_Read(EEPROM_DEVADDR, 0, DEV_ADDR_SIZE, rbuf, BYTENUM);
	while(1){

  }
}


/**
  * @brief  IIC初始化
  * @param  无
  * @retval 无
  */
static void IIC_Config_Master(void)
{
	I2C_InitTypeDef I2C_InitStruct;
	
	//配置复用IO
	GPIO_SetPinMux(GPIO0_0, IO_I2C0_SCL); //I2C_SCL
    GPIO_SetPinMux(GPIO0_1, IO_I2C0_SDA); //I2C_SDA	
    //配置IIC
	I2C_InitStruct.I2C_Mode = I2C_MASTER_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = I2C_Speedmode_Stand;
	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
	I2C_InitStruct.I2C_OwnAddress = 0x73;       //casually specify 
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
	I2C_InitStruct.I2C_SCL_HCNT = IIC_HCNT;
	I2C_InitStruct.I2C_SCL_LCNT = IIC_LCNT;
	I2C_InitStruct.I2C_SDA_HOLD = IIC_HOLD;

	I2C_InitStruct.I2C_HS_MADDR = 12;
	I2C_InitStruct.I2C_RX_TL = 0;
	I2C_InitStruct.I2C_TX_TL = 0;
    
	I2C_Reset(I2C0);

	I2C_Init(I2C0, &I2C_InitStruct);
}


