/**
  ******************************************************************************
  * @file    main.c
  * @author  MCU Application Team
  * @brief   Main program body
  ******************************************************************************
  * Copyright (c) 2022 Alpscale.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "DG32F003xx.h"
#include "DG32F003xx_utility_spi.h"
#include "DG32F003xx_spi.h"
#include "usart.h"	  
#include "delay.h"

/* Private define ------------------------------------------------------------*/
#define SCS        GPIO1_0
#define SCLK       GPIO1_1
#define MOSI       GPIO1_2
#define MISO       GPIO1_3
#define SYNC       GPIO0_4
#define TEST_BYTES 64
/* Private variables ---------------------------------------------------------*/


uint32_t buff32[TEST_BYTES];
uint16_t buff16[TEST_BYTES];
uint8_t  buff8[TEST_BYTES];


volatile int p_tx    = 0;
int          tx_size = 0;
uint8_t*     p_data;

volatile int p_rx    = 0;
int          rx_size = 0;
uint8_t*     r_data;

/* Private user code ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/




// READ DATA = BYTE
// WRITE DATA = WORD/BYTE



void SPI_IRQHandler(void)
{
  if (SPI0->CTRL1 & SPI_CTRL1_XMIT_IRQ_EN) {
    if (SPI0->CTRL1 & SPI_CTRL1_XMIT_IRQ) {
      while (SPI0->STATUS & SPI_STATUS_XMIT_NOT_FULL) {
        *(uint8_t*)&SPI0->DATA = p_data[p_tx++];
        if (p_tx == tx_size) {
          SPI0->CTRL1_CLR = SPI_CTRL1_XMIT_IRQ_EN;
          break;
        }
      }
    }
  }

  if (SPI0->CTRL1 & SPI_CTRL1_RECV_IRQ) {
    SPI0->CTRL1_CLR = SPI_CTRL1_RECV_IRQ;
    while (SPI0->STATUS & SPI_STATUS_RECV_NOT_EMPTY) {
      r_data[p_rx] = *(uint8_t*)&SPI0->DATA;
      p_rx++;
    }
  }

  if (SPI0->CTRL1 & SPI_CTRL1_RECV_TIMEOUT_IRQ) {
    SPI0->CTRL1_CLR = SPI_CTRL1_RECV_TIMEOUT_IRQ;
    while (SPI0->STATUS & SPI_STATUS_RECV_NOT_EMPTY) {
      r_data[p_rx] = *(uint8_t*)&SPI0->DATA;
      p_rx++;
    }
  }
}

void SPI_Tx_IRQ(SPI_TypeDef* SPIx, uint8_t* buff, int num)
{
  SPI_DataInitTypeDef QSPI_DataInitStruct;

  QSPI_DataInitStruct.SPI_DataLength  = num;
  QSPI_DataInitStruct.SPI_DUPLEX      = SPI_HalfDuplex;
  QSPI_DataInitStruct.SPI_TransferDir = SPI_Transfer_Write;

  p_data  = buff;
  tx_size = num;
  p_tx    = 0;

  SPI_DataConfig(SPIx, &QSPI_DataInitStruct);

  NVIC->ISER[SPI_IRQn >> 5] = 1 << (SPI_IRQn & 0x1F);
  SPIx->CTRL1_SET           = SPI_CTRL1_XMIT_IRQ_EN;

  while (p_tx < tx_size)
    ;

  while (SPI0->STATUS & SPI_STATUS_SPI_BUSY)
    ;
}

void SPI_Rx_IRQ(SPI_TypeDef* SPIx, uint8_t* buff, int num)
{
  SPI_DataInitTypeDef QSPI_DataInitStruct;

  QSPI_DataInitStruct.SPI_DataLength  = num;
  QSPI_DataInitStruct.SPI_DUPLEX      = SPI_HalfDuplex;
  QSPI_DataInitStruct.SPI_TransferDir = SPI_Transfer_Read;

  r_data  = buff;
  rx_size = num;
  p_rx    = 0;

  SPI_DataConfig(SPIx, &QSPI_DataInitStruct);

  NVIC->ISER[SPI_IRQn >> 5] = 1 << (SPI_IRQn & 0x1F);
  SPIx->CTRL1_SET           = SPI_CTRL1_RECV_IRQ_EN | SPI_CTRL1_RECV_TIMEOUT_IRQ_EN;

  while (p_rx < rx_size)
    ;
  while (SPI0->STATUS & SPI_STATUS_SPI_BUSY)
    ;
}

int main()
{
  int i;
  Debug_uart_init(115200); 
  printf("spi master 1\r\n");
  spi_flash_init(SPI0, 4, SPI_MASTER_MODE, 2, 2);
  spi_initPins(SCS, SCLK, MOSI, MISO); 

  // ���
  memset(buff8, 0xff, sizeof(buff8));
  if (GPIO_ReadPin(MISO) == 1) {
    SPI_CS_Low(SPI0);
    SpiTX(SPI0, buff8, sizeof(buff8));
    SPI_CS_High(SPI0);
  }
  if (GPIO_ReadPin(MOSI) == 1) {
    SPI_CS_Low(SPI0);
    SpiRX(SPI0, buff8, sizeof(buff8));
    SPI_CS_High(SPI0);
  }

  char cmd = '\0';
  while (1) {
    printf("spi master start\r\n");
#if 1
    cmd = 'r';
    GPIO_SetPinMux(MISO, IO_GPIO);
    GPIO_SetPinDir(MISO, GPIO_Mode_IN);
    GPIO_ConfigPull(MISO, GPIO_PULL_DOWN);
    while (GPIO_ReadPin(MISO) == 0)
      ;
    GPIO_SetPinMux(MISO, IO_SPI0_MISO);
    SPI_CS_Low(SPI0);
    SPI_Tx_IRQ(SPI0, (uint8_t*)&cmd, 1);
    SPI_CS_High(SPI0);

    memset(buff8, 0, 33);
    GPIO_SetPinMux(MOSI, IO_GPIO);
    GPIO_SetPinDir(MOSI, GPIO_Mode_IN);
    GPIO_ConfigPull(MOSI, GPIO_PULL_DOWN);
    while (GPIO_ReadPin(MOSI) == 0)
      ;
    GPIO_SetPinMux(MOSI, IO_SPI0_MOSI);

    SPI_CS_Low(SPI0);
    SPI_Rx_IRQ(SPI0, buff8, 33);
    SPI_CS_High(SPI0);

    for (i = 0; i < 33; i++) {
      if (buff8[i] != i + 1) {
        while (1)
          ;
      }
    }
#endif
#if 1
    for (i = 0; i < TEST_BYTES; i++) {
      *(buff8 + i) = i + 0x8;
    }

    cmd = 'w';
    GPIO_SetPinMux(MISO, IO_GPIO);
    GPIO_SetPinDir(MISO, GPIO_Mode_IN);
    GPIO_ConfigPull(MISO, GPIO_PULL_DOWN);
    while (GPIO_ReadPin(MISO) == 0)
      ;
    GPIO_SetPinMux(MISO, IO_SPI0_MISO);
    SPI_CS_Low(SPI0);
    SPI_Tx_IRQ(SPI0, (uint8_t*)&cmd, 1);
    SPI_CS_High(SPI0);

    GPIO_SetPinMux(MISO, IO_GPIO);
    GPIO_SetPinDir(MISO, GPIO_Mode_IN);
    GPIO_ConfigPull(MISO, GPIO_PULL_DOWN);
    while (GPIO_ReadPin(MISO) == 0)
      ;
    GPIO_SetPinMux(MISO, IO_SPI0_MISO);
    SPI_CS_Low(SPI0);
    SPI_Tx_IRQ(SPI0, buff8, 33);
    SPI_CS_High(SPI0);

#endif
#if 1
    for (i = 0; i < TEST_BYTES; i++) {
      *(buff8 + i) = i + 0x8;
    }

    cmd = 'o';
    GPIO_SetPinMux(MISO, IO_GPIO);
    GPIO_SetPinDir(MISO, GPIO_Mode_IN);
    GPIO_ConfigPull(MISO, GPIO_PULL_DOWN);
    while (GPIO_ReadPin(MISO) == 0)
      ;
    GPIO_SetPinMux(MISO, IO_SPI0_MISO);
    SPI_CS_Low(SPI0);
    SPI_Tx_IRQ(SPI0, (uint8_t*)&cmd, 1);
    SPI_CS_High(SPI0);

    GPIO_SetPinMux(MISO, IO_GPIO);
    GPIO_SetPinDir(MISO, GPIO_Mode_IN);
    GPIO_ConfigPull(MISO, GPIO_PULL_DOWN);
    while (GPIO_ReadPin(MISO) == 0)
      ;
    GPIO_SetPinMux(MISO, IO_SPI0_MISO);
    SPI_CS_Low(SPI0);
    SPI_Tx_IRQ(SPI0, buff8, 33);
    SPI_CS_High(SPI0);
#endif
  }

}
